﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PaginaMuestra/Site1.Master" AutoEventWireup="true" CodeBehind="SolicitudesPendientes1.aspx.cs" Inherits="PortalGastos.UsuarioDirector.SolicitudesPendientes1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../Style/Style.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>
        <asp:Panel ID="pnlSolicitudes" runat="server" CssClass="pnlGenerico centrado6">
            <asp:Label ID="lblTitulo" runat="server" CssClass="lblTituloge" Text="Solicitudes pendientes"></asp:Label>
            <asp:Label ID="lblTitulo2" runat="server" CssClass="lblTituloge" Text="Solicitud" Visible="false"></asp:Label>
            <br />
            <hr />
            <table style="width: 100%;">
                <tr style="height: 20px;">
                    <td style="width: 100%;"></td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblDesc" runat="server" CssClass="lblDescripcion1" Text="Solicitudes por autorizar"></asp:Label>
                    </td>
                </tr>
                <tr style="width: 100%; float:left">
                    <asp:GridView ID="Resultado" runat="server" CssClass="mGrid1 pagination-ys" PageSize="7" AllowPaging="True" OnPageIndexChanging="Resultado_PageIndexChanging" OnSelectedIndexChanged="Resultado_SelectedIndexChanged1" DataKeyNames="No. de Solicitud">
                        <Columns>
                            <asp:TemplateField HeaderText="Aceptar">
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnAceptar" runat="server" ImageUrl="~/Imagenes/vision.png" CommandName="Select"/>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </tr>
                <tr style="height: 20px;">
                    <td style="width: 25%; float:left;">
                        <asp:Button ID="btnRegresar" runat="server" Text="Regresar" Visible="false" OnClick="btnRegresar_Click" />
                    </td>
                </tr>
                <tr style="height: 20px;">
                    <td style="width: 100%;"></td>
                </tr>
                <tr style="height: 20px;">
                    <td style="width: 25%; float:left;">
                        <asp:Label ID="lblNombre" runat="server" CssClass="lblEtique" Visible="false"></asp:Label>
                    </td>
                    <td style="width: 25%; float:left;">
                        <asp:Label ID="lblSolicitud" runat="server" CssClass="lblEtique" Visible="false"></asp:Label>
                    </td>
                </tr>
                <tr style="height: 20px;">
                    <td style="width: 100%;"></td>
                </tr>
               <tr style="height: 20px;">
                   <td style="width: 25%; float:left;">
                       <asp:Label ID="lblFechaInicio" runat="server" CssClass="lblEtique" Visible="false" Text="Fecha Inicio: "></asp:Label>
                   </td>
                   <td style="width: 25%; float:left;">
                       <asp:Label ID="lblFechaInicio1" runat="server" CssClass="lblEtique1" Visible="false"></asp:Label>
                   </td>
               </tr>
                <tr style="height: 20px;">
                    <td style="width: 100%;"></td>
                </tr>
                <tr style="height: 20px;">
                    <td style="width: 25%; float:left;">
                        <asp:Label ID="lblFechaFin" runat="server" CssClass="lblEtique" Visible="false" Text="Fecha Fin:"></asp:Label>
                    </td>
                    <td style="width: 25%; float:left;">
                        <asp:Label ID="lblFechaFin1" runat="server" CssClass="lblEtique1" Visible="false"></asp:Label>
                    </td>
                </tr>
                <tr style="height: 20px;">
                    <td style="width: 100%;"></td>
                </tr>
                <tr style="height: 20px;">
                    <td style="width: 25%; float:left;">
                        <asp:Label ID="lblDestino" runat="server" CssClass="lblEtique" Visible="false" Text="Lugar Destino: "></asp:Label>
                    </td>
                    <td style="width: 25%; float:left;">
                        <asp:Label ID="lblDestino1" runat="server" CssClass="lblEtique1" Visible="false"></asp:Label>
                    </td>
                </tr>
                <tr style="height: 20px;">
                    <td style="width: 100%;"></td>
                </tr>
                <tr style="height: 20px;">
                    <td style="width: 25%; float:left;">
                        <asp:Label ID="lblPeriodo" runat="server" CssClass="lblEtique" Visible="false" Text="Periodo: "></asp:Label>
                    </td>
                    <td style="width: 25%; float:left;">
                        <asp:Label ID="lblPeriodo1" runat="server" CssClass="lblEtique1" Visible="false"></asp:Label>
                    </td>
                </tr>
                <tr style="height: 20px;">
                    <td style="width: 100%;"></td>
                </tr>
                <tr style="height: 20px;">
                    <td style="width: 25%; float:left;">
                        <asp:Label ID="lblTipoViaje" runat="server" CssClass="lblEtique" Visible="false" Text="Tipo Viaje:"></asp:Label>
                    </td>
                    <td style="width: 25%; float:left;">
                        <asp:Label ID="lblTipoViaje1" runat="server" CssClass="lblEtique1" Visible="false"></asp:Label>
                    </td>
                </tr>
                <tr style="height: 20px;">
                    <td style="width: 100%;"></td>
                </tr>
                <tr style="height: 20px;">
                    <td style="width: 25%; float:left;">
                        <asp:Label ID="lblGastoVuelo" runat="server" CssClass="lblEtique" Visible="false" Text="Gasto Vuelo:"></asp:Label>
                    </td>
                    <td style="width: 25%; float:left;">
                        <asp:Label ID="lblGastoVuelo1" runat="server" CssClass="lblEtique1" Visible="false"></asp:Label>
                    </td>
                </tr>
                <tr style="height: 20px;">
                    <td style="width: 100%;"></td>
                </tr>
                <tr style="height: 20px;">
                    <td style="width: 25%; float:left;">
                        <asp:Label ID="lblGastoHospedaje" runat="server" CssClass="lblEtique" Visible="false" Text="Gasto Hospedaje:"></asp:Label>
                    </td>
                    <td style="width: 25%; float:left;">
                        <asp:Label ID="lblGastoHospedaje1" runat="server" CssClass="lblEtique1" Visible="false"></asp:Label>
                    </td>
                </tr>
                <tr style="height: 20px;">
                    <td style="width: 100%;"></td>
                </tr>
                <tr style="height: 20px;">
                    <td style="width: 25%; float:left;">
                        <asp:Label ID="lblGastoTerrestre" runat="server" CssClass="lblEtique" Visible="false" Text="Gasto Terrestre:"></asp:Label>
                    </td>
                    <td style="width: 25%; float:left;">
                        <asp:Label ID="lblGastoTerrestre1" runat="server" CssClass="lblEtique1" Visible="false"></asp:Label>
                    </td>
                </tr>
                <tr style="height: 20px;">
                    <td style="width: 100%;"></td>
                </tr>
                <tr style="height: 20px;">
                    <td style="width: 25%; float:left;">
                        <asp:Label ID="lblGastoComida" runat="server" CssClass="lblEtique" Visible="false" Text="Gasto Comida:"></asp:Label>
                    </td>
                    <td style="width: 25%; float:left;">
                        <asp:Label ID="lblGastoComida1" runat="server" CssClass="lblEtique1" Visible="false"></asp:Label>
                    </td>
                </tr>
                <tr style="height: 20px;">
                    <td style="width: 100%;"></td>
                </tr>
                <tr style="height: 20px;">
                    <td style="width: 25%; float:left;">
                        <asp:Label ID="lblGastoRenta" runat="server" CssClass="lblEtique" Visible="false" Text="Gasto Renta:"></asp:Label>
                    </td>
                    <td style="width: 25%; float:left;">
                        <asp:Label ID="lblGastoRenta1" runat="server" CssClass="lblEtique1" Visible="false"></asp:Label>
                    </td>
                </tr>
                <tr style="height: 20px;">
                    <td style="width: 100%;"></td>
                </tr>
                <tr style="height: 20px;">
                    <td style="width: 25%; float:left;">
                        <asp:Label ID="lblGastosOtros" runat="server" CssClass="lblEtique" Visible="false" Text="Otros Gastos:"></asp:Label>
                    </td>
                    <td style="width: 25%; float:left;">
                        <asp:Label ID="lblGastosOtros1" runat="server" CssClass="lblEtique1" Visible="false"></asp:Label>
                    </td>
                </tr>
                <tr style="height: 20px;">
                    <td style="width: 100%;"></td>
                </tr>
                <tr style="height: 20px;">
                    <td style="width: 25%; float:left;">
                        <asp:Label ID="lblGastoTotal" runat="server" CssClass="lblEtique" Visible="false" Text="Total Solicitado"></asp:Label>
                    </td>
                    <td style="width: 25%; float:left;">
                        <asp:Label ID="lblGastoTotal1" runat="server" CssClass="lblEtique1" Visible="false"></asp:Label>
                    </td>
                </tr>
                <tr style="height: 20px;">
                    <td style="width: 100%;"></td>
                </tr>
                <tr style="height: 20px;">
                    <td style="width: 25%; float:left;">
                        <asp:Button ID="btnAceptar" runat="server" Visible="false" CssClass="cssBotonMediano" Text="Aceptar" OnClick="btnAceptar_Click"/>
                    </td>
                    <td style="width: 25%; float:left;">
                        <asp:Button ID="btnRechazar" runat="server" Visible="false" CssClass="cssBotonMediano" Text="Rechazar" OnClick="btnRechazar_Click"/>
                    </td>
                </tr>
            </table>
        </asp:Panel>
    </div>
</asp:Content>

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PortalGastos.UsuarioDirector
{
    public partial class UsuarioDirector : System.Web.UI.Page
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            if (Session["user"] == null)
            {
                HttpContext.Current.Response.Redirect("~/Portal/Login.aspx");
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            string nombre = Session["user"].ToString();
            lblUser.Text = "Bienvenido " + nombre;
        }

        protected void btnCrear_Click(object sender, EventArgs e)
        {
            HttpContext.Current.Response.Redirect("~/UsuarioDirector/SolicitudesPendientes1.aspx");
        }

        protected void btnBloquear_Click(object sender, EventArgs e)
        {
            HttpContext.Current.Response.Redirect("~/UsuarioDirector/GastoArea.aspx");
        }

        protected void btnCambiar_Click(object sender, EventArgs e)
        {
            HttpContext.Current.Response.Redirect("../Portal/CambioContraseña.aspx");
        }

        protected void btnCerrar_Click(object sender, EventArgs e)
        {
            FormsAuthentication.SignOut();
            Session.Clear();
            Session.Abandon();
            Response.Redirect("~/Portal/Login.aspx");
        }
    }
}
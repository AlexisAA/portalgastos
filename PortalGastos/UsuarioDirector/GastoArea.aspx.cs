﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PortalGastos.UsuarioDirector
{
    public partial class GastoArea : System.Web.UI.Page
    {
        SqlConnection conexion = new SqlConnection("Data source=(local); Initial Catalog=PortalGastos; User=sae; Password=sae ");
        protected void Page_Init(object sender, EventArgs e)
        {
            if (Session["user"] == null)
            {
                HttpContext.Current.Response.Redirect("~/Portal/Login.aspx");
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                AgregarGridMe();
            }
        }
        protected void AgregarGridMe()
        {
            string No_Sol="",Total_sol="";
            string Nom_Area = "",con1="";
            string dtAhora = DateTime.Now.ToString("yyyy-MM-dd");
            List<string> Lista = new List<string>();
            DataTable dt = new DataTable();
            dt.Columns.AddRange(new DataColumn[4] {new DataColumn("Solicitudes",typeof(string)),
                                                    new DataColumn("Importe Total",typeof(string)),
                                                    new DataColumn("Nombre Área",typeof(string)),
                                                    new DataColumn("Fecha",typeof(string))});
            conexion.Open();
            string query = "select * from v_GastoArea";
            SqlCommand select = new SqlCommand(query,conexion);
            SqlDataReader registro = select.ExecuteReader();
            while (registro.Read())
            {
                con1 = registro[0].ToString();
                Lista.Add(con1);
                for (int i = 0; i < Lista.Count; i++)
                {
                    No_Sol = Convert.ToString(registro["Numero_solicitudes"]);
                    Total_sol = Convert.ToString(registro["Total_solicitado"]);
                    Nom_Area = Convert.ToString(registro["Nombre_Area"]);
                }
                dt.Rows.Add(No_Sol+" Solicitudes","$ "+decimal.Parse(Total_sol).ToString("N"),Nom_Area,dtAhora);
                Resultado.DataSource=dt; 
                Resultado.DataBind();
            }
            conexion.Close();
        }

        protected void Resultado_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            AgregarGridMe();
            Resultado.PageIndex = e.NewPageIndex;
            Resultado.DataBind();
        }
    }
}
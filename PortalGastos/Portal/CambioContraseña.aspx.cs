﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PortalGastos.Portal
{
    public partial class CambioContraseña1 : System.Web.UI.Page
    {
        SqlConnection conexion = new SqlConnection("Data source=(local); Initial Catalog=PortalGastos; User=sae; Password=sae ");
        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["user"] == null)
            {
                HttpContext.Current.Response.Redirect("~/Portal/Login.aspx");
            }
        }

        protected void btnCambiar_Click(object sender, EventArgs e)
        {
            CambiarMe();
        }
        protected void CambiarMe()
        {
            string script = "alert('Ingresar datos correctos')";
            string script1 = "alert('Contraseña invalida')";
            string script2 = "alert('Contraseña cambiada correctamente')";
            string nombre = Session["user"].ToString();
            string conPast = Convert.ToString(txtCambio.Text);
            string conNew = Convert.ToString(txtCambio1.Text);
            string conBD = "";
            if (conPast.Length > 0 && conNew.Length > 0)
            {
                conexion.Open();
                string query = "select * from TBL_Usuarios where Usuario='" + nombre + "'";
                SqlCommand buscar = new SqlCommand(query, conexion);
                SqlDataReader registros = buscar.ExecuteReader();
                if (registros.Read())
                {
                    conBD = Convert.ToString(registros["contrasenia"]);
                }
                conexion.Close();
                if (conPast == conBD)
                {
                    conexion.Open();
                    string query1 = "update TBL_Usuarios set contrasenia = '" + conNew + "' where Usuario = '" + nombre + "'";
                    SqlCommand actualizar = new SqlCommand(query1, conexion);
                    actualizar.ExecuteNonQuery();
                    conexion.Close();
                    ScriptManager.RegisterStartupScript(this, typeof(Page), "alerta", script2, true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, typeof(Page), "alerta", script1, true);
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, typeof(Page), "alerta", script, true);
            }
        } 
    }
}
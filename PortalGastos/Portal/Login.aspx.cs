﻿using PortalGastos.Administradordesistema;
using PortalGastos.PaginaMuestra;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PortalGastos.Portal
{
    public partial class Login : System.Web.UI.Page
    {
        
        SqlConnection conexion = new SqlConnection("Data source=(local); Initial Catalog=PortalGastos; User=sae; Password=sae ");
        SqlConnection conexion1 = new SqlConnection("Data source=(local); Initial Catalog=PortalGastos; User=sae; Password=sae ");
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        protected void btnInicio_Click(object sender, EventArgs e)
        {
            string script = "alert('Usuario o contraseña incorrecto')";
            string script1 = "alert('Usuario bloqueado, favor de comunicarse con el administrador')";
            string script2 = "alert('Datos incorrectos')";
            if (txtUsuario.Text.Length > 0 && txtPassword.Text.Length > 0)
            {
                string usuario = Convert.ToString(txtUsuario.Text);
                string password = Convert.ToString(txtPassword.Text);
                int rol = 0,ID_User=0, rol1=0;
                string rs = "";
                List<string> r = new List<string>();
                Session["user"] = usuario;
                conexion.Open();
                string roles = "select ID_Usuario,Rol from TBL_Usuarios where Usuario = '"+usuario+"'";
                SqlCommand role = new SqlCommand(roles,conexion);
                SqlDataReader ro = role.ExecuteReader();
                if (ro.Read())
                {
                    rs = ro[0].ToString();
                    r.Add(rs);
                    rol = Int32.Parse(rs);
                    rol1 = Convert.ToInt32(ro["Rol"]);
                    ID_User = Convert.ToInt32(ro["ID_Usuario"]);
                }
                Session["ID_User"] = ID_User;
                Session["rol"] = rol1;
                conexion.Close();
                conexion.Open();
                string squeryAr = "select * from v_Area where Usuario='"+usuario+"'";
                string areaUs = "";
                SqlCommand SqueryAre = new SqlCommand(squeryAr,conexion);
                SqlDataReader regist = SqueryAre.ExecuteReader();
                if (regist.Read())
                {
                    areaUs = Convert.ToString(regist["Area"]);
                }
                conexion.Close();
                Session["Area"] = areaUs;
                string bloqueados = "";
                bool bloq = true;
                List<string> bo = new List<string>();
                conexion.Open();
                string bloqueado = "select bloqueado from TBL_Usuarios where Usuario='" + usuario + "'";
                SqlCommand blo = new SqlCommand(bloqueado, conexion);
                SqlDataReader re = blo.ExecuteReader();
                if (re.Read())
                {
                    bloqueados = re[0].ToString();
                    bo.Add(bloqueados);
                    bloq = Boolean.Parse(bloqueados);
                conexion.Close();
                if (bloq == false)
                {
                    try
                    {
                        conexion.Open();
                        string query = "select count(*) from TBL_Usuarios where Usuario = '" + usuario + "' and contrasenia = '" + password + "'";
                        SqlCommand conec = new SqlCommand(query, conexion);
                        bool correcto = Convert.ToInt32(conec.ExecuteScalar()) > 0;
                        if (correcto)
                        {
                            InsertLog("btnInicio", "Ingreso", "OK", "Ingreso correcto al sistema", true);
                            actualizarF(usuario);
                            Site1 usu = new Site1();
                            usu.RevisionU(usuario);
                        }
                        else
                        {
                            InsertLog("btnInicio", "Ingreso", "ERROR", "Usuario o contraseña incorrecto", true);
                            actualizarI(usuario);
                            ScriptManager.RegisterStartupScript(this, typeof(Page), "alerta", script, true);
                        }
                        conexion.Close();
                    }
                    catch (Exception ex)
                    {
                        InsertLog("btnInicio", "Ingreso", "ERROR", ex.Message, true);
                        ScriptManager.RegisterStartupScript(this, typeof(Page), "alerta", "alert('" + ex + "')", true);
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, typeof(Page), "alerta", script1, true);
                    InsertLog("btnInicio", "Ingreso", "ERROR", "Usuario bloqueado", true);
                }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, typeof(Page), "alerta", script2, true);
                    InsertLog("btnInicio", "Ingreso", "ERROR", "Datos Incorrectos", true);
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, typeof(Page), "alerta", script2, true);
                InsertLog("btnInicio", "Ingreso", "ERROR", "Datos Incorrectos", true);
            }
        }
        protected void InsertLog(string _evento, string _metodo, string _respuesta, string _descripcion,bool _esSistema)
        {
            int idUsuario = 0;
            int _esSistema_ = Convert.ToInt32(_esSistema);
            String dtAhora1 = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss");
            conexion1.Open();
            string Insert = "insert into TBL_Logs (Formulario, Evento, Metodo, Fecha, Respuesta, Descripcion, EsSistema, ID_Usuario) values ('Login','"+_evento+"','"+_metodo+"','"+dtAhora1+"','"+_respuesta+"','"+_descripcion+"',"+_esSistema_+","+idUsuario+")";
            SqlCommand insertL = new SqlCommand(Insert, conexion1);
            insertL.ExecuteNonQuery();
            conexion1.Close();
        }
        
        protected void actualizarF(string _usuario)
        {
            String dtAhora = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss");
            conexion1.Open();
            string actu = "update TBL_Usuarios set Fecha_ultimoIngreso = '"+dtAhora+"' where Usuario = '"+_usuario+"'";
            SqlCommand actua = new SqlCommand(actu,conexion1);
            actua.ExecuteNonQuery();
            conexion1.Close();
        }
        protected void actualizarI(string _usuario)
        {
            int reg = 0, co1=0;
            string registro="";
            List<string> Lista = new List<string>();
            conexion1.Open();
            string ConI = "select Intentos_ingresos from TBL_Usuarios";
            SqlCommand ConIn = new SqlCommand(ConI, conexion1);
            SqlDataReader registros = ConIn.ExecuteReader();
            while (registros.Read())
            {
                registro = registros[0].ToString();
                Lista.Add(registro);
                co1 = Int32.Parse(registro);
                reg = co1 + 1;
            }
            conexion1.Close();
            conexion1.Open();
            string actI = "update TBL_Usuarios set Intentos_ingresos="+reg+" where Usuario='"+_usuario+"'";
            SqlCommand actua = new SqlCommand(actI,conexion1);
            actua.ExecuteNonQuery();
            conexion1.Close();
        }

        protected void btnRecuperar_Click(object sender, EventArgs e)
        {
            pnlRecuperar.Visible = true;
            pnlLogin.Visible = false;
        }

        protected void btnEnviar_Click(object sender, EventArgs e)
        {
            string script = "alert('Ingresar datos correctos')";
            if (txtCorreo.Text.Length != 0 && txtUser.Text.Length != 0)
            {
                string correo = Convert.ToString(txtCorreo.Text);
                string usuario = Convert.ToString(txtUser.Text);
                string bloqueados = "", usuario1 = "", correo1 = "", contrasenia = "",empleado="";
                string script1 = "alert('Usuario bloqueaado')";
                string script2 = "alert('Usuario incorrecto')";
                string script3 = "alert('Correo incorrecto')";
                string script4 = "alert('Correo enviado correctamente')";
                bool bloq = true;
                List<string> bo = new List<string>();
                conexion.Open();
                string bloqueado = "select bloqueado from TBL_Usuarios where Usuario='" + usuario + "'";
                SqlCommand blo = new SqlCommand(bloqueado, conexion);
                SqlDataReader re = blo.ExecuteReader();
                while (re.Read())
                {
                    bloqueados = re[0].ToString();
                    bo.Add(bloqueados);
                    bloq = Boolean.Parse(bloqueados);
                }
                conexion.Close();
                if (bloq == false)
                {
                    conexion.Open();
                    string query = "select * from TBL_Usuarios where Usuario = '" + usuario + "'";
                    SqlCommand selectUs = new SqlCommand(query, conexion);
                    SqlDataReader registro = selectUs.ExecuteReader();
                    if (registro.Read())
                    {
                        usuario1 = Convert.ToString(registro["ID_Usuario"]);
                        contrasenia = Convert.ToString(registro["contrasenia"]);
                        empleado = Convert.ToString(registro["Empleado"]);
                    }
                    conexion.Close();
                    if (usuario1.Length > 0)
                    {
                        conexion.Open();
                        string query1 = "select * from TBL_Informacion_Empleados where Empleado = " + empleado;
                        SqlCommand selectEm = new SqlCommand(query1, conexion);
                        SqlDataReader registros1 = selectEm.ExecuteReader();
                        if (registros1.Read())
                        {
                            correo1 = Convert.ToString(registros1["Correo"]);
                        }
                        conexion.Close();
                        if (correo1.Length > 0)
                        {
                            EnviarCorreoME(correo1, usuario, contrasenia);
                            ScriptManager.RegisterStartupScript(this, typeof(Page), "alerta", script4, true);
                            txtCorreo.Text = "";
                            txtUser.Text = "";
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, typeof(Page), "alerta", script2, true);
                            txtCorreo.Text = "";
                            txtUser.Text = "";
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, typeof(Page), "alerta", script3, true);
                        txtCorreo.Text = "";
                        txtUser.Text = "";
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, typeof(Page), "alerta", script1, true);
                    txtCorreo.Text = "";
                    txtUser.Text = "";
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, typeof(Page), "alerta", script, true);
                txtCorreo.Text = "";
                txtUser.Text = "";
            }

        }
        protected void EnviarCorreoME(string correo, string usuario, string contrasenia)
        {
            string mensaje = "";
            mensaje = "<html style='font-size: 8px; font-family: Helvetica, Arial; margin: 0; padding: 0; text-align: center; vertical-align: top; background: #eeeeee;'>";
            mensaje += "<head><title></title><meta charset='utf-8' /></head>";
            mensaje += "<body style='width: 100%; height: 100%; margin: 0; padding: 0; text-align: left; vertical-align: top; margin-left: 10px;'>";
            mensaje += "<p style='font-size: 24px; font-weight: bold; text-transform: uppercase; color: #000000;'>Estimado trabajador:</p>";
            mensaje += "<div><p style='font-size: 16px; color: #000000;'>";
            mensaje += "Por medio de la presente permitame compartirle su contraseña su usuario es: " + usuario + " y su contraseña es: " + contrasenia;
            mensaje += "</p></div>";
            mensaje += "<div><p style='font-size: 16px; color: #000000;'>";
            mensaje += "Atentamente: Portal de Gastos.";
            mensaje += "</p></div>";
            mensaje += "</body></html>";

            AlternateView VistaHtml = AlternateView.CreateAlternateViewFromString(mensaje, Encoding.UTF8, MediaTypeNames.Text.Html);
            string strServer = ConfigurationManager.AppSettings["mailServer"].ToString();
            string strRemitente = ConfigurationManager.AppSettings["mailRemitente"].ToString();
            string strCC = ConfigurationManager.AppSettings["mailCC"].ToString();
            string strContrasenia = ConfigurationManager.AppSettings["mailContrasenia"].ToString();
            int strPuerto = Convert.ToInt32(ConfigurationManager.AppSettings["mailPuerto"].ToString());
            bool ssl = Convert.ToBoolean(ConfigurationManager.AppSettings["mailSSL"].ToString());

            SmtpClient datosSmtp = new SmtpClient();
            datosSmtp.Credentials = new System.Net.NetworkCredential(strRemitente, strContrasenia);
            datosSmtp.Host = strServer;
            datosSmtp.Port = strPuerto;
            datosSmtp.EnableSsl = ssl;

            MailMessage objetoMail = new MailMessage();
            MailAddress mailRemitente = new MailAddress(strRemitente);
            objetoMail.From = mailRemitente;
            objetoMail.AlternateViews.Add(VistaHtml);

            MailAddress mailDestinatario = new MailAddress(correo);
            objetoMail.To.Add(mailDestinatario);
            objetoMail.Subject = "Portal de Gastos, Restauración de contraseña";

            try
            {
                datosSmtp.Send(objetoMail);
                datosSmtp.Dispose();

            }
            catch (Exception ex)
            {

            }

        }

        protected void btnCerrar_Click(object sender, EventArgs e)
        {
            pnlLogin.Visible = true;
            pnlRecuperar.Visible = false;
        }
    }
}
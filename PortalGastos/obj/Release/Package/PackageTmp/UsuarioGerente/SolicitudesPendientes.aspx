﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PaginaMuestra/Site1.Master" AutoEventWireup="true" CodeBehind="SolicitudesPendientes.aspx.cs" Inherits="PortalGastos.UsuarioGerente.SolicitudesPendientes" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../Style/Style.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>
        <asp:Panel ID="pnlSolicitudes" runat="server" CssClass="pnlGenerico centrado6">
            <asp:Label ID="lblTitulo" runat="server" CssClass="lblTituloge" Text="Solicitudes pendientes"></asp:Label>
            <br />
            <hr />
            <table style="width: 100%;">
                <tr style="height: 20px;">
                    <td style="width: 100%;"></td>
                </tr>
                <tr style="width: 100%; float:left">
                    <asp:GridView ID="Resultado" runat="server" CssClass="mGrid1 pagination-ys" PageSize="7" AllowPaging="True" OnPageIndexChanging="Resultado_PageIndexChanging" OnSelectedIndexChanged="Resultado_SelectedIndexChanged1" DataKeyNames="No. de Solicitud" OnRowCommand="Resultado_RowCommand" OnRowDeleting="Resultado_RowDeleting">
                        <Columns>
                            <asp:TemplateField HeaderText="Aceptar">
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnAceptar" runat="server" ImageUrl="~/Imagenes/aceptado.png" CommandName="Select"/>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Rechazar" ShowHeader="False">
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnRechazar" runat="server" CausesValidation="False" CommandName="Delete" Text="Eliminar" ImageUrl="~/Imagenes/cerrar.png" />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </tr>
            </table>
        </asp:Panel>
    </div>
</asp:Content>

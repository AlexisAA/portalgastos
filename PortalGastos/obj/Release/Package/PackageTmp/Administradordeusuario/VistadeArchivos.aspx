﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PaginaMuestra/Site1.Master" AutoEventWireup="true" CodeBehind="VistadeArchivos.aspx.cs" Inherits="PortalGastos.PaginaMuestra.VistadeArchivos" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../Style/Style.css" rel="stylesheet" />
    <script>
        function OpenWindows(url) {
            window.open(url, " Título", "directories=no, location=no, menubar=no, scrollbars=yes, statusbar=no, tittlebar=no, resizable=yes, top=100, left=100, width=1200, height=550");
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>
        <asp:Panel ID="pnlRevisarPdf" runat="server" CssClass="pnlGenerico centrado5">
            <asp:Label ID="lblTitulo" runat="server" Text="COMPROBACIÓN DE GASTOS" CssClass="lblTituloge"></asp:Label>
            <br />
            <hr />
            <br />
            <asp:Label ID="lblDescrip" runat="server" CssClass="lblDescripcion1" Text="Cada una de las solicitudes tienen que ser revisadas"></asp:Label>
            <br />
            <asp:Label ID="lblDescrip1" runat="server" CssClass="lblDescripcion" Text="Revisando cada uno de los archivos comprobamos que sean válidos"></asp:Label>
            <table style="width: 100%;">
                <tr style="height: 20px;">
                    <td style="width: 100%"></td>
                </tr>
                <tr style="height: 20px;">
                    <td style="width: 25%; float:left;">
                        <asp:Button ID="btnRegresar" runat="server" Visible="false" Text="Regresar" OnClick="Regresar_Click" />
                    </td>
                </tr>
                <tr style="height: 20px;">
                    <td style="width: 100%; float:left;">
                        <asp:GridView ID="Resultado" runat="server" CssClass="mGrid1 pagination-ys" PageSize="7" AllowPaging="true" OnPageIndexChanging="Resultado_PageIndexChanging" OnSelectedIndexChanged="Resultado_SelectedIndexChanged" DataKeyNames="No. de Solicitud">
                            <Columns>
                                <asp:TemplateField HeaderText="Visualizar">
                                   <ItemTemplate>
                                       <asp:ImageButton ID="ImagenSelec" runat="server" CommandName="Select" ImageUrl="~/Imagenes/vision.png"/>
                                   </ItemTemplate>
                               </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </td>
                </tr>
                <tr style="height: 20px;">
                    <td style="width: 100%; float:left;">
                        <asp:GridView ID="Resultado1" runat="server" CssClass="mGrid1" DataKeyNames="Tipo gasto" Visible="false" OnSelectedIndexChanged="Resultado1_SelectedIndexChanged">
                            <Columns>
                                <asp:TemplateField HeaderText="Visualizar">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="ImagenPdf" runat="server" CommandName="Select" ImageUrl="~/Imagenes/pdf.png"/>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </td>
                </tr>
                <tr style="height: 20px;">
                    <td style="width: 100%"></td>
                    </tr>
                <tr style="height: 20px;">
                    <td style="width: 25%; float:left;">
                        <asp:Label ID="lblFecha" runat="server" CssClass="lblEtique" Visible="false"></asp:Label>
                        <br /> <br /><br />
                        <asp:Label ID="lblComentario" runat="server" CssClass="lblEtique" Visible="false"></asp:Label>
                        <asp:TextBox ID="txtComentario" runat="server" Visible="false" Width="700px" Height="200px" CssClass="lblEtique"></asp:TextBox>
                    </td>
                </tr>
                <tr style="height: 20px;">
                    <td style="width: 100%"></td>
                    </tr>
                <tr style="height: 20px;">
                    <td style="width: 25%; float:left;">
                        <asp:Button ID="btnContestar" runat="server" Text="Contestar" Visible="false" OnClick="btnContestar_Click" />
                        <asp:Button ID="btnRegresar1" runat="server" Text="Regresar" Visible="false" OnClick="btnRegresar1_Click" />
                    </td>
                    <td style="width: 25%; float:left;">
                        <asp:Button ID="btnAceptar" runat="server" Visible="false" Text="Aceptar" CssClass="Aceptar" OnClick="btnAceptar_Click"/>
                    </td>
                    <td style="width: 25%; float:left;">
                        <asp:Button ID="btnRechazar" runat="server" Visible="false" Text="Rechazar" CssClass="Rechazar" OnClick="btnRechazar_Click"/>
                    </td>
                </tr>
            </table>
        </asp:Panel>
    </div>
</asp:Content>

﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PaginaMuestra/Site1.Master" AutoEventWireup="true" CodeBehind="BloquearUsuario.aspx.cs" Inherits="PortalGastos.Administradordesistema.BloquearUsuario" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../Style/Style.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>
        <asp:Panel ID="Bloqueo" runat="server" CssClass="pnlGenerico centrado3">
            <br />
            <asp:Label ID="Titulo" runat="server" Text="BLOQUEAR, DESBLOQUEAR USUARIO" CssClass="lblTituloge"></asp:Label>
            <br />
            <hr />
            <table style="width: 100%;">
                <tr style="height: 20px;">
                    <td style="width: 100%;"></td>
                </tr>
                <tr style="width:100%">
                    <td style="height:40px; float:left">
                        <br />
                         <asp:Label ID="lblDescrip" runat="server" Text="El administrador de sistema puede bloquear y desbloquear a todos los usuarios" CssClass="lblDescripcion1"></asp:Label>
                    </td>
                </tr>
                <tr style="width:100%">
                    <td style="height:40px; float:left">
                        <asp:Label ID="info1" runat="server" Text="Botón bloquear todos, hace el bloqueo masivo de todos los empleados" CssClass="lblDescripcion"></asp:Label>
                        <br />
                        <asp:Label ID="info2" runat="server" Text="Botón desbloquar, hace el desbloqueo masivo de todos los empleados" CssClass="lblDescripcion"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td style="width:25%; float:left;">
                       <asp:Button ID="BloquearT" runat="server" CssClass="cssBotonPanel" Text="Bloquear todos" OnClick="BloquearT_Click"/>
                    </td>
                    <td style="width:25%; float:left;">
                       <asp:Button ID="DesbloquearT" runat="server" CssClass="cssBotonPanel" Text="Desbloquear todos" OnClick="DesbloquearT_Click"/>
                    </td>
                </tr>
                </table>
            <br /><br />
            <hr />
            <br />
                <asp:Label ID="lblDesc" runat="server" Text="Bloqueo, desbloqueo especifico" CssClass="lblDescripcion1"></asp:Label>
                <br />
            <asp:Label ID="Label2" runat="server" Text="Si deseas bloquear y desbloquear usuarios especificos, puedes filtrar por usuario " CssClass="lblDescripcion"></asp:Label>
            <table id="tableBloqUser">
                <tr >
                    <td >
                        <asp:Label ID="lblUsuario" runat="server" CssClass="lblEtique" Text="Usuario: "></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtUsuario" runat="server" CssClass="txt"></asp:TextBox>
                    </td>
                    <td >
                       <asp:Button ID="Buscar" runat="server" CssClass="cssBotonPanel" Text="Buscar" OnClick="Buscar_Click"/>
                    </td>
                </tr>
                <tr >
                    <td>
                        <asp:Label ID="lblPrimerNom" runat="server" CssClass="lblEtique" Text="Primer nombre: "></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblPrimerNom1" runat="server" CssClass="lblEtique1"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblSegundoNom" runat="server" CssClass="lblEtique" Text="Segundo nombre: "></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblSegundoNom1" runat="server" CssClass="lblEtique1"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblApellidoP" runat="server" CssClass="lblEtique" Text="Apellido paterno: "></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblApellidoP1" runat="server" CssClass="lblEtique1"></asp:Label>
                    </td>
                    <td>
                        <asp:Button ID="btnBloquear" runat="server" CssClass="cssBotonPanel" Text="Bloquear" OnClick="btnBloquear_Click"/>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblApellidoM" runat="server" CssClass="lblEtique" Text="Apellido materno: "></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblApellidoM1" runat="server" CssClass="lblEtique1"></asp:Label>
                    </td>
                    <td>
                        <asp:Button ID="btnDesbloquear" runat="server" CssClass="cssBotonPanel" Text="Desbloquear" OnClick="btnDesbloquear_Click"/>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblUsuario1" runat="server" CssClass="lblEtique" Text="Usuario: "></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblUsuario2" runat="server" CssClass="lblEtique1"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblArea" runat="server" CssClass="lblEtique" Text="Área:"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblArea1" runat="server" CssClass="lblEtique1"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblEstatus" runat="server" CssClass="lblEtique" Text="Estatus:"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblEstatus1" runat="server" CssClass="lblEtique1"></asp:Label>
                    </td>
                </tr>
            </table>
        </asp:Panel>
    </div>
</asp:Content>

﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PaginaMuestra/Site1.Master" AutoEventWireup="true" CodeBehind="ModificarUsuario.aspx.cs" Inherits="PortalGastos.Administradordesistema.ModificarUsuario" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../Style/Style.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>
        <asp:Panel ID="pnlModificar" runat="server" CssClass="pnlGenerico  centrado3">
            <asp:Label ID="lblTitulo" runat="server" CssClass="lblTituloge" Text="MODIFICAR USUARIO"></asp:Label>
            <br />
            <hr />
            <table style="width: 100%;">
                <tr style="height: 20px;">
                    <td style="width: 100%;">
                    </td>
                </tr>
                <tr style="height: 40px;">
                    <td style="width: 25%; float:left;">
                        <asp:Label ID="lblUsuario" runat="server" CssClass="lblEtique" Text="Usuario: "></asp:Label>
                    </td>
                    <td style="width:25%; float:left;">
                        <asp:TextBox ID="txtUsuario" runat="server" CssClass="txt"></asp:TextBox>
                    </td>
                    <td style="width:25%; float:left;">
                        <asp:Button ID="btnBuscar" runat="server" CssClass="cssBotonPanel" Text="BUSCAR" OnClick="btnBuscar_Click"/>
                    </td>
                </tr>
                <tr style="height: 40px;">
                    <td style="width: 25%; float:left;">
                    </td>
                    <td style="width: 25%; float:left;">
                        <asp:Label ID="lblEstatus" runat="server" Text="Estatus: " CssClass="lblNormalRojo"></asp:Label>
                        <asp:Label ID="lblEstatusRes" runat="server" CssClass="lblNormalRojo"></asp:Label>
                    </td>
                </tr>
                <tr style="height: 40px;">
                    <td style="width: 25%; float:left;">
                        <asp:Label ID="lblNombre1" runat="server" CssClass="lblEtique" Text="Primer Nombre: "></asp:Label>
                    </td>
                    <td style="width: 25%; float:left;">
                        <asp:TextBox ID="txtNombre1" runat="server" CssClass="txt"></asp:TextBox>
                    </td>
                </tr>
                <tr style="height: 40px;">
                    <td style="width: 25%; float:left;">
                        <asp:Label ID="lblNombre2" runat="server" CssClass="lblEtique" Text="Segundo Nombre: "></asp:Label>
                    </td>
                    <td style="width: 25%; float:left;">
                        <asp:TextBox ID="txtNombre2" runat="server" CssClass="txt"></asp:TextBox>
                    </td>
                </tr>
                <tr style="height: 40px;">
                    <td style="width: 25%; float:left;">
                        <asp:Label ID="lblAp" runat="server" CssClass="lblEtique" Text="Apellido paterno: "></asp:Label>
                    </td>
                    <td style="width: 25%; float:left;">
                        <asp:TextBox ID="txtAp" runat="server" CssClass="txt"></asp:TextBox>
                    </td>
                </tr>
                <tr style="height: 40px;">
                    <td style="width: 25%; float:left;">
                        <asp:Label ID="lblAm" runat="server" CssClass="lblEtique" Text="Apellido materno: "></asp:Label>
                    </td>
                    <td style="width: 25%; float:left;">
                        <asp:TextBox ID="txtAm" runat="server" CssClass="txt"></asp:TextBox>
                    </td>
                    <td style="width: 25%; float:left;">
                        <asp:Button ID="btnModificar" runat="server" CssClass="cssBotonPanel" Text="MODIFICAR" OnClick="btnModificar_Click" />
                    </td>
                </tr>
                <tr style="height: 40px;">
                    <td style="width: 25%; float:left;">
                        <asp:Label ID="lblUsuario1" runat="server" CssClass="lblEtique" Text="Usuario: "></asp:Label>
                    </td>
                    <td style="width: 25%; float:left;">
                        <asp:TextBox ID="txtUsuario1" runat="server" CssClass="txt"></asp:TextBox>
                    </td>
                </tr>
                <tr style="height: 40px;">
                    <td style="width: 25%; float:left;">
                        <asp:Label ID="lblArea" runat="server" CssClass="lblEtique" Text="Área: "></asp:Label>
                    </td>
                    <td style="width: 25%; float:left;">
                        <asp:TextBox ID="txtArea" runat="server" CssClass="txt"></asp:TextBox>
                    </td>
                </tr>
                <tr style="height: 40px;">
                    <td style="width: 25%; float:left;">
                        <asp:Label ID="lblPuesto" runat="server" CssClass="lblEtique" Text="Puesto: "></asp:Label>
                    </td>
                    <td style="width: 25%; float:left;">
                        <asp:TextBox ID="txtPuesto" runat="server" CssClass="txt"></asp:TextBox>
                    </td>
                </tr>
                <tr style="height: 40px;">
                    <td style="width: 25%; float:left;">
                        <asp:Label ID="lblRol" runat="server" CssClass="lblEtique" Text="Rol: "></asp:Label>
                    </td>
                    <td style="width: 25%; float:left;">
                        <asp:TextBox ID="txtRol" runat="server" CssClass="txt"></asp:TextBox>
                    </td>
                </tr>
            </table>
        </asp:Panel>
    </div>
</asp:Content>

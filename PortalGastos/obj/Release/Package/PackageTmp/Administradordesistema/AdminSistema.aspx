﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AdminSistema.aspx.cs" Inherits="PortalGastos.Administradordesistema.AdminSistema" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <link href="~/Style/Style1.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
        <div>
             <header>
        <table style="width: 100%; height: 100%; vertical-align: middle; text-align: center">
            <tr>
                <td style="width: 20%; height: 100%; vertical-align: middle; text-align: initial">
                    <asp:Image ID="imgLogo" runat="server" ImageUrl="~/Imagenes/logo_prime.png"/>
                </td>
            </tr>
        </table>
    </header>
    <section>
        <table style="width: 100%;">
        <tr style="height: 440px;" class="pnlImagen">
            <td style="width: 50%;">
                <asp:Label ID="lblUser" runat="server" CssClass="lblTituloge1"></asp:Label><br />
                <asp:Panel ID="pnlMenu" runat="server" CssClass="pnlLogin centrado1">
                    <asp:Image ID="imgMenu" runat="server" ImageUrl="~/Imagenes/Menú.png" />
                    <br />
                    <asp:Button ID="btnCrear" runat="server" Text="Crear Usuario" CssClass="cssBotonPanel" OnClick="btnCrear_Click" />
                    <br /><br />
                    <asp:Button ID="btnBloquear" runat="server" Text="Bloquear Usuario" CssClass="cssBotonPanel" OnClick="btnBloquear_Click"/>
                    <br /><br />
                    <asp:Button ID="btnModificar" runat="server" Text="Modificar Usuario" CssClass="cssBotonPanel" OnClick="btnModificar_Click" />
                    <br /><br />
                    <asp:Button ID="btnCambiar" runat="server" Text="Cambiar Contraseña" CssClass="cssBotonPanel" OnClick="btnCambiar_Click" />
                    <br /><br />
                    <asp:Button ID="btnCerrar" runat="server" Text="Cerrar Sesión" CssClass="cssBotonPanel" OnClick="btnCerrar_Click" />
                    <br /><br />
                </asp:Panel>
            </td>
            <td style="width: 50%;">
                <asp:Image ID="imgMujer" runat="server" ImageUrl="~/Imagenes/moderna-aplicacion-tecnologia_101984-47.jpg" Height="440px" Width="700px"/>
            </td>
        </tr>
    </table>
    </section>
    <footer>
        <table style="width: 100%; height: 100%; vertical-align: middle; text-align: center">
            <tr> 
                <td style="width: 30%; height: 100%; text-align: center"></td>
            </tr>
            <tr>
                <td style="width: 30%; height: 100%; text-align: center">
                    <asp:Label ID="lblEmpresa" runat="server" Text="Prime Consultoría" CssClass="lblTituloge1"></asp:Label>
                    <br />
                    <asp:HyperLink ID="hypPagina" runat="server" Text="www.prime-consultoria.com.mx" CssClass="lblTituloge1" NavigateUrl="https://prime-consultoria.com.mx/"></asp:HyperLink>
                </td>
            </tr>
        </table>
    </footer>
        </div>
    </form>
</body>
</html>

﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PaginaMuestra/Site1.Master" AutoEventWireup="true" CodeBehind="GastoArea.aspx.cs" Inherits="PortalGastos.UsuarioDirector.GastoArea" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../Style/Style.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:Panel ID="pnlGasto" runat="server" CssClass="centrado6 pnlGenerico">
        <asp:Label ID="lblTitulo" runat="server" CssClass="lblTituloge" Text="Gasto por área"></asp:Label>
        <br />
        <hr />
        <table style="width: 100%;">
            <tr style="height: 20px;">
                <td style="width: 100%;"></td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblDesc" runat="server" CssClass="lblDescripcion" Text="Gasto por área al día de hoy"></asp:Label>
                    <br />
                    <asp:Label ID="lblDesc1" runat="server" CssClass="lblDescripcion1" Text="Despliega el importe total por área"></asp:Label>
                </td>
            </tr>
            <tr style="width: 100%; float:left">
                <asp:GridView ID="Resultado" runat="server" CssClass="mGrid1 pagination-ys" PageSize="7" AllowPaging="true" OnPageIndexChanging="Resultado_PageIndexChanging">
                            <Columns>
                                <asp:TemplateField>
                                    <ItemTemplate></ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>

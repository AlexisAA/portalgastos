﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UsuarioDirector.aspx.cs" Inherits="PortalGastos.UsuarioDirector.UsuarioDirector" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <link href="~/Style/Style1.css" rel="stylesheet" />
    <style type="text/css">
        .auto-style1 {
            bottom: 164;
            margin-bottom: 0px;
        }
    </style>
</head>
<body>
     <form id="form1" runat="server">
        <div>
             <header>
        <table style="width: 100%; height: 100%; vertical-align: middle; text-align: center">
            <tr>
                <td style="width: 20%; height: 100%; vertical-align: middle; text-align: initial">
                    <asp:Image ID="imgLogo" runat="server" ImageUrl="~/Imagenes/logo_prime.png"/>
                </td>
            </tr>
        </table>
    </header>
    <section>
        <table style="width: 100%;">
        <tr style="height: 440px;" class="pnlImagen">
            <td style="width: 50%;">
                <asp:Label ID="lblUser" runat="server" CssClass="lblTituloge1"></asp:Label>
                <asp:Panel ID="pnlMenu" runat="server" CssClass="pnlLogin">
                    <br />
                    <asp:Image ID="imgMenu" runat="server" ImageUrl="~/Imagenes/Menú.png" />
                    <br /><br />
                    <asp:Button ID="btnCrear" runat="server" Text="Solicitudes pendientes" CssClass="cssBotonPanel" OnClick="btnCrear_Click"/>
                    <br /><br />
                    <asp:Button ID="btnBloquear" runat="server" Text="Gastos por área" CssClass="cssBotonPanel" OnClick="btnBloquear_Click"/>
                    <br /><br />
                    <asp:Button ID="btnCambiar" runat="server" Text="Cambiar Contraseña" CssClass="cssBotonPanel" OnClick="btnCambiar_Click" />
                    <br /><br />
                    <asp:Button ID="btnCerrar" runat="server" Text="Cerrar Sesión" CssClass="cssBotonPanel" OnClick="btnCerrar_Click" />
                    <br /><br />
                </asp:Panel>
            </td>
            <td style="width: 50%;">
                <asp:Image ID="imgMujer" runat="server" ImageUrl="~/Imagenes/mujer-negocios-terminal-bolsa-viaje_1303-20353.jpg" Height="440px" Width="700px"/>
            </td>
        </tr>
    </table>
    </section>
    <footer class="auto-style1">
        <table style="width: 100%; height: 100%; vertical-align: middle; text-align: center">
            <tr> 
                <td style="width: 30%; height: 100%; text-align: center"></td>
            </tr>
            <tr>
                <td style="width: 30%; height: 100%; text-align: center">
                    <asp:Label ID="lblEmpresa" runat="server" Text="Prime Consultoría" CssClass="lblTituloge1"></asp:Label>
                    <br />
                    <asp:HyperLink ID="hypPagina" runat="server" Text="www.prime-consultoria.com.mx" CssClass="lblTituloge1" NavigateUrl="https://prime-consultoria.com.mx/"></asp:HyperLink>
                </td>
            </tr>
        </table>
    </footer>
        </div>
    </form>
</body>
</html>

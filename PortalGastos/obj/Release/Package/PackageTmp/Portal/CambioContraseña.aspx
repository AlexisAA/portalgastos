﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PaginaMuestra/Site1.Master" AutoEventWireup="true" CodeBehind="CambioContraseña.aspx.cs" Inherits="PortalGastos.Portal.CambioContraseña1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../Style/Style.css" rel="stylesheet" />
    <style type="text/css">
        .auto-style1 {
            margin-left: 40px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>
        <asp:Panel ID="pnlCambio" runat="server" CssClass="pnlRecuperar centrado1">
            <asp:Label ID="lblTitulo" runat="server" Text="Cambio de contraseña" CssClass="lblTituloge"></asp:Label>
            <br />
            <hr />
            <table style="width: 100%;">
                <tr style="height: 40px;">
                    <td style="width: 100%; float:right;" class="auto-style1">
                        <asp:Label ID="lblCambio" runat="server" Text="Última contraseña ingresada:" CssClass="lblEtique"></asp:Label>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:TextBox ID="txtCambio" runat="server" placeholder="Última contraseña" CssClass="txt" TextMode="Password"></asp:TextBox>
                    </td>
                </tr>
                <tr style="height: 40px;">
                    <td style="width: 100%">
                    </td>
                </tr>
                <tr style="height: 40px;">
                    <td style="width: 100%;">
                        <asp:Label ID="lblCambio1" runat="server" Text="Nueva Contraseña" CssClass="lblEtique"></asp:Label>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:TextBox ID="txtCambio1" runat="server" placeholder="Contraseña nueva" CssClass="txt" TextMode="Password"></asp:TextBox>
                        <br /><br /><br /><br /><br />
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        &nbsp;&nbsp;&nbsp;
                        <asp:Button ID="btnCambiar" runat="server" Text="Cambiar" CssClass="cssBotonPanel" OnClick="btnCambiar_Click"/>
                    </td>
                </tr>
                <tr style="height: 40px;">
                    <td style="width: 100%">
                    </td>
                </tr>
                <tr style="height: 40px;">
                    <td style="width:50%; vertical-align:middle"></td>
                    <td style="width: 50%; float:left;">
                        
                    </td>
                </tr>
            </table>
        </asp:Panel>
    </div>
</asp:Content>

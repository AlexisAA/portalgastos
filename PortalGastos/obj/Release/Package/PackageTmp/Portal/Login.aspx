﻿<%@ Page Title="Login" Language="C#" MasterPageFile="~/PaginaMuestra/Site1.Master" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="PortalGastos.Portal.Login" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../Style/Style.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>
        <asp:Panel ID="pnlLogin" runat="server" CssClass="pnlLogin centradoLogin">
            <table>
                <tr >
                    <td style="text-align: center;">
                        <asp:Label ID="lblLeyenda" runat="server" Text="Inicio de Sesión" CssClass="lblH1Negro"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td style="text-align:center">
                        <asp:TextBox ID="txtUsuario" runat="server" Width="200px" placeholder="Usuario"></asp:TextBox>
                        <br /><br /><br />
                    </td>
                </tr>
                <tr>
                    <td style="text-align:center">
                        <asp:TextBox ID="txtPassword" runat="server" Width="200px" TextMode="Password" placeholder="Contraseña"></asp:TextBox>
                    </td>
                </tr>
                <tr >
                    <td style="text-align: center;">
                        <asp:Label ID="lblMensaje" runat="server" Text="" Visible="false" CssClass="lblNormalRojo"></asp:Label>
                        <br />
                    </td>
                </tr>
                <tr>
                    <td style="text-align: center">
                        <asp:Button ID="btnInicio" runat="server" Text="Ingresar" CssClass="cssBotonPanel" OnClick="btnInicio_Click" />
                        <br />
                    </td>
                </tr>
                <tr>
                    <td style="text-align: center;">
                        <br />
                        <asp:Button ID="btnRecuperar" runat="server" Text="Recuperar contraseña" CssClass="cssBotonPanel" data-target="#RecContraseña" data-toggle="modal" OnClick="btnRecuperar_Click" />
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <asp:Panel ID="pnlRecuperar" runat="server" Visible="false" CssClass="pnlRecuperar centrado1">
        <table>
            <asp:Label ID="lblTitulo" runat="server" CssClass="lblTitulo" Text="Recuperar Contraseña"></asp:Label>
            <tr>
                    <td>
                    </td>
                </tr>
            <tr>
                <td>
                    <asp:Label ID="lblCorreo" runat="server" Text="Correo: " CssClass="lblH1Negro"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtCorreo" runat="server" placeHolder="@gmail.com"></asp:TextBox>
                </td>
            </tr>
            <tr>
                    <td>
                    </td>
            <tr>
                <td>
                    <asp:Label ID="lblUsuario" runat="server" Text="Usuario: " CssClass="lblH1Negro"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtUser" runat="server" placeHolder="Usuario"></asp:TextBox>
                </td>
            </tr>
            <tr>
                    <td>
                    </td>
            <tr>
                <td>
                    <asp:Button ID="btnEnviar" runat="server" Text="Enviar" OnClick="btnEnviar_Click" CssClass="cssBotonPanel"/>
                </td>
                <td>
                    <asp:Button ID="btnCerrar" runat="server" Text="Cerrar" CssClass="cssBotonPanel" OnClick="btnCerrar_Click" />
                </td>
            </tr>
        </table>
        </asp:Panel>
    </div>
</asp:Content>


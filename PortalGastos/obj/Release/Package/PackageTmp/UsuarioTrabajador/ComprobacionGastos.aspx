﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PaginaMuestra/Site1.Master" AutoEventWireup="true" CodeBehind="ComprobacionGastos.aspx.cs" Inherits="PortalGastos.UsuarioTrabajador.ComprobacionGastos" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../Style/Style.css" rel="stylesheet" />
    <style type="text/css">
        .auto-style1 {
            margin-left: 40px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>
        <asp:Panel ID="pnlComprobacion" runat="server" CssClass="pnlGenerico centrado6">
            <asp:Label ID="lblTitulo" runat="server" Text="COMPROBACIÓN DE GASTOS" CssClass="lblTituloge"></asp:Label>
            <br />
            <hr />
            <table style="width: 100%;">
                <tr style="height: 20px;">
                    <td style="width: 100%">
                    </td>
                </tr>
                <tr style="height: 20px;">
                    <td style="width: 25%; float:left;" class="auto-style1">
                        <asp:Button ID="btnRegresar" runat="server" Text="Regresar" Visible="false" OnClick="btnRegresar_Click" />
                    </td>
                </tr>
                <tr style="height: 20px;">
                    <td style="width: 100%;"></td></tr>
                <tr style="height: 20px;">
                    <td style="width: 100%; float:left;">
                         <asp:GridView ID="Resultado" runat="server"  CssClass="mGrid1" DataKeyNames="No. de Solicitud" OnSelectedIndexChanged="Resultado_SelectedIndexChanged1">
                            <Columns>
                               <asp:TemplateField HeaderText="Visualizar">
                                   <ItemTemplate>
                                       <asp:ImageButton ID="ImagenSelec" runat="server" CommandName="Select" ImageUrl="~/Imagenes/vision.png"/>
                                   </ItemTemplate>
                               </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </td>
                </tr>
                <tr style="height: 20px;">
                    <td style="width: 100%;"></td></tr>
                <tr style="height: 20px;">
                    <td style="width: 100%; float:left;">
                        <asp:Label ID="lblNombreEmp" runat="server" CssClass="lblEtique" Visible="false" Text="Nombre.  "></asp:Label>
                    </td>
                </tr>
                <tr style="height: 20px;">
                    <td style="width: 100%;"></td></tr>
                <tr style="height: 20px;">
                    <td style="width: 25%; float:left;">
                        <asp:Label ID="lblArchivoVuelo" runat="server" CssClass="lblEtique" Text="Factura de Vuelo:" Visible="false"></asp:Label>
                    </td>
                    <td style="width: 25%; float:left;">
                        <asp:FileUpload ID="Filesubir1" runat="server" Visible="false" Text="Examinar" CssClass="lblImagen" AllowMultiple="true" />
                    </td>
                    <td style="width: 25%; float:left;">
                        <asp:Button ID="btnSubirVuelo" runat="server" Visible="false" Text="Subir Archivos" OnClick="btnSubir_Click" />
                    </td>
                </tr>
                <tr style="height: 20px;">
                    <td style="width: 100%;"></td></tr>
                <tr style="height: 20px;">
                    <td style="width: 25%; float:left;">
                        <asp:Label ID="lblArchivoHospedaje" runat="server" CssClass="lblEtique" Text="Factura de Hospedaje:" Visible="false"></asp:Label>
                    </td>
                    <td style="width: 25%; float:left;">
                        <asp:FileUpload ID="Filesubir2" runat="server" Visible="false" Text="Examinar" CssClass="lblImagen" AllowMultiple="true" />                  
                    </td>
                    <td style="width: 25%; float:left;">
                        <asp:Button ID="btnSubirHospedaje" runat="server" Visible="false" Text="Subir Archivos" OnClick="btnSubirHospedaje_Click" />
                    </td>
                </tr>
                 <tr style="height: 20px;">
                    <td style="width: 100%;"></td></tr>
                <tr style="height: 20px;">
                    <td style="width: 25%; float:left;">
                        <asp:Label ID="lblArchivoTraslado" runat="server" CssClass="lblEtique" Visible="false" Text="Factura de Traslado:"></asp:Label>
                    </td>
                        <td style="width: 25%; float:left;">
                        <asp:FileUpload ID="Filesubir3" runat="server" Visible="false" Text="Examinar" CssClass="lblImagen" AllowMultiple="true" />
                    </td>
                    <td style="width: 25%; float:left;">
                        <asp:Button ID="btnSubirTraslado" runat="server" Visible="false" Text="Subir Archivos" OnClick="btnSubirTraslado_Click" />
                    </td>
                </tr>
                <tr style="height: 20px;">
                    <td style="width: 100%;"></td></tr>
                <tr style="height: 20px;">
                    <td style="width: 25%; float:left;">
                        <asp:Label ID="lblArchivoComida" runat="server" CssClass="lblEtique" Visible="false" Text="Factura de Comida:"></asp:Label>
                    </td>
                    <td style="width: 25%; float:left;">
                        <asp:FileUpload ID="Filesubir4" runat="server" Visible="false" Text="Examinar" CssClass="lblImagen" AllowMultiple="true" />
                    </td>
                    <td style="width: 25%; float:left;">
                        <asp:Button ID="btnSubirComida" runat="server" Visible="false" Text="Subir Archivos" OnClick="btnSubirComida_Click" />
                    </td>
                </tr>
                <tr style="height: 20px;">
                    <td style="width: 100%;"></td></tr>
                 <tr style="height: 20px;">
                    <td style="width: 25%; float:left;">
                        <asp:Label ID="lblArchivoRenta" runat="server" CssClass="lblEtique" Visible="false" Text="Factura de Renta Automóvil"></asp:Label>
                    </td>
                    <td style="width: 25%; float:left;">
                        <asp:FileUpload ID="Filesubir6" runat="server" Visible="false" Text="Examinar" CssClass="lblImagen" AllowMultiple="true" />
                    </td>
                     <td style="width: 25%; float:left;">
                         <asp:Button ID="btnSubirRenta" runat="server" Visible="false" Text="Subir Archivos" OnClick="btnSubirRenta_Click" />
                     </td>
                     <tr style="height: 20px;">
                    <td style="width: 100%;"></td></tr>
                <tr style="height: 20px;">
                    <td style="width: 25%; float:left;">
                        <asp:Label ID="lblArchivoNo" runat="server" CssClass="lblEtique" Visible="false" Text="Gastos sin factura"></asp:Label>
                    </td>
                        <td style="width: 25%; float:left;">
                        <asp:FileUpload ID="Filesubir5" runat="server" Visible="false" Text="Examinar" CssClass="lblImagen" AllowMultiple="true" />
                    </td>
                    <td style="width: 25%; float:left;">
                        <asp:Button ID="btnSubirNoComprobable" runat="server" Visible="false" Text="Subir Archivos" OnClick="btnSubirNoComprobable_Click" />
                    </td>
                </tr>
                <tr style="height: 20px;">
                    <td style="width: 100%;"></td></tr>
                <tr style="height: 20px;">
                    <td style="width: 50%; float:left;">
                        <asp:TextBox ID="txtComentario" runat="server" Visible="false" Width="700px" Height="200px" Enabled="false"></asp:TextBox>
                    </td>

                    <td style="width: 25%; float:left;">
                        <asp:Button ID="btnComentario" runat="server" Visible="false" Text="Comentario" OnClick="btnComentario_Click" />
                    </td>
                </tr>
                <tr style="height: 20px;">
                    <td style="width: 25%; float:left;">
                        <asp:Button ID="btnEnviar" runat="server" Visible="false" Text="Enviar" OnClick="btnEnviar_Click" />
                    </td>
                </tr>
            </table>
        </asp:Panel>
    </div>
</asp:Content>

﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PortalGastos.UsuarioGerente
{
    public partial class SolicitudesPendientes : System.Web.UI.Page
    {
        SqlConnection conexion = new SqlConnection("Data source=(local); Initial Catalog=PortalGastos; User=sae; Password=sae ");
        protected void Page_Init(object sender, EventArgs e)
        {
            if (Session["user"] == null)
            {
                HttpContext.Current.Response.Redirect("~/Portal/Login.aspx");
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                AgregarGridMe();
            }
        }
        protected void AgregarGridMe()
        {
            string imgaceptar = string.Empty;
            string imgrechazar = string.Empty;
            imgaceptar = ConfigurationManager.AppSettings["ImgAceptar"];
            imgrechazar = ConfigurationManager.AppSettings["ImgRechazar"];
            List<string> Lista = new List<string>();
            string P_Nom = "", AP = "", To_Ga = "", Fec_Ini = "", Estatus = "", con1 = "";
            int Solicitud = 0, Empleado = 0;
            DataTable dt = new DataTable();
            dt.Columns.AddRange(new DataColumn[6] {new DataColumn("No. de Solicitud",typeof(int)),
                                                    new DataColumn("No. de Empleado",typeof(int)),
                                                    new DataColumn("Nombre de Empleado",typeof(string)),
                                                    new DataColumn("Gasto Total",typeof(string)),
                                                    new DataColumn("Fecha de Inicio",typeof(string)),
                                                    new DataColumn("Estatus",typeof(string))});
            conexion.Open();
            string query = "select * from v_Autorizador1 where Area = " + Session["Area"] + " order by ID_Solicitud desc";
            SqlCommand selec = new SqlCommand(query, conexion);
            SqlDataReader registro = selec.ExecuteReader();
            while (registro.Read())
            {
                con1 = registro[0].ToString();
                Lista.Add(con1);
                for (int i = 0; i < Lista.Count; i++)
                {
                    Solicitud = Convert.ToInt32(registro["ID_Solicitud"]);
                    Empleado = Convert.ToInt32(registro["ID_Usuario"]);
                    P_Nom = Convert.ToString(registro["Primer_nombre"]);
                    AP = Convert.ToString(registro["Apellido_paterno"]);
                    To_Ga = Convert.ToString(registro["Total_solicitado"]);
                    Fec_Ini = Convert.ToString(registro["Fecha_Inicio"]);
                    Estatus = Convert.ToString(registro["Estatus"]);
                }
                dt.Rows.Add(Solicitud, Empleado, P_Nom + " " + AP,"$"+decimal.Parse(To_Ga).ToString("N") , Fec_Ini, Estatus);
                Resultado.DataSource = dt;
                Resultado.DataBind();
            }
            conexion.Close();
        }

        protected void Resultado_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            AgregarGridMe();
            Resultado.PageIndex = e.NewPageIndex;
            Resultado.DataBind();
        }

        protected void Resultado_SelectedIndexChanged1(object sender, EventArgs e)
        {
            Resultado.Visible = false;
            lblTitulo.Visible = false;
            lblTitulo2.Visible = true;
            lblNombre.Visible = true;
            btnRegresar.Visible = true;
            lblFechaInicio.Visible = true;
            lblFechaInicio1.Visible = true;
            lblSolicitud.Visible = true;
            lblFechaFin.Visible = true;
            lblFechaFin1.Visible = true;
            lblDestino.Visible = true;
            lblDestino1.Visible = true;
            lblPeriodo.Visible = true;
            lblPeriodo1.Visible = true;
            lblTipoViaje.Visible = true;
            lblTipoViaje1.Visible = true;
            lblGastoVuelo.Visible = true;
            lblGastoVuelo1.Visible = true;
            lblGastoHospedaje.Visible = true;
            lblGastoHospedaje1.Visible = true;
            lblGastoTerrestre.Visible = true;
            lblGastoTerrestre1.Visible = true;
            lblGastoComida.Visible = true;
            lblGastoComida1.Visible = true;
            lblGastoRenta.Visible = true;
            lblGastoRenta1.Visible = true;
            lblGastosOtros.Visible = true;
            lblGastosOtros1.Visible = true;
            lblGastoTotal.Visible = true;
            lblGastoTotal1.Visible = true;
            btnAceptar.Visible = true;
            btnRechazar.Visible = true;
            string Nombre = "",Apellido_paterno="",Apellido_materno="", Fecha_inicio = "",Fecha_Fin="",LugarDestino="";
            string periodo = "",Tipo_Viaje ="",GastoVuelo ="",GastoHospedaje="",GastoTerrestre="",GastoComida="";
            string GastoRenta = "", GastoOtros = "", TotalSolicitado = "";
            int ID_solicitud = 0;
            GridViewRow row = Resultado.SelectedRow;
            int id = Convert.ToInt32(Resultado.DataKeys[row.RowIndex].Value);
            conexion.Open();
            string query = "select * from v_SolicitudesPendientes where ID_Solicitud =" + id;
            SqlCommand select = new SqlCommand(query,conexion);
            SqlDataReader registros = select.ExecuteReader();
            if (registros.Read())
            {
                Nombre = Convert.ToString(registros["Primer_nombre"]);
                Apellido_materno = Convert.ToString(registros["Apellido_materno"]);
                Apellido_paterno = Convert.ToString(registros["Apellido_paterno"]);
                ID_solicitud = Convert.ToInt32(registros["ID_Solicitud"]);
                Fecha_inicio = Convert.ToString(registros["Fecha_Inicio"]);
                Fecha_Fin = Convert.ToString(registros["Fecha_Fin"]);
                LugarDestino = Convert.ToString(registros["Lugar_Destino"]);
                periodo = Convert.ToString(registros["Periodo"]);
                Tipo_Viaje = Convert.ToString(registros["Nombre"]);
                GastoVuelo = Convert.ToString(registros["Gasto_vuelo"]);
                GastoHospedaje = Convert.ToString(registros["Gasto_hospedaje"]);
                GastoTerrestre = Convert.ToString(registros["Gasto_terrestre"]);
                GastoComida = Convert.ToString(registros["Gasto_comida"]);
                GastoRenta = Convert.ToString(registros["Gasto_Renta"]);
                GastoOtros = Convert.ToString(registros["Gasto_otros"]);
                TotalSolicitado = Convert.ToString(registros["Total_solicitado"]);

            }
            conexion.Close();
            lblNombre.Text = "Sr(a): " + Nombre + " " + Apellido_paterno + " " + Apellido_materno;
            lblSolicitud.Text = "Número de solicitud: "+ ID_solicitud;
            lblFechaInicio1.Text = Fecha_inicio;
            lblFechaFin1.Text = Fecha_Fin;
            lblDestino1.Text = LugarDestino;
            lblPeriodo1.Text = periodo + " días";
            lblTipoViaje1.Text = Tipo_Viaje;
            lblGastoVuelo1.Text = "$ " + decimal.Parse(GastoVuelo).ToString("N");
            lblGastoHospedaje1.Text = "$ "+ decimal.Parse(GastoHospedaje).ToString("N");
            lblGastoTerrestre1.Text = "$ " + decimal.Parse(GastoTerrestre).ToString("N");
            lblGastoComida1.Text = "$ " + decimal.Parse(GastoComida).ToString("N");
            lblGastoRenta1.Text = "$ " + decimal.Parse(GastoRenta).ToString("N");
            lblGastosOtros1.Text = "$ " + decimal.Parse(GastoOtros).ToString("N");
            lblGastoTotal1.Text = "$ " + decimal.Parse(TotalSolicitado).ToString("N");

        }

        protected void Resultado_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            
        }

        protected void btnRegresar_Click(object sender, EventArgs e)
        {
            Resultado.Visible = true;
            lblTitulo.Visible = true;

            lblTitulo2.Visible = false;
            lblNombre.Visible = false;
            btnRegresar.Visible = false;
            lblFechaInicio.Visible = false;
            lblFechaInicio1.Visible = false;
            lblSolicitud.Visible = false;
            lblFechaFin.Visible = false;
            lblFechaFin1.Visible = false;
            lblDestino.Visible = false;
            lblDestino1.Visible = false;
            lblPeriodo.Visible = false;
            lblPeriodo1.Visible = false;
            lblTipoViaje.Visible = false;
            lblTipoViaje1.Visible = false;
            lblGastoVuelo.Visible = false;
            lblGastoVuelo1.Visible = false;
            lblGastoHospedaje.Visible = false;
            lblGastoHospedaje1.Visible = false;
            lblGastoTerrestre.Visible = false;
            lblGastoTerrestre1.Visible = false;
            lblGastoComida.Visible = false;
            lblGastoComida1.Visible = false;
            lblGastoRenta.Visible = false;
            lblGastoRenta1.Visible = false;
            lblGastosOtros.Visible = false;
            lblGastosOtros1.Visible = false;
            lblGastoTotal.Visible = false;
            lblGastoTotal1.Visible = false;
            btnAceptar.Visible = false;
            btnRechazar.Visible = false;
        }

        protected void btnAceptar_Click(object sender, EventArgs e)
        {
            GridViewRow row = Resultado.SelectedRow;
            int id = Convert.ToInt32(Resultado.DataKeys[row.RowIndex].Value);
            string script = "alert('Solicitud aceptada, enviada a autorizador 2')";
            conexion.Open();
            string query = "update TBL_Solicitud set Estatus = 2 where ID_Solicitud = " + id;
            SqlCommand upd = new SqlCommand(query, conexion);
            upd.ExecuteNonQuery();
            conexion.Close();
            ScriptManager.RegisterStartupScript(this, typeof(Page), "alerta", script, true);
            AgregarGridMe();
        }

        protected void btnRechazar_Click(object sender, EventArgs e)
        {
            GridViewRow row = Resultado.SelectedRow;
            int id = Convert.ToInt32(Resultado.DataKeys[row.RowIndex].Value);
            string script = "alert('Solicitud rechazada')";
            conexion.Open();
            string query = "update TBL_Solicitud set Estatus = 4 where ID_Solicitud = " + id;
            SqlCommand upd = new SqlCommand(query, conexion);
            upd.ExecuteNonQuery();
            conexion.Close();
            ScriptManager.RegisterStartupScript(this, typeof(Page), "alerta", script, true);
            AgregarGridMe();
        }
    }
    }

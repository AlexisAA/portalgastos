﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PortalGastos.UsuarioGerente
{
    public partial class GastosTrabajador : System.Web.UI.Page
    {
        SqlConnection conexion = new SqlConnection("Data source=(local); Initial Catalog=PortalGastos; User=sae; Password=sae ");
        protected void Page_Init(object sender, EventArgs e)
        {
            if (Session["user"] == null)
            {
                HttpContext.Current.Response.Redirect("~/Portal/Login.aspx");
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                AgregarGridMe();
            }
        }
        protected void AgregarGridMe()
        {
            string Sol_Re = "", Tot_ga = "", Pri_nom = "", Ap = "", Nom_Are = "",con1="";
            int emp = 0;
            List<string> lista = new List<string>();
            DataTable dt = new DataTable();
            dt.Columns.AddRange(new DataColumn[5] {new DataColumn("No. de Empleado",typeof(string)),
                                                    new DataColumn("Nombre",typeof(string)),
                                                    new DataColumn("Total Solicitado",typeof(string)),
                                                    new DataColumn("Solicitudes Realizadas",typeof(string)),
                                                    new DataColumn("Nombre de Área")});
            conexion.Open();
            string query = "select * from v_GastoEmpleado";
            SqlCommand select = new SqlCommand(query,conexion);
            SqlDataReader registro = select.ExecuteReader();
            while (registro.Read())
            {
                con1 = registro[0].ToString();
                lista.Add(con1);
                for (int i = 0; i < lista.Count; i++)
                {
                    emp = Convert.ToInt32(registro["ID_Usuario"]);
                    Sol_Re = Convert.ToString(registro["Solitidudes_realizadas"]);
                    Tot_ga = Convert.ToString(registro["Total_Gastado"]);
                    Pri_nom = Convert.ToString(registro["Primer_nombre"]);
                    Ap = Convert.ToString(registro["Apellido_paterno"]);
                    Nom_Are = Convert.ToString(registro["Nombre_Area"]);
                }
                dt.Rows.Add(emp, Pri_nom + " " + Ap, "$ " + decimal.Parse(Tot_ga).ToString("N") , Sol_Re + " Solicitudes", Nom_Are);
                Resultado.DataSource = dt;
                Resultado.DataBind();
            }
            conexion.Close();
        }

        protected void Resultado_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            AgregarGridMe();
            Resultado.PageIndex = e.NewPageIndex;
            Resultado.DataBind();
        }
    }
}
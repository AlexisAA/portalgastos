﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PaginaMuestra/Site1.Master" AutoEventWireup="true" CodeBehind="ModificarUsuario.aspx.cs" Inherits="PortalGastos.Administradordesistema.ModificarUsuario" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../Style/Style.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>
        <asp:Panel ID="pnlModificar" runat="server" CssClass="pnlGenerico  centrado5">
            <asp:Label ID="lblTitulo" runat="server" CssClass="lblTituloge" Text="MODIFICAR USUARIO"></asp:Label>
            <br />
            <hr />
            <table style="width: 100%;">
                <tr style="height: 20px;">
                    <td style="width: 100%;">
                    </td>
                </tr>
                <tr style="width:100%">
                    <td style="height:40px; float:left">
                        <br />
                         <asp:Label ID="Label1" runat="server" Text="El administrador de sistema puede bloquear y desbloquear a todos los usuarios" CssClass="lblDescripcion1"></asp:Label>
                    </td>
                </tr>
                <tr style="width:100%">
                    <td style="height:40px; float:left">
                        <asp:Label ID="info1" runat="server" Text="Botón bloquear todos, hace el bloqueo masivo de todos los empleados" CssClass="lblDescripcion"></asp:Label>
                        <asp:Label ID="lblDesc" runat="server" Text="Bloqueo, desbloqueo especifico" CssClass="lblDescripcion1" Visible="false"></asp:Label>
                        <br />
                        <asp:Label ID="info2" runat="server" Text="Botón desbloquar, hace el desbloqueo masivo de todos los empleados" CssClass="lblDescripcion"></asp:Label>
                        <asp:Label ID="Label2" runat="server" Text="Si deseas bloquear y desbloquear usuarios especificos, puedes filtrar por usuario " CssClass="lblDescripcion" Visible="false"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td style="width:25%; float:left;">
                       <asp:Button ID="BloquearT" runat="server" CssClass="cssBotonPanel" Text="Bloquear todos" OnClick="BloquearT_Click"/>
                        <asp:Button ID="btnBloquear" runat="server" CssClass="cssBotonPanel" Text="Bloquear" OnClick="btnBloquear_Click" Visible="false"/>
                    </td>
                    <td style="width:25%; float:left;">
                       <asp:Button ID="DesbloquearT" runat="server" CssClass="cssBotonPanel" Text="Desbloquear todos" OnClick="DesbloquearT_Click"/>
                        <asp:Button ID="btnDesbloquear" runat="server" CssClass="cssBotonPanel" Text="Desbloquear" OnClick="btnDesbloquear_Click" Visible="false"/>
                    </td>
                </tr>
                <tr style="width:100%">
                    <td>
                        <br /><br />
                            <hr />
                        <br /><br />
                    </td>                    
                </tr>
                <tr>
                    <td>
                        <asp:GridView ID="Resultado" runat="server" CssClass="mGrid1 pagination-ys data" PageSize="7" AllowPaging="true" OnPageIndexChanging="Resultado_PageIndexChanging" OnSelectedIndexChanged="Resultado_SelectedIndexChanged" DataKeyNames="ID Usuario">
                            <Columns>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:ImageButton ID="ImgVisualizar" runat="server" CommandName="Select" ImageUrl="~/Imagenes/vision.png"/>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </td>
                </tr>
                <tr style="height: 40px;">
                    <td style="width: 25%; float:left;">
                        <asp:Button ID="btnRegresar" runat="server" Text="Regresar" Visible="false" OnClick="btnRegresar_Click" />
                    </td>
                </tr>
                <tr style="height: 40px;">
                    <td style="width: 25%; float:left;">
                        <asp:Label ID="lblUsuario" runat="server" CssClass="lblEtique" Text="Usuario: " Visible="false"></asp:Label>
                    </td>
                    <td style="width:25%; float:left;">
                        <asp:TextBox ID="txtUsuario" runat="server" CssClass="txt" Visible="false" Enabled="false"></asp:TextBox>
                    </td>
                    <td style="width:25%; float:left;">
                        <asp:Button ID="btnBuscar" runat="server" CssClass="cssBotonPanel" Text="BUSCAR" OnClick="btnBuscar_Click" Visible="false"/>
                    </td>
                </tr>
                <tr style="height: 40px;">
                    <td style="width: 25%; float:left;">
                    </td>
                    <td style="width: 25%; float:left;">
                        <asp:Label ID="lblEstatus" runat="server" Text="Estatus: " CssClass="lblNormalRojo" Visible="false"></asp:Label>
                        <asp:Label ID="lblEstatusRes" runat="server" CssClass="lblNormalRojo" Visible="false"></asp:Label>
                    </td>
                </tr>
                <tr style="height: 40px;">
                    <td style="width: 25%; float:left;">
                        <asp:Label ID="lblNombre1" runat="server" CssClass="lblEtique" Text="Primer Nombre: " Visible="false"></asp:Label>
                    </td>
                    <td style="width: 25%; float:left;">
                        <asp:TextBox ID="txtNombre1" runat="server" CssClass="txt" Visible="false"></asp:TextBox>
                    </td>
                </tr>
                <tr style="height: 40px;">
                    <td style="width: 25%; float:left;">
                        <asp:Label ID="lblNombre2" runat="server" CssClass="lblEtique" Text="Segundo Nombre: " Visible="false"></asp:Label>
                    </td>
                    <td style="width: 25%; float:left;">
                        <asp:TextBox ID="txtNombre2" runat="server" CssClass="txt" Visible="false"></asp:TextBox>
                    </td>
                </tr>
                <tr style="height: 40px;">
                    <td style="width: 25%; float:left;">
                        <asp:Label ID="lblAp" runat="server" CssClass="lblEtique" Text="Apellido paterno: " Visible="false"></asp:Label>
                    </td>
                    <td style="width: 25%; float:left;">
                        <asp:TextBox ID="txtAp" runat="server" CssClass="txt" Visible="false"></asp:TextBox>
                    </td>
                </tr>
                <tr style="height: 40px;">
                    <td style="width: 25%; float:left;">
                        <asp:Label ID="lblAm" runat="server" CssClass="lblEtique" Text="Apellido materno: " Visible="false"></asp:Label>
                    </td>
                    <td style="width: 25%; float:left;">
                        <asp:TextBox ID="txtAm" runat="server" CssClass="txt" Visible="false"></asp:TextBox>
                    </td>
                    <td style="width: 25%; float:left;">
                        <asp:Button ID="btnModificar" runat="server" CssClass="cssBotonPanel" Text="MODIFICAR" OnClick="btnModificar_Click" Visible="false" />
                    </td>
                </tr>
                <tr style="height: 40px;">
                    <td style="width: 25%; float:left;">
                        <asp:Label ID="lblUsuario1" runat="server" CssClass="lblEtique" Text="ID Usuario: " Visible="false"></asp:Label>
                    </td>
                    <td style="width: 25%; float:left;">
                        <asp:TextBox ID="txtUsuario1" runat="server" CssClass="txt" Visible="false" Enabled="false"></asp:TextBox>
                    </td>
                </tr>
                <tr style="height: 40px;">
                    <td style="width: 25%; float:left;">
                        <asp:Label ID="lblArea" runat="server" CssClass="lblEtique" Text="Área: " Visible="false"></asp:Label>
                    </td>
                    <td style="width: 25%; float:left;">
                        <asp:DropDownList ID="DrArea" runat="server" CssClass="drop" Visible="false" AutoPostBack="false"></asp:DropDownList>
                    </td>
                </tr>
                <tr style="height: 40px;">
                    <td style="width: 25%; float:left;">
                        <asp:Label ID="lblPuesto" runat="server" CssClass="lblEtique" Text="Puesto: " Visible="false"></asp:Label>
                    </td>
                    <td style="width: 25%; float:left;">
                        <asp:DropDownList ID="DrPuesto" runat="server" CssClass="drop" Visible="false" AutoPostBack="false"></asp:DropDownList>
                    </td>
                </tr>
                <tr style="height: 40px;">
                    <td style="width: 25%; float:left;">
                        <asp:Label ID="lblRol" runat="server" CssClass="lblEtique" Text="Rol: " Visible="false"></asp:Label>
                    </td>
                    <td style="width: 25%; float:left;">
                        <asp:DropDownList ID="drRol" runat="server" CssClass="drop" Visible="false" AutoPostBack="false"></asp:DropDownList>
                    </td>
                </tr>
                <tr style="height: 40px;">
                    <td style="width: 25%; float:left;">
                        <asp:Label ID="lblAutorizador1" runat="server" CssClass="lblEtique" Visible="false" Text="Autorizador 1: "></asp:Label>
                    </td>
                    <td style="width: 25%; float:left;">
                        <asp:DropDownList ID="DrAutorizador1" runat="server" CssClass="drop" Visible="false" AutoPostBack="false"></asp:DropDownList>
                    </td>
                </tr>
                <tr style="height: 40px;">
                    <td style="width: 25%; float:left;">
                        <asp:Label ID="lblAutorizador2" runat="server" CssClass="lblEtique" Visible="false" Text="Autorizador 2: "></asp:Label>
                    </td>
                    <td style="width: 25%; float:left;">
                        <asp:DropDownList ID="DrAutorizador2" runat="server" CssClass="drop" Visible="false" OnSelectedIndexChanged="DrAutorizador2_SelectedIndexChanged" AutoPostBack="false"></asp:DropDownList>
                    </td>
                </tr>
            </table>
        </asp:Panel>
    </div>
</asp:Content>

﻿using PortalGastos.PaginaMuestra;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PortalGastos.Administradordesistema
{
    public partial class AdminSistema : System.Web.UI.Page
    {
        SqlConnection conexion = new SqlConnection("Data source=(local); Initial Catalog=PortalGastos; User=sae; Password=sae ");

        protected void Page_Init(object sender, EventArgs e)
        {
            if (Session["user"] == null)
            {
                HttpContext.Current.Response.Redirect("~/Portal/Login.aspx");
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            string nombre = Session["user"].ToString();
            lblUser.Text = "Bienvenido " + nombre; 
        }

        protected void btnCerrar_Click(object sender, EventArgs e)
        {
            FormsAuthentication.SignOut();
            Session.Clear();
            Session.Abandon();
            Response.Redirect("~/Portal/Login.aspx");
        }

        protected void btnCrear_Click(object sender, EventArgs e)
        {
            HttpContext.Current.Response.Redirect("~/Administradordesistema/CrearUsuario.aspx");
        }

        protected void btnBloquear_Click(object sender, EventArgs e)
        {
            HttpContext.Current.Response.Redirect("../Administradordesistema/BloquearUsuario.aspx");
        }

        protected void btnModificar_Click(object sender, EventArgs e)
        {
            HttpContext.Current.Response.Redirect("../Administradordesistema/ModificarUsuario.aspx");
        }

        protected void btnCambiar_Click(object sender, EventArgs e)
        {
            HttpContext.Current.Response.Redirect("../Portal/CambioContraseña.aspx");
        }

    }
}
﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PaginaMuestra/Site1.Master" AutoEventWireup="true" CodeBehind="CrearUsuario.aspx.cs" Inherits="PortalGastos.Administradordesistema.CrearUsuario" %>
<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Web.UI" tagprefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../Style/Style.css" rel="stylesheet" />
    <script src="../Style/jquery-3.5.1.min.js"></script>
    <script>
        function justNumbers(e) {
            var keynum = window.event ? window.event.keyCode : e.which;
            if ((keynum == 8 || keynum == 48))
                return true;
            if (keynum <= 47 || keynum >= 58) return false;
            return /\d/.test(String.fromCharCode(keynum));

        }
        function soloLetras(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return true;

            return false;
        }
        $(document).ready(function () {
            $('.solo-telefono').keyup(function () {
                this.value = (this.value + '').replace(/[^0-9]/g, '').replace(/(\d{3})(\d{3})(\d{4})/, '$1-$2-$3');
                if ($(this).val().length > 20) {
                    this.value = this.value.substring(0, 20);
                }
            });
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>
        <asp:Panel ID="pnlUsuario" runat="server" CssClass="pnlGenerico centrado4">
            <asp:Label ID="Titulo" runat="server" Text="CREAR USUARIO" CssClass="lblTituloge"></asp:Label>
            <br />
            <hr />
            <table style="width: 100%;">
                <tr style="height: 20px;">
                    <td style="width: 100%;">
                    </td>
                </tr>
                <tr style="height: 40px;">
                    <td style="width: 25%; float:left; ">
                        <asp:Label ID="EtiquetaNom" runat="server" Text="Primer Nombre:" CssClass="lblEtique"></asp:Label>
                    </td>
                    <td style="width: 25%; float:left;">
                        <asp:TextBox ID="txtNombre" runat="server" CssClass="txt" onkeypress="return soloLetras(event)" ></asp:TextBox>
                    </td>
                </tr>
                <tr style="height: 40px;">
                    <td style="width:25%; float:left;">
                        <asp:Label ID="EtiquetaSeg" runat="server" Text="Segundo Nombre:" CssClass="lblEtique"></asp:Label>
                    </td>
                    <td style="width:25%; float:left;">
                        <asp:TextBox ID="txtSegundo" runat="server" CssClass="txt" onkeypress="return soloLetras(event)"></asp:TextBox>
                        </td>
                        <td style="width: 25%" vertical-aling: middle;>
                        </td>
                        <td style="width: 25%" vertical-aling: middle;></td>
                </tr>
                <tr style="height: 40px;">
                    <td style="width:25%; float:left;">
                        <asp:Label ID="lblAp" runat="server" Text="Apellido Paterno:" CssClass="lblEtique"></asp:Label>
                    </td>
                    <td style="width:25%; float:left;">
                        <asp:TextBox ID="txtAp" runat="server" CssClass="txt" onkeypress="return soloLetras(event)"></asp:TextBox>
                    </td>
                    <td style="width: 25%" vertical-aling: middle;></td>
                    <td style="width: 25%" vertical-aling: middle;></td>
                </tr>
                <tr style="height: 40px;">
                    <td style="width:25%; float:left;">
                        <asp:Label ID="lblAm" runat="server" Text="Apellido Materno:" CssClass="lblEtique" ></asp:Label>
                    </td>
                    <td style="width:25%; float:left;">
                        <asp:TextBox ID="txtAm" runat="server" CssClass="txt" onkeypress="return soloLetras(event)"></asp:TextBox>
                    </td>
                    <td style="width: 25%" vertical-aling: middle;></td>
                    <td style="width: 25%" vertical-aling: middle;></td>
                </tr>
                <tr style="height: 40px;">
                    <td style="width:25%; float:left;">
                        <asp:Label ID="user" runat="server" Text="Usuario: " CssClass="lblEtique"></asp:Label>
                    </td>
                    <td style="width:25%; float:left;">
                        <asp:TextBox ID="txtUser" runat="server" CssClass="txt"></asp:TextBox>
                    </td>
                    <td style="width: 25%" vertical-aling: middle;></td>
                    <td style="width: 25%" vertical-aling: middle;></td>
                </tr>
                <tr  style="height: 40px">
                    <td style="width: 25%; float:left;"">
                        <asp:Label ID="Area1" runat="server" Text="Área: " CssClass="lblEtique"></asp:Label>
                    </td>
                    <td style="width:25%; float:left;">
                        <asp:DropDownList ID="Area" runat="server" AutoPostBack="false" OnSelectedIndexChanged="SeleccionarArea" CssClass="drop">
                        </asp:DropDownList>
                    </td>
                    <td style="width: 25%" vertical-aling: middle;></td>
                </tr>
                <tr style="height: 40px">
                    <td style="width:25%; float:left;">
                        <asp:Label ID="lblPuesto" runat="server" Text="Puesto: " CssClass="lblEtique"></asp:Label>
                    </td>
                    <td style="width:25%; float:left;">
                        <asp:DropDownList ID="Puesto1" runat="server" AutoPostBack="false" OnSelectedIndexChanged="SeleccionarArea" CssClass="drop">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr style="height: 40px">
                    <td style="width: 25%; float:left;"">
                        <asp:Label ID="Rol1" runat="server" Text="Rol: " CssClass="lblEtique"></asp:Label>
                    </td>
                    <td style="width:25%; float:left;">
                        <asp:DropDownList ID="Rol" runat="server" AutoPostBack="false" CssClass="drop"></asp:DropDownList>
                    </td>
                </tr>
                <tr style="height:40px">
                    <td style="width: 25%; float:left;"">
                        <asp:Label ID="Auto1" runat="server" Text="Autorizador 1: " CssClass="lblEtique"></asp:Label>
                    </td>
                    <td style="width:25%; float:left;">
                        <asp:DropDownList ID="Autorizador1" runat="server" AutoPostBack="false" CssClass="drop"></asp:DropDownList>
                    </td>
                </tr>
                <tr style="height: 40px">
                    <td style="width: 25%; float:left;">
                        <asp:Label ID="Auto2" runat="server" Text="Autorizador 2: " CssClass="lblEtique"></asp:Label>
                    </td>
                    <td style="width:25%; float:left;">
                        <asp:DropDownList ID="Autorizador2" runat="server" AutoPostBack="false" CssClass="drop"></asp:DropDownList>
                    </td>
                </tr>
                <tr style="height: 40px">
                    <td style="width: 25%; float:left;">
                        <asp:Label ID="lblCorreo" runat="server" CssClass="lblEtique" Text="Correo"></asp:Label>
                    </td>
                    <td style="width: 25%; float:left;">
                        <asp:TextBox ID="txtCorreo" runat="server" CssClass="txt" placeHolder="@correo"></asp:TextBox>
                    </td>
                </tr>
                <tr style="height: 40px">
                    <td style="width: 25%; float:left;">
                        <asp:Label ID="lbltelefono" runat="server" CssClass="lblEtique" Text="Teléfono:"> </asp:Label>
                    </td>
                    <td style="width: 25%; float:left;">
                        <asp:TextBox ID="txtTelefono" runat="server" CssClass="txt solo-telefono" Type="tel"></asp:TextBox>
                    </td>
                </tr>
                <tr style="height: 40px">
                    <%--<td style="width: 25%;"></td>--%>
                    <td style="width: 25%; padding-left:338px;">
                        <asp:Button ID="Enviar" runat="server" CssClass="cssBotonPanel" Text="Enviar" OnClick="Enviar_Click"/>
                        </td>
                </tr>
            </table>
        </asp:Panel>
    </div>
</asp:Content>

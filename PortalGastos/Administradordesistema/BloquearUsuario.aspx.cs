﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PortalGastos.Administradordesistema
{
    public partial class BloquearUsuario : System.Web.UI.Page
    {
        SqlConnection conexion = new SqlConnection("Data source=(local); Initial Catalog=PortalGastos; User=sae; Password=sae ");
        protected void Page_Init(object sender, EventArgs e) {
            if (Session["user"] == null)
            {
                HttpContext.Current.Response.Redirect("~/Portal/Login.aspx");
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                AgregarGridMe();
            }
        }
        protected void AgregarGridMe()
        {
            string ID_usuario = "", Usuario = "", Primer_Nom = "", Segundo_Nom = "", Apellido_Pa = "", Apellido_Ma = "",con1 = "";
            bool Bloqueado = false;
            List<string> Lista = new List<string>();
            DataTable dt = new DataTable();
            dt.Columns.AddRange(new DataColumn[7]{new DataColumn("ID Usuario",typeof(string)),
                                                  new DataColumn("Estatus",typeof(string)),
                                                  new DataColumn("Usuario",typeof(string)),
                                                  new DataColumn("Primer nombre",typeof(string)),
                                                  new DataColumn("Segundo nombre",typeof(string)),
                                                  new DataColumn("Apellido paterno",typeof(string)),
                                                  new DataColumn("Apellido materno",typeof(string))});
            conexion.Open();
            string query = "select * from v_Usuarios";
            SqlCommand select = new SqlCommand(query,conexion);
            SqlDataReader registros = select.ExecuteReader();
            while (registros.Read())
            {
                con1 = registros[0].ToString();
                Lista.Add(con1);
                for (int i = 0; i < Lista.Count; i++)
                {
                    ID_usuario = Convert.ToString(registros["ID_Usuario"]);
                    Usuario = Convert.ToString(registros["Usuario"]);
                    Bloqueado = Convert.ToBoolean(registros["bloqueado"]);
                    Primer_Nom = Convert.ToString(registros["Primer_nombre"]);
                    Segundo_Nom = Convert.ToString(registros["Segundo_nombre"]);
                    Apellido_Ma = Convert.ToString(registros["Apellido_materno"]);
                    Apellido_Pa = Convert.ToString(registros["Apellido_paterno"]);
                }
                string Estatus = "";
                if (Bloqueado!=true)
                {
                    Estatus = "Usuario activo";
                }
                else
                {
                    Estatus = "Usuario bloqueado";
                }
                dt.Rows.Add(ID_usuario,Estatus,Usuario,Primer_Nom,Segundo_Nom,Apellido_Ma,Apellido_Pa);
                Resultado.DataSource = dt;
                Resultado.DataBind();
            }
            conexion.Close();
        }
        protected void MetodoBuscar()
        {
            string ID_Usuario = "", Usuario = "", Primer_Nom = "", Segundo_Nom = "", Apellido_Pa = "", Apellido_Ma = "",Area = "";
            bool bloqueado = false;
            GridViewRow row = Resultado.SelectedRow;
            int ID = Convert.ToInt32(Resultado.DataKeys[row.RowIndex].Values[0]);
            conexion.Open();
            string query = "select * from v_Usuarios where ID_Usuario = "+ID;
            SqlCommand select = new SqlCommand(query, conexion);
            SqlDataReader registros = select.ExecuteReader();
            if (registros.Read())
            {
                ID_Usuario = Convert.ToString(registros["ID_Usuario"]);
                Usuario = Convert.ToString(registros["Usuario"]);
                bloqueado = Convert.ToBoolean(registros["bloqueado"]);
                Primer_Nom = Convert.ToString(registros["Primer_nombre"]);
                Segundo_Nom = Convert.ToString(registros["Segundo_nombre"]);
                Apellido_Pa = Convert.ToString(registros["Apellido_paterno"]);
                Apellido_Ma = Convert.ToString(registros["Apellido_materno"]);
                Area = Convert.ToString(registros["Nombre_Area"]);
            }
            conexion.Close();
            string Estatus = "";
            if (bloqueado!=true)
            {
                Estatus = "Usuario Activo";
            }
            else
            {
                Estatus = "Usuario Bloqueado";
            }
            txtUsuario.Text = Usuario;
            lblPrimerNom1.Text = Primer_Nom;
            lblSegundoNom1.Text = Segundo_Nom;
            lblApellidoP1.Text = Apellido_Pa;
            lblApellidoM1.Text = Apellido_Ma;
            lblUsuario2.Text = ID_Usuario;
            lblArea1.Text = Area;
            lblEstatus1.Text = Estatus;
        }
        protected void BloquearMe()
        {
            string usuariolbl = Convert.ToString(lblUsuario2.Text);
            conexion.Open();
            string query = "update TBL_Usuarios set bloqueado = 1 where ID_Usuario = '"+usuariolbl+"'";
            SqlCommand blo = new SqlCommand(query, conexion);
            blo.ExecuteNonQuery();
            conexion.Close();
        }
        protected void DesbloquearMe()
        {
            string usuariolbl = Convert.ToString(lblUsuario2.Text);
            conexion.Open();
            string query = "update TBL_Usuarios set bloqueado = 0 where ID_Usuario = '" + usuariolbl + "'";
            SqlCommand deblo = new SqlCommand(query, conexion);
            deblo.ExecuteNonQuery();
            conexion.Close();
        }
        protected void BloquearTodo()
        {
            conexion.Open();
            string query = "update TBL_Usuarios set bloqueado = 1";
            SqlCommand bloq = new SqlCommand(query, conexion);
            bloq.ExecuteNonQuery();
            conexion.Close();
            AgregarGridMe();
        }
        protected void DesbloquearTodo()
        {
            conexion.Open();
            string query = "update TBL_Usuarios set bloqueado = 0";
            SqlCommand bloq = new SqlCommand(query, conexion);
            bloq.ExecuteNonQuery();
            conexion.Close();
            AgregarGridMe();
        }

        protected void Buscar_Click(object sender, EventArgs e)
        {
            MetodoBuscar();
            InsertLog("btnBuscar", "Bloquear Usuario", "OK", "Usuario encontrado", true);
        }

        protected void btnBloquear_Click(object sender, EventArgs e)
        {
            BloquearMe();
            MetodoBuscar();
            InsertLog("btnBloquear", "Bloquear Usuario", "OK", "Usuario Bloqueado", true);
        }
        protected void InsertLog(string _evento, string _metodo, string _respuesta, string _descripcion, bool _esSistema)
        {
            int idUsuario = 0;
            int _esSistema_ = Convert.ToInt32(_esSistema);
            String dtAhora1 = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            conexion.Open();
            string Insert = "insert into TBL_Logs (Formulario, Evento, Metodo, Fecha, Respuesta, Descripcion, EsSistema, ID_Usuario) values ('Login','" + _evento + "','" + _metodo + "'," + "Convert (Datetime,'" + dtAhora1 + "',120),'" + _respuesta + "','" + _descripcion + "'," + _esSistema_ + "," + idUsuario + ")";
            SqlCommand insertL = new SqlCommand(Insert, conexion);
            insertL.ExecuteNonQuery();
            conexion.Close();
        }

        protected void btnDesbloquear_Click(object sender, EventArgs e)
        {
            DesbloquearMe();
            MetodoBuscar();
            InsertLog("btnDesbloquear", "Bloquear Usuario", "OK", "Usuario Desbloqueado", true);
        }

        protected void BloquearT_Click(object sender, EventArgs e)
        {
            BloquearTodo();
            lblEstatus1.Text = "Usuarios Bloqueados";
            InsertLog("btnBloquearTodo", "Bloquear Usuario", "OK", "Usuarios Bloqueados", true);
        }

        protected void DesbloquearT_Click(object sender, EventArgs e)
        {
            DesbloquearTodo();
            lblEstatus1.Text = "Usuarios Desbloqueados";
            InsertLog("btnDesbloquearTodo", "Desbloquear Usuario", "OK", "Usuarios Desbloqueados", true);
        }

        protected void Resultado_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            AgregarGridMe();
            Resultado.PageIndex = e.NewPageIndex;
            Resultado.DataBind();
        }

        protected void Resultado_SelectedIndexChanged(object sender, EventArgs e)
        {
            lblDescrip.Visible = false;
            info2.Visible = false;
            info1.Visible = false;
            DesbloquearT.Visible = false;
            BloquearT.Visible = false;
            Resultado.Visible = false;
            BloquearT.Visible = false;
            DesbloquearT.Visible = false;


            btnRegresar.Visible = true;
            txtUsuario.Visible = true;
            lblPrimerNom.Visible = true;
            lblPrimerNom1.Visible = true;
            lblSegundoNom.Visible = true;
            lblSegundoNom1.Visible = true;
            lblApellidoP.Visible = true;
            lblApellidoP1.Visible = true;
            lblApellidoM.Visible = true;
            lblApellidoM1.Visible = true;
            lblUsuario.Visible = true;
            lblUsuario1.Visible = true;
            lblUsuario2.Visible = true;
            lblArea.Visible = true;
            lblArea1.Visible = true;
            lblEstatus.Visible = true;
            lblEstatus1.Visible = true;
            btnBloquear.Visible = true;
            btnDesbloquear.Visible = true;
            MetodoBuscar();
        }

        protected void btnRegresar_Click(object sender, EventArgs e)
        {
            lblDescrip.Visible = true;
            info2.Visible = true;
            info1.Visible = true;
            DesbloquearT.Visible = true;
            BloquearT.Visible = true;
            Resultado.Visible = true;
            BloquearT.Visible = true;
            DesbloquearT.Visible = true;

            btnRegresar.Visible = false;
            txtUsuario.Visible = false;
            lblPrimerNom.Visible = false;
            lblPrimerNom1.Visible = false;
            lblSegundoNom.Visible = false;
            lblSegundoNom1.Visible = false;
            lblApellidoP.Visible = false;
            lblApellidoP1.Visible = false;
            lblApellidoM.Visible = false;
            lblApellidoM1.Visible = false;
            lblUsuario.Visible = false;
            lblUsuario1.Visible = false;
            lblUsuario2.Visible = false;
            lblArea.Visible = false;
            lblArea1.Visible = false;
            lblEstatus.Visible = false;
            lblEstatus1.Visible = false;
            btnBloquear.Visible = false;
            btnDesbloquear.Visible = false;
            AgregarGridMe();
        }
    }
}
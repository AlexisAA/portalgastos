﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PortalGastos.Administradordesistema
{
    public partial class CrearUsuario : System.Web.UI.Page
    {
        SqlConnection conexion = new SqlConnection("Data source=(local); Initial Catalog=PortalGastos; User=sae; Password=sae ");
        SqlConnection conexion1 = new SqlConnection("Data source=(local); Initial Catalog=PortalGastos; User=sae; Password=sae ");
        protected void Page_Init(object sender, EventArgs e)
        {
            if (Session["user"] == null)
            {
                HttpContext.Current.Response.Redirect("~/Portal/Login.aspx");
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            
            if (!IsPostBack)
            {
                AreaE();
                RolE();
                Autorizador();
                Puesto();
            }
        }
        private void AreaE()
        {
            Area.DataSource = Consultar("Select * from TBL_Area");
            Area.DataTextField = "Nombre_Area";
            Area.DataValueField = "ID_Area";
            Area.DataBind();
            Area.Items.Insert(0,new ListItem("Seleccionar..","0"));
        }
        private void RolE()
        {
            Rol.DataSource = Consultar("Select * from TBL_Roles");
            Rol.DataTextField = "Nombre_Rol";
            Rol.DataValueField = "ID_Rol";
            Rol.DataBind();
            Rol.Items.Insert(0,new ListItem("Seleccionar..","0"));
        }
        private void Autorizador()
        {
            Autorizador1.DataSource = Consultar("select Convert(char,ID_Autorizador)+'-'+Primer_nombre as datos from TBL_Autorizadores");
            Autorizador1.DataTextField = "Datos";
            Autorizador1.DataValueField = "Datos";
            Autorizador1.DataBind();
            Autorizador1.Items.Insert(0, new ListItem("Seleccionar..", "0"));
            //autorizador 2
            Autorizador2.DataSource = Consultar("select Convert(char,ID_Autorizador)+'-'+Primer_nombre as datos from TBL_Autorizadores");
            Autorizador2.DataTextField = "Datos";
            Autorizador2.DataValueField = "Datos";
            Autorizador2.DataBind();
            Autorizador2.Items.Insert(0, new ListItem("Seleccionar..", "0"));
        }
        private void Puesto()
        {
            Puesto1.DataSource = Consultar("Select * from TBL_TipoEmpleado");
            Puesto1.DataTextField = "Nombre";
            Puesto1.DataValueField = "ID_TipoEmpleado";
            Puesto1.DataBind();
            Puesto1.Items.Insert(0, new ListItem("Seleccionar..", "0"));
        }
        protected void SeleccionarArea(object sender, EventArgs e)
        {

        }
        public DataSet Consultar(string strSQL)
        {
            string conexion = "Data source=(local); Initial Catalog=PortalGastos; User=sae; Password=sae";
            SqlConnection con = new SqlConnection(conexion);
            con.Open();
            SqlCommand cmd = new SqlCommand(strSQL, con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            con.Close();
            return ds;
        }
        protected void Insert()
        {
            string pri_Nom = "", seg_Nom = "", Ap = "", Am = "", registros = "", correo = "",tel="",Usuariotxt="", Autorizador1G = "", Autorizador2G = "",roles="";
            int  AreaG = 0, RolG = 0, PuestoG = 0, co1=0;
            String dtAhora = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss");
            List<string> Lista = new List<string>();
            pri_Nom = Convert.ToString(txtNombre.Text);
            seg_Nom = Convert.ToString(txtSegundo.Text);
            Ap = Convert.ToString(txtAp.Text);
            Am = Convert.ToString(txtAm.Text);
            AreaG = Convert.ToInt32(Area.SelectedValue);
            RolG = Convert.ToInt32(Rol.SelectedValue);
            PuestoG = Convert.ToInt32(Puesto1.SelectedValue);
            Autorizador1G = Convert.ToString(Autorizador1.SelectedValue);
            Autorizador2G = Convert.ToString(Autorizador2.SelectedValue);
            string Autorizador1G1 = Autorizador1G.Substring(0,1);
            string Autorizador2G1 = Autorizador2G.Substring(0,1);
            correo = Convert.ToString(txtCorreo.Text);
            tel = Convert.ToString(txtTelefono.Text);
            Usuariotxt = Convert.ToString(txtUser.Text);
            conexion.Open();
            string InsertU = "insert into TBL_Empleados (Primer_nombre,Segundo_nombre,Apellido_paterno,Apellido_materno,Area) values ('"+pri_Nom+"','"+seg_Nom+"','"+Ap+"','"+Am+"',"+AreaG+")";
            SqlCommand InsertUs = new SqlCommand(InsertU,conexion);
            InsertUs.ExecuteNonQuery();
            conexion.Close();
            conexion.Open();
            string query = "select ID_Empleado from TBL_Empleados where Primer_nombre='"+pri_Nom+"' and Apellido_paterno='"+Ap+"'";
            SqlCommand command = new SqlCommand(query,conexion);
            SqlDataReader registro = command.ExecuteReader();
            while (registro.Read())
            {
                registros = registro[0].ToString();
                Lista.Add(registros);
                co1 = Int32.Parse(registros);
            }
            conexion.Close();
            conexion.Open();
            string InsertIE = "insert into TBL_Informacion_Empleados (Correo,Telefono,Fecha_ingreso,Empleado) values ('" + correo + "','" + tel + "',CONVERT(Datetime,'" + dtAhora + "',120)," + co1 + ")";
            SqlCommand InsertIE1 = new SqlCommand(InsertIE,conexion);
            InsertIE1.ExecuteNonQuery();
            conexion.Close();
            conexion.Open();
            string InsertUsu = "insert into TBL_Usuarios (Usuario,Fecha_alta,Fecha_ultimoIngreso,Intentos_ingresos,Intentos_permitidos,bloqueado,Rol,Empleado,contrasenia,Autorizador_nivel1,Autorizador_nivel2,Tipo_empleado) values ('"+Usuariotxt+ "',CONVERT(Datetime,'" + dtAhora+"',120),'',0,10,0,"+RolG+","+co1+",'123',"+Autorizador1G1+","+Autorizador2G1+","+PuestoG+")";
            SqlCommand Insert = new SqlCommand(InsertUsu, conexion);
            Insert.ExecuteNonQuery();
            conexion.Close();
            conexion.Open();
            string select = "select * from TBL_Usuarios where ID_Usuario = (select MAX(ID_Usuario) ID_Usuario from TBL_Usuarios)";
            SqlCommand maximo = new SqlCommand(select, conexion);
            SqlDataReader selectMax = maximo.ExecuteReader();
            if (selectMax.Read())
            {
                roles = Convert.ToString(selectMax["Rol"]);
            }
            conexion.Close();
            if (roles == "4" || roles == "5")
            {
                conexion.Open();
                string queryIn = "insert into TBL_Autorizadores (Primer_nombre,Segundo_nombre,Apellido_paterno,Apellido_materno,Area) values ('" + pri_Nom + "','" + seg_Nom + "','" + Ap + "','" + Am + "'," + AreaG + ")";
                SqlCommand inserQ = new SqlCommand(queryIn, conexion);
                inserQ.ExecuteNonQuery();
                conexion.Close();
            }
        }

        protected void Enviar_Click(object sender, EventArgs e)
        {
            string pri_Nom = "", seg_Nom = "", Ap = "", Am = "", registros = "", correo = "", tel = "", Usuariotxt = "", Autorizador1G = "", Autorizador2G = "";
            int AreaG = 0, RolG = 0, PuestoG = 0, co1 = 0;
            pri_Nom = Convert.ToString(txtNombre.Text);
            seg_Nom = Convert.ToString(txtSegundo.Text);
            Ap = Convert.ToString(txtAp.Text);
            Am = Convert.ToString(txtAm.Text);
            AreaG = Convert.ToInt32(Area.SelectedValue);
            RolG = Convert.ToInt32(Rol.SelectedValue);
            PuestoG = Convert.ToInt32(Puesto1.SelectedValue);
            Autorizador1G = Convert.ToString(Autorizador1.SelectedValue);
            Autorizador2G = Convert.ToString(Autorizador2.SelectedValue);
            correo = Convert.ToString(txtCorreo.Text);
            tel = Convert.ToString(txtTelefono.Text);
            Usuariotxt = Convert.ToString(txtUser.Text);
            string script = "alert('Usuario ingresado correctamente')";
            string script1 = "alert('Verificar datos')";
            if (pri_Nom.Length!=0 && Ap.Length!=0 && Am.Length!=0 && AreaG!=0 && RolG!=0 && PuestoG!=0 && Autorizador1G.Length!=0 && Autorizador2G.Length!=0 && correo.Length!=0 && tel.Length!=0 && Usuariotxt.Length!=0 )
            {
                Insert();
                Enviar_Correo();
                txtNombre.Text = "";
                txtSegundo.Text = "";
                txtAp.Text = "";
                txtAm.Text = "";
                txtUser.Text = "";
                txtTelefono.Text = "";
                txtCorreo.Text = "";
                AreaE();
                RolE();
                Autorizador();
                Puesto();
                InsertLog("btnEnviar", "Crear Usuario", "OK", "Usuario dado de alta correctamente", true);
                ScriptManager.RegisterStartupScript(this, typeof(Page), "alerta", script, true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, typeof(Page), "alerta", script1, true);
            }
            
        }
        protected void Enviar_Correo()
        {
            string usuario = Convert.ToString(txtUser.Text);
            string correo = Convert.ToString(txtCorreo.Text);
            string Nom = Convert.ToString(txtNombre.Text);
            string Ap = Convert.ToString(txtAp.Text);
            //Mensaje
            string mensaje = "";
            mensaje = "<html style='font-size: 8px; font-family: Helvetica, Arial; margin: 0; padding: 0; text-align: center; vertical-align: top; background: #eeeeee;'>";
            mensaje += "<head><title></title><meta charset='utf-8' /></head>";
            mensaje += "<body style='width: 100%; height: 100%; margin: 0; padding: 0; text-align: left; vertical-align: top; margin-left: 10px;'>";
            mensaje += "<p style='font-size: 24px; font-weight: bold; text-transform: uppercase; color: #000000;'>Hola, "+ Nom +" "+ Ap +"</p>";
            mensaje += "<div><p style='font-size: 16px; color: #000000;'>";
            mensaje += "Por medio de la presente permitame compartirle su Usuario: "+usuario+" y su contraseña que deberá cambiar lo antes posible Contraseña: 123";
            mensaje += "<div><p style='font-size: 16px; color: #000000;'>";
            mensaje += "Para poder acceder al sistema ingresar: www.portalGastos.com";
            mensaje += "<A HREF='www.portalGastos.com' TARGET='_new'></A>";
            mensaje += "</p></div>";
            mensaje += "<div><p style='font-size: 16px; color: #000000;'>";
            mensaje += "Atentamente: Portal de Gastos.";
            mensaje += "</p></div>";
            mensaje += "</body></html>";

            //Imagen
            string imgCorreo = string.Empty;
            imgCorreo = ConfigurationManager.AppSettings["imgCorreo"].ToString();
            AlternateView htmlView = AlternateView.CreateAlternateViewFromString(mensaje, Encoding.UTF8, MediaTypeNames.Text.Html);
            LinkedResource img = new LinkedResource(imgCorreo, MediaTypeNames.Image.Jpeg);
            img.ContentId = "imagen";
            htmlView.LinkedResources.Add(img);

            string strServer = ConfigurationManager.AppSettings["mailServer"].ToString();
            string strRemitente = ConfigurationManager.AppSettings["mailRemitente"].ToString();
            string strCC = ConfigurationManager.AppSettings["mailCC"].ToString();
            string strContrasenia = ConfigurationManager.AppSettings["mailContrasenia"].ToString();
            int strPuerto = Convert.ToInt32(ConfigurationManager.AppSettings["mailPuerto"].ToString());
            bool ssl = Convert.ToBoolean(ConfigurationManager.AppSettings["mailSSL"].ToString());

            SmtpClient datosSmtp = new SmtpClient();
            datosSmtp.Credentials = new System.Net.NetworkCredential(strRemitente,strContrasenia);
            datosSmtp.Host = strServer;
            datosSmtp.Port = strPuerto;
            datosSmtp.EnableSsl = ssl;

            MailMessage objetoMail = new MailMessage();
            MailAddress mailRemitente = new MailAddress(strRemitente);
            objetoMail.From = mailRemitente;
            objetoMail.AlternateViews.Add(htmlView);

            MailAddress mailDestinatario = new MailAddress(correo);
            objetoMail.To.Add(mailDestinatario);
            objetoMail.Subject = "Portal de Gastos, Usuario y contraseña";

            try
            {
                datosSmtp.Send(objetoMail);
                datosSmtp.Dispose();
                InsertLog("btnEnviar", "Crear Usuario", "OK", "Correo enviado", true);
            }
            catch (Exception ex)
            {
                InsertLog("btnEnviar", "Crear Usuario", "OK", ex.Message , true);
            }
           
        }
        protected void InsertLog(string _evento, string _metodo, string _respuesta, string _descripcion, bool _esSistema)
        {
            int idUsuario = 0;
            int _esSistema_ = Convert.ToInt32(_esSistema);
            String dtAhora1 = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss");
            conexion.Close();
            conexion1.Close();
            conexion1.Open();
            string Insert = "insert into TBL_Logs (Formulario, Evento, Metodo, Fecha, Respuesta, Descripcion, EsSistema, ID_Usuario) values ('Login','" + _evento + "','" + _metodo + "'," + "CONVERT(Datetime,'"+dtAhora1+"', 120),'" + _respuesta + "','" + _descripcion + "'," + _esSistema_ + "," + idUsuario + ")";
            SqlCommand insertL = new SqlCommand(Insert, conexion1);
            insertL.ExecuteNonQuery();
            conexion1.Close();
        }

    }
}
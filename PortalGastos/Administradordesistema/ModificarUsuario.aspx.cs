﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PortalGastos.Administradordesistema
{
    public partial class ModificarUsuario : System.Web.UI.Page
    {
        SqlConnection conexion = new SqlConnection("Data source=(local); Initial Catalog=PortalGastos; User=sae; Password=sae ");
        protected void Page_Init(object sender, EventArgs e)
        {
            if (Session["user"] == null)
            {
                HttpContext.Current.Response.Redirect("~/Portal/Login.aspx");
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                AgregarGridMe();
                AreaE();
                Puesto();
                RolE();
                Autorizador();
            }
        }
        protected void AgregarGridMe()
        {
            string ID_usuario = "", Usuario = "", Primer_Nom = "", Segundo_Nom = "", Apellido_Pa = "", Apellido_Ma = "", con1 = "";
            bool Bloqueado = false;
            List<string> Lista = new List<string>();
            DataTable dt = new DataTable();
            dt.Columns.AddRange(new DataColumn[7]{new DataColumn("ID Usuario",typeof(string)),
                                                  new DataColumn("Estatus",typeof(string)),
                                                  new DataColumn("Usuario",typeof(string)),
                                                  new DataColumn("Primer nombre",typeof(string)),
                                                  new DataColumn("Segundo nombre",typeof(string)),
                                                  new DataColumn("Apellido paterno",typeof(string)),
                                                  new DataColumn("Apellido materno",typeof(string))});
            conexion.Open();
            string query = "select * from v_Usuarios";
            SqlCommand select = new SqlCommand(query, conexion);
            SqlDataReader registros = select.ExecuteReader();
            while (registros.Read())
            {
                con1 = registros[0].ToString();
                Lista.Add(con1);
                for (int i = 0; i < Lista.Count; i++)
                {
                    ID_usuario = Convert.ToString(registros["ID_Usuario"]);
                    Usuario = Convert.ToString(registros["Usuario"]);
                    Bloqueado = Convert.ToBoolean(registros["bloqueado"]);
                    Primer_Nom = Convert.ToString(registros["Primer_nombre"]);
                    Segundo_Nom = Convert.ToString(registros["Segundo_nombre"]);
                    Apellido_Ma = Convert.ToString(registros["Apellido_materno"]);
                    Apellido_Pa = Convert.ToString(registros["Apellido_paterno"]);
                }
                string Estatus = "";
                if (Bloqueado != true)
                {
                    Estatus = "Usuario activo";
                }
                else
                {
                    Estatus = "Usuario bloqueado";
                }
                dt.Rows.Add(ID_usuario, Estatus, Usuario, Primer_Nom, Segundo_Nom, Apellido_Ma, Apellido_Pa);
                Resultado.DataSource = dt;
                Resultado.DataBind();
            }
            conexion.Close();
        }
        protected void BuscarMe()
        {
            GridViewRow row = Resultado.SelectedRow;
            int ID = Convert.ToInt32(Resultado.DataKeys[row.RowIndex].Values[0]);
            string Usuario = "", Estatus = "", Primer_nom = "", Segundo_nom = "", Apellido_Pa = "", Apellido_Ma = "", ID_Usuario = "";
            string Area = "", Puesto = "", Rol = "", Autorizador1 = "", Autorizador2 = "";
            bool bloqueado = false;
            conexion.Open();
            string query = "select * from v_ModificacionUsuario where ID_Usuario = "+ID;
            SqlCommand select = new SqlCommand(query, conexion);
            SqlDataReader registros = select.ExecuteReader();
            if (registros.Read())
            {
                Usuario = Convert.ToString(registros["Usuario"]);
                ID_Usuario = Convert.ToString(registros["ID_Usuario"]);
                bloqueado = Convert.ToBoolean(registros["bloqueado"]);
                Primer_nom = Convert.ToString(registros["Primer_nombre"]);
                Segundo_nom = Convert.ToString(registros["Segundo_nombre"]);
                Apellido_Pa = Convert.ToString(registros["Apellido_paterno"]);
                Apellido_Ma = Convert.ToString(registros["Apellido_materno"]);
                Area = Convert.ToString(registros["Area"]);
                Puesto = Convert.ToString(registros["Tipo_empleado"]);
                Rol = Convert.ToString(registros["Rol"]);
                Autorizador1 = Convert.ToString(registros["Autorizador_nivel1"]);
                Autorizador2 = Convert.ToString(registros["Autorizador_nivel2"]);
            }
            conexion.Close();
            if (bloqueado == true)
            {
                Estatus = "Usuario bloqueado";
                txtUsuario.Enabled = false;
                lblEstatusRes.Enabled = false;
                txtUsuario1.Enabled = false;
                txtNombre1.Enabled = false;
                txtNombre2.Enabled = false;
                txtAp.Enabled = false;
                txtAm.Enabled = false;
                DrArea.Enabled = false;
                DrPuesto.Enabled = false;
                drRol.Enabled = false;
                DrAutorizador1.Enabled = false;
                DrAutorizador2.Enabled = false;
            }
            else
            {
                Estatus = "Usuario activo";
            }
            conexion.Open();
            string query1 = "select Convert(char,ID_Autorizador)+'-'+Primer_nombre as datos from TBL_Autorizadores where ID_Autorizador="+Autorizador1;
            SqlCommand select1 = new SqlCommand(query1, conexion);
            SqlDataReader registros1 = select1.ExecuteReader();
            if (registros1.Read())
            {
                Autorizador1 = Convert.ToString(registros1["datos"]);
            }
            conexion.Close();
            conexion.Open();
            string query2 = "select Convert(char,ID_Autorizador)+'-'+Primer_nombre as datos from TBL_Autorizadores where ID_Autorizador = "+Autorizador2;
            SqlCommand select2 = new SqlCommand(query2,conexion);
            SqlDataReader registros2 = select2.ExecuteReader();
            if (registros2.Read())
            {
                Autorizador2 = Convert.ToString(registros2["datos"]);
            }
            conexion.Close();
            txtUsuario.Text = Usuario;
            lblEstatusRes.Text = Estatus;
            txtUsuario1.Text = ID_Usuario;
            txtNombre1.Text = Primer_nom;
            txtNombre2.Text = Segundo_nom;
            txtAp.Text = Apellido_Pa;
            txtAm.Text = Apellido_Ma;
            DrArea.SelectedValue = Area;
            DrPuesto.SelectedValue = Puesto;
            drRol.SelectedValue = Rol;
            DrAutorizador1.SelectedValue = Autorizador1;
            DrAutorizador2.SelectedValue = Autorizador2;

        }
        protected void ModificarMe()
        {
            string script = "alert('Usuario actulizado.')";
            GridViewRow row = Resultado.SelectedRow;
            int ID = Convert.ToInt32(Resultado.DataKeys[row.RowIndex].Values[0]);
            string Usuario = "", Primer_nom = "", Segundo_nom = "", Apellido_Pa = "", Apellido_Ma = "";
            string Area = "", Puesto = "", Rol = "", Autorizador1 = "", Autorizador2 = "", Empleado = "";
            Usuario = Convert.ToString(txtUsuario.Text);
            Primer_nom = Convert.ToString(txtNombre1.Text);
            Segundo_nom = Convert.ToString(txtNombre2.Text);
            Apellido_Pa = Convert.ToString(txtAp.Text);
            Apellido_Ma = Convert.ToString(txtAm.Text);
            Area = Convert.ToString(DrArea.SelectedValue);
            Puesto = Convert.ToString(DrPuesto.SelectedValue);
            Rol = Convert.ToString(drRol.SelectedValue);
            Autorizador1 = Convert.ToString(DrAutorizador1.SelectedValue);
            Autorizador2 = Convert.ToString(DrAutorizador2.SelectedValue);
            Autorizador1 = Autorizador1.Substring(0,1);
            Autorizador2 = Autorizador2.Substring(0,1);
            conexion.Open();
            string query = "select * from TBL_Usuarios where ID_Usuario = "+ID;
            SqlCommand select = new SqlCommand(query,conexion);
            SqlDataReader registro = select.ExecuteReader();
            if (registro.Read())
            {
                Empleado = Convert.ToString(registro["Empleado"]);
            }
            conexion.Close();
            conexion.Open();
            string query1 = "update TBL_Empleados set Primer_nombre = '"+Primer_nom+"' where ID_Empleado = "+Empleado;
            SqlCommand update = new SqlCommand(query1,conexion);
            update.ExecuteNonQuery();
            conexion.Close();
            conexion.Open();
            string query2 = "update TBL_Empleados set Segundo_nombre = '" + Segundo_nom + "' where ID_Empleado = " + Empleado;
            SqlCommand update2 = new SqlCommand(query2, conexion);
            update2.ExecuteNonQuery();
            conexion.Close();
            conexion.Open();
            string query3 = "update TBL_Empleados set Apellido_paterno = '" + Apellido_Pa + "' where ID_Empleado = " + Empleado;
            SqlCommand update3 = new SqlCommand(query3, conexion);
            update3.ExecuteNonQuery();
            conexion.Close();
            conexion.Open();
            string query4 = "update TBL_Empleados set Apellido_materno = '" + Apellido_Ma + "' where ID_Empleado = " + Empleado;
            SqlCommand update4 = new SqlCommand(query4, conexion);
            update4.ExecuteNonQuery();
            conexion.Close();
            conexion.Open();
            string query5 = "update TBL_Empleados set Area = " + Area + " where ID_Empleado = " + ID;
            SqlCommand update5 = new SqlCommand(query5, conexion);
            update5.ExecuteNonQuery();
            conexion.Close();
            conexion.Open();
            string query6 = "update TBL_Usuarios set Tipo_empleado = " + Puesto + " where ID_Usuario = " + ID;
            SqlCommand update6 = new SqlCommand(query6, conexion);
            update6.ExecuteNonQuery();
            conexion.Close();
            conexion.Open();
            string query7 = "update TBL_Usuarios set Rol = " + Rol + " where ID_Usuario = " + ID;
            SqlCommand update7 = new SqlCommand(query7, conexion);
            update7.ExecuteNonQuery();
            conexion.Close();
            conexion.Open();
            string query8 = "update TBL_Usuarios set Autorizador_nivel1 = " + Autorizador1 + " where ID_Usuario = " + ID;
            SqlCommand update8 = new SqlCommand(query8, conexion);
            update8.ExecuteNonQuery();
            conexion.Close();
            conexion.Open();
            string query9 = "update TBL_Usuarios set Autorizador_nivel2 = " + Autorizador2 + " where ID_Usuario = " + ID;
            SqlCommand update9 = new SqlCommand(query9, conexion);
            update9.ExecuteNonQuery();
            conexion.Close();
            ScriptManager.RegisterStartupScript(this, typeof(Page), "alerta", script, true);
        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            BuscarMe();
            InsertLog("btnBuscar", "Modificar Usuario", "OK", "Usuario Encontrado", true);
        }
        protected void InsertLog(string _evento, string _metodo, string _respuesta, string _descripcion, bool _esSistema)
        {
            int idUsuario = 0;
            int _esSistema_ = Convert.ToInt32(_esSistema);
            String dtAhora1 = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            conexion.Open();
            string Insert = "insert into TBL_Logs (Formulario, Evento, Metodo, Fecha, Respuesta, Descripcion, EsSistema, ID_Usuario) values ('Modificar usuario','" + _evento + "','" + _metodo + "'," + "Convert (Datetime,'" + dtAhora1 + "',120),'" + _respuesta + "','" + _descripcion + "'," + _esSistema_ + "," + idUsuario + ")";
            SqlCommand insertL = new SqlCommand(Insert, conexion);
            insertL.ExecuteNonQuery();
            conexion.Close();
        }

        protected void btnModificar_Click(object sender, EventArgs e)
        {
            ModificarMe();
            lblEstatusRes.Text = "Modificado";
            txtNombre1.Text = "";
            txtNombre1.Enabled = true;
            txtNombre2.Text = "";
            txtNombre2.Enabled = true;
            txtAp.Text = "";
            txtAp.Enabled = true;
            txtAm.Text = "";
            txtAm.Enabled = true;
            txtUsuario1.Text = "";
            txtUsuario1.Enabled = true;
            InsertLog("btnModificar", "Modificar Usuario", "OK", "Usuario Moificado", true);
        }

        protected void Resultado_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            AgregarGridMe();
            Resultado.PageIndex = e.NewPageIndex;
            Resultado.DataBind();
        }
        public DataSet Consultar(string strSQL)
        {
            string conexion = "Data source=(local); Initial Catalog=PortalGastos; User=sae; Password=sae";
            SqlConnection con = new SqlConnection(conexion);
            con.Open();
            SqlCommand cmd = new SqlCommand(strSQL, con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            con.Close();
            return ds;
        }
        private void AreaE()
        {
            DrArea.DataSource = Consultar("Select * from TBL_Area");
            DrArea.DataTextField = "Nombre_Area";
            DrArea.DataValueField = "ID_Area";
            DrArea.DataBind();
            DrArea.Items.Insert(0, new ListItem("Seleccionar..", "0"));
        }
        private void Puesto()
        {
            DrPuesto.DataSource = Consultar("Select * from TBL_TipoEmpleado");
            DrPuesto.DataTextField = "Nombre";
            DrPuesto.DataValueField = "ID_TipoEmpleado";
            DrPuesto.DataBind();
            DrPuesto.Items.Insert(0, new ListItem("Seleccionar..", "0"));
        }
        private void RolE()
        {
            drRol.DataSource = Consultar("Select * from TBL_Roles");
            drRol.DataTextField = "Nombre_Rol";
            drRol.DataValueField = "ID_Rol";
            drRol.DataBind();
            drRol.Items.Insert(0, new ListItem("Seleccionar..", "0"));
        }
        private void Autorizador()
        {
            DrAutorizador1.DataSource = Consultar("select Convert(char,ID_Autorizador)+'-'+Primer_nombre as datos from TBL_Autorizadores");
            DrAutorizador1.DataTextField = "Datos";
            DrAutorizador1.DataValueField = "Datos";
            DrAutorizador1.DataBind();
            DrAutorizador1.Items.Insert(0, new ListItem("Seleccionar..", "0"));
            //autorizador 2
            DrAutorizador2.DataSource = Consultar("select Convert(char,ID_Autorizador)+'-'+Primer_nombre as datos from TBL_Autorizadores");
            DrAutorizador2.DataTextField = "Datos";
            DrAutorizador2.DataValueField = "Datos";
            DrAutorizador2.DataBind();
            DrAutorizador2.Items.Insert(0, new ListItem("Seleccionar..", "0"));
        }
        protected void Resultado_SelectedIndexChanged(object sender, EventArgs e)
        {
            Resultado.Visible = false;
            BloquearT.Visible = false;
            DesbloquearT.Visible = false;
            Label1.Visible = false;
            info1.Visible = false;
            info2.Visible = false;

            lblDesc.Visible = true;
            Label2.Visible = true;
            btnRegresar.Visible = true;
            lblUsuario.Visible = true;
            txtUsuario.Visible = true;
            lblEstatus.Visible = true;
            lblEstatusRes.Visible = true;
            lblNombre1.Visible = true;
            txtNombre1.Visible = true;
            lblNombre2.Visible = true;
            txtNombre2.Visible = true;
            lblAp.Visible = true;
            txtAp.Visible = true;
            lblAm.Visible = true;
            txtAm.Visible = true;
            btnModificar.Visible = true;
            lblUsuario1.Visible = true;
            txtUsuario1.Visible = true;
            lblArea.Visible = true;
            DrArea.Visible = true;
            lblPuesto.Visible = true;
            DrPuesto.Visible = true;
            lblRol.Visible = true;
            drRol.Visible = true;
            lblAutorizador1.Visible = true;
            DrAutorizador1.Visible = true;
            lblAutorizador2.Visible = true;
            DrAutorizador2.Visible = true;
            btnBloquear.Visible = true;
            btnDesbloquear.Visible = true;
            BuscarMe();
        }

        protected void btnRegresar_Click(object sender, EventArgs e)
        {
            Resultado.Visible = true;
            BloquearT.Visible = true;
            DesbloquearT.Visible = true;
            Label1.Visible = true;
            info1.Visible = true;
            info2.Visible = true;

            lblDesc.Visible = false;
            Label2.Visible = false;
            btnRegresar.Visible = false;
            lblUsuario.Visible = false;
            txtUsuario.Visible = false;
            lblEstatus.Visible = false;
            lblEstatusRes.Visible = false;
            lblNombre1.Visible = false;
            txtNombre1.Visible = false;
            lblNombre2.Visible = false;
            txtNombre2.Visible = false;
            lblAp.Visible = false;
            txtAp.Visible = false;
            lblAm.Visible = false;
            txtAm.Visible = false;
            btnModificar.Visible = false;
            lblUsuario1.Visible = false;
            txtUsuario1.Visible = false;
            lblArea.Visible = false;
            DrArea.Visible = false;
            lblPuesto.Visible = false;
            DrPuesto.Visible = false;
            lblRol.Visible = false;
            drRol.Visible = false;
            lblAutorizador1.Visible = false;
            DrAutorizador1.Visible = false;
            lblAutorizador2.Visible = false;
            DrAutorizador2.Visible = false;
            btnBloquear.Visible = false;
            btnDesbloquear.Visible = false;
            AgregarGridMe();
        }

        protected void BloquearT_Click(object sender, EventArgs e)
        {
            string script = "alert('Usuarios bloqueados')";
            BloquearTodo();
            ScriptManager.RegisterStartupScript(this, typeof(Page), "alerta", script, true);
        }
        protected void BloquearMe()
        {
            string usuariolbl = Convert.ToString(txtUsuario1.Text);
            conexion.Open();
            string query = "update TBL_Usuarios set bloqueado = 1 where ID_Usuario = '" + usuariolbl + "'";
            SqlCommand blo = new SqlCommand(query, conexion);
            blo.ExecuteNonQuery();
            conexion.Close();
        }
        protected void DesbloquearMe()
        {
            string usuariolbl = Convert.ToString(txtUsuario1.Text);
            conexion.Open();
            string query = "update TBL_Usuarios set bloqueado = 0 where ID_Usuario = '" + usuariolbl + "'";
            SqlCommand deblo = new SqlCommand(query, conexion);
            deblo.ExecuteNonQuery();
            conexion.Close();
        }
        protected void BloquearTodo()
        {
            conexion.Open();
            string query = "update TBL_Usuarios set bloqueado = 1";
            SqlCommand bloq = new SqlCommand(query, conexion);
            bloq.ExecuteNonQuery();
            conexion.Close();
            AgregarGridMe();
        }
        protected void DesbloquearTodo()
        {
            conexion.Open();
            string query = "update TBL_Usuarios set bloqueado = 0";
            SqlCommand bloq = new SqlCommand(query, conexion);
            bloq.ExecuteNonQuery();
            conexion.Close();
            AgregarGridMe();
        }

        protected void btnBloquear_Click(object sender, EventArgs e)
        {
            string script = "alert('Usuario bloqueado')";
            BloquearMe();
            ScriptManager.RegisterStartupScript(this, typeof(Page), "alerta", script, true);
            lblEstatusRes.Text = "Usuario bloqueado";
            txtNombre1.Enabled = false;
            txtNombre2.Enabled = false;
            txtAp.Enabled = false;
            txtAm.Enabled = false;
            DrArea.Enabled = false;
            DrPuesto.Enabled = false;
            drRol.Enabled = false;
            DrAutorizador1.Enabled = false;
            DrAutorizador2.Enabled = false;
        }

        protected void DesbloquearT_Click(object sender, EventArgs e)
        {
            string script = "alert('Usuarios desbloqueados')";
            DesbloquearTodo();
            ScriptManager.RegisterStartupScript(this, typeof(Page), "alerta", script, true);
        }

        protected void btnDesbloquear_Click(object sender, EventArgs e)
        {
            string script = "alert('Usuario desbloqueado')";
            DesbloquearMe();
            ScriptManager.RegisterStartupScript(this, typeof(Page), "alerta", script, true);
            lblEstatusRes.Text = "Usuario activo";
            txtNombre1.Enabled = true;
            txtNombre2.Enabled = true;
            txtAp.Enabled = true;
            txtAm.Enabled = true;
            DrArea.Enabled = true;
            DrPuesto.Enabled = true;
            drRol.Enabled = true;
            DrAutorizador1.Enabled = true;
            DrAutorizador2.Enabled = true;
        }

        protected void DrAutorizador2_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
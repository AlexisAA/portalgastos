﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PortalGastos.Administradordeusuario
{
    public partial class ViewXML : System.Web.UI.Page
    {
        SqlConnection conexion = new SqlConnection("Data source=(local); Initial Catalog=PortalGastos; User=sae; Password=sae ");
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["archivo"] != null)
            {
                string archivo = "";
                string ruta = "";
                string ext = "";

                archivo = Request.QueryString["archivo"].ToString();

                conexion.Open();
                string query = "select Ruta from TBL_Facturas where Nombre_XML='" + archivo + "'";
                SqlCommand selecRu = new SqlCommand(query, conexion);
                SqlDataReader registro = selecRu.ExecuteReader();
                if (registro.Read())
                {
                    ruta = Convert.ToString(registro["Ruta"]);
                }
                conexion.Close();

                string invertida = "";
                foreach (char letra in archivo)
                {
                    invertida = letra + invertida;
                }
                string invertida1 = "";
                string str = invertida.Substring(0, 3);

                foreach (char letra in str)
                {
                    invertida1 = letra + invertida1;
                }

                if (invertida1 == "xml")
                {
                    Response.Expires = 0;
                    Response.Clear();
                    Response.ContentType = "application/xml";
                    Response.WriteFile(ruta + "\\" + archivo);
                    Response.End();
                }
            }
        }
    }
}
﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PaginaMuestra/Site1.Master" AutoEventWireup="true" CodeBehind="Solictudes1.aspx.cs" Inherits="PortalGastos.Administradordeusuario.Solictudes" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../Style/Style.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>
        <asp:Panel ID="pnlSolicitudes" runat="server" CssClass="pnlGenerico centrado5">
            <asp:Label ID="lblTitulo" runat="server" CssClass="lblTitulo" Text="Solicitudes"></asp:Label>
            <br />
            <hr />
            <br />
            <asp:Label ID="lblDesc" runat="server" Text="Podemos ver las solicitudes que están en todos los estatus que contamos" CssClass="lblDescripcion1"></asp:Label>
            <table style="width: 100%;">
                <tr style="height: 20px;">
                    <td style="width: 100%"></td>
                </tr>
                <tr style="height: 20px;">
                    <td style="width: 100%; float:left;">
                        <asp:GridView ID="Resultado" runat="server" CssClass="mGrid1 pagination-ys" PageSize="7" AllowPaging="true" OnPageIndexChanging="Resultado_PageIndexChanging">
                            <Columns>
                                <asp:TemplateField>
                                    <ItemTemplate></ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </td>
                </tr>
            </table>
        </asp:Panel>
    </div>
</asp:Content>

﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PaginaMuestra/Site1.Master" AutoEventWireup="true" CodeBehind="Politicas.aspx.cs" Inherits="PortalGastos.Administradordeusuario.Politicas" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
        <link href="../Style/Style.css" rel="stylesheet" />
        <script src="../Style/jquery-3.5.1.min.js"></script>
        <script>
           function format(input) {
            var num = input.value.replace(/\./g,'');
               if (!isNaN(num)) {
                   num = num.toString().split('').reverse().join('').replace(/(\d{3})(?=[^$|^-])/g, "$1,");
                   num = num.split('').reverse().join('').replace(/^[\.]/,'');
            input.value = num;
            }
            else{ 
            input.value = input.value.replace(/[^\d\.]*/g,'');
            }
}
        </script>
    <meta name="viewport"  content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0"/>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div >
        <asp:Panel ID="pnlPoliticas" runat="server" CssClass="pnlGenerico centrado5">
            <asp:Label ID="lblTitulo" runat="server" CssClass="lblTituloge" Text="Políticas"></asp:Label>
            <asp:Label ID="lblTitulo2" runat="server" CssClass="lblTituloge" Text="Modificar política" Visible="false"></asp:Label>
            <asp:Label ID="lblTitulo3" runat="server" CssClass="lblTituloge" Text="Crear Política" Visible="false"></asp:Label>
            <br />
            <hr />
            <table style="width: 100%;">
                <tr style="height: 20px;">
                    <td style="width: 100%"></td>
                </tr>
                <tr style="height: 20px;">
                    <td style="width: 25%; float:left;">
                        <asp:Label ID="lblTipoViaje" runat="server" CssClass="lblEtique" Text="Tipo Viaje"></asp:Label>
                        <asp:Button ID="btnRegresar" runat="server" Text="Regresar" OnClick="btnRegresar_Click" Visible="false" />
                        <asp:Button ID="btnRegresar1" runat="server" Text="Regresar" Visible="false" OnClick="btnRegresar1_Click" />
                    </td>
                    <td style="width: 25%; float:left;">
                        <asp:Label ID="lblNombre" runat="server" CssClass="lblEtique" Text="Nombre política"></asp:Label>
                    </td>
                </tr>
                <tr style="height: 20px;">
                    <td style="width: 25%; float:left;">
                        <asp:DropDownList ID="DrTipoViaje" runat="server" CssClass="drop" AutoPostBack="false" OnSelectedIndexChanged="TipoSeleccionado"></asp:DropDownList>
                        <asp:Label ID="lblNombrePo" runat="server" CssClass="lblEtique" Text="Nombre Política: " Visible="false"></asp:Label>
                    </td>
                    <td style="width: 35%; float:left;">
                        <asp:DropDownList ID="DrNombre" runat="server" CssClass="drop" AutoPostBack="false"></asp:DropDownList>
                        <asp:Label ID="lblTipoEmpleado1" runat="server" CssClass="lblEtique" Visible="false"></asp:Label>
                        <asp:TextBox ID="txtNombrePo" runat="server" CssClass="txt" Visible="false"></asp:TextBox>
                    </td>
                </tr>
                <tr style="height: 20px;">
                    <td style="width: 100%;"></td>
                </tr>
                <tr style="height: 20px;">
                    <td style="width: 25%; float:left;">
                        <asp:Button ID="btnBuscar" runat="server" CssClass="cssBotonPanel" Text="Buscar" OnClick="btnBuscar_Click" />
                        <asp:Label ID="lblTipoEmpleado" runat="server" CssClass="lblEtique" Text="Tipo Empleado: " Visible="false"></asp:Label>
                    </td>
                    <td style="width: 35%; float:left"> 
                        <asp:Button ID="btnNuevo" runat="server" CssClass="cssBotonPanel" Text="Nueva Política" OnClick="btnNuevo_Click" />
                        <asp:Label ID="lblNombrePo1" runat="server" CssClass="lblEtique" Visible="false"></asp:Label>
                        <asp:DropDownList ID="DrTipoEmp" runat="server" AutoPostBack="false" CssClass="drop" Visible="false"></asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:GridView ID="Resultado" runat="server" CssClass="mGrid1 pagination-ys data" OnSelectedIndexChanged="Resultado_SelectedIndexChanged" PageSize="7" AllowPaging="true" DataKeyNames="ID Politica,Tipo de Viaje" OnPageIndexChanging="Resultado_PageIndexChanging">
                            <Columns>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:ImageButton ID="ImgVisualizar" runat="server" CommandName="Select" ImageUrl="~/Imagenes/vision.png"/>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </td>
                    <td style=" width: 20%; float:left">
                    </td>
                </tr>
                <tr style="height: 20px;">
                    <td style="width: 100%;"></td>
                </tr>
                <tr style="height: 20px;">
                    <td style="width: 25%; float:left;">
                        <asp:Label ID="lblTipogas" runat="server" CssClass="lblEtique" Text="Tipo de Gasto:" Visible="false"></asp:Label>
                    </td>
                    <td style="width: 25%; float:left;">
                        <asp:Label ID="lblTipogas1" runat="server" CssClass="lblEtique" Text="v" Visible="false"></asp:Label>
                        <asp:DropDownList ID="DrTipoGas" runat="server" AutoPostBack="false" CssClass="drop" Visible="false"></asp:DropDownList>
                    </td>
                </tr>
                <tr style="height: 20px;">
                    <td style="width: 100%;"></td>
                </tr>    
                <tr style="height: 20px;">
                    <td style="width: 25%; float:left;">
                        <asp:Label ID="lblTipoViaje2" runat="server" CssClass="lblEtique" Text="Tipo de Viaje: " Visible="false"></asp:Label>
                    </td>
                    <td style="width: 25%; float:left;">
                        <asp:DropDownList ID="DrTipoViaje3" runat="server" CssClass="drop" Visible="false" AutoPostBack="false"></asp:DropDownList>
                        <asp:Label ID="lblTipoViaje3" runat="server" CssClass="lblEtique" Text="Tipo de Viaje: " Visible="false"></asp:Label>
                    </td>
                </tr>
                <tr style="height: 20px;">
                    <td style="width: 100%;"></td>
                </tr>
                <tr style="height: 20px;">
                    <td style="width: 25%; float:left;">
                        <asp:Label ID="lblGastoVuelo" runat="server" CssClass="lblEtique" Text="Gasto Vuelo: " Visible="false"></asp:Label>
                    </td>
                    <td style="width: 25%; float:left;">
                        <asp:TextBox ID="txtGastoVuelo" runat="server" CssClass="txt" Visible="false" onkeyup="format(this)" onchange="format(this)"></asp:TextBox>
                    </td>
                    <td style="width: 25%; float:left;">
                        <asp:Button ID="btnEnviar" runat="server" CssClass="cssBotonPanel" Text="ENVIAR" Visible="false" OnClick="btnEnviar_Click" />
                    </td>
                </tr>
                <tr style="height: 20px;">
                    <td style="width: 100%;"></td>
                </tr>
                <tr style="height: 20px;">
                    <td  style="width: 25%; float:left;">
                        <asp:Label ID="lblGastoHospedaje" runat="server" CssClass="lblEtique" Text="Gasto Hospedaje: " Visible="false"></asp:Label>
                    </td>
                    <td style="width: 25%; float:left;">
                        <asp:TextBox ID="txtGastoHospedaje" runat="server" CssClass="txt number" Visible="false" onkeyup="format(this)" onchange="format(this)"></asp:TextBox>
                    </td>
                    <td style="width:25%; float:left;">
                        <asp:Button ID="btnModificar" runat="server" CssClass="cssBotonPanel" Text="MODIFICAR" OnClick="btnModificar_Click" Visible="false"/>
                    </td>
                </tr>
                <tr style="height: 20px;">
                    <td style="width: 100%;"></td>
                </tr>
                <tr style="height:20px;">
                    <td style="width: 25%; float:left;">
                        <asp:Label ID="lblGastoTerrestre" runat="server" CssClass="lblEtique" Text="Gasto Terrestre: " Visible="false"></asp:Label>
                    </td>
                    <td style="width: 25%; float:left;">
                        <asp:TextBox ID="txtGastoTerrestre" runat="server" CssClass="txt number" Visible="false" onkeyup="format(this)" onchange="format(this)"></asp:TextBox>
                    </td>
                </tr>
                <tr style="height: 20px;">
                    <td style="width: 100%;"></td>
                </tr>
                <tr style="height: 20px;">
                    <td style="width: 25%; float:left;">
                         <asp:Label ID="lblGastoComida" runat="server" CssClass="lblEtique" Text="Gasto Comida: " Visible="false"></asp:Label>
                    </td>
                    <td style="width: 25%; float:left;">
                        <asp:TextBox ID="txtGastoComida" runat="server" CssClass="txt number" Visible="false" onkeyup="format(this)" onchange="format(this)"></asp:TextBox>
                    </td>
                </tr>
                <tr style="height: 20px;">
                    <td style="width: 100%;"></td>
                </tr>
                <tr style="height: 20px;">
                    <td style="width: 25%; float:left;">
                        <asp:Label ID="lblGastoCurso" runat="server" CssClass="lblEtique" Text="Gasto Renta Automóvil: " Visible="false"></asp:Label>
                    </td>
                    <td style="width: 25%; float:left;">
                        <asp:TextBox ID="txtGastoCurso" runat="server" CssClass="txt number" Visible="false" onkeyup="format(this)" onchange="format(this)"></asp:TextBox>
                    </td>
                </tr>
                <tr style="height: 20px;">
                    <td style="width: 100%;"></td>
                </tr>
                <tr style="height: 20px;">
                    <td style="width: 25%; float:left;">
                        <asp:Label ID="lblGastoOtro" runat="server" CssClass="lblEtique" Text="Otro Gasto: " Visible="false"></asp:Label>
                    </td>
                    <td style="width: 25%; float:left;">
                        <asp:TextBox ID="txtGastoOtro" runat="server" CssClass="txt number" Visible="false" onkeyup="format(this)" onchange="format(this)"></asp:TextBox>
                    </td>
                </tr>
                <tr style="height: 20px;">
                    <td style="width: 100%;"></td>
                </tr>
                <tr style="height: 20px;">
                    <td style="width: 25%; float:left;">
                        <asp:Label ID="lblTotalGastos" runat="server" CssClass="lblEtique" Text="Total Gastos: " Visible="false"></asp:Label>
                    </td>
                    <td style="width: 25%; float:left;">
                         <asp:Label ID="lblTotalGastos1" runat="server" CssClass="lblEtique" Visible="false"></asp:Label>
                    </td>
                </tr>
                <tr style="height: 20px;">
                    <td style="width: 100%;"></td>
                </tr>
                <tr style="height: 20px;">
                    <td style="width: 25%; float:left;">
                         <asp:Button ID="btnCalcular" runat="server" CssClass="cssBotonPanel" Visible="false" Text="Calcular" OnClick="btnCalcular_Click" />
                    </td>
                    <td style="width: 25%; float:left;">
                        
                    </td>
                </tr>
            </table>
        </asp:Panel>
    </div>
</asp:Content>

﻿using PortalGastos.Administradordeusuario;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PortalGastos.PaginaMuestra
{

    public partial class VistadeArchivos : System.Web.UI.Page
    {
        SqlConnection conexion = new SqlConnection("Data source=(local); Initial Catalog=PortalGastos; User=sae; Password=sae ");
        protected void Page_Init(object sender, EventArgs e)
        {
            if (Session["user"] == null)
            {
                HttpContext.Current.Response.Redirect("~/Portal/Login.aspx");
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                AgregarGrid();
            }
        }
        protected void AgregarGrid()
        {
            List<string> Lista = new List<string>();
            string con1 = "", FecIn = "", FecFn = "", LDe = "", Per = "", ToS = "", Est = "", TiV = "", TiG = "";
            int NomSol = 0, Emp = 0;
            DataTable dt = new DataTable();
            dt.Columns.AddRange(new DataColumn[10] {new DataColumn("Empleado",typeof(int)),
                                                    new DataColumn("No. de Solicitud",typeof(int)),
                                                    new DataColumn("Fecha Inicio",typeof(string)),
                                                    new DataColumn("Fecha Fin",typeof(string)),
                                                    new DataColumn("Lugar Destino",typeof(string)),
                                                    new DataColumn("Período",typeof(string)),
                                                    new DataColumn("Total Solicitado",typeof(string)),
                                                    new DataColumn("Estatus",typeof(string)),
                                                    new DataColumn("Tipo de Viaje",typeof(string)),
                                                    new DataColumn("Tipo de Gasto",typeof(string))});
            conexion.Open();
            string query = "select * from v_Solicitudes where Estatus = 'Revision comprobacion de gastos' order by ID_Solicitud desc";
            SqlCommand Solicitud = new SqlCommand(query, conexion);
            SqlDataReader regSol = Solicitud.ExecuteReader();
            while (regSol.Read())
            {
                con1 = regSol[0].ToString();
                Lista.Add(con1);
                for (int i = 0; i < Lista.Count; i++)
                {
                    Emp = Convert.ToInt32(regSol["Empleado"]);
                    NomSol = Convert.ToInt32(regSol["ID_Solicitud"]);
                    FecIn = Convert.ToString(regSol["Fecha_Inicio"]);
                    FecFn = Convert.ToString(regSol["Fecha_Fin"]);
                    LDe = Convert.ToString(regSol["Lugar_Destino"]);
                    Per = Convert.ToString(regSol["Periodo"]);
                    ToS = Convert.ToString(regSol["Total_solicitado"]);
                    Est = Convert.ToString(regSol["Estatus"]);
                    TiV = Convert.ToString(regSol["Tipo_Viaje"]);
                    TiG = Convert.ToString(regSol["Tipo_Gasto"]);
                }
                dt.Rows.Add(Emp, NomSol, FecIn, FecFn, LDe, Per, "$" + decimal.Parse(ToS).ToString("N"), Est, TiV, TiG);
                Resultado.DataSource = dt;
                
                Resultado.DataBind();
            }
            conexion.Close();
        }

        protected void Resultado_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            AgregarGrid();
            Resultado.PageIndex = e.NewPageIndex;
            Resultado.DataBind();
        }

        protected void Resultado_SelectedIndexChanged(object sender, EventArgs e)
        {
            btnRegresar.Visible = true;
            Resultado.Visible = false;
            Resultado1.Visible = true;
            btnAceptar.Visible = true;
            btnRechazar.Visible = true;
            List<string> Lista = new List<string>();
            GridViewRow row = Resultado.SelectedRow;
            int id = Convert.ToInt32(Resultado.DataKeys[row.RowIndex].Value);
            int IdSol = 0;
            string NomXml = "", NomPdf = "", con1 = "", TiG = "", GasF = "",UUID="",Total="",Total1="";
            DataTable dt = new DataTable();
            dt.Columns.AddRange(new DataColumn[7]{
                                                  new DataColumn("ID Solicitud",typeof(int)),
                                                  new DataColumn("Gasto Total",typeof(string)),
                                                  new DataColumn("Total Solicitado",typeof(string)),
                                                  new DataColumn("Nombre PDF",typeof(string)),
                                                  new DataColumn("Nombre XML",typeof(string)),
                                                  new DataColumn ("Tipo gasto",typeof(string)),
                                                  new DataColumn("UUID", typeof(string))});
            conexion.Open();
            string query = "select * from TBL_Facturas where Solicitud = " + id;
            SqlCommand selectFac = new SqlCommand(query, conexion);
            SqlDataReader registro = selectFac.ExecuteReader();
            while (registro.Read())
            {
                con1 = registro[0].ToString();
                Lista.Add(con1);
                for (int i = 0; i < Lista.Count; i++)
                {
                    IdSol = Convert.ToInt32(registro["Solicitud"]);
                    GasF = Convert.ToString(registro["Gasto_Factura"]);
                    NomPdf = Convert.ToString(registro["Nombre_PDF"]);
                    NomXml = Convert.ToString(registro["Nombre_XML"]);
                    TiG = Convert.ToString(registro["Gasto"]);
                    UUID = Convert.ToString(registro["UUID"]);
                    Total = Convert.ToString(registro["Gasto_Solicitud"]);
                }
                dt.Rows.Add(IdSol, "$ "+decimal.Parse(GasF).ToString("N"),"$ "+decimal.Parse(Total).ToString("N") , NomPdf, NomXml, TiG,UUID);
                Resultado1.DataSource = dt;
                Resultado1.DataBind();
            }
            conexion.Close();
            ComentarioME();
        }

        protected void Resultado1_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridViewRow row = Resultado1.SelectedRow;
            string pdf = Convert.ToString(Resultado1.DataKeys[row.RowIndex].Values["Tipo gasto"]);
            string UUID = Convert.ToString(Resultado1.DataKeys[row.RowIndex].Values["UUID"]);
            GridViewRow row1 = Resultado.SelectedRow;
            int id = Convert.ToInt32(Resultado.DataKeys[row1.RowIndex].Value);
            GridViewRow row2 = Resultado1.SelectedRow;

            if (pdf == "Vuelo")
            {
                conexion.Open();
                string nompdf = "";
                string nomxml = "";
                string ruta = "";
                string url = "";
                string query = "select Nombre_XML,Nombre_PDF,Ruta from TBL_Facturas where Gasto='Vuelo' and Solicitud = " + id + "and UUID ='" + UUID + "'";
                SqlCommand selecNom = new SqlCommand(query, conexion);
                SqlDataReader registro = selecNom.ExecuteReader();
                if (registro.Read())
                {
                    nomxml = Convert.ToString(registro["Nombre_XML"]);
                    nompdf = Convert.ToString(registro["Nombre_PDF"]);
                    ruta = Convert.ToString(registro["Ruta"]);
                }
                conexion.Close();
                if (nompdf != null)
                {
                    url = "ViewPDF.aspx?archivo=" + nompdf;
                    string funcion = "OpenWindows('" + url + "');";
                    ScriptManager.RegisterStartupScript(Page, GetType(), "SIFEL", funcion, true);
                }
            }
            if (pdf == "Hospedaje")
            {
                conexion.Open();
                string nompdf = "";
                string nomxml = "";
                string ruta = "";
                string url = "";
                string query = "select Nombre_XML,Nombre_PDF,Ruta from TBL_Facturas where Gasto='Hospedaje' and Solicitud = " + id + "and UUID ='" + UUID + "'";
                SqlCommand selecNom = new SqlCommand(query, conexion);
                SqlDataReader registro = selecNom.ExecuteReader();
                if (registro.Read())
                {
                    nomxml = Convert.ToString(registro["Nombre_XML"]);
                    nompdf = Convert.ToString(registro["Nombre_PDF"]);
                    ruta = Convert.ToString(registro["Ruta"]);
                }
                conexion.Close();
                if (nompdf != null)
                {
                    url = "ViewPDF.aspx?archivo=" + nompdf;
                    string funcion = "OpenWindows('" + url + "');";
                    ScriptManager.RegisterStartupScript(Page, GetType(), "SIFEL", funcion, true);
                }
            }
            if (pdf == "Traslado terrestre")
            {
                conexion.Open();
                string nompdf = "";
                string nomxml = "";
                string ruta = "";
                string url = "";
                string query = "select Nombre_XML,Nombre_PDF,Ruta from TBL_Facturas where Gasto='Traslado Terrestre' and Solicitud = " + id + "and UUID ='" + UUID + "'";
                SqlCommand selecNom = new SqlCommand(query, conexion);
                SqlDataReader registro = selecNom.ExecuteReader();
                if (registro.Read())
                {
                    nomxml = Convert.ToString(registro["Nombre_XML"]);
                    nompdf = Convert.ToString(registro["Nombre_PDF"]);
                    ruta = Convert.ToString(registro["Ruta"]);
                }
                conexion.Close();
                if (nompdf != null)
                {
                    url = "ViewPDF.aspx?archivo=" + nompdf;
                    string funcion = "OpenWindows('" + url + "');";
                    ScriptManager.RegisterStartupScript(Page, GetType(), "SIFEL", funcion, true);
                }
            }
            if (pdf == "Comida")
            {
                conexion.Open();
                string nompdf = "";
                string nomxml = "";
                string ruta = "";
                string url = "";
                string query = "select Nombre_XML,Nombre_PDF,Ruta from TBL_Facturas where Gasto='Comida' and Solicitud = " + id + "and UUID ='" + UUID + "'";
                SqlCommand selecNom = new SqlCommand(query, conexion);
                SqlDataReader registro = selecNom.ExecuteReader();
                if (registro.Read())
                {
                    nomxml = Convert.ToString(registro["Nombre_XML"]);
                    nompdf = Convert.ToString(registro["Nombre_PDF"]);
                    ruta = Convert.ToString(registro["Ruta"]);
                }
                conexion.Close();
                if (nompdf != null)
                {
                    url = "ViewPDF.aspx?archivo=" + nompdf;
                    string funcion = "OpenWindows('" + url + "');";
                    ScriptManager.RegisterStartupScript(Page, GetType(), "SIFEL", funcion, true);
                }
            }
            if (pdf == "Renta Automovil")
            {
                conexion.Open();
                string nompdf = "";
                string nomxml = "";
                string ruta = "";
                string url = "";
                string query = "select Nombre_XML,Nombre_PDF,Ruta from TBL_Facturas where Gasto='Renta Automovil' and Solicitud = " + id + "and UUID ='" + UUID + "'";
                SqlCommand selecNom = new SqlCommand(query, conexion);
                SqlDataReader registro = selecNom.ExecuteReader();
                if (registro.Read())
                {
                    nomxml = Convert.ToString(registro["Nombre_XML"]);
                    nompdf = Convert.ToString(registro["Nombre_PDF"]);
                    ruta = Convert.ToString(registro["Ruta"]);
                }
                conexion.Close();
                if (nompdf != null)
                {
                    url = "ViewPDF.aspx?archivo=" + nompdf;
                    string funcion = "OpenWindows('" + url + "');";
                    ScriptManager.RegisterStartupScript(Page, GetType(), "SIFEL", funcion, true);
                }
            }
            if (pdf == "No comprobable")
            {
                conexion.Open();
                string nompdf = "";
                string ruta = "";
                string url = "";
                string query = "select Nombre_XML,Nombre_PDF,Ruta from TBL_Facturas where Gasto='No comprobable' and Solicitud = " + id;
                SqlCommand selecNom = new SqlCommand(query, conexion);
                SqlDataReader registro = selecNom.ExecuteReader();
                if (registro.Read())
                {
                    nompdf = Convert.ToString(registro["Nombre_PDF"]);
                    ruta = Convert.ToString(registro["Ruta"]);
                }
                conexion.Close();
                if (nompdf != null)
                {
                    url = "ViewPDF.aspx?archivo=" + nompdf;
                    string funcion = "OpenWindows('" + url + "');";
                    ScriptManager.RegisterStartupScript(Page, GetType(), "SIFEL", funcion, true);
                }
            }
        }
        protected void ComentarioME()
        {
            GridViewRow row1 = Resultado.SelectedRow;
            int id = Convert.ToInt32(Resultado.DataKeys[row1.RowIndex].Value);
            string com = "", FeCom = "";
            conexion.Open();
            string query = "select Comentario,Convert (varchar,Fecha_Comentario,111) as Fecha_Comentario from TBL_Comentarios where ID_Solicitud = " + id;
            SqlCommand selecCom = new SqlCommand(query,conexion);
            SqlDataReader registro = selecCom.ExecuteReader();
            if (registro.FieldCount!=0)
            {
                if (registro.Read())
                {
                    com = Convert.ToString(registro["Comentario"]);
                    FeCom = Convert.ToString(registro["Fecha_Comentario"]);
                    lblFecha.Visible = true;
                    lblComentario.Visible = true;
                    btnContestar.Visible = true;
                    lblFecha.Text = "Fecha del comentario "+FeCom;
                    lblComentario.Text = com;
                }
            }
            conexion.Close();
        }

        protected void Regresar_Click(object sender, EventArgs e)
        {
            lblFecha.Visible = false;
            lblComentario.Visible = false;
            btnContestar.Visible = false;
            Resultado1.Visible = false;
            btnRegresar.Visible = false;
            btnAceptar.Visible = false;
            btnRechazar.Visible = false;

            Resultado.Visible = true;
        }

        protected void btnContestar_Click(object sender, EventArgs e)
        {
            if (lblComentario.Visible==true)
            {
                lblComentario.Visible = false;
                txtComentario.Visible = true;
            }
            btnContestar.Visible = false;
            btnRegresar1.Visible = true;
        }

        protected void btnRegresar1_Click(object sender, EventArgs e)
        {
            if (lblComentario.Visible==false)
            {
                lblComentario.Visible = true;
                txtComentario.Visible = false;
            }
            btnContestar.Visible = true;
            btnRegresar1.Visible = false;
            List<string> Lista = new List<string>();
            GridViewRow row = Resultado.SelectedRow;
            int id = Convert.ToInt32(Resultado.DataKeys[row.RowIndex].Value);
            int IdSol = 0;
            string NomPdf = "", con1 = "", TiG = "", GasF = "";
            DataTable dt = new DataTable();
            dt.Columns.AddRange(new DataColumn[4]{
                                                  new DataColumn("ID Solicitud",typeof(int)),
                                                  new DataColumn("Gasto Total",typeof(string)),
                                                  new DataColumn("Nombre PDF",typeof(string)),
                                                  new DataColumn ("Tipo gasto",typeof(string))});
            conexion.Open();
            string query = "select * from TBL_Facturas where Solicitud = " + id;
            SqlCommand selectFac = new SqlCommand(query, conexion);
            SqlDataReader registro = selectFac.ExecuteReader();
            while (registro.Read())
            {
                con1 = registro[0].ToString();
                Lista.Add(con1);
                for (int i = 0; i < Lista.Count; i++)
                {
                    IdSol = Convert.ToInt32(registro["Solicitud"]);
                    GasF = Convert.ToString(registro["Gasto_Factura"]);
                    NomPdf = Convert.ToString(registro["Nombre_PDF"]);
                    TiG = Convert.ToString(registro["Gasto"]);
                }
                dt.Rows.Add(IdSol, "$ " + GasF, NomPdf, TiG);
                Resultado1.DataSource = dt;
                Resultado1.DataBind();
            }
            conexion.Close();
        }

        protected void btnAceptar_Click(object sender, EventArgs e)
        {
            GridViewRow row1 = Resultado.SelectedRow;
            int id = Convert.ToInt32(Resultado.DataKeys[row1.RowIndex].Value);
            conexion.Open();
            string query = "update TBL_Solicitud set Estatus = 8 where ID_Solicitud = "+id;
            SqlCommand update = new SqlCommand(query,conexion);
            update.ExecuteNonQuery();
            conexion.Close();
        }

        protected void btnRechazar_Click(object sender, EventArgs e)
        {
            GridViewRow row1 = Resultado.SelectedRow;
            int id = Convert.ToInt32(Resultado.DataKeys[row1.RowIndex].Value);
            string ruta = "", nompdf = "", nomxml = "",con="",gasto="";
            List<string> Lista1 = new List<string>();
            conexion.Open();
            string query = "update TBL_Solicitud set Estatus = 6 where ID_Solicitud = " + id;
            SqlCommand update = new SqlCommand(query, conexion);
            update.ExecuteNonQuery();
            conexion.Close();
            conexion.Open();
            string query3 = "select * from TBL_Facturas where Solicitud = "+id;
            SqlCommand selectFact = new SqlCommand(query3,conexion);
            SqlDataReader registros = selectFact.ExecuteReader();
            while (registros.Read())
            {
                con = registros[0].ToString();
                Lista1.Add(con);
                for (int i = 0; i < Lista1.Count; i++)
                {
                    ruta = Convert.ToString(registros["Ruta"]);
                    nompdf = Convert.ToString(registros["Nombre_PDF"]);
                    nomxml = Convert.ToString(registros["Nombre_XML"]);
                    gasto = Convert.ToString(registros["Gasto"]);
                    if (gasto != "No comprobable")
                    {
                        File.Delete(ruta + nompdf);
                        File.Delete(ruta + nomxml);
                    }
                    else
                    {
                        File.Delete(ruta + nompdf);
                    }
                }
            }
            conexion.Close();
            conexion.Open();
            string query1 = "delete TBL_Facturas where Solicitud = " + id;
            SqlCommand update1 = new SqlCommand(query1,conexion);
            update1.ExecuteNonQuery();
            conexion.Close();
            List<string> Lista = new List<string>();
            int IdSol = 0;
            string NomPdf = "", con1 = "", TiG = "", GasF = "";
            DataTable dt = new DataTable();
            dt.Columns.AddRange(new DataColumn[4]{
                                                  new DataColumn("ID Solicitud",typeof(int)),
                                                  new DataColumn("Gasto Total",typeof(string)),
                                                  new DataColumn("Nombre PDF",typeof(string)),
                                                  new DataColumn ("Tipo gasto",typeof(string))});
            conexion.Open();
            string query2 = "select * from TBL_Facturas where Solicitud = " + id;
            SqlCommand selectFac = new SqlCommand(query, conexion);
            SqlDataReader registro = selectFac.ExecuteReader();
            while (registro.Read())
            {
                con1 = registro[0].ToString();
                Lista.Add(con1);
                for (int i = 0; i < Lista.Count; i++)
                {
                    IdSol = Convert.ToInt32(registro["Solicitud"]);
                    GasF = Convert.ToString(registro["Gasto_Factura"]);
                    NomPdf = Convert.ToString(registro["Nombre_PDF"]);
                    TiG = Convert.ToString(registro["Gasto"]);
                }
                dt.Rows.Add(IdSol, "$ " + GasF, NomPdf, TiG);
                Resultado1.DataSource = dt;
                Resultado1.DataBind();
            }
            conexion.Close();
        }

        protected void Resultado1_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            
        }

        protected void ImagenPdf_Click(object sender, ImageClickEventArgs e)
        {
           
        }

        protected void ImagenXml_Click(object sender, ImageClickEventArgs e)
        {
            GridViewRow row = Resultado1.SelectedRow;
            string pdf = Convert.ToString(Resultado1.DataKeys[row.RowIndex].Values["Tipo gasto"]);
            string UUID = Convert.ToString(Resultado1.DataKeys[row.RowIndex].Values["UUID"]);
            GridViewRow row1 = Resultado.SelectedRow;
            int id = Convert.ToInt32(Resultado.DataKeys[row1.RowIndex].Value);
            GridViewRow row2 = Resultado1.SelectedRow;

            if (pdf == "Vuelo")
            {
                conexion.Open();
                string nompdf = "";
                string nomxml = "";
                string ruta = "";
                string url = "";
                string query = "select Nombre_XML,Nombre_PDF,Ruta from TBL_Facturas where Gasto='Vuelo' and Solicitud = " + id + "and UUID ='" + UUID + "'";
                SqlCommand selecNom = new SqlCommand(query, conexion);
                SqlDataReader registro = selecNom.ExecuteReader();
                if (registro.Read())
                {
                    nomxml = Convert.ToString(registro["Nombre_XML"]);
                    nompdf = Convert.ToString(registro["Nombre_PDF"]);
                    ruta = Convert.ToString(registro["Ruta"]);
                }
                conexion.Close();
                if (nomxml != null)
                {
                    url = "ViewXML.aspx?archivo=" + nomxml;
                    string funcion = "OpenWindows('" + url + "');";
                    ScriptManager.RegisterStartupScript(Page, GetType(), "SIFEL", funcion, true);
                }
            }
            if (pdf == "Hospedaje")
            {
                conexion.Open();
                string nompdf = "";
                string nomxml = "";
                string ruta = "";
                string url = "";
                string query = "select Nombre_XML,Nombre_PDF,Ruta from TBL_Facturas where Gasto='Hospedaje' and Solicitud = " + id + "and UUID ='" + UUID + "'";
                SqlCommand selecNom = new SqlCommand(query, conexion);
                SqlDataReader registro = selecNom.ExecuteReader();
                if (registro.Read())
                {
                    nomxml = Convert.ToString(registro["Nombre_XML"]);
                    nompdf = Convert.ToString(registro["Nombre_PDF"]);
                    ruta = Convert.ToString(registro["Ruta"]);
                }
                conexion.Close();
                if (nomxml != null)
                {
                    url = "ViewXML.aspx?archivo=" + nomxml;
                    string funcion = "OpenWindows('" + url + "');";
                    ScriptManager.RegisterStartupScript(Page, GetType(), "SIFEL", funcion, true);
                }
            }
            if (pdf == "Traslado terrestre")
            {
                conexion.Open();
                string nompdf = "";
                string nomxml = "";
                string ruta = "";
                string url = "";
                string query = "select Nombre_XML,Nombre_PDF,Ruta from TBL_Facturas where Gasto='Traslado Terrestre' and Solicitud = " + id + "and UUID ='" + UUID + "'";
                SqlCommand selecNom = new SqlCommand(query, conexion);
                SqlDataReader registro = selecNom.ExecuteReader();
                if (registro.Read())
                {
                    nomxml = Convert.ToString(registro["Nombre_XML"]);
                    nompdf = Convert.ToString(registro["Nombre_PDF"]);
                    ruta = Convert.ToString(registro["Ruta"]);
                }
                conexion.Close();
                if (nomxml != null)
                {
                    url = "ViewXML.aspx?archivo=" + nomxml;
                    string funcion = "OpenWindows('" + url + "');";
                    ScriptManager.RegisterStartupScript(Page, GetType(), "SIFEL", funcion, true);
                }
            }
            if (pdf == "Comida")
            {
                conexion.Open();
                string nompdf = "";
                string nomxml = "";
                string ruta = "";
                string url = "";
                string query = "select Nombre_XML,Nombre_PDF,Ruta from TBL_Facturas where Gasto='Comida' and Solicitud = " + id + "and UUID ='" + UUID + "'";
                SqlCommand selecNom = new SqlCommand(query, conexion);
                SqlDataReader registro = selecNom.ExecuteReader();
                if (registro.Read())
                {
                    nomxml = Convert.ToString(registro["Nombre_XML"]);
                    nompdf = Convert.ToString(registro["Nombre_PDF"]);
                    ruta = Convert.ToString(registro["Ruta"]);
                }
                conexion.Close();
                if (nomxml != null)
                {
                    url = "ViewXML.aspx?archivo=" + nomxml;
                    string funcion = "OpenWindows('" + url + "');";
                    ScriptManager.RegisterStartupScript(Page, GetType(), "SIFEL", funcion, true);
                }
            }
            if (pdf == "Renta Automovil")
            {
                conexion.Open();
                string nompdf = "";
                string nomxml = "";
                string ruta = "";
                string url = "";
                string query = "select Nombre_XML,Nombre_PDF,Ruta from TBL_Facturas where Gasto='Renta Automovil' and Solicitud = " + id + "and UUID ='" + UUID + "'";
                SqlCommand selecNom = new SqlCommand(query, conexion);
                SqlDataReader registro = selecNom.ExecuteReader();
                if (registro.Read())
                {
                    nomxml = Convert.ToString(registro["Nombre_XML"]);
                    nompdf = Convert.ToString(registro["Nombre_PDF"]);
                    ruta = Convert.ToString(registro["Ruta"]);
                }
                conexion.Close();
                if (nomxml != null)
                {
                    url = "ViewXML.aspx?archivo=" + nomxml;
                    string funcion = "OpenWindows('" + url + "');";
                    ScriptManager.RegisterStartupScript(Page, GetType(), "SIFEL", funcion, true);
                }
            }
        }
    }
}
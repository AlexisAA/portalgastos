﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PortalGastos.Administradordeusuario
{
    public partial class Solictudes : System.Web.UI.Page
    {
        SqlConnection conexion = new SqlConnection("Data source=(local); Initial Catalog=PortalGastos; User=sae; Password=sae ");
        protected void Page_Init(object sender, EventArgs e)
        {
            if (Session["user"] == null)
            {
                HttpContext.Current.Response.Redirect("~/Portal/Login.aspx");
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            AgregarGridMe();
        }

        protected void Resultado_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            AgregarGridMe();
            Resultado.PageIndex = e.NewPageIndex;
            Resultado.DataBind();
        }
        protected void AgregarGridMe()
        {
            List<string> Lista = new List<string>();
            string con1 = "", FecIn = "", FecFn = "", LDe = "", Per = "", ToS = "", Est = "", TiV = "", TiG = "";
            int NomSol = 0, Emp = 0;
            DataTable dt = new DataTable();
            dt.Columns.AddRange(new DataColumn[10] {new DataColumn("Empleado",typeof(int)),
                                                    new DataColumn("No. de Solicitud",typeof(int)),
                                                    new DataColumn("Fecha Inicio",typeof(string)),
                                                    new DataColumn("Fecha Fin",typeof(string)),
                                                    new DataColumn("Lugar Destino",typeof(string)),
                                                    new DataColumn("Período",typeof(string)),
                                                    new DataColumn("Total Solicitado",typeof(string)),
                                                    new DataColumn("Estatus",typeof(string)),
                                                    new DataColumn("Tipo Viaje",typeof(string)),
                                                    new DataColumn("Tipo Gasto",typeof(string))});
            conexion.Open();
            string query = "select * from v_Solicitudes order by ID_Solicitud desc";
            SqlCommand Solicitud = new SqlCommand(query, conexion);
            SqlDataReader regSol = Solicitud.ExecuteReader();
            while (regSol.Read())
            {
                con1 = regSol[0].ToString();
                Lista.Add(con1);
                for (int i = 0; i < Lista.Count; i++)
                {
                    Emp = Convert.ToInt32(regSol["Empleado"]);
                    NomSol = Convert.ToInt32(regSol["ID_Solicitud"]);
                    FecIn = Convert.ToString(regSol["Fecha_Inicio"]);
                    FecFn = Convert.ToString(regSol["Fecha_Fin"]);
                    LDe = Convert.ToString(regSol["Lugar_Destino"]);
                    Per = Convert.ToString(regSol["Periodo"]);
                    ToS = Convert.ToString(regSol["Total_solicitado"]);
                    Est = Convert.ToString(regSol["Estatus"]);
                    TiV = Convert.ToString(regSol["Tipo_Viaje"]);
                    TiG = Convert.ToString(regSol["Tipo_Gasto"]);
                }
                dt.Rows.Add(Emp, NomSol, FecIn, FecFn, LDe, Per, "$ " + decimal.Parse(ToS).ToString("N") , Est, TiV, TiG);
                Resultado.DataSource = dt;

                Resultado.DataBind();
            }
            conexion.Close();
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PortalGastos.Administradordeusuario
{
    public partial class Politicas : System.Web.UI.Page
    {
        SqlConnection conexion = new SqlConnection("Data source=(local); Initial Catalog=PortalGastos; User=sae; Password=sae ");
        protected void Page_Init(object sender, EventArgs e)
        {
            if (Session["user"] == null)
            {
                HttpContext.Current.Response.Redirect("~/Portal/Login.aspx");
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                DrTipoViajeMe();
                AgregarGridMe();
                DrNombreMe();
                DrTipoEmpMe();
                DrTipViajeMe();
            }
        }

        private void DrTipoViajeMe()
        {
            DrTipoViaje.DataSource = Consultar("Select * from TBL_Tipo_Viaje");
            DrTipoViaje.DataTextField = "Nombre";
            DrTipoViaje.DataValueField = "ID_Tipo";
            DrTipoViaje.DataBind();
            DrTipoViaje.Items.Insert(0, new ListItem("Seleccionar..", "0"));
        }
        private void DrNombreMe()
        {
            DrNombre.DataSource = Consultar("select * from TBL_TipoEmpleado");
            DrNombre.DataTextField = "Nombre";
            DrNombre.DataValueField = "ID_TipoEmpleado";
            DrNombre.DataBind();
            DrNombre.Items.Insert(0, new ListItem("Seleccionar..", "0"));
        }
        private void DrTipoEmpMe()
        {
            DrTipoEmp.DataSource = Consultar("select * from TBL_TipoEmpleado");
            DrTipoEmp.DataTextField = "Nombre";
            DrTipoEmp.DataValueField = "ID_TipoEmpleado";
            DrTipoEmp.DataBind();
            DrTipoEmp.Items.Insert(0, new ListItem("Seleccionar..", "0"));
        }
        private void DrTipViajeMe()
        {
            DrTipoViaje3.DataSource = Consultar("Select * from TBL_Tipo_Viaje");
            DrTipoViaje3.DataTextField = "Nombre";
            DrTipoViaje3.DataValueField = "ID_Tipo";
            DrTipoViaje3.DataBind();
            DrTipoViaje3.Items.Insert(0, new ListItem("Seleccionar..", "0"));
        }
        private void DrTipoGastoMe()
        {
            DrTipoGas.DataSource = Consultar("select * from TBL_TipoGasto");
            DrTipoGas.DataTextField = "Tipo_Gasto";
            DrTipoGas.DataValueField = "ID_TipoGasto";
            DrTipoGas.DataBind();
            DrTipoGas.Items.Insert(0, new ListItem("Seleccionar..", "0"));
        }
        
        public DataSet Consultar(string strSQL)
        {
            string conexion = "Data source=(local); Initial Catalog=PortalGastos; User=sae; Password=sae";
            SqlConnection con = new SqlConnection(conexion);
            con.Open();
            SqlCommand cmd = new SqlCommand(strSQL, con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            con.Close();
            return ds;
        }

        protected void btnModificar_Click(object sender, EventArgs e)
        {
            ModificarME();
            txtNombrePo.Text = "";
            DrTipoEmpMe();
            DrTipViajeMe();
            txtGastoVuelo.Text = "";
            txtGastoHospedaje.Text = "";
            txtGastoTerrestre.Text = "";
            txtGastoComida.Text = "";
            txtGastoCurso.Text = "";
            txtGastoOtro.Text = "";
            lblTotalGastos1.Text = "";
        }
        protected void buscarMe()
        {
            int TipoID = Convert.ToInt32(DrTipoViaje.SelectedValue);
            int NombreID = Convert.ToInt32(DrNombre.SelectedValue);
            string NomTi = "",NomTi1="",con1="", NomPo = "", ToG = "", TiV = "", TiE = "";
            int ID_po = 0;
            List<string> Lista = new List<string>();
            DataTable dt = new DataTable();
            dt.Columns.AddRange(new DataColumn[5]{new DataColumn("ID Politica",typeof(int)),
                                                  new DataColumn("Nombre Política",typeof(string)),
                                                  new DataColumn("Total Gastos",typeof(string)),
                                                  new DataColumn("Tipo de Viaje",typeof(string)),
                                                  new DataColumn("Tipo de Empleado",typeof(string))});
            if (TipoID != 0 && NombreID == 0)
            {
                conexion.Open();
                string consul = "select * from TBL_Tipo_Viaje where ID_tipo = "+TipoID;
                SqlCommand selectTi = new SqlCommand(consul,conexion);
                SqlDataReader selectTip = selectTi.ExecuteReader();
                if (selectTip.Read())
                {
                    NomTi = Convert.ToString(selectTip["Nombre"]);
                }
                conexion.Close();
                conexion.Open();
                string consul1 = "select * from v_politicas where Tipo_Viaje = '"+NomTi+"'";
                SqlCommand selectPo = new SqlCommand(consul1,conexion);
                SqlDataReader selectPol = selectPo.ExecuteReader();
                while (selectPol.Read())
                {
                    con1 = selectPol[0].ToString();
                    Lista.Add(con1);
                    for (int i = 0; i < Lista.Count; i++)
                    {
                        ID_po = Convert.ToInt32(selectPol["ID_ViajeInterno"]);
                        NomPo = Convert.ToString(selectPol["Nombre_Politica"]);
                        ToG = Convert.ToString(selectPol["Total_Gastos"]+"N");
                        TiV = Convert.ToString(selectPol["Tipo_Viaje"]);
                        TiE = Convert.ToString(selectPol["Tipo_Empleado"]);
                    }
                    dt.Rows.Add(ID_po, NomPo, "$ "+ decimal.Parse(ToG).ToString("N"), TiV, TiE);
                    Resultado.DataSource = dt;
                    Resultado.DataBind();
                }
                conexion.Close();
            }
            if (TipoID == 0 && NombreID != 0)
            {
                conexion.Open();
                string consul = "select * from TBL_TipoEmpleado where ID_TipoEmpleado = " + NombreID;
                SqlCommand selectTi = new SqlCommand(consul, conexion);
                SqlDataReader selectTip = selectTi.ExecuteReader();
                if (selectTip.Read())
                {
                    NomTi = Convert.ToString(selectTip["Nombre"]);
                }
                conexion.Close();
                conexion.Open();
                string consul1 = "select * from v_politicas where Nombre_Politica = '" + NomTi + "'";
                SqlCommand selectPo = new SqlCommand(consul1, conexion);
                SqlDataReader selectPol = selectPo.ExecuteReader();
                while (selectPol.Read())
                {
                    con1 = selectPol[0].ToString();
                    Lista.Add(con1);
                    for (int i = 0; i < Lista.Count; i++)
                    {
                        ID_po = Convert.ToInt32(selectPol["ID_ViajeInterno"]);
                        NomPo = Convert.ToString(selectPol["Nombre_Politica"]);
                        ToG = Convert.ToString(selectPol["Total_Gastos"]);
                        TiV = Convert.ToString(selectPol["Tipo_Viaje"]);
                        TiE = Convert.ToString(selectPol["Tipo_Empleado"]);
                    }
                    dt.Rows.Add(ID_po,NomPo, ToG, TiV, TiE);
                    Resultado.DataSource = dt;
                    Resultado.DataBind();
                }
                conexion.Close();
            }
            if (TipoID != 0 && NombreID != 0)
            {
                conexion.Open();
                string consul = "select * from TBL_TipoEmpleado where ID_TipoEmpleado = " + NombreID;
                SqlCommand selectTi = new SqlCommand(consul, conexion);
                SqlDataReader selectTip = selectTi.ExecuteReader();
                if (selectTip.Read())
                {
                    NomTi = Convert.ToString(selectTip["Nombre"]);
                }
                conexion.Close();
                conexion.Open();
                string consultT = "select * from TBL_Tipo_Viaje where ID_tipo = " + TipoID;
                SqlCommand selectTipo = new SqlCommand(consultT, conexion);
                SqlDataReader selectTipo1 = selectTipo.ExecuteReader();
                if (selectTipo1.Read())
                {
                    NomTi1 = Convert.ToString(selectTipo1["Nombre"]);
                }
                conexion.Close();
                conexion.Close();
                conexion.Open();
                string consul1 = "select * from v_politicas where Nombre_Politica = '" + NomTi + "' and Tipo_Viaje = '"+NomTi1+"'";
                SqlCommand selectPo = new SqlCommand(consul1, conexion);
                SqlDataReader selectPol = selectPo.ExecuteReader();
                while (selectPol.Read())
                {
                    con1 = selectPol[0].ToString();
                    Lista.Add(con1);
                    for (int i = 0; i < Lista.Count; i++)
                    {
                        ID_po = Convert.ToInt32(selectPol["ID_ViajeInterno"]);
                        NomPo = Convert.ToString(selectPol["Nombre_Politica"]);
                        ToG = Convert.ToString(selectPol["Total_Gastos"]);
                        TiV = Convert.ToString(selectPol["Tipo_Viaje"]);
                        TiE = Convert.ToString(selectPol["Tipo_Empleado"]);
                    }
                    dt.Rows.Add(ID_po, NomPo, ToG, TiV, TiE);
                    Resultado.DataSource = dt;
                    Resultado.DataBind();
                }
                conexion.Close();
            }
            
        }
        protected void ModificarME()
        {
            string NomP = Convert.ToString(lblNombrePo1.Text);
            string TiVi = Convert.ToString(lblTipoEmpleado1.Text);
            string GaV = Convert.ToString(txtGastoVuelo.Text);
            string GaH = Convert.ToString(txtGastoHospedaje.Text);
            string GaT = Convert.ToString(txtGastoTerrestre.Text);
            string GaC = Convert.ToString(txtGastoComida.Text);
            string OG = Convert.ToString(txtGastoOtro.Text);
            string GaR = Convert.ToString(txtGastoCurso.Text);
            string ToT = Convert.ToString(lblTotalGastos1.Text); 

            int Nom = 0;

            conexion.Open();
            string query = "select * from TBL_Tipo_Viaje where Nombre = '" + TiVi + "'";
            SqlCommand selectTi = new SqlCommand(query, conexion);
            SqlDataReader selectTip = selectTi.ExecuteReader();
            if (selectTip.Read())
            {
                Nom = Convert.ToInt32(selectTip["ID_Tipo"]);
            }
            conexion.Close();

            if (Nom == 1  || Nom == 3)
            {
                GaV = GaV.Replace(",", "");
                conexion.Open();
                string query1 = "update TBL_Interno set Gasto_vuelo = "+GaV+" where Nombre_Politica = '"+NomP+"' and Tipo_Viaje = '"+Nom+"'";
                SqlCommand actuGV = new SqlCommand(query1, conexion);
                actuGV.ExecuteNonQuery();
                conexion.Close();
                GaH = GaH.Replace(",","");
                conexion.Open();
                string query2 = "update TBL_Interno set Gasto_hospedaje = " + GaH + " where Nombre_Politica = '" + NomP + "' and Tipo_Viaje = '" + Nom +"'";
                SqlCommand actuGH = new SqlCommand(query2, conexion);
                actuGH.ExecuteNonQuery();
                conexion.Close();
                GaC = GaC.Replace(",", "");
                conexion.Open();
                string query3 = "update TBL_Interno set Gasto_comida = " + GaC + " where Nombre_Politica = '" + NomP + "' and Tipo_Viaje = '" + Nom + "'";
                SqlCommand actuGC = new SqlCommand(query1, conexion);
                actuGC.ExecuteNonQuery();
                conexion.Close();
                GaR = GaR.Replace(",","");
                conexion.Open();
                string query4 = "update TBL_Interno set Gasto_Renta = " + GaR + " where Nombre_Politica = '" + NomP + "' and Tipo_Viaje = '" + Nom + "'";
                SqlCommand actuGR = new SqlCommand(query4, conexion);
                actuGR.ExecuteNonQuery();
                conexion.Close();
                OG = OG.Replace(",", "");
                conexion.Open();
                string query5 = "update TBL_Interno set Gasto_otros = " + OG + " where Nombre_Politica = '" + NomP + "' and Tipo_Viaje = '" + Nom + "'";
                SqlCommand actuGO = new SqlCommand(query5, conexion);
                actuGO.ExecuteNonQuery();
                conexion.Close();
                ToT = ToT.Replace(",", "");
                conexion.Open();
                string query6 = "update TBL_Interno set Total_Gastos = " + ToT + " where Nombre_Politica = '" + NomP + "' and Tipo_Viaje = '" + Nom + "'";
                SqlCommand actuGT = new SqlCommand(query6, conexion);
                actuGT.ExecuteNonQuery();
                conexion.Close();
            }
            else
            {
                GaV = GaV.Replace(",", "");
                conexion.Open();
                string query1 = "update TBL_Externo set Gasto_vuelo = " + GaV + " where Nombre_Politica = '" + NomP + "' and Tipo_Viaje = '" + Nom + "'";
                SqlCommand actuGV = new SqlCommand(query1, conexion);
                actuGV.ExecuteNonQuery();
                conexion.Close();
                GaH = GaH.Replace(",", "");
                conexion.Open();
                string query2 = "update TBL_Externo set Gasto_hospedaje = " + GaH + " where Nombre_Politica = '" + NomP + "' and Tipo_Viaje = '" + Nom + "'";
                SqlCommand actuGH = new SqlCommand(query2, conexion);
                actuGH.ExecuteNonQuery();
                GaC = GaC.Replace(",", "");
                conexion.Close();
                conexion.Open();
                string query3 = "update TBL_Externo set Gasto_comida = " + GaC + " where Nombre_Politica = '" + NomP + "' and Tipo_Viaje = '" + Nom + "'";
                SqlCommand actuGC = new SqlCommand(query1, conexion);
                actuGC.ExecuteNonQuery();
                conexion.Close();
                GaR = GaR.Replace(",", "");
                conexion.Open();
                string query4 = "update TBL_Externo set Gasto_Renta = " + GaR + " where Nombre_Politica = '" + NomP + "' and Tipo_Viaje = '" + Nom + "'";
                SqlCommand actuGR = new SqlCommand(query4, conexion);
                actuGR.ExecuteNonQuery();
                conexion.Close();
                OG = OG.Replace(",", "");
                conexion.Open();
                string query5 = "update TBL_Externo set Gasto_otros = " + OG + " where Nombre_Politica = '" + NomP + "' and Tipo_Viaje = '" + Nom + "'";
                SqlCommand actuGO = new SqlCommand(query5, conexion);
                actuGO.ExecuteNonQuery();
                conexion.Close();
                ToT = ToT.Replace(",", "");
                conexion.Open();
                string query6 = "update TBL_Externo set Total_Gastos = " + ToT + " where Nombre_Politica = '" + NomP + "' and Tipo_Viaje = '" + Nom + "'";
                SqlCommand actuGT = new SqlCommand(query6, conexion);
                actuGT.ExecuteNonQuery();
                conexion.Close();
            }
        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            buscarMe();
            
        }
        protected void AgregarGridMe()
        {
            string NomPo = "", ToG = "", TiV = "", TiE = "",con1="";
            int ID_po=0;
            List<string> Lista = new List<string>();
            DataTable dt = new DataTable();
            dt.Columns.AddRange(new DataColumn[5]{new DataColumn("ID Politica",typeof(int)),
                                                  new DataColumn("Nombre Política",typeof(string)),
                                                  new DataColumn("Total Gastos",typeof(string)),
                                                  new DataColumn("Tipo de Viaje",typeof(string)),
                                                  new DataColumn("Tipo de Empleado",typeof(string))});
            conexion.Open();
            string query = "select * from v_politicas";
            SqlCommand selectIn = new SqlCommand(query, conexion);
            SqlDataReader registro = selectIn.ExecuteReader();
            while (registro.Read())
            {
                con1 = registro[0].ToString();
                Lista.Add(con1);
                for (int i = 0; i < Lista.Count; i++)
                {
                    ID_po = Convert.ToInt32(registro["ID_ViajeInterno"]);
                    NomPo = Convert.ToString(registro["Nombre_Politica"]);
                    ToG = Convert.ToString(registro["Total_Gastos"]);
                    TiV = Convert.ToString(registro["Tipo_Viaje"]);
                    TiE = Convert.ToString(registro["Tipo_Empleado"]);
                }
                dt.Rows.Add(ID_po, NomPo, "$ " + decimal.Parse(ToG).ToString("N"), TiV, TiE);
                Resultado.DataSource = dt;
                Resultado.DataBind();
            }
            conexion.Close();
            
        }
        protected void InsertLog(string _evento, string _metodo, string _respuesta, string _descripcion, bool _esSistema)
        {
            int idUsuario = 0;
            int _esSistema_ = Convert.ToInt32(_esSistema);
            String dtAhora1 = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss");
            conexion.Open();
            string Insert = "insert into TBL_Logs (Formulario, Evento, Metodo, Fecha, Respuesta, Descripcion, EsSistema, ID_Usuario) values ('Login','" + _evento + "','" + _metodo + "','" + dtAhora1 + "','" + _respuesta + "','" + _descripcion + "'," + _esSistema_ + "," + idUsuario + ")";
            SqlCommand insertL = new SqlCommand(Insert, conexion);
            insertL.ExecuteNonQuery();
            conexion.Close();
        }

        protected void TipoSeleccionado(object sender, EventArgs e)
        {
        }

        protected void Resultado_SelectedIndexChanged(object sender, EventArgs e)
        {
            Resultado.Visible = false;
            lblTitulo.Visible = false;
            lblTipoViaje.Visible = false;
            lblTipoEmpleado.Visible = false;
            lblNombre.Visible = false;
            DrNombre.Visible = false;
            DrTipoViaje.Visible = false;
            btnBuscar.Visible = false;
            btnNuevo.Visible = false;

            lblTipogas.Visible = true;
            lblTipogas1.Visible = true;
            btnCalcular.Visible = true;
            btnRegresar.Visible = true;
            lblTitulo2.Visible = true;
            lblNombrePo.Visible = true;
            lblNombrePo1.Visible = true;
            lblTipoEmpleado.Visible = true;
            lblTipoEmpleado1.Visible = true;
            lblGastoVuelo.Visible = true;
            txtGastoVuelo.Visible = true;
            lblGastoHospedaje.Visible = true;
            txtGastoHospedaje.Visible = true;
            lblGastoTerrestre.Visible = true;
            txtGastoTerrestre.Visible = true;
            lblGastoComida.Visible = true;
            txtGastoComida.Visible = true;
            lblGastoCurso.Visible = true;
            txtGastoCurso.Visible = true;
            lblGastoOtro.Visible = true;
            txtGastoOtro.Visible = true;
            lblTotalGastos.Visible = true;
            lblTotalGastos1.Visible = true;
            btnModificar.Visible = true;
            lblTipoViaje2.Visible = true;
            lblTipoViaje3.Visible = true;
            GridViewRow row = Resultado.SelectedRow;
            int ID = Convert.ToInt32(Resultado.DataKeys[row.RowIndex].Values[0]);
            string datos = Convert.ToString(Resultado.DataKeys[row.RowIndex].Values[1]);
            string Nompo = "",TipoEm="",GaV = "",GaH = "",GaT = "",GaC = "", GaR = "",Otr="",ToG="",TiG="" ;
                conexion.Open();
                string viaje = "select * from v_politicasT where ID_ViajeInterno = "+ID+" and Tipo_Viaje = '"+datos+"'";
                SqlCommand TipoV = new SqlCommand(viaje,conexion);
                SqlDataReader TipoVi = TipoV.ExecuteReader();
                if (TipoVi.Read())
                {
                    Nompo = Convert.ToString(TipoVi["Nombre_Politica"]);
                    TipoEm = Convert.ToString(TipoVi["Tipo_Viaje"]);
                    GaV = Convert.ToString(TipoVi["Gasto_vuelo"]);
                    GaH = Convert.ToString(TipoVi["Gasto_hospedaje"]);
                    GaT = Convert.ToString(TipoVi["Gasto_terrestre"]);
                    GaC = Convert.ToString(TipoVi["Gasto_Comida"]);
                    GaR = Convert.ToString(TipoVi["Gasto_Renta"]);
                    Otr = Convert.ToString(TipoVi["Gasto_otros"]);
                    ToG = Convert.ToString(TipoVi["Total_Gastos"]);
                    TiG = Convert.ToString(TipoVi["Tipo_Gasto"]);
                }
                conexion.Close();
            lblNombrePo1.Text = Nompo;
            lblTipoEmpleado1.Text = TipoEm;
            txtGastoVuelo.Text = "$ " + decimal.Parse(GaV).ToString("N");
            txtGastoHospedaje.Text = "$ " + decimal.Parse(GaH).ToString("N");
            txtGastoTerrestre.Text = "$ " + decimal.Parse(GaT).ToString("N");
            txtGastoComida.Text = "$ " + decimal.Parse(GaC).ToString("N");
            txtGastoCurso.Text = "$ " + decimal.Parse(GaR).ToString("N");
            txtGastoOtro.Text = "$ " + decimal.Parse(Otr).ToString("N");
            lblTotalGastos1.Text = "$ " + decimal.Parse(ToG).ToString("N");
            lblTipogas1.Text = TiG;
            lblTipoViaje3.Text = TipoEm;
        }

        protected void btnRegresar_Click(object sender, EventArgs e)
        {
            Resultado.Visible = true;
            lblTitulo.Visible = true;
            lblTipoViaje.Visible = true;
            lblTipoEmpleado.Visible = true;
            lblNombre.Visible = true;
            DrNombre.Visible = true;
            DrTipoViaje.Visible = true;
            btnBuscar.Visible = true;
            btnNuevo.Visible = true;
            AgregarGridMe();

            lblTipogas.Visible = false;
            lblTipogas1.Visible = false;
            lblTitulo2.Visible = false;
            btnCalcular.Visible = false;
            btnRegresar.Visible = false;
            lblNombrePo.Visible = false;
            lblNombrePo1.Visible = false;
            lblTipoEmpleado.Visible = false;
            lblTipoEmpleado1.Visible = false;
            lblGastoVuelo.Visible = false;
            txtGastoVuelo.Visible = false;
            lblGastoHospedaje.Visible = false;
            txtGastoHospedaje.Visible = false;
            lblGastoTerrestre.Visible = false;
            txtGastoTerrestre.Visible = false;
            lblGastoComida.Visible = false;
            txtGastoComida.Visible = false;
            lblGastoCurso.Visible = false;
            txtGastoCurso.Visible = false;
            lblGastoOtro.Visible = false;
            txtGastoOtro.Visible = false;
            lblTotalGastos.Visible = false;
            lblTotalGastos1.Visible = false;
            btnModificar.Visible = false;
            lblTipoViaje2.Visible = false;
            lblNombrePo1.Text = "";
            lblTipoEmpleado1.Text = "";
            txtGastoVuelo.Text = "";
            txtGastoHospedaje.Text = "";
            txtGastoTerrestre.Text = "";
            txtGastoComida.Text = "";
            txtGastoCurso.Text = "";
            txtGastoOtro.Text = "";
            lblTotalGastos1.Text = "";
            lblTipoViaje3.Text = "";
        }

        protected void btnCalcular_Click(object sender, EventArgs e)
        {
            decimal GaV = 0, GaH = 0, GaT = 0, GaC = 0, OG = 0, GaR = 0, Total1 = 0;
            int GaV1 = 0, GaH1 = 0,GaT1 = 0,GaC1=0, OG1=0, GaR1=0;
            string GaV11 = "", GaH11 = "", GaT11 = "", GaC11 = "", OG11 = "", GaR11 = "", Total = "";
            string GaV2 = "", GaH2 = "", GaT2 = "", GaC2 = "", OG2 = "", GaR2 = "";
            GaV1 = Convert.ToInt32(txtGastoVuelo.Text.Length);
            if (GaV1 == 0)
            {
                GaV = 0;
            }
            else
            {
                GaV11 = Convert.ToString(txtGastoVuelo.Text);
                GaV11 = GaV11.Replace(",", "");
                GaV11 = GaV11.Replace("$", "");
                GaV = Convert.ToDecimal(GaV11);
                GaV2 = Convert.ToString(GaV);
                txtGastoVuelo.Text = "$ " + decimal.Parse(GaV2).ToString("N");
            }
            GaH1 = Convert.ToInt32(txtGastoHospedaje.Text.Length);
            if (GaH1 == 0)
            {
                GaH = 0;
            }
            else
            {
                GaH11 = Convert.ToString(txtGastoHospedaje.Text);
                GaH11 = GaH11.Replace(",", "");
                GaH11 = GaH11.Replace("$", "");
                GaH = Convert.ToDecimal(GaH11);
                GaH2 = Convert.ToString(GaH);
                txtGastoHospedaje.Text = "$ " + decimal.Parse(GaH2).ToString("N");
            }
            GaT1 = Convert.ToInt32(txtGastoTerrestre.Text.Length);
            if (GaT1 == 0)
            {
                GaT = 0;
            }
            else
            {
                GaT11 = Convert.ToString(txtGastoTerrestre.Text);
                GaT11 = GaH11.Replace(",", "");
                GaT11 = GaH11.Replace("$", "");
                GaT = Convert.ToDecimal(GaT11);
                GaT2 = Convert.ToString(GaT);
                txtGastoTerrestre.Text = "$ " + decimal.Parse(GaT2).ToString("N");
            }
            GaC1 = Convert.ToInt32(txtGastoComida.Text.Length);
            if (GaC1==0)
            {
                GaC1 = 0;
            }
            else
            {
                GaC11 = Convert.ToString(txtGastoComida.Text);
                GaC11 = GaC11.Replace(",", "");
                GaC11 = GaC11.Replace("$", "");
                GaC = Convert.ToDecimal(GaC11);
                GaC2 = Convert.ToString(GaC);
                txtGastoComida.Text = "$ " + decimal.Parse(GaC2).ToString("N");
            }
            OG1 = Convert.ToInt32(txtGastoOtro.Text.Length);
            if (OG1==0)
            {
                OG = 0;
            }
            else
            {
                OG11 = Convert.ToString(txtGastoOtro.Text);
                OG11 = OG11.Replace(",", "");
                OG11 = OG11.Replace("$", "");
                OG = Convert.ToDecimal(OG11);
                OG2 = Convert.ToString(OG);
                txtGastoOtro.Text = "$ " + decimal.Parse(OG2).ToString("N");
             }
            GaR1 = Convert.ToInt32(txtGastoCurso.Text.Length);
            if (GaR1==0)
            {
                GaR = 0;
            }
            else
            {
                GaR11 = Convert.ToString(txtGastoCurso.Text);
                GaR11 = GaR11.Replace(",", "");
                GaR11 = GaR11.Replace("$", "");
                GaR = Convert.ToDecimal(GaR11);
                GaR2 = Convert.ToString(GaR);
                txtGastoCurso.Text = "$ " + decimal.Parse(GaR2).ToString("N");
            }
            Total1 = GaV + GaH + GaT + GaC + OG + GaR;
            Total = Convert.ToString(Total1);
            lblTotalGastos1.Text = "$ " + decimal.Parse(Total).ToString("N");
               
        }
        protected void NuevoMe()
        {

        }

        protected void btnNuevo_Click(object sender, EventArgs e)
        {
            Resultado.Visible = false;
            lblTitulo.Visible = false;
            lblTipoViaje.Visible = false;
            lblTipoEmpleado.Visible = false;
            lblNombre.Visible = false;
            DrNombre.Visible = false;
            DrTipoViaje.Visible = false;
            btnBuscar.Visible = false;
            btnNuevo.Visible = false;

            lblTitulo3.Visible = true;
            btnRegresar1.Visible = true;
            btnEnviar.Visible = true;
            lblNombrePo.Visible = true;
            txtNombrePo.Visible = true;
            lblTipoEmpleado.Visible = true;
            DrTipoEmp.Visible = true;
            lblTipoViaje2.Visible = true;
            DrTipoViaje3.Visible = true;
            lblGastoVuelo.Visible = true;
            txtGastoVuelo.Visible = true;
            lblGastoHospedaje.Visible = true;
            txtGastoHospedaje.Visible = true;
            lblGastoTerrestre.Visible = true;
            txtGastoTerrestre.Visible = true;
            lblGastoComida.Visible = true;
            txtGastoComida.Visible = true;
            lblGastoCurso.Visible = true;
            txtGastoCurso.Visible = true;
            lblGastoOtro.Visible = true;
            txtGastoOtro.Visible = true;
            lblTotalGastos.Visible = true;
            lblTotalGastos1.Visible = true;
            btnCalcular.Visible = true;
            lblTipogas.Visible = true;
            DrTipoGas.Visible = true;
            DrTipoGastoMe();
        }

        protected void btnRegresar1_Click(object sender, EventArgs e)
        {
            Resultado.Visible = true;
            lblTitulo.Visible = true;
            lblTipoViaje.Visible = true;
            lblTipoEmpleado.Visible = true;
            lblNombre.Visible = true;
            DrNombre.Visible = true;
            DrTipoViaje.Visible = true;
            btnBuscar.Visible = true;
            btnNuevo.Visible = true;
            AgregarGridMe();

            DrTipoGas.Visible = false;
            lblTitulo3.Visible = false;
            btnRegresar1.Visible = false;
            btnEnviar.Visible = false;
            lblNombrePo.Visible = false;
            txtNombrePo.Visible = false;
            lblTipoEmpleado.Visible = false;
            DrTipoEmp.Visible = false;
            lblTipoViaje2.Visible = false;
            DrTipoViaje3.Visible = false;
            lblGastoVuelo.Visible = false;
            txtGastoVuelo.Visible = false;
            lblGastoHospedaje.Visible = false;
            txtGastoHospedaje.Visible = false;
            lblGastoTerrestre.Visible = false;
            txtGastoTerrestre.Visible = false;
            lblGastoComida.Visible = false;
            txtGastoComida.Visible = false;
            lblGastoCurso.Visible = false;
            txtGastoCurso.Visible = false;
            lblGastoOtro.Visible = false;
            txtGastoOtro.Visible = false;
            lblTotalGastos.Visible = false;
            lblTotalGastos1.Visible = false;
            btnCalcular.Visible = false;
            lblTipogas.Visible = false;
            txtNombrePo.Text = "";
            DrTipoEmpMe();
            DrTipoViajeMe();
            txtGastoVuelo.Text = "";
            txtGastoHospedaje.Text = "";
            txtGastoTerrestre.Text = "";
            txtGastoComida.Text = "";
            txtGastoCurso.Text = "";
            txtGastoOtro.Text = "";
            lblTotalGastos1.Text = "";
        }

        protected void btnEnviar_Click(object sender, EventArgs e)
        {
            //insertar 
            int TipoID = Convert.ToInt32(DrTipoEmp.SelectedValue);
            int TipoGasto = Convert.ToInt32(DrTipoGas.SelectedValue);
            int TipoViaje = Convert.ToInt32(DrTipoViaje3.SelectedValue);
            string NombrePo = Convert.ToString(txtNombrePo.Text);
            string GastoV = Convert.ToString(txtGastoVuelo.Text);
            string GastoH = Convert.ToString(txtGastoHospedaje.Text);
            string GastoC = Convert.ToString(txtGastoComida.Text);
            string GastoO = Convert.ToString(txtGastoOtro.Text);
            string GastoT = Convert.ToString(lblTotalGastos1.Text);
            string GastoTe = Convert.ToString(txtGastoTerrestre.Text);
            string GastoR = Convert.ToString(txtGastoCurso.Text);

            if (TipoViaje == 1 || TipoViaje == 3)
            {
                conexion.Open();
                string query = "insert into TBL_Interno (Nombre_Politica,Gasto_vuelo,Gasto_hospedaje,Gasto_comida,Gasto_otros,Total_Gastos,Tipo_Viaje,Tipo_Empleado,Gasto_terrestre,Gasto_Renta,Tipo_gasto) values ('" + NombrePo + "','" + GastoV + "','" + GastoH + "','" + GastoC + "','" + GastoO + "','" + GastoT + "','" + TipoViaje + "','" + TipoID + "','" + GastoTe + "','" + GastoR + "',"+TipoGasto+")";
                SqlCommand InsertarPol = new SqlCommand(query, conexion);
                InsertarPol.ExecuteNonQuery();
                conexion.Close();
            }
            else
            {
                conexion.Open();
                string query = "insert into TBL_Externo (Nombre_Politica,Gasto_vuelo,Gasto_hospedaje,Gasto_comida,Gasto_otros,Total_Gastos,Tipo_Viaje,Tipo_Empleado,Gasto_terrestre,Gasto_Renta,Tipo_gasto) values ('" + NombrePo + "','" + GastoV + "','" + GastoH + "','" + GastoC + "','" + GastoO + "','" + GastoT + "','" + TipoViaje + "','" + TipoID + "','" + GastoTe + "','" + GastoR + "',"+TipoGasto+")";
                SqlCommand InsertarPol = new SqlCommand(query, conexion);
                InsertarPol.ExecuteNonQuery();
                conexion.Close();
            }
            txtNombrePo.Text = "";
            DrTipoEmpMe();
            DrTipViajeMe();
            DrTipoGastoMe();
            txtGastoVuelo.Text = "";
            txtGastoHospedaje.Text = "";
            txtGastoTerrestre.Text = "";
            txtGastoComida.Text = "";
            txtGastoCurso.Text = "";
            txtGastoOtro.Text = "";
            lblTotalGastos1.Text = "";
        }

        protected void Resultado_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            AgregarGridMe();
            Resultado.PageIndex = e.NewPageIndex;
            Resultado.DataBind();
        }
    }
}
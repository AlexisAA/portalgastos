﻿using PortalGastos.Portal;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PortalGastos.PaginaMuestra
{
    public partial class Site1 : System.Web.UI.MasterPage
    {

        SqlConnection conexion = new SqlConnection("Data source=(local); Initial Catalog=PortalGastos; User=sae; Password=sae ");
        protected void Page_Load(object sender, EventArgs e)
        {
            int rol = 0;
            try
            {
                rol = (int)Session["rol"];
            }
            catch (Exception)
            {

            }
            string pageName = Path.GetFileName(Request.Path);
            if (rol == 1)
            {
                string menu = "";
                menu = "<nav class='navegacion' id='Menu1'>";
                menu += "<ul class='menu'>";
                menu += "<li><a href='#'>Menú</a>";
                menu += "<ul class='submenu active' style='float:left;'>";
                menu += "<li class='"+Cambiarclase("CrearUsuario.aspx",pageName)+"'><a href='../Administradordesistema/CrearUsuario.aspx'>Crear usuario</a></li>";
                menu += "<li class='"+Cambiarclase("ModificarUsuario.aspx",pageName)+"'><a href='../Administradordesistema/ModificarUsuario.aspx'>Modificar usuario</a></li>";
                menu += "<li class='" + Cambiarclase("CambioContraseña.aspx", pageName) + "'><a href='../Portal/CambioContraseña.aspx'>Cambio contraseña</a></li>";
                menu += "<li class='" + Cambiarclase("CerrarSesion.aspx", pageName) + "'><a href='../Portal/CerrarSesion.aspx'>Cerrar sesión</a></li>";
                menu += "</ul>";
                menu += "</li>";
                menu += "</ul>";
                menu += "</nav>";
                this.LitlMenu.Text = menu;
                
            }
            if (rol == 2)
            {
                string menu = "";
                menu = "<nav class='navegacion' id='Menu1'>";
                menu += "<ul class='menu'>";
                menu += "<li><a href='#'>Menú</a>";
                menu += "<ul class='submenu active' style='float:left;'>";
                menu += "<li class='" + Cambiarclase("Politicas.aspx", pageName) + "'><a href='../Administradordeusuario/Politicas.aspx'>Políticas</a></li>";
                menu += "<li class='" + Cambiarclase("Solictudes1.aspx", pageName) + "'><a href='../Administradordeusuario/Solictudes1.aspx'>Solicitudes</a></li>";
                menu += "<li class='" + Cambiarclase("VistadeArchivos.aspx", pageName) + "'><a href='../Administradordeusuario/VistadeArchivos.aspx'>Comproación gastos</a></li>";
                menu += "<li class='" + Cambiarclase("CambioContraseña.aspx", pageName) + "'><a href='../Portal/CambioContraseña.aspx'>Cambio contraseña</a></li>";
                menu += "<li class='" + Cambiarclase("CerrarSesion.aspx", pageName) + "'><a href='../Portal/CerrarSesion.aspx'>Cerrar sesión</a></li>";
                menu += "</ul>";
                menu += "</li>";
                menu += "</ul>";
                menu += "</nav>";
                this.LitlMenu.Text = menu;
            }
            if (rol == 3)
            {
                string menu = "";
                menu = "<nav class='navegacion' id='Menu1'>";
                menu += "<ul class='menu'>";
                menu += "<li><a href='#'>Menú</a>";
                menu += "<ul class='submenu active' style='float:left;'>";
                menu += "<li class='" + Cambiarclase("Solicitudes.aspx", pageName) + "'><a href='../UsuarioTrabajador/Solicitudes.aspx'>Solicitudes</a></li>";
                menu += "<li class='" + Cambiarclase("ComprobacionGastos.aspx", pageName) + "'><a href='../UsuarioTrabajador/ComprobacionGastos.aspx'>Comprobación de gastos</a></li>";
                menu += "<li class='" + Cambiarclase("CambioContraseña.aspx", pageName) + "'><a href='../Portal/CambioContraseña.aspx'>Cambio contraseña</a></li>";
                menu += "<li class='" + Cambiarclase("CerrarSesion.aspx", pageName) + "'><a href='../Portal/CerrarSesion.aspx'>Cerrar sesión</a></li>";
                menu += "</ul>";
                menu += "</li>";
                menu += "</ul>";
                menu += "</nav>";
                this.LitlMenu.Text = menu;
            }
            if (rol == 4)
            {
                string menu = "";
                menu = "<nav class='navegacion' id='Menu1'>";
                menu += "<ul class='menu'>";
                menu += "<li><a href='#'>Menú</a>";
                menu += "<ul class='submenu active' style='float:left;'>";
                menu += "<li class='" + Cambiarclase("SolicitudesPendientes.aspx", pageName) + "'><a href='../UsuarioGerente/SolicitudesPendientes.aspx'>Solicitudes pendientes</a></li>";
                menu += "<li class='" + Cambiarclase("GastosTrabajador.aspx", pageName) + "'><a href='../UsuarioGerente/GastosTrabajador.aspx'>Gastos por trabajador</a></li>";
                menu += "<li class='" + Cambiarclase("CambioContraseña.aspx", pageName) + "'><a href='../Portal/CambioContraseña.aspx'>Cambio contraseña</a></li>";
                menu += "<li class='" + Cambiarclase("CerrarSesion.aspx", pageName) + "'><a href='../Portal/CerrarSesion.aspx'>Cerrar sesión</a></li>";
                menu += "</ul>";
                menu += "</li>";
                menu += "</ul>";
                menu += "</nav>";
                this.LitlMenu.Text = menu;
            }
            if (rol == 5)
            {
                string menu = "";
                menu = "<nav class='navegacion' id='Menu1'>";
                menu += "<ul class='menu'>";
                menu += "<li><a href='#'>Menú</a>";
                menu += "<ul class='submenu active' style='float:left;'>";
                menu += "<li class='" + Cambiarclase("SolicitudesPendientes1.aspx", pageName) + "'><a href='../UsuarioDirector/SolicitudesPendientes1.aspx'>Solicitudes pendientes</a></li>";
                menu += "<li class='" + Cambiarclase("GastoArea.aspx", pageName) + "'><a href='../UsuarioDirector/GastoArea.aspx'>Gastos por área</a></li>";
                menu += "<li class='" + Cambiarclase("CambioContraseña.aspx", pageName) + "'><a href='../Portal/CambioContraseña.aspx'>Cambio contraseña</a></li>";
                menu += "<li class='" + Cambiarclase("CerrarSesion.aspx", pageName) + "'><a href='../Portal/CerrarSesion.aspx'>Cerrar sesión</a></li>";
                menu += "</ul>";
                menu += "</li>";
                menu += "</ul>";
                menu += "</nav>";
                this.LitlMenu.Text = menu;
            }
        }
        protected string Cambiarclase(string hard, string namepage)
        {
            string classe = "";
                if (hard == namepage)
                {
                    classe = "Activeli";
                }
                else
                {
                    classe = "comun";
                }
            return classe;
        }
        public void RevisionU(string _usuario)
        {
            string registro = "";
            int co1 = 0;
            List<string> Lista = new List<string>();
            List<string> ListaR = new List<string>();
            conexion.Open();
            string squery = "select Rol from TBL_Usuarios where Usuario='" + _usuario + "'";
            SqlCommand comando = new SqlCommand(squery, conexion);
            SqlDataReader registros = comando.ExecuteReader();
            while (registros.Read())
            {
                registro = registros[0].ToString();
                Lista.Add(registro);
                co1 = Int32.Parse(registro);
            }
            conexion.Close();
            if (co1 == 1)
            {
                HttpContext.Current.Response.Redirect("../Administradordesistema/AdminSistema.aspx");
            }
            if (co1 == 2)
            {
                HttpContext.Current.Response.Redirect("../Administradordeusuario/AdminUsuario.aspx");
            }
            if (co1 == 3)
            {
                HttpContext.Current.Response.Redirect("../UsuarioTrabajador/UsuarioTrabajador.aspx");
            }
            if (co1 == 4)
            {
                HttpContext.Current.Response.Redirect("../UsuarioGerente/UsuarioGerente.aspx");
            }
            if (co1 == 5)
            {
                HttpContext.Current.Response.Redirect("../UsuarioDirector/UsuarioDirector.aspx");
            }
        }
        
    }
}
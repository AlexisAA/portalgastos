﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Mail;
using System.Net.Mime;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Windows.Forms;

namespace PortalGastos.UsuarioTrabajador
{
    public partial class Solicitudes : System.Web.UI.Page
    {
        SqlConnection conexion = new SqlConnection("Data source=(local); Initial Catalog=PortalGastos; User=sae; Password=sae ");
        protected void Page_Init(object sender, EventArgs e)
        {
            if (Session["user"] == null)
            {
                HttpContext.Current.Response.Redirect("~/Portal/Login.aspx");
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                txtEmpeladoNo.Text = Session["ID_User"].ToString();
                DrEstatusMe();
                DrNoSolicitudMe();
                DrFechaMe();
                DrTipoGastoMe();
                DrTipoViajeMe();
                AgregarGrid();
            }
        }
        private void DrEstatusMe()
        {
            DrEstatus.DataSource = Consultar("Select * from TBL_Estatus");
            DrEstatus.DataTextField = "Estatus";
            DrEstatus.DataValueField = "ID_Estatus";
            DrEstatus.DataBind();
            DrEstatus.Items.Insert(0, new ListItem("Seleccionar..", "0"));
        }
        private void DrNoSolicitudMe()
        {
            DrLugar.DataSource = Consultar("select * from TBL_Solicitud");
            DrLugar.DataTextField = "Lugar_Destino";
            DrLugar.DataValueField = "ID_Solicitud";
            DrLugar.DataBind();
            DrLugar.Items.Insert(0, new ListItem("Seleccionar..", "0"));
        }
        private void DrFechaMe()
        {
            DrFecha.DataSource = Consultar("select ID_Solicitud,CONVERT (varchar,TBL_Solicitud.Fecha_Inicio,111) as Fecha_Inicio from TBL_Solicitud");
            DrFecha.DataTextField = "Fecha_Inicio";
            DrFecha.DataValueField = "Fecha_Inicio";
            DrFecha.DataBind();
            DrFecha.Items.Insert(0, new ListItem("Seleccionar..", "0"));
        }
        private void DrTipoViajeMe()
        {
            DrTipoViaje.DataSource = Consultar("Select * from TBL_Tipo_Viaje");
            DrTipoViaje.DataTextField = "Nombre";
            DrTipoViaje.DataValueField = "ID_Tipo";
            DrTipoViaje.DataBind();
            DrTipoViaje.Items.Insert(0, new ListItem("Seleccionar..", "0"));
        }
        private void DrTipoGastoMe()
        {
            DrTipoGasto.DataSource = Consultar("Select * from TBL_TipoGasto");
            DrTipoGasto.DataTextField = "Tipo_Gasto";
            DrTipoGasto.DataValueField = "ID_TipoGasto";
            DrTipoGasto.DataBind();
            DrTipoGasto.Items.Insert(0, new ListItem("Seleccionar..", "0"));
        }
        public DataSet Consultar(string strSQL)
        {
            string conexion = "Data source=(local); Initial Catalog=PortalGastos; User=sae; Password=sae";
            SqlConnection con = new SqlConnection(conexion);
            con.Open();
            SqlCommand cmd = new SqlCommand(strSQL, con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            con.Close();
            return ds;
        }

        protected void EstatusSeleccionado(object sender, EventArgs e)
        {   
        }

        protected void NumeroSulicitud(object sender, EventArgs e)
        {
        }
        protected void ConsulMe()
        {
            int ES = 0, LD = 0, FE = 0;
            string EST = "", LDE = "", FEC = "", Fech = "", con1 = "";
            List<string> Lista = new List<string>();
            Fech = Convert.ToString(DrFecha.SelectedItem);
            ES = Convert.ToInt32(DrEstatus.SelectedValue);
            LD = Convert.ToInt32(DrLugar.SelectedValue);
            FE = Convert.ToInt32(DrFecha.Text.Length);

            if (ES != 0 && LD != 0 && FE != 0)
            {
                conexion.Open();
                string query = "select * from TBL_Estatus where ID_Estatus =" + ES;
                SqlCommand SelecEs = new SqlCommand(query, conexion);
                SqlDataReader registro = SelecEs.ExecuteReader();
                if (registro.Read())
                {
                    EST = Convert.ToString(registro["ID_Estatus"]);
                }
                conexion.Close();
                conexion.Open();
                string query2 = "select * from TBL_Solicitud where ID_Solicitud = " + LD;
                SqlCommand selectLD = new SqlCommand(query2, conexion);
                SqlDataReader registro1 = selectLD.ExecuteReader();
                if (registro1.Read())
                {
                    LDE = Convert.ToString(registro1["Lugar_Destino"]);
                }
                conexion.Close();
                conexion.Open();
                string query3 = "select * from TBL_Solicitud where ID_Solicitud =" + FE;
                SqlCommand selectFe = new SqlCommand(query3, conexion);
                SqlDataReader registro2 = selectFe.ExecuteReader();
                if (registro2.Read())
                {
                    FEC = Convert.ToString(registro2["Fecha_Inicio"]);
                }
                conexion.Close();
                int tamaño = 10;
                int tamañoRe = FEC.Length;
                for (int i = 0; i < tamañoRe; i += tamaño)
                {
                    if (i + tamaño > tamañoRe) tamaño = tamañoRe - i;
                    Fech = FEC.Substring(i, tamaño);
                    if (i == 0)
                    {
                        break;
                    }
                }
                conexion.Open();
                string ID_soli = "", FI = "", FF = "", LDe = "", PE = "", TSo = "", ESTA = "", NOM = "", TG = "", EMP = "";
                string query5 = "select * from v_Solicitudes" +
                    "where Estatus= " + EST + " and Lugar_Destino='" + LDE + "' " +
                    "and Fecha_Inicio = '" + Fech + "'";
                SqlCommand selectA = new SqlCommand(query5, conexion);
                SqlDataReader registro3 = selectA.ExecuteReader();
                if (registro3.Read())
                {
                    EMP = Convert.ToString(registro3["Empleado"]);
                    ID_soli = Convert.ToString(registro3["ID_Solicitud"]);
                    FI = Convert.ToString(registro3["Fecha_Inicio"]);
                    FF = Convert.ToString(registro3["Fecha_Fin"]);
                    LDe = Convert.ToString(registro3["Lugar_Destino"]);
                    PE = Convert.ToString(registro3["Periodo"]);
                    TSo = Convert.ToString(registro3["Total_solicitado"]);
                    ESTA = Convert.ToString(registro3["Estatus"]);
                    NOM = Convert.ToString(registro3["Tipo_Viaje"]);
                    TG = Convert.ToString(registro3["Tipo_Gasto"]);
                }
                conexion.Close();
                DataTable dt = new DataTable();
                dt.Columns.AddRange(new DataColumn[10] {new DataColumn("Empleado",typeof(int)),
                                                    new DataColumn("No. de Solicitud",typeof(int)),
                                                    new DataColumn("Fecha Inicio",typeof(string)),
                                                    new DataColumn("Fecha Fin",typeof(string)),
                                                    new DataColumn("Lugar de Destino",typeof(string)),
                                                    new DataColumn("Período",typeof(string)),
                                                    new DataColumn("Total Solicitado",typeof(string)),
                                                    new DataColumn("Estatus",typeof(string)),
                                                    new DataColumn("Tipo de Viaje",typeof(string)),
                                                    new DataColumn("Tipo de Gasto",typeof(string))});
                dt.Rows.Add(EMP, ID_soli, FI, FF, LDe, PE, "$" + TSo, ESTA, NOM, TG);
                Resultado.DataSource = dt;
                Resultado.DataBind();
            }
            if (ES != 0 && LD == 0 && FE == 1 )
            {
                DataTable dt = new DataTable();
                dt.Columns.AddRange(new DataColumn[10] {new DataColumn("Empleado",typeof(int)),
                                                    new DataColumn("No. de Solicitud",typeof(int)),
                                                    new DataColumn("Fecha Inicio",typeof(string)),
                                                    new DataColumn("Fecha Fin",typeof(string)),
                                                    new DataColumn("Lugar Destino",typeof(string)),
                                                    new DataColumn("Período",typeof(string)),
                                                    new DataColumn("Total Solicitado",typeof(string)),
                                                    new DataColumn("Estatus",typeof(string)),
                                                    new DataColumn("Tipo de Viaje",typeof(string)),
                                                    new DataColumn("Tipo de Gasto",typeof(string))});
                conexion.Open();
                string query = "select * from TBL_Estatus where ID_Estatus =" + ES;
                SqlCommand SelecEs = new SqlCommand(query, conexion);
                SqlDataReader registro = SelecEs.ExecuteReader();
                if (registro.Read())
                {
                    EST = Convert.ToString(registro["Estatus"]);
                }
                conexion.Close();
                conexion.Open();
                string ID_soli = "", FI = "", FF = "", LDe = "", PE = "", TSo = "", ESTA = "", NOM = "", TG = "", EMP = "";
                string query5 = "select * from v_Solicitudes where Estatus= '" + EST + "'";
                SqlCommand selectA = new SqlCommand(query5, conexion);
                SqlDataReader registro3 = selectA.ExecuteReader();
                while (registro3.Read())
                {
                    con1 = registro3[0].ToString();
                    Lista.Add(con1);
                    for (int i = 0; i < Lista.Count; i++)
                    {
                        EMP = Convert.ToString(registro3["Empleado"]);
                        ID_soli = Convert.ToString(registro3["ID_Solicitud"]);
                        FI = Convert.ToString(registro3["Fecha_Inicio"]);
                        FF = Convert.ToString(registro3["Fecha_Fin"]);
                        LDe = Convert.ToString(registro3["Lugar_Destino"]);
                        PE = Convert.ToString(registro3["Periodo"]);
                        TSo = Convert.ToString(registro3["Total_solicitado"]);
                        ESTA = Convert.ToString(registro3["Estatus"]);
                        NOM = Convert.ToString(registro3["Tipo_Viaje"]);
                        TG = Convert.ToString(registro3["Tipo_Gasto"]);
                    }
                    dt.Rows.Add(EMP, ID_soli, FI, FF, LDe, PE, "$" + TSo, ESTA, NOM, TG);
                    Resultado.DataSource = dt;
                    Resultado.DataBind();
                }
                conexion.Close();
            }
            if (ES==0 && LD!=0 && FE==1 )
            {
                DataTable dt = new DataTable();
                dt.Columns.AddRange(new DataColumn[10] {new DataColumn("Empleado",typeof(int)),
                                                    new DataColumn("No. de Solicitud",typeof(int)),
                                                    new DataColumn("Fecha Inicio",typeof(string)),
                                                    new DataColumn("Fecha Fin",typeof(string)),
                                                    new DataColumn("Lugar de Destino",typeof(string)),
                                                    new DataColumn("Período",typeof(string)),
                                                    new DataColumn("Total Solicitado",typeof(string)),
                                                    new DataColumn("Estatus",typeof(string)),
                                                    new DataColumn("Tipo de Viaje",typeof(string)),
                                                    new DataColumn("Tipo de Gasto",typeof(string))});
                conexion.Open();
                string query2 = "select * from TBL_Solicitud where ID_Solicitud = " + LD;
                SqlCommand selectLD = new SqlCommand(query2, conexion);
                SqlDataReader registro1 = selectLD.ExecuteReader();
                if (registro1.Read())
                {
                    LDE = Convert.ToString(registro1["Lugar_Destino"]);
                }
                conexion.Close();
                conexion.Open();
                string ID_soli = "", FI = "", FF = "", LDe = "", PE = "", TSo = "", ESTA = "", NOM = "", TG = "", EMP = "";
                string query5 = "select * from v_Solicitudes where Lugar_Destino='"+ LDE + "' ";
                SqlCommand selectA = new SqlCommand(query5, conexion);
                SqlDataReader registro3 = selectA.ExecuteReader();
                while (registro3.Read())
                {
                    con1 = registro3[0].ToString();
                    Lista.Add(con1);
                    for (int i = 0; i < Lista.Count; i++)
                    {
                        EMP = Convert.ToString(registro3["Empleado"]);
                        ID_soli = Convert.ToString(registro3["ID_Solicitud"]);
                        FI = Convert.ToString(registro3["Fecha_Inicio"]);
                        FF = Convert.ToString(registro3["Fecha_Fin"]);
                        LDe = Convert.ToString(registro3["Lugar_Destino"]);
                        PE = Convert.ToString(registro3["Periodo"]);
                        TSo = Convert.ToString(registro3["Total_solicitado"]);
                        ESTA = Convert.ToString(registro3["Estatus"]);
                        NOM = Convert.ToString(registro3["Tipo_Viaje"]);
                        TG = Convert.ToString(registro3["Tipo_Gasto"]);
                    }
                    dt.Rows.Add(EMP, ID_soli, FI, FF, LDe, PE, "$" + TSo, ESTA, NOM, TG);
                    Resultado.DataSource = dt;
                    Resultado.DataBind();
                }
                conexion.Close();
            }

            if (ES==0 && LD==0 && FE!=1)
            {
                DataTable dt = new DataTable();
                dt.Columns.AddRange(new DataColumn[10] {new DataColumn("Empleado",typeof(int)),
                                                    new DataColumn("No. de Solicitud",typeof(int)),
                                                    new DataColumn("Fecha Inicio",typeof(string)),
                                                    new DataColumn("Fecha Fin",typeof(string)),
                                                    new DataColumn("Lugar de Destino",typeof(string)),
                                                    new DataColumn("Periodo",typeof(string)),
                                                    new DataColumn("Total Solicitado",typeof(string)),
                                                    new DataColumn("Estatus",typeof(string)),
                                                    new DataColumn("Tipo de Viaje",typeof(string)),
                                                    new DataColumn("Tipo de Gasto",typeof(string))});
                conexion.Open();
                string ID_soli = "", FI = "", FF = "", LDe = "", PE = "", TSo = "", ESTA = "", NOM = "", TG = "", EMP = "";
                string query5 = "select * from v_Solicitudes where Fecha_Inicio = '" + Fech + "'"; 
                SqlCommand selectA = new SqlCommand(query5, conexion);
                SqlDataReader registro3 = selectA.ExecuteReader();
                if (registro3.Read())
                {
                    con1 = registro3[0].ToString();
                    Lista.Add(con1);
                    for (int i = 0; i < Lista.Count; i++)
                    {
                        EMP = Convert.ToString(registro3["Empleado"]);
                        ID_soli = Convert.ToString(registro3["ID_Solicitud"]);
                        FI = Convert.ToString(registro3["Fecha_Inicio"]);
                        FF = Convert.ToString(registro3["Fecha_Fin"]);
                        LDe = Convert.ToString(registro3["Lugar_Destino"]);
                        PE = Convert.ToString(registro3["Periodo"]);
                        TSo = Convert.ToString(registro3["Total_solicitado"]);
                        ESTA = Convert.ToString(registro3["Estatus"]);
                        NOM = Convert.ToString(registro3["Tipo_Viaje"]);
                        TG = Convert.ToString(registro3["Tipo_Gasto"]);
                    }
                    dt.Rows.Add(EMP, ID_soli, FI, FF, LDe, PE, "$" + TSo, ESTA, NOM, TG);
                    Resultado.DataSource = dt;
                    Resultado.DataBind();
                }
                conexion.Close();   
            }
        }


        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            ConsulMe();
        }
        protected void AgregarGrid()
        {
            string ID_User = Session["ID_User"].ToString();
            List<string> Lista = new List<string>();
            string  con1 = "", FecIn = "",FecFn = "", LDe = "", Per = "",ToS = "",Est="",TiV="",TiG="";
            int NomSol = 0, Emp = 0;
            DataTable dt = new DataTable();
            dt.Columns.AddRange(new DataColumn[11] {new DataColumn("Empleado",typeof(int)),
                                                    new DataColumn("No. de Solicitud",typeof(int)),
                                                    new DataColumn("Fecha Inicio",typeof(string)),
                                                    new DataColumn("Fecha Fin",typeof(string)),
                                                    new DataColumn("Lugar de Destino",typeof(string)),
                                                    new DataColumn("Periodo",typeof(string)),
                                                    new DataColumn("Total Solicitado",typeof(string)),
                                                    new DataColumn("Estatus",typeof(string)),
                                                    new DataColumn("Tipo de Viaje",typeof(string)),
                                                    new DataColumn("Tipo de Gasto",typeof(string)),
                                                    new DataColumn("Fecha Limite",typeof(string))});
            conexion.Open();
            string query = "select * from v_Solicitudes where Empleado =" + ID_User + " order by ID_Solicitud desc";
            SqlCommand Solicitud = new SqlCommand(query,conexion);
            SqlDataReader regSol = Solicitud.ExecuteReader();
            while (regSol.Read())
            {
                con1 = regSol[0].ToString();
                Lista.Add(con1);
                for (int i = 0; i < Lista.Count; i++)
                {
                    Emp = Convert.ToInt32(regSol["Empleado"]);
                    NomSol = Convert.ToInt32(regSol["ID_Solicitud"]);
                    FecIn = Convert.ToString(regSol["Fecha_Inicio"]);
                    FecFn = Convert.ToString(regSol["Fecha_Fin"]);
                    LDe = Convert.ToString(regSol["Lugar_Destino"]);
                    Per = Convert.ToString(regSol["Periodo"]);
                    ToS = Convert.ToString(regSol["Total_solicitado"]);
                    Est = Convert.ToString(regSol["Estatus"]);
                    TiV = Convert.ToString(regSol["Tipo_Viaje"]);
                    TiG = Convert.ToString(regSol["Tipo_Gasto"]);
                }
                DateTime dtAfter = new DateTime();
                DateTime dtAfter1 = new DateTime();
                dtAfter = Convert.ToDateTime(FecFn);
                dtAfter1 = dtAfter.AddDays(15.0); 
                dt.Rows.Add(Emp, NomSol, FecIn, FecFn, LDe, Per +" días", "$"+ decimal.Parse(ToS).ToString("N") , Est, TiV, TiG, dtAfter1.ToString("yyyy/MM/dd"));
                Resultado.DataSource = dt;
                Resultado.DataBind();
            }
            conexion.Close();
        }

        protected void Resultado_SelectedIndexChanged1(object sender, EventArgs e)
        {
            string NoEmp = "", FecIn = "", FecFn = "", LDes = "", Per = "", GVu = "", GHo = "", GTe = "", GCo = "", GCu = "", OG = "", TSol = "",IDSoli ="",Estatus="", TiV = "", TiG = "";
            string PrN = "", ApP = "", PrN1 = "", ApP1 = "",Cliente = "";
            int Esta = 0;
            GridViewRow row = Resultado.SelectedRow;
            int id = Convert.ToInt32(Resultado.DataKeys[row.RowIndex].Value);
            conexion.Open();
            string query = "select * from v_SolicitudG " +
                "where ID_Solicitud = " + id;
            SqlCommand SelecEs = new SqlCommand(query, conexion);
            SqlDataReader registro = SelecEs.ExecuteReader();
            if (registro.Read())
            {
                IDSoli = Convert.ToString(registro["ID_Solicitud"]); 
                Esta = Convert.ToInt32(registro["ID_Estatus"]);
                NoEmp = Convert.ToString(registro["Empleado"]);
                FecIn = Convert.ToString(registro["Fecha_Inicio"]);
                FecFn = Convert.ToString(registro["Fecha_Fin"]);
                LDes = Convert.ToString(registro["Lugar_Destino"]);
                Per = Convert.ToString(registro["Periodo"]);
                GVu = Convert.ToString(registro["Gasto_vuelo"]);
                GHo = Convert.ToString(registro["Gasto_hospedaje"]);
                GTe = Convert.ToString(registro["Gasto_terrestre"]);
                GCo = Convert.ToString(registro["Gasto_comida"]);
                GCu = Convert.ToString(registro["Gasto_Renta"]);
                OG = Convert.ToString(registro["Gasto_otros"]);
                TSol = Convert.ToString(registro["Total_solicitado"]);
                TiV = Convert.ToString(registro["Tipo_Viaje"]);
                TiG = Convert.ToString(registro["Tipo_gasto"]);
                Cliente = Convert.ToString(registro["Cliente"]);
            }
            conexion.Close();
            conexion.Open();
            string conex = "select * from TBL_Estatus where ID_Estatus = " + Esta;
            SqlCommand command = new SqlCommand(conex, conexion);
            SqlDataReader reader = command.ExecuteReader();
            if (reader.Read())
            {
                Estatus = Convert.ToString(reader["Estatus"]);
            }
            conexion.Close();
            conexion.Open();
            string autorizador1 = "select * from v_AutorizadorEstatus1 where Empleado ="+ NoEmp;
            SqlCommand select = new SqlCommand(autorizador1,conexion);
            SqlDataReader selectA = select.ExecuteReader();
            if (selectA.Read())
            {
                PrN = Convert.ToString(selectA["Primer_nombre"]);
                ApP = Convert.ToString(selectA["Apellido_paterno"]);
            }
            conexion.Close();
            conexion.Open();
            string autorizador2 = "select * from v_AutorizadorEstatus2 where Empleado ="+ NoEmp;
            SqlCommand select1 = new SqlCommand(autorizador2,conexion);
            SqlDataReader selectA1 = select1.ExecuteReader();
            if (selectA1.Read())
            {
                PrN1 = Convert.ToString(selectA1["Primer_nombre"]);
                ApP1 = Convert.ToString(selectA1["Apellido_paterno"]);
            }
            conexion.Close();
            if (Estatus == "Enviada                                           ")
            {
                ImgFlujo.ImageUrl = "~/Imagenes/uno.png";
                ImgFlujo.ToolTip = "Solicitud enviada";
                lblFlujo1.Text = "Pendiente por autorizar: "+ PrN+" "+ApP;
            }
            if (Estatus == "Guardada                                          ")
            {
                ImgFlujo.ImageUrl = "~/Imagenes/cero.png";
                ImgFlujo.ToolTip = "Solicitud guardada";
                lblFlujo1.Text = "Solicitud pendiente";
            }
            if (Estatus == "Aprobada por Autorizador 1                        ")
            {
                ImgFlujo.ImageUrl = "~/Imagenes/dos.png";
                ImgFlujo.ToolTip = "Solicitud aceptada";
                lblFlujo1.Text = "Pendiente por autorizar: "+ PrN1 + " " + ApP1;
            }
            if (Estatus == "Aprobada por Autorizador 2                        ")
            {
                ImgFlujo.ImageUrl = "~/Imagenes/cero1.png";
                ImgFlujo.ToolTip = "Solicitud aceptada";
                lblFlujo1.Text = "Solicitud aceptada se puede realizar viaje";
            }
            if (Estatus == "Rechazada por autorizador 1                       ")
            {
                ImgFlujo.ImageUrl = "~/Imagenes/cuatro.png";
                ImgFlujo.ToolTip = "Solicitud rechazada";
                lblFlujo1.Text = "Solicitud rechazada: "+PrN + " " + ApP;
            }
            if (Estatus == "Rechazada por autorizador 2                       ")
            {
                ImgFlujo.ImageUrl = "~/Imagenes/cinco.png";
                ImgFlujo.ToolTip = "Solicitud rechazada";
                lblFlujo1.Text = "Solicitud rechazada: " + PrN1 + " " + ApP1;
            }
            if (Estatus == "Enviado a comprobacion de gastos                  ")
            {
                ImgFlujo.ImageUrl = "~/Imagenes/cero1.png";
                ImgFlujo.ToolTip = "Enviado a comprobación de gastos";
                lblFlujo1.Text = "Comprobar gastos";
            }
            if (Estatus == "Completo                                          ")
            {
                ImgFlujo.ImageUrl = "~/Imagenes/ocho.png";
                ImgFlujo.ToolTip = "Cromprobación de gastos correcta";
                lblFlujo1.Text = "Comprobación de gastos finalizada.";
            }
            if (Estatus == "Rechazada comprobacion de gastos                  ")
            {
                ImgFlujo.ImageUrl = "~/Imagenes/siete.png";
                ImgFlujo.ToolTip = "Cromprobación de gastos rechazada";
                lblFlujo1.Text = "Comprobación de gastos rechazada revisar archivos";
            }
            if (Estatus == "Aceptada                                          ")
            {
                ImgFlujo.ImageUrl = "~/Imagenes/cero.png";
                ImgFlujo.ToolTip = "Solicitud aceptada";
                lblFlujo1.Text = "Solicitud enviada a comprobación de gastos";
            }
            if (Estatus== "Revision comprobacion de gastos                   ")
            {
                ImgFlujo.ImageUrl = "~/Imagenes/seis.png";
                ImgFlujo.ToolTip = "Revision realizada por administrador";
                lblFlujo1.Text = "Comprobacion en revision";
            }
            lblTitulo.Visible = false;
            Resultado.Visible = false;
            lblLugarDestino.Visible = false;
            lblEstatus.Visible = false;
            lblFecha.Visible = false;
            DrEstatus.Visible = false;
            DrFecha.Visible = false;
            DrLugar.Visible = false;
            btnBuscar.Visible = false;
            btnEnviarS.Visible = false;
            btnNuevo.Visible = false;

            lblCliente.Visible = true;
            txtCliente.Visible = true;
            lblFlujo.Visible = true;
            ImgFlujo.Visible = true;
            lblFlujo1.Visible = true;
            lblTipoViaje.Visible = true;
            DrTipoViaje.Visible = true;
            lblTipoGasto.Visible = true;
            DrTipoGasto.Visible = true;
            btnRegresar.Visible = true;
            lblSolicitudNo.Visible = true;
            lblSolicitudNo1.Visible = true;
            lblEstatuslbl.Visible = true;
            lblEstatuslbl1.Visible = true;
            lblTitulo2.Visible = true;
            lblEmpleadoNo.Visible = true;
            txtEmpeladoNo.Visible = true;
            lblFechaInicio.Visible = true;
            txtCalendar.Visible = true;
            ImgCalendar.Visible = true;
            Calendar.Visible = false;
            txtCalendar1.Visible = true;
            ImgCalendar1.Visible = true;
            Calendar1.Visible = false;
            lblFechaFin.Visible = true;
            lblCalcular.Visible = true;
            lblComida.Visible = true;
            lblFechaFin.Visible = true;
            lblFechaInicio.Visible = true;
            lblGastoCurso.Visible = true;
            lblGastoH.Visible = true;
            lblGastoOtro.Visible = true;
            lblGastoTerrestre.Visible = true;
            lblGastoV.Visible = true;
            lblPeriodo.Visible = true;
            lblTotalSolicitado.Visible = true;
            txtComida.Visible = true;
            txtEmpeladoNo.Visible = true;
            txtGastoCurso.Visible = true;
            txtGastoH.Visible = true;
            txtGastoOtro.Visible = true;
            txtGastoTerrestre.Visible = true;
            txtGastoV.Visible = true;
            txtLugardestino.Visible = true;
            txtPeriodo.Visible = true;
            btnEnviar.Visible = true;
            btnCalcular.Visible = true;
            lblDestino.Visible = true;
            if (Esta==1 || Esta==2 || Esta==3)
            {
                txtCalendar.Enabled = false;
                Calendar.Enabled = false;
                txtCalendar.Text = FecIn;
                txtCalendar1.Enabled = false;
                Calendar1.Enabled = false;
                txtCalendar1.Text = FecFn;
                txtComida.Enabled = false;
                txtComida.Text = "$ " + decimal.Parse(GCo).ToString("N");
                txtEmpeladoNo.Enabled = false;
                txtEmpeladoNo.Text = NoEmp;
                txtGastoCurso.Enabled = false;
                txtGastoCurso.Text = "$ "+ decimal.Parse(GCu).ToString("N"); 
                txtGastoH.Enabled = false;
                txtGastoH.Text = "$ " + decimal.Parse(GHo).ToString("N"); 
                txtGastoOtro.Enabled = false;
                txtGastoOtro.Text = "$ " + decimal.Parse(OG).ToString("N");
                txtGastoTerrestre.Enabled = false;
                txtGastoTerrestre.Text = "$ " + decimal.Parse(GTe).ToString("N"); 
                txtGastoV.Enabled = false;
                txtGastoV.Text = "$ " + decimal.Parse(GVu).ToString("N");
                txtLugardestino.Enabled = true;
                txtLugardestino.Text = LDes;
                txtPeriodo.Enabled = false;
                txtPeriodo.Text = Per+" días";
                lblEstatuslbl1.Text = Estatus;
                lblSolicitudNo1.Text = IDSoli;
                lblCalcular.Text = "$ " + decimal.Parse(TSol).ToString("N");
                //Aqui
                DrTipoViaje.SelectedValue = TiV;
                DrTipoGasto.SelectedValue = TiG;
            }
            else
            {
                txtCalendar.Enabled = true;
                Calendar.Enabled = true;
                txtCalendar.Text = FecIn;
                txtCalendar1.Enabled = true;
                Calendar1.Enabled = true;
                txtCalendar1.Text = FecFn;
                txtComida.Enabled = true;
                txtComida.Text = "$ " + decimal.Parse(GCo).ToString("N");
                txtEmpeladoNo.Enabled = false;
                txtEmpeladoNo.Text = NoEmp;
                txtGastoCurso.Enabled = true;
                txtGastoCurso.Text = "$ " + decimal.Parse(GCu).ToString("N");
                txtGastoH.Enabled = true;
                txtGastoH.Text = "$ " + decimal.Parse(GHo).ToString("N");
                txtGastoOtro.Enabled = true;
                txtGastoOtro.Text = "$ " + decimal.Parse(OG).ToString("N");
                txtGastoTerrestre.Enabled = true;
                txtGastoTerrestre.Text = "$ " + decimal.Parse(GTe).ToString("N");
                txtGastoV.Enabled = true;
                txtGastoV.Text = "$ " + decimal.Parse(GVu).ToString("N");
                txtLugardestino.Enabled = true;
                txtLugardestino.Text = LDes;
                txtPeriodo.Enabled = true;
                DrTipoViaje.SelectedValue = TiV;
                DrTipoGasto.SelectedValue = TiG;
                txtPeriodo.Text = Per+ " días";
                lblEstatuslbl1.Text = Estatus;
                lblSolicitudNo1.Text = IDSoli;
                lblCalcular.Text = "$ " + decimal.Parse( TSol).ToString("N");
            }
        }

        protected void ImgCalendar_Click(object sender, ImageClickEventArgs e)
        {
            if (Calendar.Visible)
            {
                Calendar.Visible = false;
            }
            else
            {
                Calendar.Visible = true;
            }
            Calendar.Attributes.Add("style", "position:absolute");
        }

        protected void Calendar_DayRender(object sender, DayRenderEventArgs e)
        {
            if (e.Day.IsOtherMonth)
            {
                e.Day.IsSelectable = false;
            }
        }

        protected void Calendar_SelectionChanged(object sender, EventArgs e)
        {
            txtCalendar.Text = Calendar.SelectedDate.ToString("yyyy/MM/dd");
            Calendar.Visible = false;
        }

        protected void ImgCalendar1_Click(object sender, ImageClickEventArgs e)
        {
            if (Calendar1.Visible)
            {
                Calendar1.Visible = false;
            }
            else
            {
                Calendar1.Visible = true;
            }
            Calendar1.Attributes.Add("style", "position:absolute");
        }

        protected void Calendar1_SelectionChanged(object sender, EventArgs e)
        {
            txtCalendar1.Text = Calendar1.SelectedDate.ToString("yyyy/MM/dd");
            Calendar1.Visible = false;
        }

        protected void btnEnviar_Click(object sender, EventArgs e)
        {
            ActualizarMe();
            txtEmpeladoNo.Text = Session["ID_User"].ToString();
        }
        protected void ActualizarMe()
        {
            GridViewRow row = Resultado.SelectedRow;
            int id = Convert.ToInt32(Resultado.DataKeys[row.RowIndex].Value);
            string FecIn1 = Convert.ToString(txtCalendar.Text);
            conexion.Open();
            string query1 = "update TBL_Solicitud set Fecha_Inicio = '" + FecIn1 + "' where ID_Solicitud = " + id;
            SqlCommand act = new SqlCommand(query1, conexion);
            act.ExecuteNonQuery();
            conexion.Close();
            string FecFn1 = Convert.ToString(txtCalendar1.Text);
            conexion.Open();
            string query2 = "update TBL_Solicitud set Fecha_Fin = '" + FecFn1 + "' where ID_Solicitud = " + id;
            SqlCommand act1 = new SqlCommand(query2, conexion);
            act1.ExecuteNonQuery();
            conexion.Close();
            string Gco1 = Convert.ToString(txtComida.Text);
            Gco1 = Gco1.Replace(",","");
            conexion.Open();
            string query3 = "update TBL_Solicitud set Gasto_comida = " + Gco1 + " where ID_Solicitud = " + id;
            SqlCommand act2 = new SqlCommand(query3, conexion);
            act2.ExecuteNonQuery();
            conexion.Close();
            string GCu1 = Convert.ToString(txtGastoCurso.Text);
            GCu1 = GCu1.Replace(",", "");
            conexion.Open();
            string query4 = "update TBL_Solicitud set Gasto_Renta = " + GCu1 + " where ID_Solicitud = " + id;
            SqlCommand act3 = new SqlCommand(query4, conexion);
            act3.ExecuteNonQuery();
            conexion.Close();
            string GHo1 = Convert.ToString(txtGastoH.Text);
            GHo1 = GHo1.Replace(",", "");
            conexion.Open();
            string query5 = "update TBL_Solicitud set Gasto_hospedaje = " + GHo1 + " where ID_Solicitud = " + id;
            SqlCommand act4 = new SqlCommand(query2, conexion);
            act4.ExecuteNonQuery();
            conexion.Close();
            string OG1 = Convert.ToString(txtGastoOtro.Text);
            OG1 = OG1.Replace(",", "");
            conexion.Open();
            string query6 = "update TBL_Solicitud set Gasto_otros = " + OG1 + " where ID_Solicitud = " + id;
            SqlCommand act5 = new SqlCommand(query6, conexion);
            act5.ExecuteNonQuery();
            conexion.Close();
            string GTe1 = Convert.ToString(txtGastoTerrestre.Text);
            GTe1 = GTe1.Replace(",", "");
            conexion.Open();
            string query7 = "update TBL_Solicitud set Gasto_terrestre = " + GTe1 + " where ID_Solicitud = " + id;
            SqlCommand act6 = new SqlCommand(query7, conexion);
            act6.ExecuteNonQuery();
            conexion.Close();
            string GVu1 = Convert.ToString(txtGastoV.Text);
            GVu1 = GVu1.Replace(",", "");
            conexion.Open();
            string query8 = "update TBL_Solicitud set Gasto_vuelo = " + GVu1 + " where ID_Solicitud = " + id;
            SqlCommand act7 = new SqlCommand(query8, conexion);
            act7.ExecuteNonQuery();
            conexion.Close();
            string LDes1 = Convert.ToString(txtLugardestino.Text);
            conexion.Open();
            string query9 = "update TBL_Solicitud set Lugar_Destino = '" + LDes1 + "' where ID_Solicitud = " + id;
            SqlCommand act8 = new SqlCommand(query9, conexion);
            act8.ExecuteNonQuery();
            conexion.Close();
            string Per1 = Convert.ToString(txtPeriodo.Text);
            string Per2 = Per1.Substring(0,1);
            conexion.Open();
            string query10 = "update TBL_Solicitud set Periodo = " + Per2 + " where ID_Solicitud = " + id;
            SqlCommand act9 = new SqlCommand(query10, conexion);
            act9.ExecuteNonQuery();
            conexion.Close();
            string TotalS = Convert.ToString(lblCalcular.Text);
            TotalS = TotalS.Replace(",","");
            conexion.Open();
            string query11 = "update TBL_Solicitud set Total_solicitado = " +TotalS + " where ID_Solicitud = "+id;
            SqlCommand act10 = new SqlCommand(query11, conexion);
            act10.ExecuteNonQuery();
            conexion.Close();
            conexion.Open();
            string query12 = "update TBL_Solicitud set Estatus = 1 where ID_Solicitud = "+id;
            SqlCommand act11 = new SqlCommand(query12, conexion);
            act11.ExecuteNonQuery();
            conexion.Close();
            txtCalendar.Text = "";
            txtCalendar1.Text = "";
            txtComida.Text = "";
            txtEmpeladoNo.Text = "";
            txtGastoCurso.Text = "";
            txtGastoH.Text = "";
            txtGastoOtro.Text = "";
            txtGastoTerrestre.Text = "";
            txtGastoV.Text = "";
            txtLugardestino.Text = "";
            txtPeriodo.Text = "";
            lblCalcular.Text = "";
            lblSoli.Text = "Solicitud actualizada";
        }

        protected void btnRegresar_Click(object sender, EventArgs e)
        {
            lblTitulo.Visible = true;
            Resultado.Visible = true;
            lblLugarDestino.Visible = true;
            lblEstatus.Visible = true;
            lblFecha.Visible = true;
            DrEstatus.Visible = true;
            DrFecha.Visible = true;
            DrLugar.Visible = true;
            btnNuevo.Visible = true;
            btnBuscar.Visible = true;

            lblCliente.Visible = false;
            txtCliente.Visible = false;
            lblFlujo.Visible = false;
            ImgFlujo.Visible = false;
            lblFlujo1.Visible = false;
            imgGuardar.Visible = false;
            lblTipoViaje.Visible = false;
            DrTipoViaje.Visible = false;
            lblTipoGasto.Visible = false;
            DrTipoGasto.Visible = false;
            lblSolicitudNo.Visible = false;
            lblSolicitudNo1.Visible = false;
            lblEstatuslbl.Visible = false;
            lblEstatuslbl1.Visible = false;
            btnRegresar.Visible = false;
            lblTitulo2.Visible = false;
            lblEmpleadoNo.Visible = false;
            txtEmpeladoNo.Visible = false;
            lblFechaInicio.Visible = false;
            txtCalendar.Visible = false;
            ImgCalendar.Visible = false;
            Calendar.Visible = false;
            txtCalendar1.Visible = false;
            ImgCalendar1.Visible = false;
            Calendar1.Visible = false;
            lblFechaFin.Visible = false;
            lblCalcular.Visible = false;
            lblComida.Visible = false;
            lblFechaFin.Visible = false;
            lblFechaInicio.Visible = false;
            lblGastoCurso.Visible = false;
            lblGastoH.Visible = false;
            lblGastoOtro.Visible = false;
            lblGastoTerrestre.Visible = false;
            lblGastoV.Visible = false;
            lblPeriodo.Visible = false;
            lblTotalSolicitado.Visible = false;
            txtComida.Visible = false;
            txtEmpeladoNo.Visible = false;
            txtGastoCurso.Visible = false;
            txtGastoH.Visible = false;
            txtGastoOtro.Visible = false;
            txtGastoTerrestre.Visible = false;
            txtGastoV.Visible = false;
            txtLugardestino.Visible = false;
            txtPeriodo.Visible = false;
            btnEnviar.Visible = false;
            btnCalcular.Visible = false;
            lblDestino.Visible = false;
            txtEmpeladoNo.Text = "";
            txtCalendar.Text = "";
            txtCalendar1.Text = "";
            lblSolicitudNo1.Text = "";
            lblEstatuslbl1.Text = "";
            txtLugardestino.Text = "";
            txtPeriodo.Text = "";
            txtGastoV.Text = "";
            txtGastoH.Text = "";
            txtGastoTerrestre.Text = "";
            txtComida.Text = "";
            txtGastoCurso.Text = "";
            txtGastoOtro.Text = "";
            lblCalcular.Text = "";
        }
        protected void CalcularMe()
        {
            string GV = Convert.ToString(txtGastoV.Text);
            string GH = Convert.ToString(txtGastoH.Text);
            string GT = Convert.ToString(txtGastoTerrestre.Text);
            string GC = Convert.ToString(txtComida.Text);
            string GR = Convert.ToString(txtGastoCurso.Text);
            string GO = Convert.ToString(txtGastoOtro.Text);

            string parte1 = GV.Remove(0, 2);
            string parte2 = GH.Remove(0, 2);
            string parte3 = GT.Remove(0, 2);
            string parte4 = GC.Remove(0, 2);
            string parte5 = GR.Remove(0, 2);
            string parte6 = GO.Remove(0, 2);

            decimal Total = 0;
            decimal GV1 = 0;
            decimal GH1 = 0;
            decimal GT1 = 0;
            decimal GC1 = 0;
            decimal GR1 = 0;
            decimal GO1 = 0;

            int GV2 = Convert.ToInt32(txtGastoV.Text.Length);
            int GH2 = Convert.ToInt32(txtGastoH.Text.Length);
            int GT2 = Convert.ToInt32(txtGastoTerrestre.Text.Length);
            int GC2 = Convert.ToInt32(txtComida.Text.Length);
            int GR2 = Convert.ToInt32(txtGastoCurso.Text.Length);
            int GO2 = Convert.ToInt32(txtGastoOtro.Text.Length);

            if (GV2==0)
            {
                GV1 = 0;
            }
            else
            {
                GV1 = Convert.ToDecimal(parte1);
            }
            if (GH2==0)
            {
                GH1 = 0;
            }
            else
            {
                GH1 = Convert.ToDecimal(parte2);
            }
            if (GT2==0)
            {
                GT1 = 0;
            }
            else
            {
                GT1 = Convert.ToDecimal(parte3);
            }
            if (GC2==0)
            {
                GC1 = 0;
            }
            else
            {
                GC1 = Convert.ToDecimal(parte4);
            }
            if (GR2==0)
            {
                GR1 = 0;
            }
            else
            {
                GR1 = Convert.ToDecimal(parte5);
            }
            if (GO2==0)
            {
                GO1 = 0;
            }
            else
            {
                GO1 = Convert.ToDecimal(parte6);
            }
            Total = (GV1*2)+((GH1+GT1+GC1+GR1+GO1)*3);
            string Total1 = "" + Total;
            lblCalcular.Text = "$ " + decimal.Parse(Total1).ToString("N");
        }

        protected void btnCalcular_Click(object sender, EventArgs e)
        {
            CalcularMe();
        }

        protected void btnNuevo_Click(object sender, EventArgs e)
        {
            lblEstatusSol.Text = "Estatus";
            lblTitulo.Visible = false;
            Resultado.Visible = false;
            lblLugarDestino.Visible = false;
            lblEstatus.Visible = false;
            lblFecha.Visible = false;
            DrEstatus.Visible = false;
            DrFecha.Visible = false;
            DrLugar.Visible = false;
            btnBuscar.Visible = false;
            btnNuevo.Visible = false;
            txtEmpeladoNo.Text = Session["ID_User"].ToString();
            imgGuardar.Visible = true;
            lblEstatusSol.Visible = true;
            btnEnviarS.Visible = true;
            btnRegresar1.Visible = true;
            lblTitulo3.Visible = true;
            lblEmpleadoNo.Visible = true;
            txtEmpeladoNo.Visible = true;
            lblFechaInicio.Visible = true;
            txtCalendar.Visible = true;
            ImgCalendar.Visible = true;
            Calendar.Visible = false;
            txtCalendar1.Visible = true;
            ImgCalendar1.Visible = true;
            Calendar1.Visible = false;
            lblTipoViaje.Visible = true;
            DrTipoViaje.Visible = true;
            lblFechaFin.Visible = true;
            lblCalcular.Visible = true;
            lblComida.Visible = true;
            lblFechaFin.Visible = true;
            lblFechaInicio.Visible = true;
            lblGastoCurso.Visible = true;
            lblGastoH.Visible = true;
            lblGastoOtro.Visible = true;
            lblGastoTerrestre.Visible = true;
            lblGastoV.Visible = true;
            lblPeriodo.Visible = true;
            lblTotalSolicitado.Visible = true;
            txtComida.Visible = true;
            txtComida.Enabled = false;
            txtEmpeladoNo.Visible = true;
            txtGastoCurso.Visible = true;
            txtGastoCurso.Enabled = false;
            txtGastoH.Visible = true;
            txtGastoH.Enabled = false;
            txtGastoOtro.Visible = true;
            txtGastoOtro.Enabled = false;
            txtGastoTerrestre.Visible = true;
            txtGastoTerrestre.Enabled = false;
            txtGastoV.Visible = true;
            txtGastoV.Enabled = false;
            txtLugardestino.Visible = true;
            txtPeriodo.Visible = true;
            txtPeriodo.Enabled = false;
            lblDestino.Visible = true;
            lblTipoGasto.Visible = true;
            DrTipoGasto.Visible = true;
            lblCliente.Visible = true;
            txtCliente.Visible = true;
        }

        protected void btRegresar1_Click(object sender, EventArgs e)
        {
            lblTitulo.Visible = true;
            Resultado.Visible = true;
            lblLugarDestino.Visible = true;
            lblEstatus.Visible = true;
            lblFecha.Visible = true;
            DrEstatus.Visible = true;
            DrFecha.Visible = true;
            DrLugar.Visible = true;
            btnBuscar.Visible = true;
            btnNuevo.Visible = true;
            AgregarGrid();

            lblCliente.Visible = false;
            txtCliente.Visible = false;
            imgGuardar.Visible = false;
            btnEnviarS.Visible = false;
            lblEstatusSol.Visible = false;
            btnRegresar1.Visible = false;
            lblTitulo3.Visible = false;
            lblEmpleadoNo.Visible = false;
            txtEmpeladoNo.Visible = false;
            lblFechaInicio.Visible = false;
            txtCalendar.Visible = false;
            ImgCalendar.Visible = false;
            Calendar.Visible = false;
            txtCalendar1.Visible = false;
            ImgCalendar1.Visible = false;
            Calendar1.Visible = false;
            lblTipoViaje.Visible = false;
            DrTipoViaje.Visible = false;
            lblFechaFin.Visible = false;
            lblCalcular.Visible = false;
            lblComida.Visible = false;
            lblFechaFin.Visible = false;
            lblFechaInicio.Visible = false;
            lblGastoCurso.Visible = false;
            lblGastoH.Visible = false;
            lblGastoOtro.Visible = false;
            lblGastoTerrestre.Visible = false;
            lblGastoV.Visible = false;
            lblPeriodo.Visible = false;
            lblTotalSolicitado.Visible = false;
            txtComida.Visible = false;
            txtEmpeladoNo.Visible = false;
            txtGastoCurso.Visible = false;
            txtGastoH.Visible = false;
            txtGastoOtro.Visible = false;
            txtGastoTerrestre.Visible = false;
            txtGastoV.Visible = false;
            txtLugardestino.Visible = false;
            txtPeriodo.Visible = false;
            btnEnviar.Visible = false;
            lblDestino.Visible = false;
            lblTipoGasto.Visible = false;
            DrTipoGasto.Visible = false;
            txtEmpeladoNo.Text = "";
            txtCalendar.Text = "";
            txtCalendar1.Text = "";
            lblSolicitudNo1.Text = "";
            lblEstatuslbl1.Text = "";
            txtLugardestino.Text = "";
            txtPeriodo.Text = "";
            txtGastoV.Text = "";
            txtGastoH.Text = "";
            txtGastoTerrestre.Text = "";
            txtComida.Text = "";
            txtGastoCurso.Text = "";
            txtGastoOtro.Text = "";
            lblCalcular.Text = "";
        }

        protected void DrTipoViaje_SelectedIndexChanged(object sender, EventArgs e)
        {
            string dias1 = "";
            string script = "alert('Datos incorrectos, revisar datos')";
            string script1 = "alert('Fechas incorrectas revisar')";
            decimal Total = 0;
            decimal Gastov1 = 0, GastoH1 = 0, GastoTe1 = 0, GastoC1 = 0, GastoCu1 = 0, OtroGa1 = 0;
            decimal GastoV = 0, GastoH = 0, GastoTe = 0, GastoC = 0, GastoCu = 0, OtroGa = 0;
            string IDemp = "", sEm = "", GaV = "", GaH = "", GaC = "", GaO = "", GaT = "", GaTe = "", GaR = "";
            int TV = 0, TVi = 0;
            TV = Convert.ToInt32(DrTipoViaje.SelectedValue);
            IDemp = Convert.ToString(txtEmpeladoNo.Text);
            string fecha1 = Convert.ToString(txtCalendar.Text);
            string fecha2 = Convert.ToString(txtCalendar1.Text);
            if (fecha1.Length != 0 || fecha2.Length != 0)
            {
                DateTime FechaInicio = Convert.ToDateTime(txtCalendar.Text);
                DateTime FechaFin = Convert.ToDateTime(txtCalendar1.Text);
                TimeSpan dias = FechaFin.Subtract(FechaInicio);
                dias1 = dias.Days.ToString();
                int dias3 = Convert.ToInt32(dias1);
                if (dias3 >= 0)
                {
                    conexion.Open();
                    string query2 = "select * from TBL_Tipo_Viaje where ID_Tipo = " + TV;
                    SqlCommand selecTi = new SqlCommand(query2, conexion);
                    SqlDataReader registro = selecTi.ExecuteReader();
                    if (registro.Read())
                    {
                        TVi = Convert.ToInt32(registro["ID_Tipo"]);
                    }
                    conexion.Close();
                    if (TVi == 1 || TVi == 3)
                    {
                        conexion.Open();
                        string consulEm = "select * from TBL_Usuarios where ID_Usuario = " + IDemp;
                        SqlCommand selectEm = new SqlCommand(consulEm, conexion);
                        SqlDataReader slecEm = selectEm.ExecuteReader();
                        if (slecEm.Read())
                        {
                            sEm = Convert.ToString(slecEm["Tipo_empleado"]);
                        }
                        conexion.Close();
                        conexion.Open();
                        string selectVi = "select * from TBL_Interno where Tipo_Viaje= " + TVi + " and Tipo_Empleado = " + sEm;
                        SqlCommand selecIn = new SqlCommand(selectVi, conexion);
                        SqlDataReader selectInt = selecIn.ExecuteReader();
                        if (selectInt.Read())
                        {
                            GaV = Convert.ToString(selectInt["Gasto_vuelo"]);
                            GaH = Convert.ToString(selectInt["Gasto_hospedaje"]);
                            GaC = Convert.ToString(selectInt["Gasto_comida"]);
                            GaO = Convert.ToString(selectInt["Gasto_otros"]);
                            GaT = Convert.ToString(selectInt["Total_Gastos"]);
                            GaTe = Convert.ToString(selectInt["Gasto_terrestre"]);
                            GaR = Convert.ToString(selectInt["Gasto_Renta"]);
                        }
                        conexion.Close();
                        Gastov1 = Convert.ToDecimal(GaV);
                        if (Gastov1 == 0)
                        {
                            GastoV = 0;
                        }
                        else
                        {
                            GastoV = Gastov1;
                        }
                        GastoH1 = Convert.ToDecimal(GaH);
                        if (GastoH1 == 0)
                        {
                            GastoH = 0;
                        }
                        else
                        {
                            GastoH = GastoH1;
                        }
                        GastoTe1 = Convert.ToDecimal(GaTe);
                        if (GastoTe1 == 0)
                        {
                            GastoTe = 0;
                        }
                        else
                        {
                            GastoTe = GastoTe1;
                        }
                        GastoC1 = Convert.ToDecimal(GaC);
                        if (GastoC1 == 0)
                        {
                            GastoC = 0;
                        }
                        else
                        {
                            GastoC = GastoC1;
                        }
                        GastoCu1 = Convert.ToDecimal(GaR);
                        if (GastoCu1 == 0)
                        {
                            GastoCu = 0;
                        }
                        else
                        {
                            GastoCu = GastoCu1;
                        }
                        OtroGa1 = Convert.ToDecimal(GaO);
                        if (OtroGa1 == 0)
                        {
                            OtroGa = 0;
                        }
                        else
                        {
                            OtroGa = OtroGa1;
                        }
                        int dias2 = Convert.ToInt32(dias1);

                        Total = GastoV + ((GastoH + GastoTe + GastoC + GastoCu + OtroGa) * dias2);
                        lblCalcular.Text = "$ " + decimal.Parse(Total.ToString()).ToString("N");
                        txtPeriodo.Text = dias1 + " días";
                        txtGastoV.Text = "$ " + decimal.Parse(GaV).ToString("N");
                        txtGastoH.Text = "$ " + decimal.Parse(GaH).ToString("N");
                        txtComida.Text = "$ " + decimal.Parse(GaC).ToString("N");
                        txtGastoOtro.Text = "$ " + decimal.Parse(GaO).ToString("N");
                        txtGastoTerrestre.Text = "$ " + decimal.Parse(GaTe).ToString("N");
                        txtGastoCurso.Text = "$ " + decimal.Parse(GaR).ToString("N");

                    }
                    if (TVi == 2 || TVi == 4)
                    {
                        conexion.Open();
                        string consulEm = "select * from TBL_Usuarios where ID_Usuario = " + IDemp;
                        SqlCommand selectEm = new SqlCommand(consulEm, conexion);
                        SqlDataReader slecEm = selectEm.ExecuteReader();
                        if (slecEm.Read())
                        {
                            sEm = Convert.ToString(slecEm["Tipo_empleado"]);
                        }
                        conexion.Close();
                        conexion.Open();
                        string selectVi = "select * from TBL_Externo where Tipo_Viaje= " + TVi + " and Tipo_Empleado = " + sEm;
                        SqlCommand selecIn = new SqlCommand(selectVi, conexion);
                        SqlDataReader selectInt = selecIn.ExecuteReader();
                        if (selectInt.Read())
                        {
                            GaV = Convert.ToString(selectInt["Gasto_vuelo"]);
                            GaH = Convert.ToString(selectInt["Gasto_hospedaje"]);
                            GaC = Convert.ToString(selectInt["Gasto_comida"]);
                            GaO = Convert.ToString(selectInt["Gasto_otros"]);
                            GaT = Convert.ToString(selectInt["Total_Gastos"]);
                            GaTe = Convert.ToString(selectInt["Gasto_terrestre"]);
                            GaR = Convert.ToString(selectInt["Gasto_Renta"]);
                        }
                        conexion.Close();
                        Gastov1 = Convert.ToInt32(GaV);
                        if (Gastov1 == 0)
                        {
                            GastoV = 0;
                        }
                        else
                        {
                            GastoV = Gastov1;
                        }
                        GastoH1 = Convert.ToInt32(GaH);
                        if (GastoH1 == 0)
                        {
                            GastoH = 0;
                        }
                        else
                        {
                            GastoH = GastoH1;
                        }
                        GastoTe1 = Convert.ToInt32(GaTe);
                        if (GastoTe1 == 0)
                        {
                            GastoTe = 0;
                        }
                        else
                        {
                            GastoTe = GastoTe1;
                        }
                        GastoC1 = Convert.ToInt32(GaC);
                        if (GastoC1 == 0)
                        {
                            GastoC = 0;
                        }
                        else
                        {
                            GastoC = GastoC1;
                        }
                        GastoCu1 = Convert.ToInt32(GaR);
                        if (GastoCu1 == 0)
                        {
                            GastoCu = 0;
                        }
                        else
                        {
                            GastoCu = GastoCu1;
                        }
                        OtroGa1 = Convert.ToInt32(GaO);
                        if (OtroGa1 == 0)
                        {
                            OtroGa = 0;
                        }
                        else
                        {
                            OtroGa = OtroGa1;
                        }
                        int dias2 = Convert.ToInt32(dias1);
                        Total = GastoV + ((GastoH + GastoTe + GastoC + GastoCu + OtroGa) * dias2);
                        lblCalcular.Text = "$ " + decimal.Parse(Total.ToString()).ToString("N");
                        txtPeriodo.Text = dias1 + " días";
                        txtGastoV.Text = "$ " + decimal.Parse(GaV).ToString("N");
                        txtGastoH.Text = "$ " + decimal.Parse(GaH).ToString("N");
                        txtComida.Text = "$ " + decimal.Parse(GaC).ToString("N");
                        txtGastoOtro.Text = "$ " + decimal.Parse(GaO).ToString("N");
                        txtGastoTerrestre.Text = "$ " + decimal.Parse(GaTe).ToString("N");
                        txtGastoCurso.Text = "$ " + decimal.Parse(GaR).ToString("N");
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, typeof(Page), "alerta", script1, true);
                    txtCalendar.Text = "";
                    txtCalendar1.Text = "";
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, typeof(Page), "alerta", script, true);
            }
        }
        protected void AgregarMe()
        {
            string IDemp = "", FeIn = "", FeFn = "", LuDe = "", Pe = "", GV = "",
                GH = "", GT = "", GC = "", GCu = "", GO = "", TS = "", TVi = "", Max = "", Est = "", Tipo_Emp = "",
                Max_Id = "", TGa = "", dias1 = "",MaxSo ="",Cliente="";
            string dtAhora = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss");
            int TV = 0, TG = 0;
            DateTime FechaInicio = Convert.ToDateTime(txtCalendar.Text);
            DateTime FechaFin = Convert.ToDateTime(txtCalendar1.Text);
            TimeSpan dias = FechaFin.Subtract(FechaInicio);
            dias1 = dias.Days.ToString();
            IDemp = Convert.ToString(txtEmpeladoNo.Text);
            FeIn = Convert.ToString(txtCalendar.Text);
            FeFn = Convert.ToString(txtCalendar1.Text);
            LuDe = Convert.ToString(txtLugardestino.Text);
            Pe = Convert.ToString(txtPeriodo.Text);
            GV = Convert.ToString(txtGastoV.Text);
            GH = Convert.ToString(txtGastoH.Text);
            GT = Convert.ToString(txtGastoTerrestre.Text);
            GC = Convert.ToString(txtComida.Text);
            GCu = Convert.ToString(txtGastoCurso.Text);
            GO = Convert.ToString(txtGastoOtro.Text);
            TS = Convert.ToString(lblCalcular.Text);
            TV = Convert.ToInt32(DrTipoViaje.SelectedValue);
            TG = Convert.ToInt32(DrTipoGasto.SelectedValue);
            Cliente = Convert.ToString(txtCliente.Text);

            string parte1 = GV.Remove(0, 2);
            string parte2 = GH.Remove(0, 2);
            string parte3 = GT.Remove(0, 2);
            string parte4 = GC.Remove(0, 2);
            string parte5 = GCu.Remove(0, 2);
            string parte6 = GO.Remove(0, 2);
            string parte7 = TS.Remove(0, 2);


            conexion.Open();
            string query2 = "select * from TBL_Tipo_Viaje where ID_Tipo = " + TV;
            SqlCommand selecTi = new SqlCommand(query2, conexion);
            SqlDataReader registro = selecTi.ExecuteReader();
            if (registro.Read())
            {
                TVi = Convert.ToString(registro["ID_Tipo"]);
            }
            conexion.Close();

            conexion.Open();
            string query3 = "select * from TBL_TipoGasto where ID_TipoGasto = " + TG;
            SqlCommand selectTGa = new SqlCommand(query3, conexion);
            SqlDataReader registro4 = selectTGa.ExecuteReader();
            if (registro4.Read())
            {
                TGa = Convert.ToString(registro4["ID_TipoGasto"]);
            }
            conexion.Close();

            conexion.Open();
            string query = "insert into TBL_Solicitud (Empleado,Fecha_Inicio,Fecha_Fin,Lugar_Destino,Periodo,Total_solicitado,Fecha_solicitud,Tipo_Viaje,Gasto_vuelo,Gasto_hospedaje,Gasto_terrestre,Gasto_comida,Gasto_otros,Estatus,Gasto_Renta,Tipo_gasto,Cliente) values (" + IDemp + ",'" + FeIn + "','" + FeFn + "','" + LuDe + "'," + dias1 + ",'" + parte7 + "','" + dtAhora + "'," + TVi + ",'" + parte1 + "','" + parte2 + "','" + parte3 + "','" + parte4 + "','" + parte6 + "',1,'" + parte5 + "'," + TGa + ",'"+Cliente+"')";
            SqlCommand insertSo = new SqlCommand(query, conexion);
            insertSo.ExecuteNonQuery();
            conexion.Close();

            conexion.Open();
            string consuMax = "select Estatus from TBL_Solicitud where ID_Solicitud in (select MAX (ID_Solicitud) from TBL_Solicitud)";
            SqlCommand selecMax = new SqlCommand(consuMax, conexion);
            SqlDataReader registro1 = selecMax.ExecuteReader();
            while (registro1.Read())
            {
                Max = registro1[0].ToString();
            }
            conexion.Close();

            conexion.Open();
            string consuEsta = "select * from TBL_Estatus where ID_Estatus = " + Max;
            SqlCommand selectEs = new SqlCommand(consuEsta, conexion);
            SqlDataReader reader = selectEs.ExecuteReader();
            if (reader.Read())
            {
                Est = Convert.ToString(reader["Estatus"]);
            }
            conexion.Close();

            conexion.Open();
            string consu = "select Tipo_empleado from TBL_Usuarios where Empleado=" + IDemp;
            SqlCommand selectEmp = new SqlCommand(consu, conexion);
            SqlDataReader registros2 = selectEmp.ExecuteReader();
            if (registros2.Read())
            {
                Tipo_Emp = Convert.ToString(registros2["Tipo_empleado"]);
            }
            conexion.Close();
            conexion.Open();
            string ConsuMaxID = "select ID_Solicitud from TBL_Solicitud where ID_Solicitud in (select MAX (ID_Solicitud) from TBL_Solicitud)";
            SqlCommand selectIDMax = new SqlCommand(ConsuMaxID, conexion);
            SqlDataReader registros3 = selectIDMax.ExecuteReader();
            while (registros3.Read())
            {
                Max_Id = registros3[0].ToString();
            }
            conexion.Close();
            conexion.Open();
            string insertAuto = "insert into TBL_Autorizado (Autorizo,Fecha_autorizado,Comentario,Solicitud,Tipo_Empleado) values (0,'',''," + Max_Id + "," + Tipo_Emp + ")";
            SqlCommand insertAut = new SqlCommand(insertAuto, conexion);
            insertAut.ExecuteNonQuery();
            conexion.Close();
            conexion.Open();
            string SelectEsta = "select ID_Solicitud from TBL_Solicitud where ID_Solicitud in (select MAX (ID_Solicitud) from TBL_Solicitud)";
            SqlCommand selecEsta = new SqlCommand(SelectEsta, conexion);
            SqlDataReader registro5 = selecEsta.ExecuteReader();
            while (registro5.Read())
            {
                MaxSo = registro5[0].ToString();
            }
            conexion.Close();
            string script = "alert('Solicitud enviada, solictud numero: "+ MaxSo +"')";
            ScriptManager.RegisterStartupScript(this, typeof(Page), "alerta", script, true);
            lblEstatusSol.Text = "Solicitud numero: "+MaxSo;
            txtEmpeladoNo.Text = "";
            txtCalendar.Text = "";
            txtCalendar1.Text = "";
            DrTipoViajeMe();
            DrTipoGastoMe();
            txtLugardestino.Text = "";
            txtPeriodo.Text = "";
            txtGastoV.Text = "";
            txtGastoH.Text = "";
            txtGastoTerrestre.Text = "";
            txtComida.Text = "";
            txtGastoCurso.Text = "";
            txtGastoOtro.Text = "";
            lblCalcular.Text = "";
            txtCliente.Text = "";
        }

        protected void btnEnviarS_Click(object sender, EventArgs e)
        {
            string fecha1 = Convert.ToString(txtCalendar.Text);
            string fecha2 = Convert.ToString(txtCalendar1.Text);
            int TV = Convert.ToInt32(DrTipoViaje.SelectedValue);
            int TG = Convert.ToInt32(DrTipoGasto.SelectedValue);
            string LugarDestino = Convert.ToString(txtLugardestino.Text);
            string script = "alert('Datos incorrectos, revisar datos')";
            string periodo = Convert.ToString(txtPeriodo.Text);
            string GastoVuelo = Convert.ToString(txtGastoV.Text);
            string GastoHospedaje = Convert.ToString(txtGastoH.Text);
            string Traslado = Convert.ToString(txtGastoTerrestre.Text);
            string comida = Convert.ToString(txtComida.Text);
            string RentaAuto = Convert.ToString(txtGastoCurso.Text);
            string OtrosGastos = Convert.ToString(txtGastoOtro.Text);
            string Total = Convert.ToString(lblCalcular.Text);
            string Cliente = Convert.ToString(txtCliente.Text);
            if (fecha1.Length != 0 || fecha2.ToString() != "" || TV != 0 || TG != 0 || LugarDestino.Length != 0 || 
                periodo.Length != 0 || GastoVuelo.Length != 0 || GastoHospedaje.Length != 0 || Traslado.Length != 0 || 
                comida.Length != 0 || RentaAuto.Length != 0 || OtrosGastos.Length != 0 || Total.Length != 0 || Cliente.Length != 0 )
            {
                AgregarMe();
                txtEmpeladoNo.Text = Session["ID_User"].ToString();
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, typeof(Page), "alerta", script, true);
                txtEmpeladoNo.Text = Session["ID_User"].ToString();
            }
            
        }

        protected void Resultado_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            AgregarGrid();
            Resultado.PageIndex = e.NewPageIndex;
            Resultado.DataBind();
        }

        protected void imgGuardar_Click(object sender, ImageClickEventArgs e)
        {
            string script = "alert('Solicitud guardada')";
            string parte1;
            string parte2;
            string parte3;
            string parte4;
            string parte5;
            string parte6;
            string parte7;
            string per;
            TimeSpan dias;
            string dias1;
            DateTime FechaInicio;
            DateTime FechaFin;
            string DtAhora = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss");
            string No_Empleado = Convert.ToString(txtEmpeladoNo.Text);
            string Fecha_Inicio = Convert.ToString(txtCalendar.Text);
            string Fecha_Fin = Convert.ToString(txtCalendar1.Text);

            if (Fecha_Inicio.Length != 0 || Fecha_Fin.Length != 0)
            {
                FechaInicio = Convert.ToDateTime(txtCalendar.Text);
                FechaFin = Convert.ToDateTime(txtCalendar1.Text);
                dias = FechaFin.Subtract(FechaInicio);
                dias1 = dias.Days.ToString();
                
            }
            else
            {

                dias1 = "0";
            }
            int TV = Convert.ToInt32(DrTipoViaje.SelectedValue);
            int TG = Convert.ToInt32(DrTipoGasto.SelectedValue);
            string Lugar_Destino = Convert.ToString(txtLugardestino.Text);
            string Periodo = Convert.ToString(txtPeriodo.Text);
            string gastoVuelo = Convert.ToString(txtGastoV.Text);
            string gastoHospedaje = Convert.ToString(txtGastoH.Text);
            string gastoTraslado = Convert.ToString(txtGastoTerrestre.Text);
            string gastoComida = Convert.ToString(txtComida.Text);
            string gastoRenta = Convert.ToString(txtGastoCurso.Text);
            string gastoOtro = Convert.ToString(txtGastoOtro.Text);
            string Total = Convert.ToString(lblCalcular.Text);
            string Cliente = Convert.ToString(txtCliente.Text);
            string TVi = "", TGa = "";
            if (TV != 0)
            {
                conexion.Open();
                string query2 = "select * from TBL_Tipo_Viaje where ID_Tipo = " + TV;
                SqlCommand selecTi = new SqlCommand(query2, conexion);
                SqlDataReader registro = selecTi.ExecuteReader();
                if (registro.Read())
                {
                    TVi = Convert.ToString(registro["ID_Tipo"]);
                }
                conexion.Close();
            }
            else
            {
                TVi = "5";
            }
            if (TG != 0)
            {
                conexion.Open();
                string query3 = "select * from TBL_TipoGasto where ID_TipoGasto = " + TG;
                SqlCommand selectTGa = new SqlCommand(query3, conexion);
                SqlDataReader registro4 = selectTGa.ExecuteReader();
                if (registro4.Read())
                {
                    TGa = Convert.ToString(registro4["ID_TipoGasto"]);
                }
                conexion.Close();
            }
            else
            {
                TGa = "3";
            }
            if (Total != "")
            {
                parte1 = Total.Remove(0, 2);
            }
            else
            {
                parte1 = "";
            }
            if (gastoVuelo != "")
            {
                parte2 = gastoVuelo.Remove(0, 2);
            }
            else
            {
                parte2 = "";
            }
            if (gastoHospedaje != "")
            {
                parte3 = gastoHospedaje.Remove(0, 2);
            }
            else
            {
                parte3 = "";
            }
            if (gastoTraslado != "")
            {
                parte4 = gastoTraslado.Remove(0, 2);

            }
            else
            {
                parte4 = "";
            }
            if (gastoComida != "")
            {
                parte5 = gastoComida.Remove(0, 2);
            }
            else
            {
                parte5 = "";
            }
            if (gastoOtro != "")
            {
                parte6 = gastoOtro.Remove(0, 2);
            }
            else
            {
                parte6 = "";
            }
            if (gastoRenta != "")
            {
                parte7 = gastoRenta.Remove(0, 2);
            }
            else
            {
                parte7 = "";
            }
            conexion.Open();
            if (Periodo != "")
            {
                per = Periodo.Remove(1, 5);
            }
            else
            {
                per = "";
            }
            string query = "insert into TBL_Solicitud (Empleado,Fecha_Inicio,Fecha_Fin,Lugar_Destino,Periodo,Total_solicitado," +
                "Fecha_solicitud,Tipo_Viaje,Gasto_vuelo,Gasto_hospedaje,Gasto_terrestre,Gasto_comida,Gasto_otros,Estatus," +
                "Gasto_Renta,Tipo_gasto,Cliente) values " +
                "(" + No_Empleado + ",'" + Fecha_Inicio + "','" + Fecha_Fin + "','" + Lugar_Destino + "'," +
                "" + dias1 + ",'" + parte1 + "','" + DtAhora + "'," + TVi + ",'" + parte2 + "','" + parte3 + "'," +
                "'" + parte4 + "','" + parte5 + "','" + parte6 + "',10,'" + parte7 + "'," + TGa + ",'"+Cliente+"')";
            SqlCommand insert = new SqlCommand(query, conexion);
            insert.ExecuteNonQuery();
            conexion.Close();
            ScriptManager.RegisterStartupScript(this, typeof(Page), "alerta", script, true);
            txtEmpeladoNo.Text = Session["ID_User"].ToString();
        }
    }
    }
  

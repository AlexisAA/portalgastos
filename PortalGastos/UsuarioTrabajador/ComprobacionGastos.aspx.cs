﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Diagnostics;
using System.Windows.Forms;
using System.IO;
using System.Xml;
using System.Threading;
using System.Drawing;
using System.Configuration;
using System.Net.Mail;
using System.Text;
using System.Net.Mime;

namespace PortalGastos.UsuarioTrabajador
{
    public partial class ComprobacionGastos : System.Web.UI.Page
    {
        SqlConnection conexion = new SqlConnection("Data source=(local); Initial Catalog=PortalGastos; User=sae; Password=sae ");
        protected void Page_Init(object sender, EventArgs e)
        {
            if (Session["user"] == null)
            {
                HttpContext.Current.Response.Redirect("~/Portal/Login.aspx");
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                AgregarGrid();
            }
        }

        protected void Resultado_SelectedIndexChanged1(object sender, EventArgs e)
        {
            string LuDe = "", Prim = "", Ap = "", ToS = "", GV = "", GH = "", GT = "", GC = "", GO = "", GR = "", GTo = "";
            int IDSol = 0;
            GridViewRow row = Resultado.SelectedRow;
            int id = Convert.ToInt32(Resultado.DataKeys[row.RowIndex].Value);
            conexion.Open();
            string query = "select * from v_ComprobacionG where ID_Solicitud = " + id;
            SqlCommand selecCom = new SqlCommand(query, conexion);
            SqlDataReader registro = selecCom.ExecuteReader();
            if (registro.Read())
            {
                IDSol = Convert.ToInt32(registro["ID_Solicitud"]);
                ToS = Convert.ToString(registro["Total_solicitado"]);
                LuDe = Convert.ToString(registro["Lugar_Destino"]);
                Prim = Convert.ToString(registro["Primer_nombre"]);
                Ap = Convert.ToString(registro["Apellido_paterno"]);
            }
            conexion.Close();
            conexion.Open();
            string query1 = "select * from TBL_Solicitud where ID_Solicitud = " + IDSol;
            SqlCommand selectS = new SqlCommand(query1, conexion);
            SqlDataReader registro1 = selectS.ExecuteReader();
            if (registro1.Read())
            {
                GV = Convert.ToString(registro1["Gasto_vuelo"]);
                GH = Convert.ToString(registro1["Gasto_hospedaje"]);
                GT = Convert.ToString(registro1["Gasto_terrestre"]);
                GC = Convert.ToString(registro1["Gasto_comida"]);
                GO = Convert.ToString(registro1["Gasto_otros"]);
                GR = Convert.ToString(registro1["Gasto_Renta"]);
                GTo = Convert.ToString(registro1["Total_solicitado"]);
            }
            conexion.Close();
            lblNombreEmp.Text = "Sr(a). " + Prim + " " + Ap + " su solicitud número " + IDSol + " con lugar destino " + LuDe + " tiene que ser comprobado, el monto solicitado es: $ " + decimal.Parse(ToS).ToString("N");
            lblTotalSolicitado.Text = "$ " + decimal.Parse(ToS).ToString("N");
            lblVuelo.Text = "$ " + decimal.Parse(GV).ToString("N");
            lblHospedaje.Text = "$ " + decimal.Parse(GH).ToString("N");
            lblTraslado.Text = "$ " + decimal.Parse(GT).ToString("N");
            lblComida.Text = "$ " + decimal.Parse(GC).ToString("N");
            lblRenta.Text = "$ " + decimal.Parse(GR).ToString("N");
            lblNoComprobable.Text = "$ " + decimal.Parse(GO).ToString("N");

            lblSolicitado.CssClass = "border lblEtique Solicitado";
            lblImporteXML.CssClass = "border lblEtique Importe";
            lblDiferencia.CssClass = "border lblEtique Diferencia";
            lblVuelo.CssClass = "border lblEtique Solicitado";
            lblHospedaje.CssClass = "border lblEtique Solicitado";
            lblTraslado.CssClass = "border lblEtique Solicitado";
            lblComida.CssClass= "border lblEtique Solicitado";
            lblRenta.CssClass= "border lblEtique Solicitado";
            lblNoComprobable.CssClass = "border lblEtique Solicitado";
            lblVuelo1.CssClass = "border Importe lblEtique";
            lblVuelo2.CssClass = "border Diferencia lblEtique";
            lblVuelo1.Text = "$ " + decimal.Parse("0").ToString("N");
            lblVuelo2.Text = "$ " + decimal.Parse("0").ToString("N");
            lblHospedaje1.CssClass = "border Importe lblEtique";
            lblHospedaje2.CssClass = "border Diferencia lblEtique";
            lblHospedaje1.Text = "$ " + decimal.Parse("0").ToString("N");
            lblHospedaje2.Text = "$ " + decimal.Parse("0").ToString("N");
            lblTraslado1.CssClass = "border Importe lblEtique";
            lblTraslado2.CssClass = "border Diferencia lblEtique";
            lblTraslado1.Text = "$ " + decimal.Parse("0").ToString("N");
            lblTraslado2.Text = "$ " + decimal.Parse("0").ToString("N");
            lblComida1.CssClass = "border Importe lblEtique";
            lblComida2.CssClass = "border Diferencia lblEtique";
            lblComida1.Text = "$ " + decimal.Parse("0").ToString("N");
            lblComida2.Text = "$ " + decimal.Parse("0").ToString("N");
            lblRenta1.CssClass = "border Importe lblEtique";
            lblRenta2.CssClass = "border Diferencia lblEtique";
            lblRenta1.Text = "$ " + decimal.Parse("0").ToString("N");
            lblRenta2.Text = "$ " + decimal.Parse("0").ToString("N");
            lblFacturas.Text = "$ " + decimal.Parse("0").ToString("N");
            txtHospedaje.Visible = true;
            btnHospedaje.Visible = true;
            txtTraslado.Visible = true;
            btnTraslado.Visible = true;
            txtRenta.Visible = true;
            btnRenta.Visible = true;
            txtComida.Visible = true;
            btnComida.Visible = true;
            txtVuelo.Visible = true;
            btnVuelo.Visible = true;
            lblGastos.Visible = true;
            lblFacturas.Visible = true;
            lblTotalSolicitado.Visible = true;
            lblCantidad1.Visible = true;
            lblNoComprobable.Visible = true;
            txtCantidad.Visible = true;
            lblVuelo2.Visible = true;
            lblVuelo1.Visible = true;
            lblVuelo.Visible = true;
            lblHospedaje1.Visible = true;
            lblHospedaje2.Visible = true;
            lblHospedaje.Visible = true;
            lblTraslado1.Visible = true;
            lblTraslado2.Visible = true;
            lblTraslado.Visible = true;
            lblComida1.Visible = true;
            lblComida2.Visible = true;
            lblComida.Visible = true;
            lblRenta1.Visible = true;
            lblRenta2.Visible = true;
            lblRenta.Visible = true;
            lblSolicitado.Visible = true;
            lblImporteXML.Visible = true;
            lblDiferencia.Visible = true;
            btnRegresar.Visible = true;
            lblNombreEmp.Visible = true;
            lblArchivoVuelo.Visible = true;
            lblArchivoHospedaje.Visible = true;
            lblArchivoTraslado.Visible = true;
            lblArchivoComida.Visible = true;
            lblArchivoRenta.Visible = true;
            lblArchivoNo.Visible = true;
            Filesubir1.Visible = true;
            Filesubir2.Visible = true;
            Filesubir3.Visible = true;
            Filesubir4.Visible = true;
            Filesubir5.Visible = true;
            Filesubir6.Visible = true;
            btnSubirVuelo.Visible = true;
            btnSubirHospedaje.Visible = true;
            btnSubirComida.Visible = true;
            btnSubirRenta.Visible = true;
            btnSubirTraslado.Visible = true;
            btnSubirNoComprobable.Visible = true;
            btnEnviar.Visible = true;
            txtComentario.Visible = true;
            btnComentario.Visible = true;
            btnCalcular.Visible = true;
            lblGastosT.Visible = true;
            Resultado.Visible = false;
        }
        protected void AgregarGrid()
        {
            string ID_User = Session["ID_User"].ToString();
            List<string> Lista = new List<string>();
            string  FeIn = "", FeFn = "", LuD = "", ToS = "", TiV = "" , con1="";
            int IDEmp = 0, IDSol = 0;
            DataTable dt = new DataTable();
            dt.Columns.AddRange(new DataColumn[8] {new DataColumn("No. de Empleado",typeof(int)),
                                                    new DataColumn("No. de Solicitud",typeof(int)),
                                                    new DataColumn("Fecha Inicio",typeof(string)),
                                                    new DataColumn("Fecha Fin",typeof(string)),
                                                    new DataColumn("Lugar de Destino",typeof(string)),
                                                    new DataColumn("Total Solicitado",typeof(string)),
                                                    new DataColumn("Tipo de Viaje",typeof(string)),
                                                    new DataColumn("Fecha Limite",typeof(string))});
            conexion.Open();
            string query = "select * from v_Comprobar where Empleado = "+ID_User;
            SqlCommand comp = new SqlCommand(query,conexion);
            SqlDataReader registro = comp.ExecuteReader();
            while (registro.Read())
            {
                con1 = registro[0].ToString();
                Lista.Add(con1);
                for (int i = 0; i < Lista.Count; i++)
                {
                    IDSol = Convert.ToInt32(registro["ID_Solicitud"]);
                    IDEmp = Convert.ToInt32(registro["Empleado"]);
                    FeIn = Convert.ToString(registro["Fecha_Inicio"]);
                    FeFn = Convert.ToString(registro["Fecha_Fin"]);
                    LuD = Convert.ToString(registro["Lugar_Destino"]);
                    ToS = Convert.ToString(registro["Total_solicitado"]);
                    TiV = Convert.ToString(registro["Tipo_Viaje"]);
                }
                DateTime dtAfter = new DateTime();
                DateTime dtAfter1 = new DateTime();
                dtAfter = Convert.ToDateTime(FeFn);
                dtAfter1 = dtAfter.AddDays(15.0);
                dt.Rows.Add(IDEmp,IDSol,FeIn,FeFn,LuD,"$ "+decimal.Parse(ToS).ToString("N"),TiV,dtAfter1.ToString("yyyy/MM/dd"));
                Resultado.DataSource = dt;
                Resultado.DataBind();
            }
            conexion.Close();
        }

        protected void btnRegresar_Click(object sender, EventArgs e)
        {
            lblGastos.Visible = false;
            lblFacturas.Visible = false;
            lblTotalSolicitado.Visible = false;
            lblCantidad1.Visible = false;
            btnCalcular.Visible = false;
            lblVuelo2.Visible = false;
            lblVuelo1.Visible =false;
            lblVuelo.Visible = false;
            lblHospedaje1.Visible = false;
            lblHospedaje2.Visible = false;
            lblHospedaje.Visible = false;
            lblTraslado1.Visible = false;
            lblTraslado2.Visible = false;
            lblTraslado.Visible = false;
            lblComida1.Visible = false;
            lblComida2.Visible = false;
            lblComida.Visible = false;
            lblRenta1.Visible = false;
            lblRenta2.Visible = false;
            lblRenta.Visible = false;
            btnRegresar.Visible = false;
            lblNombreEmp.Visible = false;
            lblSolicitado.Visible = false;
            lblImporteXML.Visible = false;
            lblDiferencia.Visible = false;
            lblArchivoVuelo.Visible = false;
            lblArchivoHospedaje.Visible = false;
            lblArchivoTraslado.Visible = false;
            lblArchivoComida.Visible = false;
            lblArchivoNo.Visible = false;
            lblArchivoRenta.Visible = false;
            Filesubir1.Visible = false;
            Filesubir2.Visible = false;
            Filesubir3.Visible = false;
            Filesubir4.Visible = false;
            Filesubir5.Visible = false;
            Filesubir6.Visible = false;
            btnSubirVuelo.Visible = false;
            btnSubirHospedaje.Visible = false;
            btnSubirHospedaje.Visible = false;
            btnSubirComida.Visible = false;
            btnSubirRenta.Visible = false;
            btnSubirTraslado.Visible = false;
            btnSubirNoComprobable.Visible = false;
            btnEnviar.Visible = false;
            txtComentario.Visible = false;
            btnComentario.Visible = false;
            lblGastosT.Visible = false;
            txtVuelo.Visible = false;
            btnVuelo.Visible = false;
            lblNoComprobable.Visible = false;
            txtCantidad.Visible = false;
            txtHospedaje.Visible = false;
            btnHospedaje.Visible = false;
            txtTraslado.Visible = false;
            btnTraslado.Visible = false;
            txtRenta.Visible = false;
            btnRenta.Visible = false;
            txtComida.Visible = false;
            btnComida.Visible = false;

            Resultado.Visible = true;
            AgregarGrid();
        }

        protected void btnSubir_Click(object sender, EventArgs e)
        {
            VueloMe();
        }
        protected void VueloMe()
        {
            string[] datosXML = new string[8];
            string xmlVersion = string.Empty;
            string xmlrfcReceptor = string.Empty;
            string xmlrfcEmisor = string.Empty;
            string xmlFolio = string.Empty;
            string xmlSerie = string.Empty;
            string xmlUuid = string.Empty;
            DateTime xmlFfactura = new DateTime();
            decimal xmlImporte = 0;
            string[] respuestaSAT = new string[2];
            var nompdf = "";
            var nomxml = "";
            var destino = "";
            string dt = DateTime.Now.ToString("yyyy/MM/dd");
            string script = "alert('Favor de ingresar XML y PDF vuelo')";
            string script1 = "alert('Factura existente en SAT')";
            string script2 = "alert('Factura no existente en SAT, revisar factura de vuelo')";
            string script3 = "alert('Esta factura ya fue ingresada')";
            string script4 = "alert('El UUID de la factura ya fue ingresado')";
            string ruta = "C:/Archivos/" + dt + "/Vuelo/";
            string XMLBD="";
            if (!Directory.Exists(ruta))
            {
                Directory.CreateDirectory(ruta);
            }
            foreach (var archivo in Filesubir1.PostedFiles)
            {
                int arch = Convert.ToInt32(Filesubir1.PostedFiles.Count);
                if (arch == 2)
                {
                    string filename = Path.GetFileName(archivo.FileName);
                    string[] var = Directory.GetFiles(ruta,filename);
                    int valor = 0;
                    valor = var.Length;
                    if (valor!=1)
                    {
                        string ext = Path.GetExtension(archivo.FileName);
                        if (ext == ".pdf" || ext == ".xml")
                        {
                            if (ext == ".pdf")
                            {
                                nompdf = Path.GetFileName(archivo.FileName);
                                destino = Path.Combine(ruta, nompdf);
                                archivo.SaveAs(destino);
                            }
                            if (ext == ".xml")
                            {
                                nomxml = Path.GetFileName(archivo.FileName);
                                destino = Path.Combine(ruta, nomxml);
                                archivo.SaveAs(destino);
                                //Leer XML
                                datosXML = obtenDatosXml(ruta + "/" + nomxml);
                                if (datosXML != null)
                                {
                                    xmlVersion = datosXML[0] == null ? string.Empty : datosXML[0].ToString();
                                    xmlrfcReceptor = datosXML[1] == null ? string.Empty : datosXML[1].ToString();
                                    xmlrfcEmisor = datosXML[2] == null ? string.Empty : datosXML[2].ToString();
                                    xmlFolio = datosXML[3] == null ? string.Empty : datosXML[3].ToString();
                                    xmlSerie = datosXML[4] == null ? string.Empty : datosXML[4].ToString();
                                    xmlUuid = datosXML[5] == null ? string.Empty : datosXML[5].ToString();
                                    xmlFfactura = datosXML[6] == null ? DateTime.Now : Convert.ToDateTime(datosXML[6].ToString());
                                    xmlImporte = datosXML[7] == null ? 0 : Convert.ToDecimal(datosXML[7].ToString());
                                }
                                else
                                {
                                    ScriptManager.RegisterStartupScript(this, typeof(Page), "alerta", script, true);
                                }
                                RespuestaSAT respue = new RespuestaSAT();
                                srefer.WebService1SoapClient fade = new srefer.WebService1SoapClient();
                                srefer.ValidaCFDIRequest xchecaSAT = new srefer.ValidaCFDIRequest();
                                srefer.RespuestaSAT refa = new srefer.RespuestaSAT();
                                refa = fade.ValidaCFDI(datosXML[5].ToString(), datosXML[2].ToString(), datosXML[1].ToString(), datosXML[7].ToString());

                                respuestaSAT[0] = refa.CodigoEstatus.ToString();
                                respuestaSAT[1] = refa.Estado.ToString();

                                if (respuestaSAT[1].ToString() == "Vigente")
                                {
                                    conexion.Open();
                                    string select = "select * from TBL_Facturas where UUID = '" + xmlUuid + "'";
                                    SqlCommand selectXML = new SqlCommand(select, conexion);
                                    SqlDataReader registrosXML = selectXML.ExecuteReader();
                                    if (registrosXML.Read())
                                    {
                                        XMLBD = Convert.ToString(registrosXML["UUID"]);
                                    }
                                    conexion.Close();
                                    if (XMLBD == "")
                                    {
                                        ScriptManager.RegisterStartupScript(this, typeof(Page), "alerta", script1, true);
                                        btnSubirVuelo.BorderColor = Color.Green;
                                        btnSubirVuelo.ForeColor = Color.Green;
                                        guardarFac(xmlImporte, nomxml, "Vigente", nompdf, "Vuelo", ruta, xmlUuid);
                                        string to = "";
                                        to = Convert.ToString(lblVuelo2.Text);
                                        to = to.Substring(1);
                                        decimal to1 = Convert.ToDecimal(to);
                                        if (to1 != 0)
                                        {
                                            string difere = Convert.ToString(lblVuelo2.Text);
                                            string diferen = difere.Substring(1);
                                            decimal dife = Convert.ToDecimal(diferen);
                                            decimal Total = dife - xmlImporte;
                                            if (Total >= 0)
                                            {
                                                string tot = Convert.ToString(Total);
                                                lblVuelo2.Text = "$" + decimal.Parse(tot).ToString("N");
                                                string Importe = Convert.ToString(xmlImporte);
                                                lblVuelo1.Text = "$" + decimal.Parse(Importe).ToString("N");
                                                lblVuelo2.ForeColor = Color.Green;
                                                string vacio = Convert.ToString(lblFacturas.Text);
                                                if (vacio == "")
                                                {
                                                    decimal suma = 0;
                                                    decimal total = suma + xmlImporte;
                                                    string total1 = Convert.ToString(total);
                                                    lblFacturas.Text = "$ " + decimal.Parse(total1).ToString("N");
                                                }
                                                else
                                                {
                                                    string fac = Convert.ToString(lblFacturas.Text);
                                                    string fac1 = fac.Substring(1);
                                                    decimal suma = Convert.ToDecimal(fac1);
                                                    decimal total = suma + xmlImporte;
                                                    string total1 = Convert.ToString(total);
                                                    lblFacturas.Text = "$ " + decimal.Parse(total1).ToString("N");
                                                }
                                            }
                                            else
                                            {
                                                string tot = Convert.ToString(Total);
                                                lblVuelo2.Text = "$" + decimal.Parse(tot).ToString("N");
                                                string Importe = Convert.ToString(xmlImporte);
                                                lblVuelo1.Text = "$" + decimal.Parse(Importe).ToString("N");
                                                lblVuelo2.ForeColor = Color.Red;
                                                string vacio = Convert.ToString(lblFacturas.Text);
                                                if (vacio == "")
                                                {
                                                    decimal suma = 0;
                                                    decimal total = suma + xmlImporte;
                                                    string total1 = Convert.ToString(total);
                                                    lblFacturas.Text = "$ " + decimal.Parse(total1).ToString("N");
                                                }
                                                else
                                                {
                                                    string fac = Convert.ToString(lblFacturas.Text);
                                                    string fac1 = fac.Substring(1);
                                                    decimal suma = Convert.ToDecimal(fac1);
                                                    decimal total = suma + xmlImporte;
                                                    string total1 = Convert.ToString(total);
                                                    lblFacturas.Text = "$ " + decimal.Parse(total1).ToString("N");
                                                }
                                            }
                                        }
                                        else
                                        {
                                            string difere = Convert.ToString(lblVuelo.Text);
                                            string diferen = difere.Substring(1);
                                            decimal dife = Convert.ToDecimal(diferen);
                                            decimal Total = dife - xmlImporte;
                                            if (Total >= 0)
                                            {
                                                string tot = Convert.ToString(Total);
                                                lblVuelo2.Text = "$" + decimal.Parse(tot).ToString("N");
                                                string Importe = Convert.ToString(xmlImporte);
                                                lblVuelo1.Text = "$" + decimal.Parse(Importe).ToString("N");
                                                lblVuelo2.ForeColor = Color.Green;
                                                string vacio = Convert.ToString(lblFacturas.Text);
                                                if (vacio == "")
                                                {
                                                    decimal suma = 0;
                                                    decimal total = suma + xmlImporte;
                                                    string total1 = Convert.ToString(total);
                                                    lblFacturas.Text = "$ " + decimal.Parse(total1).ToString("N");
                                                }
                                                else
                                                {
                                                    string fac = Convert.ToString(lblFacturas.Text);
                                                    string fac1 = fac.Substring(1);
                                                    decimal suma = Convert.ToDecimal(fac1);
                                                    decimal total = suma + xmlImporte;
                                                    string total1 = Convert.ToString(total);
                                                    lblFacturas.Text = "$ " + decimal.Parse(total1).ToString("N");
                                                }
                                            }
                                            else
                                            {
                                                string tot = Convert.ToString(Total);
                                                lblVuelo2.Text = "$" + decimal.Parse(tot).ToString("N");
                                                string Importe = Convert.ToString(xmlImporte);
                                                lblVuelo1.Text = "$" + decimal.Parse(Importe).ToString("N");
                                                lblVuelo2.ForeColor = Color.Red;
                                                string vacio = Convert.ToString(lblFacturas.Text);
                                                if (vacio == "")
                                                {
                                                    decimal suma = 0;
                                                    decimal total = suma + xmlImporte;
                                                    string total1 = Convert.ToString(total);
                                                    lblFacturas.Text = "$ " + decimal.Parse(total1).ToString("N");
                                                }
                                                else
                                                {
                                                    string fac = Convert.ToString(lblFacturas.Text);
                                                    string fac1 = fac.Substring(1);
                                                    decimal suma = Convert.ToDecimal(fac1);
                                                    decimal total = suma + xmlImporte;
                                                    string total1 = Convert.ToString(total);
                                                    lblFacturas.Text = "$ " + decimal.Parse(total1).ToString("N");
                                                }
                                            }
                                        }
                                        
                                    }
                                    else
                                    {
                                        ScriptManager.RegisterStartupScript(this, typeof(Page), "alerta", script4, true);
                                        btnSubirVuelo.BorderColor = Color.Red;
                                        btnSubirVuelo.ForeColor = Color.Red;
                                        File.Delete(ruta + nompdf);
                                        File.Delete(ruta + nomxml);
                                    }
                                    
                                }
                                else
                                {
                                    ScriptManager.RegisterStartupScript(this, typeof(Page), "alerta", script2, true);
                                    btnSubirVuelo.BorderColor = Color.Red;
                                    btnSubirVuelo.ForeColor = Color.Red;
                                    File.Delete(ruta+nompdf);
                                    File.Delete(ruta+nomxml);
                                }
                            }
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, typeof(Page), "alerta", script3, true);
                        btnSubirVuelo.BorderColor = Color.Red;
                        btnSubirVuelo.ForeColor = Color.Red;
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, typeof(Page), "alerta", script, true);
                    btnSubirVuelo.BorderColor = Color.Red;
                    btnSubirVuelo.ForeColor = Color.Red;
                }
            }
        }
        protected void HospedajeMe()
        {
            string[] datosXML = new string[8];
            string xmlVersion = string.Empty;
            string xmlrfcReceptor = string.Empty;
            string xmlrfcEmisor = string.Empty;
            string xmlFolio = string.Empty;
            string xmlSerie = string.Empty;
            string xmlUuid = string.Empty;
            DateTime xmlFfactura = new DateTime();
            decimal xmlImporte = 0;
            string[] respuestaSAT = new string[2];
            var nompdf = "";
            var nomxml = "";
            var destino = "";
            string dt = DateTime.Now.ToString("yyyy/MM/dd");
            string script = "alert('Favor de ingresar XML y PDF hospedaje')";
            string script1 = "alert('Factura existente en SAT')";
            string script2 = "alert('Factura no existente en SAT, revisar factura de hospedaje')";
            string script3 = "alert('Esta factura ya fue ingresada')";
            string script4 = "alert('El UUID de la factura ya fue ingresado')";
            string XMLBD = "";
            string ruta = "C:/Archivos/" + dt + "/Hospedaje/";

            if (!Directory.Exists(ruta))
            {
                Directory.CreateDirectory(ruta);
            }
            foreach (var archivo in Filesubir2.PostedFiles)
            {
                int arch = Convert.ToInt32(Filesubir2.PostedFiles.Count);
                if (arch == 2)
                {
                    string filename = Path.GetFileName(archivo.FileName);
                    string[] var = Directory.GetFiles(ruta, filename);
                    int valor = 0;
                    valor = var.Length;
                    if (valor != 1)
                    {
                        string ext = Path.GetExtension(archivo.FileName);
                        if (ext == ".pdf" || ext == ".xml")
                        {
                            if (ext == ".pdf")
                            {
                                nompdf = Path.GetFileName(archivo.FileName);
                                destino = Path.Combine(ruta, nompdf);
                                archivo.SaveAs(destino);
                            }
                            if (ext == ".xml")
                            {
                                nomxml = Path.GetFileName(archivo.FileName);
                                destino = Path.Combine(ruta, nomxml);
                                archivo.SaveAs(destino);
                                //Leer XML
                                datosXML = obtenDatosXml(ruta + "/" + nomxml);
                                if (datosXML != null)
                                {
                                    xmlVersion = datosXML[0] == null ? string.Empty : datosXML[0].ToString();
                                    xmlrfcReceptor = datosXML[1] == null ? string.Empty : datosXML[1].ToString();
                                    xmlrfcEmisor = datosXML[2] == null ? string.Empty : datosXML[2].ToString();
                                    xmlFolio = datosXML[3] == null ? string.Empty : datosXML[3].ToString();
                                    xmlSerie = datosXML[4] == null ? string.Empty : datosXML[4].ToString();
                                    xmlUuid = datosXML[5] == null ? string.Empty : datosXML[5].ToString();
                                    xmlFfactura = datosXML[6] == null ? DateTime.Now : Convert.ToDateTime(datosXML[6].ToString());
                                    xmlImporte = datosXML[7] == null ? 0 : Convert.ToDecimal(datosXML[7].ToString());
                                }
                                else
                                {
                                    ScriptManager.RegisterStartupScript(this, typeof(Page), "alerta", script, true);
                                }
                                RespuestaSAT respue = new RespuestaSAT();
                                srefer.WebService1SoapClient fade = new srefer.WebService1SoapClient();
                                srefer.ValidaCFDIRequest xchecaSAT = new srefer.ValidaCFDIRequest();
                                srefer.RespuestaSAT refa = new srefer.RespuestaSAT();
                                refa = fade.ValidaCFDI(datosXML[5].ToString(), datosXML[2].ToString(), datosXML[1].ToString(), datosXML[7].ToString());

                                respuestaSAT[0] = refa.CodigoEstatus.ToString();
                                respuestaSAT[1] = refa.Estado.ToString();

                                if (respuestaSAT[1].ToString() == "Vigente")
                                {
                                    conexion.Open();
                                    string select = "select * from TBL_Facturas where UUID = '" + xmlUuid + "'";
                                    SqlCommand selectXML = new SqlCommand(select, conexion);
                                    SqlDataReader registrosXML = selectXML.ExecuteReader();
                                    if (registrosXML.Read())
                                    {
                                        XMLBD = Convert.ToString(registrosXML["UUID"]);
                                    }
                                    conexion.Close();
                                    if (XMLBD=="")
                                    {
                                        ScriptManager.RegisterStartupScript(this, typeof(Page), "alerta", script1, true);
                                        btnSubirHospedaje.BorderColor = Color.Green;
                                        btnSubirHospedaje.ForeColor = Color.Green;
                                        guardarFac(xmlImporte, nomxml, "Vigente", nompdf, "Hospedaje", ruta, xmlUuid);
                                        string to = "";
                                        to = Convert.ToString(lblHospedaje2.Text);
                                        to = to.Substring(1);
                                        decimal to1 = Convert.ToDecimal(to);
                                        if (to1!=0)
                                        {
                                            string difere = Convert.ToString(lblHospedaje2.Text);
                                            string diferen = difere.Substring(1);
                                            decimal dife = Convert.ToDecimal(diferen);
                                            decimal Total = dife - xmlImporte;
                                            if (Total >= 0)
                                            {
                                                string tot = Convert.ToString(Total);
                                                lblHospedaje2.Text = "$" + decimal.Parse(tot).ToString("N");
                                                string Importe = Convert.ToString(xmlImporte);
                                                lblHospedaje1.Text = "$" + decimal.Parse(Importe).ToString("N");
                                                lblHospedaje2.ForeColor = Color.Green;
                                                string vacio = Convert.ToString(lblFacturas.Text);
                                                if (vacio == "")
                                                {
                                                    decimal suma = 0;
                                                    decimal total = suma + xmlImporte;
                                                    string total1 = Convert.ToString(total);
                                                    lblFacturas.Text = "$ " + decimal.Parse(total1).ToString("N");
                                                }
                                                else
                                                {
                                                    string fac = Convert.ToString(lblFacturas.Text);
                                                    string fac1 = fac.Substring(1);
                                                    decimal suma = Convert.ToDecimal(fac1);
                                                    decimal total = suma + xmlImporte;
                                                    string total1 = Convert.ToString(total);
                                                    lblFacturas.Text = "$ " + decimal.Parse(total1).ToString("N");
                                                }

                                            }
                                            else
                                            {
                                                string tot = Convert.ToString(Total);
                                                lblHospedaje2.Text = "$" + decimal.Parse(tot).ToString("N");
                                                string Importe = Convert.ToString(xmlImporte);
                                                lblHospedaje1.Text = "$" + decimal.Parse(Importe).ToString("N");
                                                lblHospedaje2.ForeColor = Color.Red;
                                                string vacio = Convert.ToString(lblFacturas.Text);
                                                if (vacio == "")
                                                {
                                                    decimal suma = 0;
                                                    decimal total = suma + xmlImporte;
                                                    string total1 = Convert.ToString(total);
                                                    lblFacturas.Text = "$ " + decimal.Parse(total1).ToString("N");
                                                }
                                                else
                                                {
                                                    string fac = Convert.ToString(lblFacturas.Text);
                                                    string fac1 = fac.Substring(1);
                                                    decimal suma = Convert.ToDecimal(fac1);
                                                    decimal total = suma + xmlImporte;
                                                    string total1 = Convert.ToString(total);
                                                    lblFacturas.Text = "$ " + decimal.Parse(total1).ToString("N");
                                                }
                                            }
                                        }
                                        else
                                        {
                                            string difere = Convert.ToString(lblHospedaje.Text);
                                            string diferen = difere.Substring(1);
                                            decimal dife = Convert.ToDecimal(diferen);
                                            decimal Total = dife - xmlImporte;
                                            if (Total >= 0)
                                            {
                                                string tot = Convert.ToString(Total);
                                                lblHospedaje2.Text = "$" + decimal.Parse(tot).ToString("N");
                                                string Importe = Convert.ToString(xmlImporte);
                                                lblHospedaje1.Text = "$" + decimal.Parse(Importe).ToString("N");
                                                lblHospedaje2.ForeColor = Color.Green;
                                                string vacio = Convert.ToString(lblFacturas.Text);
                                                if (vacio == "")
                                                {
                                                    decimal suma = 0;
                                                    decimal total = suma + xmlImporte;
                                                    string total1 = Convert.ToString(total);
                                                    lblFacturas.Text = "$ " + decimal.Parse(total1).ToString("N");
                                                }
                                                else
                                                {
                                                    string fac = Convert.ToString(lblFacturas.Text);
                                                    string fac1 = fac.Substring(1);
                                                    decimal suma = Convert.ToDecimal(fac1);
                                                    decimal total = suma + xmlImporte;
                                                    string total1 = Convert.ToString(total);
                                                    lblFacturas.Text = "$ " + decimal.Parse(total1).ToString("N");
                                                }

                                            }
                                            else
                                            {
                                                string tot = Convert.ToString(Total);
                                                lblHospedaje2.Text = "$" + decimal.Parse(tot).ToString("N");
                                                string Importe = Convert.ToString(xmlImporte);
                                                lblHospedaje1.Text = "$" + decimal.Parse(Importe).ToString("N");
                                                lblHospedaje2.ForeColor = Color.Red;
                                                string vacio = Convert.ToString(lblFacturas.Text);
                                                if (vacio == "")
                                                {
                                                    decimal suma = 0;
                                                    decimal total = suma + xmlImporte;
                                                    string total1 = Convert.ToString(total);
                                                    lblFacturas.Text = "$ " + decimal.Parse(total1).ToString("N");
                                                }
                                                else
                                                {
                                                    string fac = Convert.ToString(lblFacturas.Text);
                                                    string fac1 = fac.Substring(1);
                                                    decimal suma = Convert.ToDecimal(fac1);
                                                    decimal total = suma + xmlImporte;
                                                    string total1 = Convert.ToString(total);
                                                    lblFacturas.Text = "$ " + decimal.Parse(total1).ToString("N");
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        ScriptManager.RegisterStartupScript(this, typeof(Page), "alerta", script4, true);
                                        btnSubirHospedaje.BorderColor = Color.Red;
                                        btnSubirHospedaje.ForeColor = Color.Red;
                                        File.Delete(ruta + nompdf);
                                        File.Delete(ruta + nomxml);
                                    }
                                    
                                }
                                else
                                {
                                    ScriptManager.RegisterStartupScript(this, typeof(Page), "alerta", script2, true);
                                    btnSubirHospedaje.BorderColor = Color.Red;
                                    btnSubirHospedaje.ForeColor = Color.Red;
                                    File.Delete(ruta + nompdf);
                                    File.Delete(ruta + nomxml);
                                }
                            }
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, typeof(Page), "alerta", script3, true);
                        btnSubirHospedaje.BorderColor = Color.Red;
                        btnSubirHospedaje.ForeColor = Color.Red;
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, typeof(Page), "alerta", script, true);
                    btnSubirHospedaje.BorderColor = Color.Red;
                    btnSubirHospedaje.ForeColor = Color.Red;
                }
            }
        }
        protected void TrasladoMe()
        {
            string[] datosXML = new string[8];
            string xmlVersion = string.Empty;
            string xmlrfcReceptor = string.Empty;
            string xmlrfcEmisor = string.Empty;
            string xmlFolio = string.Empty;
            string xmlSerie = string.Empty;
            string xmlUuid = string.Empty;
            DateTime xmlFfactura = new DateTime();
            decimal xmlImporte = 0;
            string[] respuestaSAT = new string[2];
            var nompdf = "";
            var nomxml = "";
            var destino = "";
            string dt = DateTime.Now.ToString("yyyy/MM/dd");
            string script = "alert('Favor de ingresar XML y PDF traslado terrestre')";
            string script1 = "alert('Factura existente en SAT')";
            string script2 = "alert('Factura no existente en SAT, revisar factura de traslado terrestre')";
            string script3 = "alert('Esta factura ya fue ingresada')";
            string script4 = "alert('El UUID de la factura ya fue ingresado')";
            string XMLBD = "";
            string ruta = "C:/Archivos/" + dt + "/Traslado Terrestre/";

            if (!Directory.Exists(ruta))
            {
                Directory.CreateDirectory(ruta);
            }
            foreach (var archivo in Filesubir3.PostedFiles)
            {
                int arch = Convert.ToInt32(Filesubir3.PostedFiles.Count);
                if (arch == 2)
                {
                    string filename = Path.GetFileName(archivo.FileName);
                    string[] var = Directory.GetFiles(ruta, filename);
                    int valor = 0;
                    valor = var.Length;
                    if (valor != 1)
                    {
                        string ext = Path.GetExtension(archivo.FileName);
                        if (ext == ".pdf" || ext == ".xml")
                        {
                            if (ext == ".pdf")
                            {
                                nompdf = Path.GetFileName(archivo.FileName);
                                destino = Path.Combine(ruta, nompdf);
                                archivo.SaveAs(destino);
                            }
                            if (ext == ".xml")
                            {
                                nomxml = Path.GetFileName(archivo.FileName);
                                destino = Path.Combine(ruta, nomxml);
                                archivo.SaveAs(destino);
                                //Leer XML
                                datosXML = obtenDatosXml(ruta + "/" + nomxml);
                                if (datosXML != null)
                                {
                                    xmlVersion = datosXML[0] == null ? string.Empty : datosXML[0].ToString();
                                    xmlrfcReceptor = datosXML[1] == null ? string.Empty : datosXML[1].ToString();
                                    xmlrfcEmisor = datosXML[2] == null ? string.Empty : datosXML[2].ToString();
                                    xmlFolio = datosXML[3] == null ? string.Empty : datosXML[3].ToString();
                                    xmlSerie = datosXML[4] == null ? string.Empty : datosXML[4].ToString();
                                    xmlUuid = datosXML[5] == null ? string.Empty : datosXML[5].ToString();
                                    xmlFfactura = datosXML[6] == null ? DateTime.Now : Convert.ToDateTime(datosXML[6].ToString());
                                    xmlImporte = datosXML[7] == null ? 0 : Convert.ToDecimal(datosXML[7].ToString());
                                }
                                else
                                {
                                    ScriptManager.RegisterStartupScript(this, typeof(Page), "alerta", script, true);
                                }
                                RespuestaSAT respue = new RespuestaSAT();
                                srefer.WebService1SoapClient fade = new srefer.WebService1SoapClient();
                                srefer.ValidaCFDIRequest xchecaSAT = new srefer.ValidaCFDIRequest();
                                srefer.RespuestaSAT refa = new srefer.RespuestaSAT();
                                refa = fade.ValidaCFDI(datosXML[5].ToString(), datosXML[2].ToString(), datosXML[1].ToString(), datosXML[7].ToString());

                                respuestaSAT[0] = refa.CodigoEstatus.ToString();
                                respuestaSAT[1] = refa.Estado.ToString();

                                if (respuestaSAT[1].ToString() == "Vigente")
                                {
                                    conexion.Open();
                                    string select = "select * from TBL_Facturas where UUID = '" + xmlUuid + "'";
                                    SqlCommand selectXML = new SqlCommand(select, conexion);
                                    SqlDataReader registrosXML = selectXML.ExecuteReader();
                                    if (registrosXML.Read())
                                    {
                                        XMLBD = Convert.ToString(registrosXML["UUID"]);
                                    }
                                    conexion.Close();
                                    if (XMLBD=="")
                                    {
                                        ScriptManager.RegisterStartupScript(this, typeof(Page), "alerta", script1, true);
                                        btnSubirTraslado.BorderColor = Color.Green;
                                        btnSubirTraslado.ForeColor = Color.Green;
                                        guardarFac(xmlImporte, nomxml, "Vigente", nompdf, "Traslado terrestre", ruta, xmlUuid);
                                        string to = "";
                                        to = Convert.ToString(lblTraslado2.Text);
                                        to = to.Substring(1);
                                        decimal to1 = Convert.ToDecimal(to);
                                        if (to1!=0)
                                        {
                                            string difere = Convert.ToString(lblTraslado2.Text);
                                            string diferen = difere.Substring(1);
                                            decimal dife = Convert.ToDecimal(diferen);
                                            decimal Total = dife - xmlImporte;
                                            if (Total >= 0)
                                            {
                                                string tot = Convert.ToString(Total);
                                                lblTraslado2.Text = "$" + decimal.Parse(tot).ToString("N");
                                                string Importe = Convert.ToString(xmlImporte);
                                                lblTraslado1.Text = "$" + decimal.Parse(Importe).ToString("N");
                                                lblTraslado2.ForeColor = Color.Green;
                                                string vacio = Convert.ToString(lblFacturas.Text);
                                                if (vacio == "")
                                                {
                                                    decimal suma = 0;
                                                    decimal total = suma + xmlImporte;
                                                    string total1 = Convert.ToString(total);
                                                    lblFacturas.Text = "$ " + decimal.Parse(total1).ToString("N");
                                                }
                                                else
                                                {
                                                    string fac = Convert.ToString(lblFacturas.Text);
                                                    string fac1 = fac.Substring(1);
                                                    decimal suma = Convert.ToDecimal(fac1);
                                                    decimal total = suma + xmlImporte;
                                                    string total1 = Convert.ToString(total);
                                                    lblFacturas.Text = "$ " + decimal.Parse(total1).ToString("N");
                                                }

                                            }
                                            else
                                            {
                                                string tot = Convert.ToString(Total);
                                                lblTraslado2.Text = "$" + decimal.Parse(tot).ToString("N");
                                                string Importe = Convert.ToString(xmlImporte);
                                                lblTraslado1.Text = "$" + decimal.Parse(Importe).ToString("N");
                                                lblTraslado2.ForeColor = Color.Red;
                                                string vacio = Convert.ToString(lblFacturas.Text);
                                                if (vacio == "")
                                                {
                                                    decimal suma = 0;
                                                    decimal total = suma + xmlImporte;
                                                    string total1 = Convert.ToString(total);
                                                    lblFacturas.Text = "$ " + decimal.Parse(total1).ToString("N");
                                                }
                                                else
                                                {
                                                    string fac = Convert.ToString(lblFacturas.Text);
                                                    string fac1 = fac.Substring(1);
                                                    decimal suma = Convert.ToDecimal(fac1);
                                                    decimal total = suma + xmlImporte;
                                                    string total1 = Convert.ToString(total);
                                                    lblFacturas.Text = "$ " + decimal.Parse(total1).ToString("N");
                                                }
                                            }
                                        }
                                        else
                                        {
                                            string difere = Convert.ToString(lblTraslado.Text);
                                            string diferen = difere.Substring(1);
                                            decimal dife = Convert.ToDecimal(diferen);
                                            decimal Total = dife - xmlImporte;
                                            if (Total >= 0)
                                            {
                                                string tot = Convert.ToString(Total);
                                                lblTraslado2.Text = "$" + decimal.Parse(tot).ToString("N");
                                                string Importe = Convert.ToString(xmlImporte);
                                                lblTraslado1.Text = "$" + decimal.Parse(Importe).ToString("N");
                                                lblTraslado2.ForeColor = Color.Green;
                                                string vacio = Convert.ToString(lblFacturas.Text);
                                                if (vacio == "")
                                                {
                                                    decimal suma = 0;
                                                    decimal total = suma + xmlImporte;
                                                    string total1 = Convert.ToString(total);
                                                    lblFacturas.Text = "$ " + decimal.Parse(total1).ToString("N");
                                                }
                                                else
                                                {
                                                    string fac = Convert.ToString(lblFacturas.Text);
                                                    string fac1 = fac.Substring(1);
                                                    decimal suma = Convert.ToDecimal(fac1);
                                                    decimal total = suma + xmlImporte;
                                                    string total1 = Convert.ToString(total);
                                                    lblFacturas.Text = "$ " + decimal.Parse(total1).ToString("N");
                                                }

                                            }
                                            else
                                            {
                                                string tot = Convert.ToString(Total);
                                                lblTraslado2.Text = "$" + decimal.Parse(tot).ToString("N");
                                                string Importe = Convert.ToString(xmlImporte);
                                                lblTraslado1.Text = "$" + decimal.Parse(Importe).ToString("N");
                                                lblTraslado2.ForeColor = Color.Red;
                                                string vacio = Convert.ToString(lblFacturas.Text);
                                                if (vacio == "")
                                                {
                                                    decimal suma = 0;
                                                    decimal total = suma + xmlImporte;
                                                    string total1 = Convert.ToString(total);
                                                    lblFacturas.Text = "$ " + decimal.Parse(total1).ToString("N");
                                                }
                                                else
                                                {
                                                    string fac = Convert.ToString(lblFacturas.Text);
                                                    string fac1 = fac.Substring(1);
                                                    decimal suma = Convert.ToDecimal(fac1);
                                                    decimal total = suma + xmlImporte;
                                                    string total1 = Convert.ToString(total);
                                                    lblFacturas.Text = "$ " + decimal.Parse(total1).ToString("N");
                                                }
                                            }
                                        }
                                        
                                    }
                                    else
                                    {
                                        ScriptManager.RegisterStartupScript(this, typeof(Page), "alerta", script4, true);
                                        btnSubirTraslado.BorderColor = Color.Red;
                                        btnSubirTraslado.ForeColor = Color.Red;
                                        File.Delete(ruta + nompdf);
                                        File.Delete(ruta + nomxml);
                                    }
                                    
                                }
                                else
                                {
                                    ScriptManager.RegisterStartupScript(this, typeof(Page), "alerta", script2, true);
                                    btnSubirTraslado.BorderColor = Color.Red;
                                    btnSubirTraslado.ForeColor = Color.Red;
                                    File.Delete(ruta + nompdf);
                                    File.Delete(ruta + nomxml);
                                }
                            }
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, typeof(Page), "alerta", script3, true);
                        btnSubirTraslado.BorderColor = Color.Red;
                        btnSubirTraslado.ForeColor = Color.Red;
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, typeof(Page), "alerta", script, true);
                    btnSubirTraslado.BorderColor = Color.Red;
                    btnSubirTraslado.ForeColor = Color.Red;
                }
            }
        }
        protected void ComidaMe()
        {
            string[] datosXML = new string[8];
            string xmlVersion = string.Empty;
            string xmlrfcReceptor = string.Empty;
            string xmlrfcEmisor = string.Empty;
            string xmlFolio = string.Empty;
            string xmlSerie = string.Empty;
            string xmlUuid = string.Empty;
            DateTime xmlFfactura = new DateTime();
            decimal xmlImporte = 0;
            string[] respuestaSAT = new string[2];
            var nompdf = "";
            var nomxml = "";
            var destino = "";
            string dt = DateTime.Now.ToString("yyyy/MM/dd");
            string script = "alert('Favor de ingresar XML y PDF comida')";
            string script1 = "alert('Factura existente en SAT')";
            string script2 = "alert('Factura no existente en SAT, revisar factura de comida')";
            string script3 = "alert('Esta factura ya fue ingresada')";
            string script4 = "alert('El UUID de la factura ya fue ingresado')";
            string XMLBD = "";
            string ruta = "C:/Archivos/" + dt + "/Comida/";

            if (!Directory.Exists(ruta))
            {
                Directory.CreateDirectory(ruta);
            }
            foreach (var archivo in Filesubir4.PostedFiles)
            {
                int arch = Convert.ToInt32(Filesubir4.PostedFiles.Count);
                if (arch == 2)
                {
                    string filename = Path.GetFileName(archivo.FileName);
                    string[] var = Directory.GetFiles(ruta, filename);
                    int valor = 0;
                    valor = var.Length;
                    if (valor != 1)
                    {
                        string ext = Path.GetExtension(archivo.FileName);
                        if (ext == ".pdf" || ext == ".xml")
                        {
                            if (ext == ".pdf")
                            {
                                nompdf = Path.GetFileName(archivo.FileName);
                                destino = Path.Combine(ruta, nompdf);
                                archivo.SaveAs(destino);
                            }
                            if (ext == ".xml")
                            {
                                nomxml = Path.GetFileName(archivo.FileName);
                                destino = Path.Combine(ruta, nomxml);
                                archivo.SaveAs(destino);
                                //Leer XML
                                datosXML = obtenDatosXml(ruta + "/" + nomxml);
                                if (datosXML != null)
                                {
                                    xmlVersion = datosXML[0] == null ? string.Empty : datosXML[0].ToString();
                                    xmlrfcReceptor = datosXML[1] == null ? string.Empty : datosXML[1].ToString();
                                    xmlrfcEmisor = datosXML[2] == null ? string.Empty : datosXML[2].ToString();
                                    xmlFolio = datosXML[3] == null ? string.Empty : datosXML[3].ToString();
                                    xmlSerie = datosXML[4] == null ? string.Empty : datosXML[4].ToString();
                                    xmlUuid = datosXML[5] == null ? string.Empty : datosXML[5].ToString();
                                    xmlFfactura = datosXML[6] == null ? DateTime.Now : Convert.ToDateTime(datosXML[6].ToString());
                                    xmlImporte = datosXML[7] == null ? 0 : Convert.ToDecimal(datosXML[7].ToString());
                                }
                                else
                                {
                                    ScriptManager.RegisterStartupScript(this, typeof(Page), "alerta", script, true);
                                }
                                RespuestaSAT respue = new RespuestaSAT();
                                srefer.WebService1SoapClient fade = new srefer.WebService1SoapClient();
                                srefer.ValidaCFDIRequest xchecaSAT = new srefer.ValidaCFDIRequest();
                                srefer.RespuestaSAT refa = new srefer.RespuestaSAT();
                                refa = fade.ValidaCFDI(datosXML[5].ToString(), datosXML[2].ToString(), datosXML[1].ToString(), datosXML[7].ToString());

                                respuestaSAT[0] = refa.CodigoEstatus.ToString();
                                respuestaSAT[1] = refa.Estado.ToString();

                                if (respuestaSAT[1].ToString() == "Vigente")
                                {
                                    conexion.Open();
                                    string select = "select * from TBL_Facturas where UUID = '" + xmlUuid + "'";
                                    SqlCommand selectXML = new SqlCommand(select, conexion);
                                    SqlDataReader registrosXML = selectXML.ExecuteReader();
                                    if (registrosXML.Read())
                                    {
                                        XMLBD = Convert.ToString(registrosXML["UUID"]);
                                    }
                                    conexion.Close();
                                    if (XMLBD=="")
                                    {
                                        ScriptManager.RegisterStartupScript(this, typeof(Page), "alerta", script1, true);
                                        btnSubirComida.BorderColor = Color.Green;
                                        btnSubirComida.ForeColor = Color.Green;
                                        guardarFac(xmlImporte, nomxml, "Vigente", nompdf, "Comida", ruta, xmlUuid);
                                        string to = "";
                                        to = Convert.ToString(lblTraslado2.Text);
                                        to = to.Substring(1);
                                        decimal to1 = Convert.ToDecimal(to);
                                        if (to1!=0)
                                        {
                                            string difere = Convert.ToString(lblComida2.Text);
                                            string diferen = difere.Substring(1);
                                            decimal dife = Convert.ToDecimal(diferen);
                                            decimal Total = dife - xmlImporte;
                                            if (Total >= 0)
                                            {
                                                string tot = Convert.ToString(Total);
                                                lblComida2.Text = "$" + decimal.Parse(tot).ToString("N");
                                                string Importe = Convert.ToString(xmlImporte);
                                                lblComida1.Text = "$" + decimal.Parse(Importe).ToString("N");
                                                lblComida2.ForeColor = Color.Green;
                                                string vacio = Convert.ToString(lblFacturas.Text);
                                                if (vacio == "")
                                                {
                                                    decimal suma = 0;
                                                    decimal total = suma + xmlImporte;
                                                    string total1 = Convert.ToString(total);
                                                    lblFacturas.Text = "$ " + decimal.Parse(total1).ToString("N");
                                                }
                                                else
                                                {
                                                    string fac = Convert.ToString(lblFacturas.Text);
                                                    string fac1 = fac.Substring(1);
                                                    decimal suma = Convert.ToDecimal(fac1);
                                                    decimal total = suma + xmlImporte;
                                                    string total1 = Convert.ToString(total);
                                                    lblFacturas.Text = "$ " + decimal.Parse(total1).ToString("N");
                                                }
                                            }
                                            else
                                            {
                                                string tot = Convert.ToString(Total);
                                                lblComida2.Text = "$" + decimal.Parse(tot).ToString("N");
                                                string Importe = Convert.ToString(xmlImporte);
                                                lblComida1.Text = "$" + decimal.Parse(Importe).ToString("N");
                                                lblComida2.ForeColor = Color.Red;
                                                string vacio = Convert.ToString(lblFacturas.Text);
                                                if (vacio == "")
                                                {
                                                    decimal suma = 0;
                                                    decimal total = suma + xmlImporte;
                                                    string total1 = Convert.ToString(total);
                                                    lblFacturas.Text = "$ " + decimal.Parse(total1).ToString("N");
                                                }
                                                else
                                                {
                                                    string fac = Convert.ToString(lblFacturas.Text);
                                                    string fac1 = fac.Substring(1);
                                                    decimal suma = Convert.ToDecimal(fac1);
                                                    decimal total = suma + xmlImporte;
                                                    string total1 = Convert.ToString(total);
                                                    lblFacturas.Text = "$ " + decimal.Parse(total1).ToString("N");
                                                }
                                            }
                                        }
                                        else
                                        {
                                            string difere = Convert.ToString(lblComida.Text);
                                            string diferen = difere.Substring(1);
                                            decimal dife = Convert.ToDecimal(diferen);
                                            decimal Total = dife - xmlImporte;
                                            if (Total >= 0)
                                            {
                                                string tot = Convert.ToString(Total);
                                                lblComida2.Text = "$" + decimal.Parse(tot).ToString("N");
                                                string Importe = Convert.ToString(xmlImporte);
                                                lblComida1.Text = "$" + decimal.Parse(Importe).ToString("N");
                                                lblComida2.ForeColor = Color.Green;
                                                string vacio = Convert.ToString(lblFacturas.Text);
                                                if (vacio == "")
                                                {
                                                    decimal suma = 0;
                                                    decimal total = suma + xmlImporte;
                                                    string total1 = Convert.ToString(total);
                                                    lblFacturas.Text = "$ " + decimal.Parse(total1).ToString("N");
                                                }
                                                else
                                                {
                                                    string fac = Convert.ToString(lblFacturas.Text);
                                                    string fac1 = fac.Substring(1);
                                                    decimal suma = Convert.ToDecimal(fac1);
                                                    decimal total = suma + xmlImporte;
                                                    string total1 = Convert.ToString(total);
                                                    lblFacturas.Text = "$ " + decimal.Parse(total1).ToString("N");
                                                }
                                            }
                                            else
                                            {
                                                string tot = Convert.ToString(Total);
                                                lblComida2.Text = "$" + decimal.Parse(tot).ToString("N");
                                                string Importe = Convert.ToString(xmlImporte);
                                                lblComida1.Text = "$" + decimal.Parse(Importe).ToString("N");
                                                lblComida2.ForeColor = Color.Red;
                                                string vacio = Convert.ToString(lblFacturas.Text);
                                                if (vacio == "")
                                                {
                                                    decimal suma = 0;
                                                    decimal total = suma + xmlImporte;
                                                    string total1 = Convert.ToString(total);
                                                    lblFacturas.Text = "$ " + decimal.Parse(total1).ToString("N");
                                                }
                                                else
                                                {
                                                    string fac = Convert.ToString(lblFacturas.Text);
                                                    string fac1 = fac.Substring(1);
                                                    decimal suma = Convert.ToDecimal(fac1);
                                                    decimal total = suma + xmlImporte;
                                                    string total1 = Convert.ToString(total);
                                                    lblFacturas.Text = "$ " + decimal.Parse(total1).ToString("N");
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        ScriptManager.RegisterStartupScript(this, typeof(Page), "alerta", script4, true);
                                        btnSubirComida.BorderColor = Color.Red;
                                        btnSubirComida.ForeColor = Color.Red;
                                        File.Delete(ruta + nompdf);
                                        File.Delete(ruta + nomxml);
                                    }
                                }
                                else
                                {
                                    ScriptManager.RegisterStartupScript(this, typeof(Page), "alerta", script2, true);
                                    btnSubirComida.BorderColor = Color.Red;
                                    btnSubirComida.ForeColor = Color.Red;
                                    File.Delete(ruta + nompdf);
                                    File.Delete(ruta + nomxml);
                                }
                            }
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, typeof(Page), "alerta", script3, true);
                        btnSubirComida.BorderColor = Color.Red;
                        btnSubirComida.ForeColor = Color.Red;
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, typeof(Page), "alerta", script, true);
                    btnSubirComida.BorderColor = Color.Red;
                    btnSubirComida.ForeColor = Color.Red;
                }
            }
        }
        protected void RentaMe()
        {
            string[] datosXML = new string[8];
            string xmlVersion = string.Empty;
            string xmlrfcReceptor = string.Empty;
            string xmlrfcEmisor = string.Empty;
            string xmlFolio = string.Empty;
            string xmlSerie = string.Empty;
            string xmlUuid = string.Empty;
            DateTime xmlFfactura = new DateTime();
            decimal xmlImporte = 0;
            string[] respuestaSAT = new string[2];
            var nompdf = "";
            var nomxml = "";
            var destino = "";
            string dt = DateTime.Now.ToString("yyyy/MM/dd");
            string script = "alert('Favor de ingresar XML y PDF renta automovil')";
            string script1 = "alert('Factura existente en SAT')";
            string script2 = "alert('Factura no existente en SAT, revisar factura de automovil')";
            string script3 = "alert('Esta factura ya fue ingresada')";
            string script4 = "alert('El UUID de la factura ya fue ingresado')";
            string XMLBD = "";
            string ruta = "C:/Archivos/" + dt + "/Renta Automovil/";

            if (!Directory.Exists(ruta))
            {
                Directory.CreateDirectory(ruta);
            }
            foreach (var archivo in Filesubir6.PostedFiles)
            {
                int arch = Convert.ToInt32(Filesubir6.PostedFiles.Count);
                if (arch == 2)
                {
                    string filename = Path.GetFileName(archivo.FileName);
                    string[] var = Directory.GetFiles(ruta, filename);
                    int valor = 0;
                    valor = var.Length;
                    if (valor != 1)
                    {
                        string ext = Path.GetExtension(archivo.FileName);
                        if (ext == ".pdf" || ext == ".xml")
                        {
                            if (ext == ".pdf")
                            {
                                nompdf = Path.GetFileName(archivo.FileName);
                                destino = Path.Combine(ruta, nompdf);
                                archivo.SaveAs(destino);
                            }
                            if (ext == ".xml")
                            {
                                nomxml = Path.GetFileName(archivo.FileName);
                                destino = Path.Combine(ruta, nomxml);
                                archivo.SaveAs(destino);
                                //Leer XML
                                datosXML = obtenDatosXml(ruta + "/" + nomxml);
                                if (datosXML != null)
                                {
                                    xmlVersion = datosXML[0] == null ? string.Empty : datosXML[0].ToString();
                                    xmlrfcReceptor = datosXML[1] == null ? string.Empty : datosXML[1].ToString();
                                    xmlrfcEmisor = datosXML[2] == null ? string.Empty : datosXML[2].ToString();
                                    xmlFolio = datosXML[3] == null ? string.Empty : datosXML[3].ToString();
                                    xmlSerie = datosXML[4] == null ? string.Empty : datosXML[4].ToString();
                                    xmlUuid = datosXML[5] == null ? string.Empty : datosXML[5].ToString();
                                    xmlFfactura = datosXML[6] == null ? DateTime.Now : Convert.ToDateTime(datosXML[6].ToString());
                                    xmlImporte = datosXML[7] == null ? 0 : Convert.ToDecimal(datosXML[7].ToString());
                                }
                                else
                                {
                                    ScriptManager.RegisterStartupScript(this, typeof(Page), "alerta", script, true);
                                }
                                RespuestaSAT respue = new RespuestaSAT();
                                srefer.WebService1SoapClient fade = new srefer.WebService1SoapClient();
                                srefer.ValidaCFDIRequest xchecaSAT = new srefer.ValidaCFDIRequest();
                                srefer.RespuestaSAT refa = new srefer.RespuestaSAT();
                                refa = fade.ValidaCFDI(datosXML[5].ToString(), datosXML[2].ToString(), datosXML[1].ToString(), datosXML[7].ToString());

                                respuestaSAT[0] = refa.CodigoEstatus.ToString();
                                respuestaSAT[1] = refa.Estado.ToString();

                                if (respuestaSAT[1].ToString() == "Vigente")
                                {
                                    conexion.Open();
                                    string select = "select * from TBL_Facturas where UUID = '" + xmlUuid + "'";
                                    SqlCommand selectXML = new SqlCommand(select, conexion);
                                    SqlDataReader registrosXML = selectXML.ExecuteReader();
                                    if (registrosXML.Read())
                                    {
                                        XMLBD = Convert.ToString(registrosXML["UUID"]);
                                    }
                                    conexion.Close();
                                    if (XMLBD=="")
                                    {
                                        ScriptManager.RegisterStartupScript(this, typeof(Page), "alerta", script1, true);
                                        btnSubirRenta.BorderColor = Color.Green;
                                        btnSubirRenta.ForeColor = Color.Green;
                                        guardarFac(xmlImporte, nomxml, "Vigente", nompdf, "Renta Automovil", ruta, xmlUuid);
                                        string to = "";
                                        to = Convert.ToString(lblTraslado2.Text);
                                        to = to.Substring(1);
                                        decimal to1 = Convert.ToDecimal(to);
                                        if (to1!=0)
                                        {
                                            string difere = Convert.ToString(lblRenta2.Text);
                                            string diferen = difere.Substring(1);
                                            decimal dife = Convert.ToDecimal(diferen);
                                            decimal Total = dife - xmlImporte;
                                            if (Total >= 0)
                                            {
                                                string tot = Convert.ToString(Total);
                                                lblRenta2.Text = "$" + decimal.Parse(tot).ToString("N");
                                                string Importe = Convert.ToString(xmlImporte);
                                                lblRenta1.Text = "$" + decimal.Parse(Importe).ToString("N");
                                                lblRenta2.ForeColor = Color.Green;
                                                string vacio = Convert.ToString(lblFacturas.Text);
                                                if (vacio == "")
                                                {
                                                    decimal suma = 0;
                                                    decimal total = suma + xmlImporte;
                                                    string total1 = Convert.ToString(total);
                                                    lblFacturas.Text = "$ " + decimal.Parse(total1).ToString("N");
                                                }
                                                else
                                                {
                                                    string fac = Convert.ToString(lblFacturas.Text);
                                                    string fac1 = fac.Substring(1);
                                                    decimal suma = Convert.ToDecimal(fac1);
                                                    decimal total = suma + xmlImporte;
                                                    string total1 = Convert.ToString(total);
                                                    lblFacturas.Text = "$ " + decimal.Parse(total1).ToString("N");
                                                }

                                            }
                                            else
                                            {
                                                string tot = Convert.ToString(Total);
                                                lblRenta2.Text = "$" + decimal.Parse(tot).ToString("N");
                                                string Importe = Convert.ToString(xmlImporte);
                                                lblRenta1.Text = "$" + decimal.Parse(Importe).ToString("N");
                                                lblRenta2.ForeColor = Color.Red;
                                                string vacio = Convert.ToString(lblFacturas.Text);
                                                if (vacio == "")
                                                {
                                                    decimal suma = 0;
                                                    decimal total = suma + xmlImporte;
                                                    string total1 = Convert.ToString(total);
                                                    lblFacturas.Text = "$ " + decimal.Parse(total1).ToString("N");
                                                }
                                                else
                                                {
                                                    string fac = Convert.ToString(lblFacturas.Text);
                                                    string fac1 = fac.Substring(1);
                                                    decimal suma = Convert.ToDecimal(fac1);
                                                    decimal total = suma + xmlImporte;
                                                    string total1 = Convert.ToString(total);
                                                    lblFacturas.Text = "$ " + decimal.Parse(total1).ToString("N");
                                                }
                                            }
                                        }
                                        else
                                        {
                                            string difere = Convert.ToString(lblRenta.Text);
                                            string diferen = difere.Substring(1);
                                            decimal dife = Convert.ToDecimal(diferen);
                                            decimal Total = dife - xmlImporte;
                                            if (Total >= 0)
                                            {
                                                string tot = Convert.ToString(Total);
                                                lblRenta2.Text = "$" + decimal.Parse(tot).ToString("N");
                                                string Importe = Convert.ToString(xmlImporte);
                                                lblRenta1.Text = "$" + decimal.Parse(Importe).ToString("N");
                                                lblRenta2.ForeColor = Color.Green;
                                                string vacio = Convert.ToString(lblFacturas.Text);
                                                if (vacio == "")
                                                {
                                                    decimal suma = 0;
                                                    decimal total = suma + xmlImporte;
                                                    string total1 = Convert.ToString(total);
                                                    lblFacturas.Text = "$ " + decimal.Parse(total1).ToString("N");
                                                }
                                                else
                                                {
                                                    string fac = Convert.ToString(lblFacturas.Text);
                                                    string fac1 = fac.Substring(1);
                                                    decimal suma = Convert.ToDecimal(fac1);
                                                    decimal total = suma + xmlImporte;
                                                    string total1 = Convert.ToString(total);
                                                    lblFacturas.Text = "$ " + decimal.Parse(total1).ToString("N");
                                                }

                                            }
                                            else
                                            {
                                                string tot = Convert.ToString(Total);
                                                lblRenta2.Text = "$" + decimal.Parse(tot).ToString("N");
                                                string Importe = Convert.ToString(xmlImporte);
                                                lblRenta1.Text = "$" + decimal.Parse(Importe).ToString("N");
                                                lblRenta2.ForeColor = Color.Red;
                                                string vacio = Convert.ToString(lblFacturas.Text);
                                                if (vacio == "")
                                                {
                                                    decimal suma = 0;
                                                    decimal total = suma + xmlImporte;
                                                    string total1 = Convert.ToString(total);
                                                    lblFacturas.Text = "$ " + decimal.Parse(total1).ToString("N");
                                                }
                                                else
                                                {
                                                    string fac = Convert.ToString(lblFacturas.Text);
                                                    string fac1 = fac.Substring(1);
                                                    decimal suma = Convert.ToDecimal(fac1);
                                                    decimal total = suma + xmlImporte;
                                                    string total1 = Convert.ToString(total);
                                                    lblFacturas.Text = "$ " + decimal.Parse(total1).ToString("N");
                                                }
                                            }
                                        }
                                        
                                    }
                                    else
                                    {
                                        ScriptManager.RegisterStartupScript(this, typeof(Page), "alerta", script4, true);
                                        btnSubirRenta.BorderColor = Color.Red;
                                        btnSubirRenta.ForeColor = Color.Red;
                                        File.Delete(ruta + nompdf);
                                        File.Delete(ruta + nomxml);
                                    }
                                    
                                }
                                else
                                {
                                    ScriptManager.RegisterStartupScript(this, typeof(Page), "alerta", script2, true);
                                    btnSubirRenta.BorderColor = Color.Red;
                                    btnSubirRenta.ForeColor = Color.Red;
                                    File.Delete(ruta + nompdf);
                                    File.Delete(ruta + nomxml);
                                }
                            }
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, typeof(Page), "alerta", script3, true);
                        btnSubirRenta.BorderColor = Color.Red;
                        btnSubirRenta.ForeColor = Color.Red;
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, typeof(Page), "alerta", script, true);
                    btnSubirRenta.BorderColor = Color.Red;
                    btnSubirRenta.ForeColor = Color.Red;
                }
            }
        }
        protected void NoComparableMe()
        {
            string dt = DateTime.Now.ToString("yyyy/MM/dd");
            string ruta = "C:/Archivos/" + dt + "/No Comprobable/";
            string nomfac = "";
            string destino = "";
            string script = "alert('Archivo ingresado correctamente')";
            string script1 = "alert('Favor de ingresar archivo')";
            string script2 = "alert('Archivo ya ingresado')";
            string ruta1 = ruta;
            int arch = 0;
            if (!Directory.Exists(ruta))
            {
                Directory.CreateDirectory(ruta);
            }
            arch = Convert.ToInt32(Filesubir5.PostedFiles.Count);
                foreach (var archivo in Filesubir5.PostedFiles)
                {
                if (arch == 1)
                {
                    string filename = Path.GetFileName(archivo.FileName);
                    string[] var = Directory.GetFiles(ruta, filename);
                    int valor = 0;
                    valor = var.Length;
                    if (valor!=1)
                    {
                        nomfac = Path.GetFileName(archivo.FileName);
                        destino = Path.Combine(ruta, nomfac);
                        if (destino != ruta)
                        {
                            archivo.SaveAs(destino);
                            ScriptManager.RegisterStartupScript(this, typeof(Page), "alerta", script, true);
                            btnSubirNoComprobable.BorderColor = Color.Green;
                            btnSubirNoComprobable.ForeColor = Color.Green;
                            guardarFac(0, "", "Vigente", nomfac, "No comprobable", ruta,"");
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, typeof(Page), "alerta", script1, true);
                            btnSubirNoComprobable.BorderColor = Color.Red;
                            btnSubirNoComprobable.ForeColor = Color.Red;
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, typeof(Page), "alerta", script2, true);
                        btnSubirNoComprobable.BorderColor = Color.Red;
                        btnSubirNoComprobable.ForeColor = Color.Red;
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, typeof(Page), "alerta", script1, true);
                    btnSubirNoComprobable.BorderColor = Color.Red;
                    btnSubirNoComprobable.ForeColor = Color.Red;
                }
            }
        }
        private string [] obtenDatosXml(string rutaC)
        {
            string[] respuesta = new string[8];
            //Documento XML
            XmlDocument xmlFactura = new XmlDocument();
            xmlFactura.Load(rutaC);
            //Nodos del documento
            XmlNode nodoFactura = xmlFactura.DocumentElement;
            //Atributos Root
            XmlAttribute rootAtributo = nodoFactura.Attributes["Version"];
            if (rootAtributo != null)
                respuesta[0] = rootAtributo.Value.ToString();

            rootAtributo = null;
            if (nodoFactura.Prefix == "cfdi")
                respuesta[1] = nodoFactura["cfdi:Receptor"].GetAttribute("Rfc");
            else
                respuesta[1] = nodoFactura["Receptor"].GetAttribute("Rfc");

            rootAtributo = null;
            if (nodoFactura.Prefix == "cfdi")
                respuesta[2] = nodoFactura["cfdi:Emisor"].GetAttribute("Rfc");
            else
                respuesta[2] = nodoFactura["Emisor"].GetAttribute("Rfc");

            rootAtributo = null;
            rootAtributo = nodoFactura.Attributes["Folio"];
            if (rootAtributo != null)
                respuesta[3] = rootAtributo.Value.ToString();

            rootAtributo = null;
            rootAtributo = nodoFactura.Attributes["Serie"];
            if (rootAtributo != null)
                respuesta[4] = rootAtributo.Value.ToString();

            XmlNodeList nodeList = xmlFactura.GetElementsByTagName("cfdi:Complemento");
            foreach (XmlElement nodo in nodeList)
            {
                respuesta[5] = nodo["tfd:TimbreFiscalDigital"].GetAttribute("UUID");
            }

            rootAtributo = null;
            rootAtributo = nodoFactura.Attributes["Fecha"];
            if (rootAtributo != null)
                respuesta[6] = rootAtributo.Value.ToString();

            rootAtributo = null;
            rootAtributo = nodoFactura.Attributes["Total"];
            if (rootAtributo != null)
                respuesta[7] = rootAtributo.Value.ToString();
            return respuesta;
        }
        public class RespuestaSAT
        {
            public string UUID { get; set; }
            public string CodigoEstatus { get; set; }
            public string EsCancelable { get; set; }
            public string Estado { get; set; }
        }

        protected void btnSubirHospedaje_Click(object sender, EventArgs e)
        {
            HospedajeMe();
        }

        protected void btnSubirTraslado_Click(object sender, EventArgs e)
        {
            TrasladoMe();
        }

        protected void btnSubirComida_Click(object sender, EventArgs e)
        {
            ComidaMe();
        }

        protected void btnSubirRenta_Click(object sender, EventArgs e)
        {
            RentaMe();
        }

        protected void btnSubirNoComprobable_Click(object sender, EventArgs e)
        {
            NoComparableMe();
        }

        protected void btnEnviar_Click(object sender, EventArgs e)
        {
            conexion.Open();
            GridViewRow row = Resultado.SelectedRow;
            int id = Convert.ToInt32(Resultado.DataKeys[row.RowIndex].Value);
            string query = "update TBL_Solicitud set Estatus = 12  where ID_Solicitud = " + id;
            SqlCommand update = new SqlCommand(query,conexion);
            update.ExecuteNonQuery();
            conexion.Close();
            string com = "";
            string dt = DateTime.Now.ToString("yyyy/MM/dd");
            com = Convert.ToString(txtComentario.Text);
            int anc = com.Length;
            if (anc != 0)
            {
                conexion.Open();
                string query3 = "insert into TBL_Comentarios (Comentario,ID_Solicitud,Fecha_Comentario) values ('" + com + "'," + id + ",'" + dt + "')";
                SqlCommand inserCom = new SqlCommand(query3, conexion);
                inserCom.ExecuteNonQuery();
                conexion.Close();
            }
        }
        protected void guardarFac(decimal Gasto,string NomXML,string Respuesta,string NomPDF,string gasto,string ruta,string UUID)
        {
            GridViewRow row = Resultado.SelectedRow;
            int id = Convert.ToInt32(Resultado.DataKeys[row.RowIndex].Value);
            int IDusu = 0,IDemp=0,au1=0,au2=0,au3=0,au4=0,au5=0,au6=0,au7=0;
            conexion.Open();
            string query = "select * from TBL_Solicitud where ID_Solicitud = "+id;
            SqlCommand selecSol = new SqlCommand(query,conexion);
            SqlDataReader registro = selecSol.ExecuteReader();
            if (registro.Read())
            {
                IDemp = Convert.ToInt32(registro["Empleado"]);
                au1 = Convert.ToInt32(registro["Total_solicitado"]);
                au2 = Convert.ToInt32(registro["Gasto_vuelo"]);
                au3 = Convert.ToInt32(registro["Gasto_hospedaje"]);
                au4 = Convert.ToInt32(registro["Gasto_terrestre"]);
                au5 = Convert.ToInt32(registro["Gasto_comida"]);
                au6 = Convert.ToInt32(registro["Gasto_otros"]);
                au7 = Convert.ToInt32(registro["Gasto_Renta"]);
            }
            conexion.Close();
            conexion.Open();
            string query1 = "select * from TBL_Usuarios where Empleado="+IDemp;
            SqlCommand selecUsu = new SqlCommand(query1, conexion);
            SqlDataReader registro1 = selecUsu.ExecuteReader();
            if (registro1.Read())
            {
                IDusu = Convert.ToInt32(registro1["ID_Usuario"]);
            }
            conexion.Close();
            if (gasto=="Vuelo")
            {
                conexion.Open();
                string query2 = "insert into TBL_Facturas (Usuario,Solicitud,Gasto_Factura,Nombre_XML,Respuesta,Nombre_PDF,Gasto,Ruta,UUID,Gasto_Solicitud) values (" + IDusu + "," + id + "," + Gasto + ",'" + NomXML + "','" + Respuesta + "','" + NomPDF + "','" + gasto + "','" + ruta + "','" + UUID + "','"+au2+"')";
                SqlCommand insertFac = new SqlCommand(query2, conexion);
                insertFac.ExecuteNonQuery();
                conexion.Close();
                txtComentario.Text = "";
            }
            if (gasto=="Hospedaje")
            {
                conexion.Open();
                string query2 = "insert into TBL_Facturas (Usuario,Solicitud,Gasto_Factura,Nombre_XML,Respuesta,Nombre_PDF,Gasto,Ruta,UUID,Gasto_Solicitud) values (" + IDusu + "," + id + "," + Gasto + ",'" + NomXML + "','" + Respuesta + "','" + NomPDF + "','" + gasto + "','" + ruta + "','" + UUID + "','" + au3 + "')";
                SqlCommand insertFac = new SqlCommand(query2, conexion);
                insertFac.ExecuteNonQuery();
                conexion.Close();
                txtComentario.Text = "";
            }
            if (gasto=="Traslado terrestre")
            {
                conexion.Open();
                string query2 = "insert into TBL_Facturas (Usuario,Solicitud,Gasto_Factura,Nombre_XML,Respuesta,Nombre_PDF,Gasto,Ruta,UUID,Gasto_Solicitud) values (" + IDusu + "," + id + "," + Gasto + ",'" + NomXML + "','" + Respuesta + "','" + NomPDF + "','" + gasto + "','" + ruta + "','" + UUID + "','" + au4 + "')";
                SqlCommand insertFac = new SqlCommand(query2, conexion);
                insertFac.ExecuteNonQuery();
                conexion.Close();
                txtComentario.Text = "";
            }
            if (gasto=="Comida")
            {
                conexion.Open();
                string query2 = "insert into TBL_Facturas (Usuario,Solicitud,Gasto_Factura,Nombre_XML,Respuesta,Nombre_PDF,Gasto,Ruta,UUID,Gasto_Solicitud) values (" + IDusu + "," + id + "," + Gasto + ",'" + NomXML + "','" + Respuesta + "','" + NomPDF + "','" + gasto + "','" + ruta + "','" + UUID + "','" + au5 + "')";
                SqlCommand insertFac = new SqlCommand(query2, conexion);
                insertFac.ExecuteNonQuery();
                conexion.Close();
                txtComentario.Text = "";
            }
            if (gasto=="Renta Automovil")
            {
                conexion.Open();
                string query2 = "insert into TBL_Facturas (Usuario,Solicitud,Gasto_Factura,Nombre_XML,Respuesta,Nombre_PDF,Gasto,Ruta,UUID,Gasto_Solicitud) values (" + IDusu + "," + id + "," + Gasto + ",'" + NomXML + "','" + Respuesta + "','" + NomPDF + "','" + gasto + "','" + ruta + "','" + UUID + "','" + au7 + "')";
                SqlCommand insertFac = new SqlCommand(query2, conexion);
                insertFac.ExecuteNonQuery();
                conexion.Close();
                txtComentario.Text = "";
            }
            if (gasto=="No Comprobable")
            {
                conexion.Open();
                string query2 = "insert into TBL_Facturas (Usuario,Solicitud,Gasto_Factura,Nombre_XML,Respuesta,Nombre_PDF,Gasto,Ruta,UUID,Gasto_Solicitud) values (" + IDusu + "," + id + "," + Gasto + ",'" + NomXML + "','" + Respuesta + "','" + NomPDF + "','" + gasto + "','" + ruta + "','" + UUID + "','" + au6 + "')";
                SqlCommand insertFac = new SqlCommand(query2, conexion);
                insertFac.ExecuteNonQuery();
                conexion.Close();
                txtComentario.Text = "";
            }
        }

        protected void btnComentario_Click(object sender, EventArgs e)
        {
            if (txtComentario.Enabled == false)
            {
                txtComentario.Enabled = true;
            }
            else
            {
                txtComentario.Enabled = false;
            }
            
        }
        protected void Calc(decimal importe)
        {

        }
        protected void Enviar_Correo(string correo)
        {
            
            //Mensaje
            string mensaje = "";
            mensaje = "<html style='font-size: 8px; font-family: Helvetica, Arial; margin: 0; padding: 0; text-align: center; vertical-align: top; background: #eeeeee;'>";
            mensaje += "<head><title></title><meta charset='utf-8' /></head>";
            mensaje += "<body style='width: 100%; height: 100%; margin: 0; padding: 0; text-align: left; vertical-align: top; margin-left: 10px;'>";
            mensaje += "<p style='font-size: 24px; font-weight: bold; text-transform: uppercase; color: #000000;'>Hola, " + "" + " " + "" + "</p>";
            mensaje += "<div><p style='font-size: 16px; color: #000000;'>";
            mensaje += "Por medio de la presente permitame compartirle su Usuario: " + "" + " y su contraseña que deberá cambiar lo antes posible Contraseña: 123";
            mensaje += "<div><p style='font-size: 16px; color: #000000;'>";
            mensaje += "Para poder acceder al sistema ingresar: www.portalGastos.com";
            mensaje += "<A HREF='www.portalGastos.com' TARGET='_new'></A>";
            mensaje += "</p></div>";
            mensaje += "<div><p style='font-size: 16px; color: #000000;'>";
            mensaje += "Atentamente: Portal de Gastos.";
            mensaje += "</p></div>";
            mensaje += "</body></html>";

            //Imagen
            string imgCorreo = string.Empty;
            imgCorreo = ConfigurationManager.AppSettings["imgCorreo"].ToString();
            AlternateView htmlView = AlternateView.CreateAlternateViewFromString(mensaje, Encoding.UTF8, MediaTypeNames.Text.Html);
            LinkedResource img = new LinkedResource(imgCorreo, MediaTypeNames.Image.Jpeg);
            img.ContentId = "imagen";
            htmlView.LinkedResources.Add(img);

            string strServer = ConfigurationManager.AppSettings["mailServer"].ToString();
            string strRemitente = ConfigurationManager.AppSettings["mailRemitente"].ToString();
            string strCC = ConfigurationManager.AppSettings["mailCC"].ToString();
            string strContrasenia = ConfigurationManager.AppSettings["mailContrasenia"].ToString();
            int strPuerto = Convert.ToInt32(ConfigurationManager.AppSettings["mailPuerto"].ToString());
            bool ssl = Convert.ToBoolean(ConfigurationManager.AppSettings["mailSSL"].ToString());

            SmtpClient datosSmtp = new SmtpClient();
            datosSmtp.Credentials = new System.Net.NetworkCredential(strRemitente, strContrasenia);
            datosSmtp.Host = strServer;
            datosSmtp.Port = strPuerto;
            datosSmtp.EnableSsl = ssl;

            MailMessage objetoMail = new MailMessage();
            MailAddress mailRemitente = new MailAddress(strRemitente);
            objetoMail.From = mailRemitente;
            objetoMail.AlternateViews.Add(htmlView);

            MailAddress mailDestinatario = new MailAddress(correo);
            objetoMail.To.Add(mailDestinatario);
            objetoMail.Subject = "Portal de Gastos, Usuario y contraseña";

            try
            {
                datosSmtp.Send(objetoMail);
                datosSmtp.Dispose();
            }
            catch (Exception ex)
            {
                
            }

        }

        protected void btnCalcular_Click(object sender, EventArgs e)
        {
            string script = "alert('Ingresar cantidad valida')";
            string val = Convert.ToString(lblNoComprobable.Text);
            string val1 = Convert.ToString(txtCantidad.Text);
            string val2 = Convert.ToString(lblCantidad1.Text);
            if (val1 !="")
            {
                if (val2 == "")
                {
                    val = val.Substring(1);
                    decimal val11 = Convert.ToDecimal(val1);
                    decimal vall = Convert.ToDecimal(val);
                    decimal total = vall - val11;
                    if (total>=0)
                    {
                        string total1 = Convert.ToString(total);
                        lblCantidad1.Text = "$ " + decimal.Parse(total1).ToString("N");
                        lblCantidad1.ForeColor = Color.Green;
                        txtCantidad.Text = "";
                    }
                    else
                    {
                        string total1 = Convert.ToString(total);
                        lblCantidad1.Text = "$ " + decimal.Parse(total1).ToString("N");
                        lblCantidad1.ForeColor = Color.Red;
                        txtCantidad.Text = "";
                    }
                }
                else
                {
                    val2 = val2.Substring(1);
                    decimal val11 = Convert.ToDecimal(val1);
                    decimal val22 = Convert.ToDecimal(val2);
                    decimal total = val22 - val11;
                    if (total>=0)
                    {
                        string total1 = Convert.ToString(total);
                        lblCantidad1.Text = "$ " + decimal.Parse(total1).ToString("N");
                        lblCantidad1.ForeColor = Color.Green;
                        txtCantidad.Text = "";
                    }
                    else
                    {
                        string total1 = Convert.ToString(total);
                        lblCantidad1.Text = "$ " + decimal.Parse(total1).ToString("N");
                        lblCantidad1.ForeColor = Color.Red;
                        txtCantidad.Text = "";
                    }
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, typeof(Page), "alerta", script, true);
            }
        }
        protected void operaciones(string Gasto_Fac,string Gasto,string Gasto_Sol)
        {
            GridViewRow row = Resultado.SelectedRow;
            int id = Convert.ToInt32(Resultado.DataKeys[row.RowIndex].Value);
            string ID_User = Session["ID_User"].ToString();
            conexion.Open();
            string query= "insert into TBL_Facturas (Usuario,Solicitud,Gasto_Factura,Nombre_XML,Respuesta,Nombre_PDF,Gasto,Ruta,UUID,Gasto_Solicitud) " +
                "values ("+ID_User+","+id+","+Gasto_Fac+",'','Gasto sin factura','','"+Gasto+"','','',"+Gasto_Sol+")";
            SqlCommand insertF = new SqlCommand(query,conexion);
            insertF.ExecuteNonQuery();
            conexion.Close();
        }

        protected void btnVuelo_Click(object sender, EventArgs e)
        {
            string script = "alert('Ingresar cantidad valida')";
            string lblVuelos = Convert.ToString(lblVuelo.Text);
            string lblVuelos1 = Convert.ToString(lblVuelo1.Text);
            string lblVuelos2 = Convert.ToString(lblVuelo2.Text);
            string lblfacturass = Convert.ToString(lblFacturas.Text);
            string txtVuelos = Convert.ToString(txtVuelo.Text);
            if (txtVuelos != "")
            {
                if (lblVuelos2 == "$ 0.00" && lblfacturass == "$ 0.00")
                {
                    lblVuelos = lblVuelos.Substring(1);
                    lblVuelos1 = lblVuelos1.Substring(1);
                    lblVuelos2 = lblVuelos2.Substring(1);
                    decimal txtVuelos1 = Convert.ToDecimal(txtVuelos);
                    decimal lblVuelos11 = Convert.ToDecimal(lblVuelos);
                    decimal total = lblVuelos11 - txtVuelos1;
                    decimal total2 = Math.Abs(total);
                    string total3 = Convert.ToString(total2);
                    string total1 = Convert.ToString(total);
                    if (total > 0)
                    {
                        lblFacturas.Text = "$ " + decimal.Parse(txtVuelos).ToString("N");
                        lblVuelo2.Text = "$ " + decimal.Parse(total1).ToString("N");
                        lblVuelo1.Text = "$ " + decimal.Parse(txtVuelos).ToString("N");
                        lblVuelo2.ForeColor = Color.Green;
                        txtVuelo.Text = "";
                        lblVuelos = lblVuelos.Replace(",", "");
                        operaciones(txtVuelos, "Vuelo", lblVuelos);
                    }
                    else
                    {
                        lblFacturas.Text = "$ " + decimal.Parse(txtVuelos).ToString("N");
                        lblVuelo2.Text = "$ " + decimal.Parse(total1).ToString("N");
                        lblVuelo1.Text = "$ " + decimal.Parse(txtVuelos).ToString("N");
                        lblVuelo2.ForeColor = Color.Red;
                        txtVuelo.Text = "";
                        lblVuelos = lblVuelos.Replace(",", "");
                        operaciones(txtVuelos, "Vuelo", lblVuelos);
                    }
                }
                else
                {
                    lblVuelos2 = lblVuelos2.Substring(1);
                    lblfacturass = lblfacturass.Substring(1);
                    lblVuelos = lblVuelos.Substring(1);
                    lblVuelos = lblVuelos.Replace(",", "");
                    lblVuelos2 = lblVuelos2.Replace(",", "");
                    decimal txtVuelos1 = Convert.ToDecimal(txtVuelos);
                    decimal lblVuelo22 = Convert.ToDecimal(lblVuelos2);
                    decimal lblFacturass1 = Convert.ToDecimal(lblfacturass);
                    decimal total = lblVuelo22 - txtVuelos1;
                    decimal total1 = txtVuelos1 + lblFacturass1;
                    string total2 = "", total3 = "", res = "";
                    total2 = Convert.ToString(total);
                    total3 = Convert.ToString(total1);
                    res = Convert.ToString(txtVuelos1);
                    if (total > 0)
                    {
                        lblFacturas.Text = "$ " + decimal.Parse(total3).ToString("N");
                        lblVuelo2.Text = "$ " + decimal.Parse(total2).ToString("N");
                        lblVuelo1.Text = "$ " + decimal.Parse(res).ToString("N");
                        lblVuelo2.ForeColor = Color.Green;
                        txtVuelo.Text = "";
                        lblVuelos = lblVuelos.Replace(",", "");
                        operaciones(txtVuelos, "Vuelo", lblVuelos);
                    }
                    else
                    {
                        lblFacturas.Text = "$ " + decimal.Parse(total3).ToString("N");
                        lblVuelo2.Text = "$ " + decimal.Parse(total2).ToString("N");
                        lblVuelo1.Text = "$ " + decimal.Parse(res).ToString("N");
                        lblVuelo2.ForeColor = Color.Red;
                        txtVuelo.Text = "";
                        lblVuelos = lblVuelos.Replace(",", "");
                        operaciones(txtVuelos, "Vuelo", lblVuelos);
                    }
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, typeof(Page), "alerta", script, true);
            }
            
        }

        protected void btnHospedaje_Click(object sender, EventArgs e)
        {
            string script = "alert('Ingresar cantidad valida')";
            string lblHospedajes = Convert.ToString(lblHospedaje.Text);
            string lblHospedajes1 = Convert.ToString(lblHospedaje.Text);
            string lblHospedajes2 = Convert.ToString(lblHospedaje2.Text);
            string lblfacturass = Convert.ToString(lblFacturas.Text);
            string txtHospedajes = Convert.ToString(txtHospedaje.Text);
            if (txtHospedajes!="")
            {
                if (lblHospedajes2 == "$ 0.00" && lblfacturass == "$ 0.00")
                {
                    lblHospedajes = lblHospedajes.Substring(1);
                    lblHospedajes1 = lblHospedajes1.Substring(1);
                    lblHospedajes = lblHospedajes2.Substring(1);
                    decimal txtHospedajes1 = Convert.ToDecimal(txtHospedajes);
                    decimal lblHospedajes11 = Convert.ToDecimal(lblHospedajes1);
                    decimal total = lblHospedajes11 - txtHospedajes1;
                    decimal total2 = Math.Abs(total);
                    string total3 = Convert.ToString(total2);
                    string total1 = Convert.ToString(total);
                    if (total > 0)
                    {
                        lblFacturas.Text = "$ " + decimal.Parse(txtHospedajes).ToString("N");
                        lblHospedaje2.Text = "$ " + decimal.Parse(total1).ToString("N");
                        lblHospedaje1.Text = "$ " + decimal.Parse(txtHospedajes).ToString("N");
                        lblHospedaje2.ForeColor = Color.Green;
                        txtHospedaje.Text = "";
                        lblHospedajes = lblHospedajes.Replace(",", "");
                        operaciones(txtHospedajes, "Hospedaje", lblHospedajes);
                    }
                    else
                    {
                        lblFacturas.Text = "$ " + decimal.Parse(txtHospedajes).ToString("N");
                        lblHospedaje2.Text = "$ " + decimal.Parse(total1).ToString("N");
                        lblHospedaje1.Text = "$ " + decimal.Parse(txtHospedajes).ToString("N");
                        lblHospedaje2.ForeColor = Color.Red;
                        txtHospedaje.Text = "";
                        lblHospedajes = lblHospedajes.Replace(",", "");
                        operaciones(txtHospedajes, "Hospedaje", lblHospedajes);
                    }
                }
                else
                {
                    lblHospedajes1 = lblHospedajes1.Substring(1);
                    lblfacturass = lblfacturass.Substring(1);
                    lblHospedajes2 = lblHospedajes2.Substring(1);
                    lblHospedajes2 = lblHospedajes2.Replace(",", "");
                    lblHospedajes = lblHospedajes.Substring(1);
                    lblHospedajes = lblHospedajes.Replace(",", "");
                    decimal txtHospedajes1 = Convert.ToDecimal(txtHospedajes);
                    decimal lblHospedaje22 = Convert.ToDecimal(lblHospedajes2);
                    decimal lblFacturass1 = Convert.ToDecimal(lblfacturass);
                    decimal total = lblHospedaje22 - txtHospedajes1;
                    decimal total1 = txtHospedajes1 + lblFacturass1;
                    string total2 = "", total3 = "", res = "";
                    total2 = Convert.ToString(total);
                    total3 = Convert.ToString(total1);
                    res = Convert.ToString(txtHospedajes1);
                    if (total > 0)
                    {
                        lblFacturas.Text = "$ " + decimal.Parse(total3).ToString("N");
                        lblHospedaje2.Text = "$ " + decimal.Parse(total2).ToString("N");
                        lblHospedaje1.Text = "$ " + decimal.Parse(res).ToString("N");
                        lblHospedaje2.ForeColor = Color.Green;
                        txtHospedaje.Text = "";
                        lblHospedajes = lblHospedajes.Replace(",", "");
                        operaciones(txtHospedajes, "Hospedaje", lblHospedajes);
                    }
                    else
                    {
                        lblFacturas.Text = "$ " + decimal.Parse(total3).ToString("N");
                        lblHospedaje2.Text = "$ " + decimal.Parse(total2).ToString("N");
                        lblHospedaje1.Text = "$ " + decimal.Parse(res).ToString("N");
                        lblHospedaje2.ForeColor = Color.Red;
                        txtHospedaje.Text = "";
                        lblHospedajes = lblHospedajes.Replace(",", "");
                        operaciones(txtHospedajes, "Hospedaje", lblHospedajes);
                    }
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, typeof(Page), "alerta", script, true);
            }
        }

        protected void btnTraslado_Click(object sender, EventArgs e)
        {
            string script = "alert('Ingresar cantidad valida')";
            string lblTraslados = Convert.ToString(lblTraslado.Text);
            string lblTraslados1 = Convert.ToString(lblTraslado.Text);
            string lblTraslados2 = Convert.ToString(lblTraslado2.Text);
            string lblfacturass = Convert.ToString(lblFacturas.Text);
            string txtTraslados = Convert.ToString(txtTraslado.Text);
            if (txtTraslados!="")
            {
                if (lblTraslados2 == "$ 0.00" && lblfacturass == "$ 0.00")
                {
                    lblTraslados = lblTraslados.Substring(1);
                    lblTraslados1 = lblTraslados1.Substring(1);
                    lblTraslados = lblTraslados2.Substring(1);
                    decimal txtTraslados1 = Convert.ToDecimal(txtTraslados);
                    decimal lblTraslados11 = Convert.ToDecimal(lblTraslados1);
                    decimal total = lblTraslados11 - txtTraslados1;
                    decimal total2 = Math.Abs(total);
                    string total3 = Convert.ToString(total2);
                    string total1 = Convert.ToString(total);
                    if (total > 0)
                    {
                        lblFacturas.Text = "$ " + decimal.Parse(txtTraslados).ToString("N");
                        lblTraslado2.Text = "$ " + decimal.Parse(total1).ToString("N");
                        lblTraslado1.Text = "$ " + decimal.Parse(txtTraslados).ToString("N");
                        lblTraslado2.ForeColor = Color.Green;
                        txtTraslado.Text = "";
                        lblTraslados = lblTraslados.Replace(",", "");
                        operaciones(txtTraslados, "Traslado terrestre", lblTraslados);
                    }
                    else
                    {
                        lblFacturas.Text = "$ " + decimal.Parse(txtTraslados).ToString("N");
                        lblTraslado2.Text = "$ " + decimal.Parse(total1).ToString("N");
                        lblTraslado1.Text = "$ " + decimal.Parse(txtTraslados).ToString("N");
                        lblTraslado2.ForeColor = Color.Red;
                        txtTraslado.Text = "";
                        lblTraslados = lblTraslados.Replace(",", "");
                        operaciones(txtTraslados, "Traslado terrestre", lblTraslados);
                    }
                }
                else
                {
                    lblTraslados2 = lblTraslados2.Substring(1);
                    lblTraslados2 = lblTraslados2.Replace(",", "");
                    lblfacturass = lblfacturass.Substring(1);
                    lblTraslados = lblTraslados.Substring(1);
                    lblTraslados = lblTraslados.Replace(",", "");
                    decimal txtTraslados1 = Convert.ToDecimal(txtTraslados);
                    decimal lblTraslados22 = Convert.ToDecimal(lblTraslados2);
                    decimal lblFacturass1 = Convert.ToDecimal(lblfacturass);
                    decimal total = lblTraslados22 - txtTraslados1;
                    decimal total1 = txtTraslados1 + lblFacturass1;
                    string total2 = "", total3 = "", res = "";
                    total2 = Convert.ToString(total);
                    total3 = Convert.ToString(total1);
                    res = Convert.ToString(txtTraslados1);
                    if (total > 0)
                    {
                        lblFacturas.Text = "$ " + decimal.Parse(total3).ToString("N");
                        lblTraslado2.Text = "$ " + decimal.Parse(total2).ToString("N");
                        lblTraslado1.Text = "$ " + decimal.Parse(res).ToString("N");
                        lblTraslado2.ForeColor = Color.Green;
                        txtTraslado.Text = "";
                        lblTraslados = lblTraslados.Replace(",", "");
                        operaciones(txtTraslados, "Traslado terrestre", lblTraslados);
                    }
                    else
                    {
                        lblFacturas.Text = "$ " + decimal.Parse(total3).ToString("N");
                        lblTraslado2.Text = "$ " + decimal.Parse(total2).ToString("N");
                        lblTraslado1.Text = "$ " + decimal.Parse(res).ToString("N");
                        lblTraslado2.ForeColor = Color.Red;
                        txtTraslado.Text = "";
                        lblTraslados = lblTraslados.Replace(",", "");
                        operaciones(txtTraslados, "Traslado terrestre", lblTraslados);
                    }
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, typeof(Page), "alerta", script, true);
            }
            
        }

        protected void btnComida_Click(object sender, EventArgs e)
        {
            string script = "alert('Ingresar cantidad valida')";
            string lblComidas = Convert.ToString(lblComida.Text);
            string lblComidas1 = Convert.ToString(lblComida1.Text);
            string lblComidas2 = Convert.ToString(lblComida2.Text);
            string lblfacturass = Convert.ToString(lblFacturas.Text);
            string txtComidas = Convert.ToString(txtComida.Text);
            if (txtComidas!="")
            {
                if (lblComidas2 == "$ 0.00" && lblfacturass == "$ 0.00")
                {
                    lblComidas = lblComidas.Substring(1);
                    lblComidas1 = lblComidas1.Substring(1);
                    lblComidas2 = lblComidas2.Substring(1);
                    decimal txtComidas1 = Convert.ToDecimal(txtComidas);
                    decimal lblComidass = Convert.ToDecimal(lblComidas);
                    decimal total = lblComidass - txtComidas1;
                    decimal total2 = Math.Abs(total);
                    string total3 = Convert.ToString(total2);
                    string total1 = Convert.ToString(total);
                    if (total > 0)
                    {
                        lblFacturas.Text = "$ " + decimal.Parse(txtComidas).ToString("N");
                        lblComida2.Text = "$ " + decimal.Parse(total1).ToString("N");
                        lblComida1.Text = "$ " + decimal.Parse(txtComidas).ToString("N");
                        lblComida2.ForeColor = Color.Green;
                        txtComida.Text = "";
                        lblComidas = lblComidas.Replace(",", "");
                        operaciones(txtComidas, "Comida", lblComidas);
                    }
                    else
                    {
                        lblFacturas.Text = "$ " + decimal.Parse(txtComidas).ToString("N");
                        lblComida2.Text = "$ " + decimal.Parse(total1).ToString("N");
                        lblComida1.Text = "$ " + decimal.Parse(txtComidas).ToString("N");
                        lblComida2.ForeColor = Color.Red;
                        txtComida.Text = "";
                        lblComidas = lblComidas.Replace(",", "");
                        operaciones(txtComidas, "Comida", lblComidas);
                    }
                }
                else
                {
                    lblComidas2 = lblComidas2.Substring(1);
                    lblComidas2 = lblComidas2.Replace(",", "");
                    lblfacturass = lblfacturass.Substring(1);
                    lblComidas = lblComidas.Substring(1);
                    lblComidas = lblComidas.Replace(",", "");
                    decimal txtComidas1 = Convert.ToDecimal(txtComidas);
                    decimal lblComidas22 = Convert.ToDecimal(lblComidas2);
                    decimal lblFacturass1 = Convert.ToDecimal(lblfacturass);
                    decimal total = lblComidas22 - txtComidas1;
                    decimal total1 = txtComidas1 + lblFacturass1;
                    string total2 = "", total3 = "", res = "";
                    total2 = Convert.ToString(total);
                    total3 = Convert.ToString(total1);
                    res = Convert.ToString(txtComidas1);
                    if (total > 0)
                    {
                        lblFacturas.Text = "$ " + decimal.Parse(total3).ToString("N");
                        lblComida2.Text = "$ " + decimal.Parse(total2).ToString("N");
                        lblComida1.Text = "$ " + decimal.Parse(res).ToString("N");
                        lblComida2.ForeColor = Color.Green;
                        txtComida.Text = "";
                        lblComidas = lblComidas.Replace(",", "");
                        operaciones(txtComidas, "Comida", lblComidas);
                    }
                    else
                    {
                        lblFacturas.Text = "$ " + decimal.Parse(total3).ToString("N");
                        lblComida2.Text = "$ " + decimal.Parse(total2).ToString("N");
                        lblComida1.Text = "$ " + decimal.Parse(res).ToString("N");
                        lblComida2.ForeColor = Color.Red;
                        txtComida.Text = "";
                        lblComidas = lblComidas.Replace(",", "");
                        operaciones(txtComidas, "Comida", lblComidas);
                    }
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, typeof(Page), "alerta", script, true);
            }
        }

        protected void btnRenta_Click(object sender, EventArgs e)
        {
            string script = "alert('Ingresar cantidad valida')";
            string lblRentas = Convert.ToString(lblRenta.Text);
            string lblRentas1 = Convert.ToString(lblRenta1.Text);
            string lblRentas2 = Convert.ToString(lblRenta2.Text);
            string lblfacturass = Convert.ToString(lblFacturas.Text);
            string txtRentas = Convert.ToString(txtRenta.Text);
            if (txtRentas!="")
            {
                if (lblRentas2 == "$ 0.00" && lblfacturass == "$ 0.00")
                {
                    lblRentas = lblRentas.Substring(1);
                    lblRentas1 = lblRentas1.Substring(1);
                    lblRentas2 = lblRentas2.Substring(1);
                    decimal txtRentas1 = Convert.ToDecimal(txtRentas);
                    decimal lblRentass = Convert.ToDecimal(lblRentas);
                    decimal total = lblRentass - txtRentas1;
                    decimal total2 = Math.Abs(total);
                    string total3 = Convert.ToString(total2);
                    string total1 = Convert.ToString(total);
                    if (total > 0)
                    {
                        lblFacturas.Text = "$ " + decimal.Parse(txtRentas).ToString("N");
                        lblRenta2.Text = "$ " + decimal.Parse(total1).ToString("N");
                        lblRenta1.Text = "$ " + decimal.Parse(txtRentas).ToString("N");
                        lblRenta2.ForeColor = Color.Green;
                        txtRenta.Text = "";
                        lblRentas = lblRentas.Replace(",", "");
                        operaciones(txtRentas, "Renta automovil", lblRentas);
                    }
                    else
                    {
                        lblFacturas.Text = "$ " + decimal.Parse(txtRentas).ToString("N");
                        lblRenta2.Text = "$ " + decimal.Parse(total1).ToString("N");
                        lblRenta1.Text = "$ " + decimal.Parse(txtRentas).ToString("N");
                        lblRenta2.ForeColor = Color.Red;
                        txtRenta.Text = "";
                        lblRentas = lblRentas.Replace(",", "");
                        operaciones(txtRentas, "Renta automovil", lblRentas);
                    }
                }
                else
                {
                    lblRentas2 = lblRentas2.Substring(1);
                    lblRentas2 = lblRentas2.Replace(",", "");
                    lblfacturass = lblfacturass.Substring(1);
                    lblRentas = lblRentas.Substring(1);
                    lblRentas = lblRentas.Replace(",", "");
                    decimal txtRentas1 = Convert.ToDecimal(txtRentas);
                    decimal lblRentas22 = Convert.ToDecimal(lblRentas2);
                    decimal lblFacturass1 = Convert.ToDecimal(lblfacturass);
                    decimal total = lblRentas22 - txtRentas1;
                    decimal total1 = txtRentas1 + lblFacturass1;
                    string total2 = "", total3 = "", res = "";
                    total2 = Convert.ToString(total);
                    total3 = Convert.ToString(total1);
                    res = Convert.ToString(txtRentas1);
                    if (total > 0)
                    {
                        lblFacturas.Text = "$ " + decimal.Parse(total3).ToString("N");
                        lblRenta2.Text = "$ " + decimal.Parse(total2).ToString("N");
                        lblRenta1.Text = "$ " + decimal.Parse(res).ToString("N");
                        lblRenta2.ForeColor = Color.Green;
                        txtRenta.Text = "";
                        lblRentas = lblRentas.Replace(",", "");
                        operaciones(txtRentas, "Renta automovil", lblRentas);
                    }
                    else
                    {
                        lblFacturas.Text = "$ " + decimal.Parse(total3).ToString("N");
                        lblRenta2.Text = "$ " + decimal.Parse(total2).ToString("N");
                        lblRenta1.Text = "$ " + decimal.Parse(res).ToString("N");
                        lblRenta2.ForeColor = Color.Red;
                        txtRenta.Text = "";
                        lblRentas = lblRentas.Replace(",", "");
                        operaciones(txtRentas, "Renta automovil", lblRentas);
                    }
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, typeof(Page), "alerta", script, true);
            }
        }
    }
}
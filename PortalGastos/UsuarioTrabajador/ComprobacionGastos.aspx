﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PaginaMuestra/Site1.Master" AutoEventWireup="true" CodeBehind="ComprobacionGastos.aspx.cs" Inherits="PortalGastos.UsuarioTrabajador.ComprobacionGastos" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../Style/Style.css" rel="stylesheet" />
    <script>
           function format(input) {
            var num = input.value.replace(/\./g,'');
               if (!isNaN(num)) {
                   num = num.toString().split('').reverse().join('').replace(/(\d{3})(?=[^$|^-])/g, "$1,");
                   num = num.split('').reverse().join('').replace(/^[\.]/,'');
            input.value = num;
            }
            else{ 
            input.value = input.value.replace(/[^\d\.]*/g,'');
            }
}
        </script>
    <style type="text/css">
        .auto-style1 {
            margin-left: 40px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>
        <asp:Panel ID="pnlComprobacion" runat="server" CssClass="pnlGenericoSol centrado6">
            <asp:Label ID="lblTitulo" runat="server" Text="COMPROBACIÓN DE GASTOS" CssClass="lblTituloge"></asp:Label>
            <br />
            <hr />
            <table style="width: 100%;">
                <tr style="height: 20px;">
                    <td style="width: 100%">
                    </td>
                </tr>
                <tr style="height: 20px;">
                    <td style="width: 25%; float:left;" class="auto-style1">
                        <asp:Button ID="btnRegresar" runat="server" Text="Regresar" Visible="false" OnClick="btnRegresar_Click" />
                    </td>
                </tr>
                <tr style="height: 20px;">
                    <td style="width: 100%;"></td></tr>
                <tr style="height: 20px;">
                    <td style="width: 100%; float:left;">
                         <asp:GridView ID="Resultado" runat="server"  CssClass="mGrid1" DataKeyNames="No. de Solicitud" OnSelectedIndexChanged="Resultado_SelectedIndexChanged1">
                            <Columns>
                               <asp:TemplateField HeaderText="Visualizar">
                                   <ItemTemplate>
                                       <asp:ImageButton ID="ImagenSelec" runat="server" CommandName="Select" ImageUrl="~/Imagenes/vision.png"/>
                                   </ItemTemplate>
                               </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </td>
                </tr>
                <tr style="height: 20px;">
                    <td style="width: 100%;"></td></tr>
                <tr style="height: 20px;">
                    <td style="width: 100%; float:left;">
                        <asp:Label ID="lblNombreEmp" runat="server" CssClass="lblEtique" Visible="false" Text="Nombre.  "></asp:Label>
                    </td>
                </tr>
                <tr style="height: 20px;">
                    <td style="width: 100%;"></td></tr>
                <tr style="height: 20px;">
                    <td style="width: 15%; float:left;">
                        <asp:Label ID="lblGastos" runat="server" Text="Gastos: " CssClass="lblEtique" Visible="false"></asp:Label>
                    </td>
                    <td style="width: 20%; float:left;">
                        <asp:Label ID="lblSolicitado" runat="server" Visible="false" CssClass="lblEtique Solicitado" Text="Solicitado"></asp:Label>
                    </td>
                    <td style="width: 20%; float:left;">
                        <asp:Label ID="lblDiferencia" runat="server" Visible="false" CssClass="lblEtique Diferencia" Text="Diferencia"></asp:Label>
                    </td>
                    <td style="width: 20%; float:left;">
                        <asp:Label ID="lblImporteXML" runat="server" Visible="false" CssClass="lblEtique Importe" Text="Importe XML"></asp:Label>
                    </td>
                </tr>
                <tr style="height: 20px;">
                    <td style="width: 100%;"></td></tr>
                <tr style="height: 20px;">
                    <td style="width: 15%; float:left;">
                        <asp:Label ID="lblArchivoVuelo" runat="server" CssClass="lblEtique" Text="Factura de Vuelo:" Visible="false"></asp:Label>
                    </td>
                    <td style="width: 20%; float:left;">
                        <asp:Label ID="lblVuelo" runat="server" Visible="false" CssClass="lblEtique Solicitado"></asp:Label>
                    </td>
                    <td style="width: 20%; float:left;">
                        <asp:Label ID="lblVuelo2" runat="server" Visible="false" CssClass="lblEtique Diferencia" ></asp:Label>
                    </td>
                    <td style="width: 20%; float:left;">
                        <asp:Label ID="lblVuelo1" runat="server" Visible="false" CssClass="lblEtique Importe"></asp:Label>
                    </td>
                    <td style="width: 20%; float:left;">
                        <asp:FileUpload ID="Filesubir1" runat="server" Visible="false" Text="Examinar" CssClass="lblImagen" AllowMultiple="true" />
                        <br />
                        <asp:Button ID="btnSubirVuelo" runat="server" Visible="false" Text="Subir Archivos" OnClick="btnSubir_Click" />
                        <br />
                        <br />
                        <asp:TextBox ID="txtVuelo" runat="server" Visible="false" CssClass="txtImporte1"  onkeyup="format(this)" onchange="format(this)" placeHolder="$ 0.00"></asp:TextBox>
                        <br />
                        <asp:Button ID="btnVuelo" runat="server" Visible="false" CssClass="txtImporte1" Text="Comprobar" OnClick="btnVuelo_Click" />
                    </td>
                </tr>
                <tr style="height: 20px;">
                    <td style="width: 100%;"></td></tr>
                <tr style="height: 20px;">
                    <td style="width: 15%; float:left;">
                        <asp:Label ID="lblArchivoHospedaje" runat="server" CssClass="lblEtique" Text="Factura de Hospedaje:" Visible="false"></asp:Label>
                    </td>
                    <td style="width: 20%; float:left;">
                        <asp:Label ID="lblHospedaje" runat="server" Visible="false" CssClass="lblEtique1 Solicitado"></asp:Label>
                    </td>
                    <td style="width: 20%; float:left;">
                        <asp:Label ID="lblHospedaje2" runat="server" Visible="false" CssClass="lblEtique1 Diferencia" ></asp:Label>
                    </td>
                    <td style="width: 20%; float:left;">
                        <asp:Label ID="lblHospedaje1" runat="server" Visible="false" CssClass="lblEtique1 Importe"></asp:Label>
                    </td>
                    <td style="width: 20%; float:left;">
                        <asp:FileUpload ID="Filesubir2" runat="server" Visible="false" Text="Examinar" CssClass="lblImagen" AllowMultiple="true" />
                        <br />
                        <asp:Button ID="btnSubirHospedaje" runat="server" Visible="false" Text="Subir Archivos" OnClick="btnSubirHospedaje_Click" />
                        <br />
                        <br />
                        <asp:TextBox ID="txtHospedaje" runat="server" Visible="false" CssClass="txtImporte1"  onkeyup="format(this)" onchange="format(this)" placeHolder="$ 0.00"></asp:TextBox>
                        <br />
                        <asp:Button ID="btnHospedaje" runat="server" Visible="false" CssClass="txtImporte1" Text="Comprobar" OnClick="btnHospedaje_Click" />
                    </td>
                </tr>
                 <tr style="height: 20px;">
                    <td style="width: 100%;"></td></tr>
                <tr style="height: 20px;">
                    <td style="width: 15%; float:left;">
                        <asp:Label ID="lblArchivoTraslado" runat="server" CssClass="lblEtique" Visible="false" Text="Factura de Traslado:"></asp:Label>
                    </td>
                    <td style="width: 20%; float:left;">
                        <asp:Label ID="lblTraslado" runat="server" Visible="false" CssClass="lblEtique1 Solicitado"></asp:Label>
                    </td>
                    <td style="width: 20%; float:left;">
                        <asp:Label ID="lblTraslado2" runat="server" Visible="false" CssClass="lblEtique1 Diferencia" ></asp:Label>
                    </td>
                    <td style="width: 20%; float:left;">
                        <asp:Label ID="lblTraslado1" runat="server" Visible="false" CssClass="lblEtique1 Importe"></asp:Label>
                    </td>
                    <td style="width: 20%; float:left;">
                        <asp:FileUpload ID="Filesubir3" runat="server" Visible="false" Text="Examinar" CssClass="lblImagen" AllowMultiple="true" />
                            <br />
                        <asp:Button ID="btnSubirTraslado" runat="server" Visible="false" Text="Subir Archivos" OnClick="btnSubirTraslado_Click" />
                        <br />
                        <br />
                        <asp:TextBox ID="txtTraslado" runat="server" Visible="false" CssClass="txtImporte1"  onkeyup="format(this)" onchange="format(this)" placeHolder="$ 0.00"></asp:TextBox>
                        <br />
                        <asp:Button ID="btnTraslado" runat="server" Visible="false" CssClass="txtImporte1" Text="Comprobar" OnClick="btnTraslado_Click" />
                    </td>
                </tr>
                <tr style="height: 20px;">
                    <td style="width: 100%;"></td></tr>
                <tr style="height: 20px;">
                    <td style="width: 15%; float:left;">
                        <asp:Label ID="lblArchivoComida" runat="server" CssClass="lblEtique" Visible="false" Text="Factura de Comida:"></asp:Label>
                    </td>
                    <td style="width: 20%; float:left;">
                        <asp:Label ID="lblComida" runat="server" Visible="false" CssClass="lblEtique1 Solicitado"></asp:Label>
                    </td>
                    <td style="width: 20%; float:left;">
                        <asp:Label ID="lblComida2" runat="server" Visible="false" CssClass="lblEtique1 Diferencia" ></asp:Label>
                    </td>
                    <td style="width: 20%; float:left;">
                        <asp:Label ID="lblComida1" runat="server" Visible="false" CssClass="lblEtique1 Importe"></asp:Label>
                    </td>
                    <td style="width: 20%; float:left;">
                        <asp:FileUpload ID="Filesubir4" runat="server" Visible="false" Text="Examinar" CssClass="lblImagen" AllowMultiple="true" />
                        <br />
                        <asp:Button ID="btnSubirComida" runat="server" Visible="false" Text="Subir Archivos" OnClick="btnSubirComida_Click" />
                        <br />
                        <br />
                        <asp:TextBox ID="txtComida" runat="server" Visible="false" CssClass="txtImporte1"  onkeyup="format(this)" onchange="format(this)" placeHolder="$ 0.00"></asp:TextBox>
                        <br />
                        <asp:Button ID="btnComida" runat="server" Visible="false" CssClass="txtImporte1" Text="Comprobar" OnClick="btnComida_Click" />
                    </td>
                </tr>
                <tr style="height: 20px;">
                    <td style="width: 100%;"></td></tr>
                 <tr style="height: 20px;">
                    <td style="width: 15%; float:left;">
                        <asp:Label ID="lblArchivoRenta" runat="server" CssClass="lblEtique" Visible="false" Text="Factura de Renta Automóvil"></asp:Label>
                    </td>
                     <td style="width: 20%; float:left;">
                         <asp:Label ID="lblRenta" runat="server" Visible="false" CssClass="lblEtique1 Solicitado"></asp:Label>
                     </td>
                     <td style="width: 20%; float:left;">
                        <asp:Label ID="lblRenta2" runat="server" Visible="false" CssClass="lblEtique1 Diferencia" ></asp:Label>
                     </td>
                     <td style="width: 20%; float:left;">
                        <asp:Label ID="lblRenta1" runat="server" Visible="false" CssClass="lblEtique1 Importe"></asp:Label>
                     </td>
                     <td style="width: 20%; float:left;">
                        <asp:FileUpload ID="Filesubir6" runat="server" Visible="false" Text="Examinar" CssClass="lblImagen" AllowMultiple="true" />
                        <br />
                        <asp:Button ID="btnSubirRenta" runat="server" Visible="false" Text="Subir Archivos" OnClick="btnSubirRenta_Click" />
                         <br />
                         <br />
                         <asp:TextBox ID="txtRenta" runat="server" Visible="false" CssClass="txtImporte1"  onkeyup="format(this)" onchange="format(this)" placeHolder="$ 0.00"></asp:TextBox>
                        <br />
                        <asp:Button ID="btnRenta" runat="server" Visible="false" CssClass="txtImporte1" Text="Comprobar" OnClick="btnRenta_Click" />
                    </td>
                     </tr>
                     <tr style="height: 20px;">
                    <td style="width: 100%;"></td></tr>
                <tr style="height: 20px;">
                    <td style="width: 15%; float:left;">
                        <asp:Label ID="lblArchivoNo" runat="server" CssClass="lblEtique" Visible="false" Text="Gastos sin factura"></asp:Label>
                    <td style="width: 20%; float:left;">
                        <asp:Label ID="lblNoComprobable" runat="server" Visible="false" CssClass="lblEtique Solicitado"></asp:Label>
                    </td>
                    <td style="width: 20%; float:left;">
                        <asp:TextBox ID="txtCantidad" runat="server" Visible="false" CssClass="txtImporte"  onkeyup="format(this)" onchange="format(this)" placeHolder="$ 0.00"></asp:TextBox>
                        <br />
                        <br />
                        <asp:Button ID="btnCalcular" runat="server" Visible="false" CssClass="txtImporte" Text="Comprobar" OnClick="btnCalcular_Click" />
                    </td>
                    <td style="width: 20%; float:left;">
                        <asp:Label ID="lblCantidad1" runat="server" Visible="false" CssClass=" lblEtique Diferencia"></asp:Label>
                    </td>
                    </td>
                        <td style="width: 20%; float:left;">
                        <asp:FileUpload ID="Filesubir5" runat="server" Visible="false" Text="Examinar" CssClass="lblImagen" AllowMultiple="true" />
                            <br />
                            <br />
                        <asp:Button ID="btnSubirNoComprobable" runat="server" Visible="false" Text="Subir Archivos" OnClick="btnSubirNoComprobable_Click" />
                    </td>
                </tr>
                <tr style="height: 20px;">
                    <td style="width: 100%;"></td></tr>
                <tr style="height: 20px;">
                    <td style="width: 20%; float:left;">
                        <asp:Label ID="lblGastosT" runat="server" CssClass="lblEtique" Visible="false" Text="Gastos totales: "></asp:Label>
                    </td>
                    <td style="width: 20%; float:left;">
                        <asp:Label ID="lblTotalSolicitado" runat="server" CssClass="lblEtique Solicitado1" Visible="false"></asp:Label>
                    </td>
                    <td style="width: 20%; float:left;">
                        <asp:Label ID="lblFacturas" runat="server" CssClass="lblEtique Importe1" Visible="false"></asp:Label>
                    </td>
                    <td style="width: 40%; float:left;">
                        <asp:TextBox ID="txtComentario" runat="server" Visible="false" Width="500px" Height="200px" Enabled="false"></asp:TextBox>
                    </td>
                </tr>
                <tr style="height: 20px;">
                    <td style="width: 50%; float:left"></td>
                    <td style="width: 25%; float:right">
                        <asp:Button ID="btnEnviar" runat="server" Visible="false" Text="Enviar" OnClick="btnEnviar_Click" CssClass="cssBotonMediano"/>
                        <asp:Button ID="btnComentario" runat="server" CssClass="cssBotonMediano" Visible="false" Text="Comentario" OnClick="btnComentario_Click" />
                    </td>
                </tr>
            </table>
        </asp:Panel>
    </div>
</asp:Content>

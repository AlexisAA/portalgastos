﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PaginaMuestra/Site1.Master" AutoEventWireup="true" CodeBehind="Solicitudes.aspx.cs" Inherits="PortalGastos.UsuarioTrabajador.Solicitudes" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../Style/Style.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>
        <asp:Panel ID="pnlSolicitudes" runat="server" CssClass="pnlGenerico centrado6">
            <asp:Label ID="lblTitulo3" runat="server" CssClass="lblTituloge" Text="NUEVA SOLICITUD" Visible="false"></asp:Label>
            <asp:Label ID="lblTitulo2" runat="server" CssClass="lblTituloge" Text="SOLICITUD" Visible="false"></asp:Label>
            <asp:Label ID="lblTitulo" runat="server" CssClass="lblTituloge" Text="HISTORIAL SOLICITUDES"></asp:Label>
            <br />
            <hr />
            <table style="width: 100%;">
                <tr style="height: 20px;">
                    <td style="width: 100%"></td>
                </tr>
                <tr style="height: 20px;">
                    <td style="width: 25%; float:left;">
                        <asp:Label ID="lblSoli" runat="server" CssClass="lblNormalRojo" Visible="false"></asp:Label>
                        <asp:Button ID="btnRegresar" runat="server" Text="REGRESAR" Visible="false" OnClick="btnRegresar_Click" />
                        <asp:Button ID="btnRegresar1" runat="server" Text="REGRESAR" Visible="false" OnClick="btRegresar1_Click" />
                    </td>
                    <td style="width: 25%; float:left;">
                    </td>
                    <td style="width: 25%; float:left;">
                        <asp:Label ID="lblSolicitudNo" runat="server" CssClass="lblEtique" Text="No. de Solicitud" Visible="false"></asp:Label>
                        <asp:Label ID="lblSolicitudNo1" runat="server" CssClass="lblEtique" Visible="false"></asp:Label>
                        <asp:Label ID="lblEstatusSol" runat="server" Text="Estatus" Visible="false" CssClass="lblEtique"></asp:Label>
                    </td>
                    <td style="width: 25%; float:left;">
                        <asp:Label ID="lblEstatuslbl" runat="server" CssClass="lblEtique" Text="Estatus: " Visible="false"></asp:Label>
                        <asp:Label ID="lblEstatuslbl1" runat="server" CssClass="lblEtique" Visible="false"></asp:Label>
                    </td>
                    <td style="width: 25%; float:left;">
                        <asp:ImageButton ID="imgGuardar" runat="server" Visible="false" ImageUrl="~/Imagenes/salvar.png" OnClick="imgGuardar_Click"/>
                    </td>
                    <td style="width: 25%; float:left;">
                        <asp:Label ID="lblFlujo" runat="server" Visible="false" Text="Flujo de autorización" CssClass="lblEtique"></asp:Label>
                        <br />
                        <asp:Image ID="ImgFlujo" runat="server" Visible="false" CssClass="imgflujo"/>
                        <br />
                        <asp:Label ID="lblFlujo1" runat="server" Visible="false" CssClass="lblflujo"></asp:Label>
                    </td>
                </tr>
                <tr style="height: 20px;">
                    <td style="width: 100%"></td>
                </tr>
                <tr style="height: 20px;">
                    <td style="width: 25%; float:left;">
                        <asp:Label ID="lblEstatus" runat="server" CssClass="lblEtique" Text="Estatus"></asp:Label>
                        <asp:Label ID="lblEmpleadoNo" runat="server" CssClass="lblEtique" Text="No. de Empleado" Visible="false"></asp:Label>
                    </td>
                    <td style="width: 25%; float:left;">
                        <asp:Label ID="lblLugarDestino" runat="server" CssClass="lblEtique" Text="Lugar de Destino"></asp:Label>
                        <asp:TextBox ID="txtEmpeladoNo" runat="server" CssClass="txt" Visible="false" Enabled="False"></asp:TextBox>
                    </td>
                    <td style="width: 25%; float:left;">
                        <asp:Label ID="lblFecha" runat="server" CssClass="lblEtique" Text="Fecha de Viaje"></asp:Label>
                    </td>
                </tr>
                <tr style="height: 20px;">
                    <td style="width: 100%;"></td>
                </tr>
                <tr style="height: 20px;">
                    <td style="width: 25%; float:left;">
                        <asp:DropDownList ID="DrEstatus" runat="server" CssClass="drop" AutoPostBack="true" OnSelectedIndexChanged="EstatusSeleccionado"></asp:DropDownList>
                        <asp:Label ID="lblFechaInicio" runat="server" CssClass="lblEtique" Text="Fecha Inicio" Visible="false"></asp:Label>
                    </td>
                    <td style="width: 25%; float:left;">
                        <asp:DropDownList ID="DrLugar" runat="server" CssClass="drop" AutoPostBack="true" OnSelectedIndexChanged="NumeroSulicitud"></asp:DropDownList>
                        <asp:TextBox ID="txtCalendar" runat="server" CssClass="txt" Visible="false"> </asp:TextBox>
                        <asp:ImageButton ID="ImgCalendar" runat="server" ImageUrl="~/Imagenes/calendario.png" ImageAlign="AbsBottom" OnClick="ImgCalendar_Click" Visible="false"/>
                        <asp:Calendar ID="Calendar" runat="server" CssClass="Calendar" Height="200px" Width="220px" OnSelectionChanged="Calendar_SelectionChanged" OnDayRender="Calendar_DayRender" Visible="false"></asp:Calendar>
                    </td>
                    <td style="width: 25%; float:left;">
                        <asp:DropDownList ID="DrFecha" runat="server" CssClass="drop" AutoPostBack="true"></asp:DropDownList>
                    </td>
                </tr>
                <tr style="height: 20px;">
                    <td style="width: 100%;"></td>
                </tr>
                <tr style="height: 20px;">
                    <td style="width: 25%; float:left;">
                        <asp:Label ID="lblFechaFin" runat="server" CssClass="lblEtique" Text="Fecha Fin: " Visible="false"></asp:Label>
                    </td>
                    <td style="width: 25%; float:left;">
                        <asp:Button ID="btnBuscar" runat="server" CssClass="cssBotonPanel" Text="BUSCAR" OnClick="btnBuscar_Click" />
                        <asp:TextBox ID="txtCalendar1" runat="server" CssClass="txt" Visible="false"></asp:TextBox>
                        <asp:ImageButton ID="ImgCalendar1" runat="server" ImageUrl="~/Imagenes/calendario.png" ImageAlign="AbsBottom" OnClick="ImgCalendar1_Click" Visible="false"/>
                        <asp:Calendar ID="Calendar1" runat="server" CssClass="Calendar" Height="200px" Width="220px" OnSelectionChanged="Calendar1_SelectionChanged" OnDayRender="Calendar_DayRender" Visible="false"></asp:Calendar>
                    </td>
                    <td style="width: 25%; float:left;">
                        <asp:Button ID="btnNuevo"  runat="server" CssClass="cssBotonPanel" Text="NUEVA SOLICITUD" OnClick="btnNuevo_Click"/>
                    </td>
                </tr>
                <tr style="height: 20px;">
                    <td style="width: 100%;"></td></tr>
                <tr style="height: 20px;">
                    <td style="width: 25%;">
                        <asp:GridView ID="Resultado" runat="server"  CssClass="mGrid1 pagination-ys" PageSize="7" AllowPaging="true" DataKeyNames="No. de Solicitud" OnSelectedIndexChanged="Resultado_SelectedIndexChanged1" OnPageIndexChanging="Resultado_PageIndexChanging">
                            <Columns>
                               <asp:TemplateField HeaderText="Visualizar">
                                   <ItemTemplate>
                                       <asp:ImageButton ID="ImagenSelec" runat="server" CommandName="Select" ImageUrl="~/Imagenes/vision.png"/>
                                   </ItemTemplate>
                               </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </td>
                </tr>
                <tr style="height: 20px;"> 
                    <td style="width: 25%; float:left;">
                        <asp:Label ID="lblTipoViaje" runat="server" CssClass="lblEtique" Text="Tipo Viaje: " Visible="false"></asp:Label>
                    </td>
                    <td style="width: 25%; float:left;">
                        <asp:DropDownList ID="DrTipoViaje" runat="server" CssClass="drop" AutoPostBack="true" Visible="false" OnSelectedIndexChanged="DrTipoViaje_SelectedIndexChanged"></asp:DropDownList>
                    </td>
                </tr>
                <tr style="height: 20px;">
                    <td style="width: 100%;"></td></tr>
                <tr style="height: 20px;">
                    <td style="width: 25%; float:left;">
                        <asp:Label ID="lblTipoGasto" runat="server" CssClass="lblEtique" Text="Tipo de Gasto: " Visible="false"></asp:Label>
                    </td>
                    <td style="width: 25%; float:left;">
                        <asp:DropDownList ID="DrTipoGasto" runat="server" CssClass="drop" AutoPostBack="true" Visible="false"></asp:DropDownList>
                    </td>
                </tr>
                <tr style="height: 20px;">
                    <td style="width: 100%;"></td></tr>
                <tr style="height: 20px;">
                    <td style="width: 25%; float:left;">
                        <asp:Label ID="lblDestino" runat="server" CssClass="lblEtique" Text="Lugar de Destino: " Visible="false"></asp:Label>
                    </td>
                    <td style="width: 25%; float:left;">
                        <asp:TextBox ID="txtLugardestino" runat="server" CssClass="txt" Visible="false"></asp:TextBox>
                    </td>
                </tr>
                <tr style="height: 20px;">
                    <td style="width: 100%;"></td>
                </tr>
                <tr style="height: 20px;">
                    <td style="width: 25%; float:left;">
                        <asp:Label ID="lblCliente" runat="server" CssClass="lblEtique" Text="Comprobar cliente:" Visible="false"></asp:Label>
                    </td>
                    <td style="width: 25%; float:left;">
                        <asp:TextBox ID="txtCliente" runat="server" CssClass="txt" Visible="false"></asp:TextBox>
                    </td>
                </tr>
                <tr style="height: 20px;">
                    <td style="width: 100%;"></td>
                </tr>
                <tr style="height: 20px;">
                    <td style="width: 25%; float:left;">
                        <asp:Label ID="lblPeriodo" runat="server" CssClass="lblEtique" Text="Período: " Visible="false"></asp:Label>
                    </td>
                    <td style="width: 25%; float:left;">
                        <asp:TextBox ID="txtPeriodo" runat="server" CssClass="txt" Visible="false"></asp:TextBox>
                    </td>
                </tr>
                <tr style="height: 20px;">
                    <td style="width: 100%;"></td>
                </tr>
                <tr style="height: 20px;">
                    <td style="width: 25%; float:left;">
                        <asp:Label ID="lblGastoV" runat="server" CssClass="lblEtique" Text="Gasto de Vuelo: " Visible="false"></asp:Label>
                    </td>
                    <td style="width: 25%; float:left;">
                        <asp:TextBox ID="txtGastoV" runat="server" CssClass="txt" Visible="false" placeholder="0.00"></asp:TextBox>
                    </td>
                    <td style="width: 25%; float:left;">
                        <asp:Button ID="btnEnviar" runat="server" CssClass="cssBotonPanel" Text="ENVIAR" Visible="false" OnClick="btnEnviar_Click" />
                        <asp:Button ID="btnEnviarS" runat="server" CssClass="cssBotonPanel" Text="ENVIAR SOLICITUD" Visible="false" OnClick="btnEnviarS_Click" />
                    </td>
                </tr>
                <tr style="height: 20px;">
                    <td style="width: 100%;"></td>
                </tr>
                <tr style="height: 20px;">
                    <td style="width: 25%; float:left;">
                        <asp:Label ID="lblGastoH" runat="server" CssClass="lblEtique" Text="Gasto de Hospedaje: " Visible="false"></asp:Label>
                    </td>
                    <td style="width: 25%; float:left;">
                        <asp:TextBox ID="txtGastoH" runat="server" CssClass="txt" Visible="false"  placeholder="0.00"></asp:TextBox>
                    </td>
                </tr>
                <tr style="height: 20px;">
                    <td style="width: 100%;"></td>
                </tr>
                <tr style="height: 20px;">
                    <td style="width: 25%; float:left;">
                        <asp:Label ID="lblGastoTerrestre" runat="server" CssClass="lblEtique" Text="Gasto de Traslado Terrestre" Visible="false"></asp:Label>
                    </td>
                    <td style="width: 25%; float:left;">
                        <asp:TextBox ID="txtGastoTerrestre" runat="server" CssClass="txt" Visible="false" placeholder="0.00"></asp:TextBox>
                    </td>
                </tr>
                <tr style="height: 20px;">
                    <td style="width: 100%;"></td>
                </tr>
                <tr style="height: 20px;">
                    <td style="width: 25%; float:left;">
                        <asp:Label ID="lblComida" runat="server" CssClass="lblEtique" Text="Gasto de Comida: " Visible="false"> </asp:Label>
                    </td>
                    <td style="width: 25%; float:left;">
                        <asp:TextBox ID="txtComida" runat="server" CssClass="txt" Visible="false" placeholder="0.00"></asp:TextBox>
                    </td>
                </tr>
                <tr style="height: 20px;">
                    <td style="width: 100%;"></td>
                </tr>
                <tr style="height: 20px;">
                    <td style="width: 25%; float:left;">
                        <asp:Label ID="lblGastoCurso" runat="server" CssClass="lblEtique" Text="Gasto de Renta Automóvil: " Visible="false"></asp:Label>
                    </td>
                    <td style="width: 25%; float:left;">
                        <asp:TextBox ID="txtGastoCurso" runat="server" CssClass="txt" Visible="false" placeholder="0.00"></asp:TextBox>
                    </td>
                </tr>
                <tr style="height: 20px;">
                    <td style="width: 100%;"></td>
                </tr>
                <tr style="height: 20px;">
                    <td style="width: 25%; float:left;">
                        <asp:Label ID="lblGastoOtro" runat="server" CssClass="lblEtique" Text="Otros Gastos: " Visible="false"></asp:Label>
                    </td>
                    <td style="width: 25%; float:left;">
                        <asp:TextBox ID="txtGastoOtro" runat="server" CssClass="txt" Visible="false" placeholder="0.00"></asp:TextBox>
                    </td>
                </tr>
                <tr style="height: 20px;">
                    <td style="width: 100%;"></td>
                </tr>
                <tr style="height: 20px;">
                    <td style="width: 25%; float:left;">
                        <asp:Label ID="lblTotalSolicitado" runat="server" CssClass="lblEtique" Text="Total Solicitado: " Visible="false"></asp:Label>
                    </td>
                    <td style="width: 25%; float:left;">
                        <asp:Label ID="lblCalcular" runat="server" CssClass="lblEtique" Visible="false" placeholder="0.00"></asp:Label>
                    </td>
                </tr>
                <tr style="height: 20px;">
                    <td style="width: 100%;"></td>
                </tr>
                <tr style="height: 20px;">
                    <td style="width: 25%; float:left;">
                        <asp:Button ID="btnCalcular" runat="server" Text="CALCULAR" Visible="false" OnClick="btnCalcular_Click"/>
                    </td>
                </tr>
            </table>
        </asp:Panel>
    </div>
</asp:Content>
